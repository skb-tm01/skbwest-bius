```json
{
    "type": "{contact|virtual}",

    "id"  : "id_1",

    "icon_name": "icon1.png",
 
    "alert":
    {
		"display_top": true,

        "sound":"sound1", /* result file path: /data/sounds/{sound_id}_{language}.wav */
        "color":"black",
        
        "text":
        {
            "en":"text",
            "ru":"text"
        },
        "manual_title":
        {
            "en":"text",
            "ru":"text"
        },
        "manual_description":
        {
            "en":"text",
            "ru":"text"
        }
    },
    
	"contact":
    {
		"bit": 12
    }
	
}
```

