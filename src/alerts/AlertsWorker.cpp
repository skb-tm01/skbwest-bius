#include <QApplication>
#include <QDebug>
#include <QMainWindow>

#include "alerts/AlertsWorker.h"
#include "core/AudioHelper.h"
#include "core/SleepHelper.h"
#include "widgets/warningmessage.h"
#include "widgets/mainwindow.h"
#include "peripherials/sensors/SensorManager.h"
#include "peripherials/PeripherialsManager.h"
#include "widgets/WindowManager.h"
#include <QDateTime>
AlertsWorker::AlertsWorker(MainWindow* mainWindow, QObject *parent) : QObject(parent)
{
    _terminationRequested = false;
    _mainWindowInitialized = false;
    _showDebug = false;
    _isCanConnectionOK = true;
    if(_showDebug) qDebug()<<"AlertsWorker::Run(): _isCanConnectionOK = true; constructor ";
    _mainWindow = mainWindow;

    QObject::connect(this, SIGNAL(alert_required_window(AlertDescription*)), _mainWindow, SLOT(ShowAlertWindow(AlertDescription*)));
    QObject::connect(this, SIGNAL(alert_required_topbar(AlertDescription*)), _mainWindow, SLOT(ShowAlertTopbar(AlertDescription*)));
//    QObject::connect(_mainWindow, SIGNAL(alert_can_lost()), this, SLOT(OnCanConnection_Lost()));
//    QObject::connect(_mainWindow, SIGNAL(alert_can_restored()), this, SLOT(OnCanConnection_Restored()));

    OnCanConnection_Restored();
}

void AlertsWorker::Run()
{
    while(!_terminationRequested)
    {
        auto* audioworker = PeripherialsManager::instance().AudioWorker_Get();
        if(audioworker != nullptr && audioworker->IsPlaying())
        {
            SleepHelper::usleep(250*1000);
            continue;
        }

        if(_showDebug) qDebug()<<"AlertsWorker::Run(): _alerts_queue_window.IsEmpty() "<< _alerts_queue_window.IsEmpty();

        if(!_alerts_queue_window.IsEmpty() && (_mainWindow->activeWarning()==nullptr))
        {
            auto* alert_description = this->_alerts_queue_window.Dequeue();
            if(_showDebug) qDebug()<<"AlertsWorker::Run(): alert to show:"<<alert_description->GetId();

            //check if error persist. If not, then delete it from queue
            bool should_display = true;

            uint32_t current_time = ((QDateTime::currentMSecsSinceEpoch())/1000);

            if(_showDebug) qDebug()<<"AlertsWorker::Run() current_time "<<current_time;

            auto* sensor = SensorManager::instance().GetSensor(alert_description->GetId());
            if(sensor != nullptr){
                //Do not display alert if sensor is not under control
                if(!sensor->IsUnderControl()) {
                    if(_showDebug) qDebug()<<"AlertsWorker::Run(): remove from queue because is not under control";
                    should_display = false;
                }

                //Do not display alert if sensor is OK
                if(!sensor->IsErrorOccured()) {
                    if(_showDebug) qDebug()<<"AlertsWorker::Run(): remove from queue because error is not occurred";
                    should_display = false;
                }
            }

            bool isittime = IsItTimeToDisplay(alert_description);
            if(_showDebug) qDebug()<<"AlertsWorker::Run(): isittime = "<< isittime;
            if(_showDebug) qDebug()<<"AlertsWorker::Run(): alert_description->GetId() "<< alert_description->GetId();

            if(alert_description->GetId() == "can_connection")
            {
                auto validCANstate = _mainWindow->GetAlertPanel()->GetCanState();
                /*
                 * for some reason CANworker signals can't reach AlertsWorker (here)
                 * so we're taking CAN state from other place
                 */
                if(validCANstate != _isCanConnectionOK)
                {
                    if(validCANstate)
                        OnCanConnection_Restored();
                    else
                        OnCanConnection_Lost();
                }

                //if(_showDebug) qDebug()<<"AlertsWorker::Run(): _isCanConnectionOK ? "<< _isCanConnectionOK;
                //_isCanConnectionOK = _mainWindow->GetAlertPanel()->GetCanState();
                if(_showDebug) qDebug()<<"AlertsWorker::Run(): _isCanConnectionOK ? "<< _isCanConnectionOK;
                if(_isCanConnectionOK)
                {
                    continue;
                }
                else {
                    should_display = true;
                }
            }

            if(_showDebug) qDebug()<<"AlertsWorker::Run(): should_display = "<<should_display;

            if(!should_display || !isittime)
            {
                this->_alerts_queue_window.Enqueue(alert_description);
                if(_showDebug) qDebug()<<"AlertsWorker::Run(): continue";
                continue;
            }

            auto* mainmenu = qobject_cast<MainWindow*>(QApplication::activeWindow());
            if(mainmenu != nullptr && WindowManager::getInstance()->IsEmpty())
            {
                if(_showDebug) qDebug()<<"AlertsWorker::Run(): request window for"<<alert_description->GetId();
                emit alert_required_window(alert_description);
                AddTopAlert(alert_description);
                MarkAsDisplayed(alert_description);
            }
            else
            {
                 if(_showDebug) qDebug()<<"AlertsWorker::Run(): add alert to queue"<<alert_description->GetId();
                _alerts_queue_window.Enqueue(alert_description);

            }
        this->_alerts_queue_window.Enqueue(alert_description);
        }
        SleepHelper::usleep(600*1000);
        if(_showDebug) qDebug()<<"AlertsWorker::Run() =========================";
    }
    emit finished();
}

void AlertsWorker::MainWindowInitialized()
{
    _mainWindowInitialized = true;
}

void AlertsWorker::AddTopAlert(AlertDescription *alertDescription)
{
    if(alertDescription->GetDisplayingTop())
        emit alert_required_topbar(alertDescription);
}

void AlertsWorker::AddCenterAlert(AlertDescription *alertDescription)
{
    if(alertDescription->GetDiplayingMsg()) {
        this->_alerts_queue_window.Enqueue(alertDescription);
    }
}

int AlertsWorker::GetDisplayInterval(AlertDescription* alert_description)
{
    auto display_interval = 0;
    if(alert_description->GetColor() == "red")
        display_interval = _showDebug? 200 : 60*3;
    else if(alert_description->GetId() == "grain_tank_70")
        display_interval = _showDebug? 200 : 60*3;
    else display_interval = _showDebug? 200 : 60*5;
    return display_interval;
}

bool AlertsWorker::IsItTimeToDisplay(AlertDescription* alert_description)
{
    if(_last_display_time.value(alert_description->GetId())==0)
        return true;

    uint32_t current_time = ((QDateTime::currentMSecsSinceEpoch())/1000);
    auto display_interval = GetDisplayInterval(alert_description);
    if(_showDebug) qDebug()<<"AlertsWorker::Run(): display_interval = "<<display_interval;
    uint32_t dt = current_time-_last_display_time[alert_description->GetId()];
    if(_showDebug) qDebug()<<"AlertsWorker::Run(): dt = "<< dt;
    bool isittime = (current_time-_last_display_time[alert_description->GetId()]) > display_interval;
    return isittime;
}

void AlertsWorker::MarkAsDisplayed(AlertDescription* alert_description)
{
    MarkAsDisplayed(alert_description->GetId());
}

void AlertsWorker::MarkAsDisplayed(QString alertId)
{
    uint32_t current_time = ((QDateTime::currentMSecsSinceEpoch())/1000);

    if(_last_display_time.value(alertId)>0)
    {
        uint32_t last_displayed = _last_display_time[alertId];
        if(_showDebug) qDebug()<<"AlertsWorker::Run() last_display_time "<<last_displayed;
        _last_display_time[alertId] = current_time;
    }
    else
    {
        _last_display_time.insert(alertId, current_time);
    }
    if(_showDebug) qDebug() << "alerts worker: MarkAsDisplayed " << alertId;
}

void AlertsWorker::RemoveAlert(QString alertId)
{
    _mainWindow->GetAlertPanel()->RemoveAlert(alertId);
    if(_last_display_time.value(alertId)>0)
    {
        _last_display_time.remove(alertId);
    }
}

void AlertsWorker::OnCanConnection_Lost()
{
    if(_showDebug) qDebug()<<"AlertsWorker::Run(): CAN LOST ";
    _isCanConnectionOK = false;
    if(_showDebug) qDebug()<<"AlertsWorker::Run(): _isCanConnectionOK = false; OnCanConnection_Lost()";
    //TODO: rewrite in more adequate way
    _alerts_queue_window.Clear();
    _last_display_time.clear();

    auto alert_description = &SensorManager::instance().GetSensor("can_connection")->GetAlertDescription();
    _alerts_queue_window.Enqueue(alert_description);

    auto alertId = alert_description->GetId();
    uint32_t current_time = ((QDateTime::currentMSecsSinceEpoch())/1000);
    _last_display_time.insert(alertId, current_time-GetDisplayInterval(alert_description));
}

void AlertsWorker::OnCanConnection_Restored()
{
    if(_showDebug) qDebug()<<"AlertsWorker::Run(): CAN RESTORED ";
    _isCanConnectionOK = true;
    if(_showDebug) qDebug()<<"AlertsWorker::Run(): _isCanConnectionOK = true;OnCanConnection_Restored()";
}

void AlertsWorker::ClearAlerts(bool leaveSpecialAlertsShown)
{
    if(leaveSpecialAlertsShown)
    {
        if(_showDebug) qDebug() << "ClearAlerts(leaveSpecialAlertsShown="<<leaveSpecialAlertsShown<<")";

        _mainWindow->GetAlertPanel()->MarkAlertsAsDisplayedOnStartup(this, leaveSpecialAlertsShown);
        if(_showDebug) qDebug() << "           _mainWindow->GetAlertPanel()->ClearAlerts ";
    }
    else {
        if(_showDebug) qDebug() << "ClearAlerts(all);";
        _mainWindow->GetAlertPanel()->ClearAlerts();
    }
}
