#pragma once

#include <QThread>

#include "alerts/AlertsWorker.h"
#include "core/Singleton.h"
#include "widgets/mainwindow.h"

class AlertsManager : public Singleton<AlertsManager>
{
public:
    void Alerts_Worker_Initialize(MainWindow* mainWindow);
    AlertsWorker* Alerts_Worker_Get();
private:
    QThread* alerts_worker_thread;
    AlertsWorker* alerts_worker_object;
};
