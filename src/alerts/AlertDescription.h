#pragma once

#include <QMap>
#include <QMetaType>
#include <QString>

#include "3rdparty/jsoncpp/json.h"

class SensorBase;

/**
 * @brief Alert description class
 * @remark Look at /docs/json_schema.md for details
 */
class AlertDescription
{
   /**
   * @remark set it to sensor id
   */
  QString id;

  /**
   * @remark set it to sensor image
   */
  QString image;

  QString color;

  QString sound;

  bool sound_ring;

  QMap<QString,QString> text;

  bool diplay_top_panel;

  bool display_message;

  bool show_on_startup;

public:
  AlertDescription();
  AlertDescription(Json::Value& json);

  QString GetId();
  void SetId(QString id);

  QString GetImage();
  void SetImage(QString imagePath);

  QString GetColor();
  void SetColor(QString color);

  QString GetSoundFile(QString language);
  void SetSoundFile(QString filepath);

  bool ShouldRing();

  QString GetText(QString language);
  void SetText(QString language, QString text);

  bool GetDisplayingTop();
  void SetDisplayingTop(bool displ);

  bool GetDiplayingMsg();
  void SetDisplayingMsg(bool displ);

  bool GetShowOnStartup();
  void SetShowOnStartup(bool show);
};

Q_DECLARE_METATYPE(AlertDescription)
Q_DECLARE_METATYPE(AlertDescription*)
