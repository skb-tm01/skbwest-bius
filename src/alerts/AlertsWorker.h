#pragma once

#include <atomic>

#include <QObject>
#include <QQueue>
#include <QVector>
#include <QTimer>
#include <QWidget>

#include "core/ThreadSafeQueue.h"
#include "alerts/AlertDescription.h"
#include "alerts/AlertsWorker.h"

class MainWindow;

class AlertsWorker : public QObject
{
    Q_OBJECT
public:
    explicit AlertsWorker(MainWindow* mainWindow, QObject *parent = 0);

signals:
    void finished();
    void alert_required_window(AlertDescription*);
    void alert_required_topbar(AlertDescription*);

public slots:
    void Run();
    void MainWindowInitialized();
    void AddTopAlert(AlertDescription* alertDescription);
    void AddCenterAlert(AlertDescription* alertDescription);
    void MarkAsDisplayed(AlertDescription* alertDescription);
    void MarkAsDisplayed(QString alertId);
    void RemoveAlert(QString alertId);
    void OnCanConnection_Lost();
    void OnCanConnection_Restored();
    void ClearAlerts(bool leaveSpecialAlertsShown = false);  // leave some messages shown (for example, on startup)

private:
    bool IsItTimeToDisplay(AlertDescription* alert_description);
    int GetDisplayInterval(AlertDescription* alert_description);
    std::atomic_bool _terminationRequested;

    ThreadSafeQueue<AlertDescription*> _alerts_queue_window;
    MainWindow* _mainWindow;
    bool _mainWindowInitialized;

    bool _isCanConnectionOK;

    bool _showDebug;
//    QStringList alerts_ids_center;  // список тех, кого не показывать
    QMap<QString,uint32_t> _last_display_time;
};
