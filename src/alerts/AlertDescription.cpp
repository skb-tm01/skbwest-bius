#include <QCoreApplication>

#include "alerts/AlertDescription.h"
#include "core/PathHelper.h"
#include "peripherials/sensors/SensorBase.h"

AlertDescription::AlertDescription()
{
    show_on_startup = false;
    this->id = "";
    this->diplay_top_panel = true;
    this->display_message = true;
}

AlertDescription::AlertDescription(Json::Value &json)
{
    show_on_startup = false;
    //visibility
    this->diplay_top_panel = json["display_top"].asBool();
    if(json["display_msg"].empty())
        this->display_message = true;
    else
        this->display_message = json["display_msg"].asBool();

    this->show_on_startup = json["show_on_startup"].asBool();

    //color
    this->color = QString::fromUtf8(json["color"].asString().c_str());

    //sound
    this->sound = QString::fromUtf8(json["sound"].asString().c_str());
    if(json["sound_ring"].empty())
        this->sound_ring = true;
    else
        this->sound_ring = json["sound_ring"].asBool();

    //text
    Json::Value j_text = json["text"];
    std::vector<std::string> j_text_keys = j_text.getMemberNames();
    for (size_t i = 0; i<j_text_keys.size(); i++)
    {
        text.insert(j_text_keys[i].c_str(),QString::fromUtf8(j_text[j_text_keys[i]].asString().c_str()));
    }
}

QString AlertDescription::GetId()
{
    return id;
}

void AlertDescription::SetId(QString id)
{
    this->id = id;
}

QString AlertDescription::GetImage()
{
    return image;
}

void AlertDescription::SetImage(QString imagePath)
{
    image = imagePath;
}


QString AlertDescription::GetColor()
{
    return color;
}

void AlertDescription::SetColor(QString color)
{
    this->color = color;
}

QString AlertDescription::GetSoundFile(QString language)
{
    return PathHelper::pathAppend(QCoreApplication::applicationDirPath(),"data/sounds/"+sound+"_"+language+".wav");
}

void AlertDescription::SetSoundFile(QString filepath)
{
    this->sound = filepath;
}

bool AlertDescription::ShouldRing() {
    return this->sound_ring;
}

QString AlertDescription::GetText(QString language)
{
    if(!text.contains(language))
        return "";

    return text[language];
}

void AlertDescription::SetText(QString language, QString text)
{
    this->text[language]=text;
}

bool AlertDescription::GetDisplayingTop()
{
    return diplay_top_panel;
}

void AlertDescription::SetDisplayingTop(bool displ)
{
    diplay_top_panel = displ;
}

bool AlertDescription::GetDiplayingMsg()
{
    return display_message;
}

void AlertDescription::SetDisplayingMsg(bool displ)
{
    display_message = displ;
}

// ошибка не должна убираться при старте
bool AlertDescription::GetShowOnStartup()
{
    return show_on_startup;
}

void AlertDescription::SetShowOnStartup(bool show)
{
    diplay_top_panel = show_on_startup;
}
