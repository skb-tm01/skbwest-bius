 #include <QObject>

#include "alerts/AlertsManager.h"
#include "alerts/AlertsWorker.h"
#include "widgets/mainwindow.h"

void AlertsManager::Alerts_Worker_Initialize(MainWindow* mainWindow)
{
    alerts_worker_thread = new QThread();
    alerts_worker_thread->setObjectName("Alerts/Worker");

    alerts_worker_object = new AlertsWorker(mainWindow);
    alerts_worker_object->moveToThread(alerts_worker_thread);

    //connect(can_worker, SIGNAL (error(QString)), this, SLOT (errorString(QString)));
    QObject::connect(alerts_worker_thread, SIGNAL (started()), alerts_worker_object, SLOT (Run()));
    QObject::connect(alerts_worker_object, SIGNAL (finished()), alerts_worker_thread, SLOT (quit()));
    QObject::connect(alerts_worker_object, SIGNAL (finished()), alerts_worker_object, SLOT (deleteLater()));
    QObject::connect(alerts_worker_thread, SIGNAL (finished()), alerts_worker_thread, SLOT (deleteLater()));

    alerts_worker_thread->start();
}

AlertsWorker* AlertsManager::Alerts_Worker_Get()
{
    return alerts_worker_object;
}
