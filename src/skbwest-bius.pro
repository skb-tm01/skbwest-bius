linux-* {
     DESTDIR_TARGET = /root
}

QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = skbwest-bius
TEMPLATE = app

#enable C++11
QMAKE_CXXFLAGS += -std=c++0x -g -rdynamic

# The following define makes your compiler emit warnings if you use
DEFINES += QT_DEPRECATED_WARNINGS

# disables all the APIs deprecated before Qt 6.0.0
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000

HEADERS = \
    widgets/flowwindow.h \
    widgets/SensorWidget.h \
    widgets/SensorStateInstructions.h \
    widgets/SensorType1Screen.h \
    widgets/SensorType2Screen.h \
    widgets/EventFilters/MenuButtonEventFilter.h \
    widgets/SensorsStatisticsEnum.h \
    widgets/Windows/ConfirmationWindow.h \
    peripherials/can/CanDataRetriever.h \
    widgets/EventFilters/ComboboxEventFilter.h \
    widgets/EventFilters/CheckBoxEventFilter.h \
    widgets/EventFilters/SliderEventFilter.h \
    widgets/EventFilters/ScreenShotEventFilter.h \
    peripherials/can/packets/AirFlowTemp.h \
    peripherials/combines/Combine.h \
    peripherials/combines/CombineKeeper.h \
    widgets/WindowManager.h \
    widgets/EventFilters/mousepressredirectfilter.h


SOURCES = \
    widgets/flowwindow.cpp \
    widgets/SensorWidget.cpp \
    widgets/SensorStateInstructions.cpp \
    widgets/SensorType1Screen.cpp \
    widgets/SensorType2Screen.cpp \
    widgets/EventFilters/MenuButtonEventFilter.cpp \
    widgets/Windows/ConfirmationWindow.cpp \
    peripherials/can/CanDataRetriever.cpp \
    widgets/EventFilters/ComboboxEventFilter.cpp \
    widgets/EventFilters/CheckBoxEventFilter.cpp \
    widgets/EventFilters/SliderEventFilter.cpp \
    widgets/EventFilters/ScreenShotEventFilter.cpp \
    peripherials/can/packets/AirFlowTemp.cpp \
    peripherials/combines/CombineKeeper.cpp \
    peripherials/combines/Combine.cpp \
    widgets/WindowManager.cpp \
    widgets/EventFilters/mousepressredirectfilter.cpp

FORMS =  widgets/Windows/ConfirmationWindow.ui \
    widgets/ToInstructions.ui \
    widgets/flowwindow.ui \
    widgets/SensorWidget.ui \
    widgets/SensorStateInstructions.ui \
    widgets/SensorType1Screen.ui \
    widgets/SensorType2Screen.ui \

#3rdparty/flickcharm
HEADERS_3RDPARTY_FLICKCHARM += \
    3rdparty/flickcharm/flickcharm.h

SOURCES_3RDPARTY_FLICKCHARM += \
    3rdparty/flickcharm/flickcharm.cpp

HEADERS += $$HEADERS_3RDPARTY_FLICKCHARM \
    peripherials/spi/IlluminationMonitor.h
SOURCES += $$SOURCES_3RDPARTY_FLICKCHARM \
    peripherials/spi/IlluminationMonitor.cpp

#3rdparty/jsoncpp
HEADERS_3RDPARTY_JSONCPP += \
    3rdparty/jsoncpp/json.h

SOURCES_3RDPARTY_JSONCPP += \
    3rdparty/jsoncpp/jsoncpp.cpp

HEADERS += $$HEADERS_3RDPARTY_JSONCPP
SOURCES += $$SOURCES_3RDPARTY_JSONCPP


#3rdparty/jsonutils
HEADERS_3RDPARTY_JSONUTILS += \
    3rdparty/jsonutils/jsonutils.h

SOURCES_3RDPARTY_JSONUTILS += \
    3rdparty/jsonutils/jsonutils.cpp

HEADERS += $$HEADERS_3RDPARTY_JSONUTILS
SOURCES += $$SOURCES_3RDPARTY_JSONUTILS


#Core
HEADERS_CORE += \
    core/FlowLayout.h \
    core/AudioHelper.h \
    core/ByteConversion.h \
    core/Singleton.h \
    core/SystemHelper.h \
    core/PathHelper.h \
    core/helper.h \
    core/ClickableLabel.h \
    core/ThreadSafeQueue.h \
    core/ValueSmoothingHelper.h \
    core/SleepHelper.h

SOURCES_CORE += \
    core/AudioHelper.cpp \
    core/FlowLayout.cpp \
    core/PathHelper.cpp \
    core/SystemHelper.cpp \
    core/helper.cpp \
    core/ClickableLabel.cpp \
    core/ValueSmoothingHelper.cpp \
    core/SleepHelper.cpp

HEADERS += $$HEADERS_CORE
SOURCES += $$SOURCES_CORE

#Alerts
HEADERS_ALERTS += \
    alerts/AlertDescription.h \
    alerts/AlertsManager.h \
    alerts/AlertsWorker.h

SOURCES_ALERTS += \
    alerts/AlertDescription.cpp \
    alerts/AlertsManager.cpp \
    alerts/AlertsWorker.cpp

HEADERS += $$HEADERS_ALERTS
SOURCES += $$SOURCES_ALERTS

#Peripherials
HEADERS_PERIPHERIALS += \
    peripherials/PeripherialsManager.h \
    peripherials/audio/AudioWorker.h \

SOURCES_PERIPHERIALS += \
    peripherials/PeripherialsManager.cpp \
    peripherials/audio/AudioWorker.cpp \


HEADERS += $$HEADERS_PERIPHERIALS
SOURCES += $$SOURCES_PERIPHERIALS


#Peripherials/CAN
HEADERS_PERIPHERIALS_CAN += \
    peripherials/can/CanWorker.h \
    peripherials/can/CanBackend_SocketCan.h \
    peripherials/can/ICanBackend.h \
    peripherials/can/CanMessage.h

SOURCES_PERIPHERIALS_CAN += \
    peripherials/can/CanWorker.cpp \
    peripherials/can/CanBackend_SocketCan.cpp

HEADERS += $$HEADERS_PERIPHERIALS_CAN
SOURCES += $$SOURCES_PERIPHERIALS_CAN


#Peripherials/CAN/Packets
HEADERS_PERIPHERIALS_CAN_PACKETS += \
    peripherials/can/packets/Analog.h \
    peripherials/can/packets/TahoCalibration.h \
    peripherials/can/packets/Taho.h \
    peripherials/can/packets/Probeg.h \
    peripherials/can/packets/ProbegReset.h \
    peripherials/can/packets/Time.h \
    peripherials/can/packets/Settings.h \
    peripherials/can/packets/ContactState.h \
    peripherials/can/packets/ContactStateSettings.h \
    peripherials/can/packets/ContactStateTime.h \
    peripherials/can/packets/J1939.h \
    peripherials/can/packets/Control.h \
    peripherials/can/packets/LossSave.h \
    peripherials/can/packets/IPSState.h \
    peripherials/can/packets/TahoSave.h \
    peripherials/can/packets/Calibration.h \
    peripherials/can/packets/OutSensorSettings.h \
    peripherials/can/packets/RpmSettings1.h \
    peripherials/can/packets/RpmSettings2.h \
    peripherials/can/packets/RpmSettings3.h \
    peripherials/can/packets/AdcSettings1.h \
    peripherials/can/packets/AdcSettings2.h \
    peripherials/can/packets/AdcSettings3.h \
    peripherials/can/packets/AdcSettings4.h \
    peripherials/can/packets/WorkModeStatus.h \
    peripherials/can/packets/Journal.h \
    peripherials/can/packets/Request3.h \
    peripherials/can/packets/AirFlowTemp.h \
    peripherials/can/packets/DataGet.h

SOURCES_PERIPHERIALS_CAN_PACKETS += \
    peripherials/can/packets/Analog.cpp \
    peripherials/can/packets/TahoCalibration.cpp \
    peripherials/can/packets/Taho.cpp \
    peripherials/can/packets/Probeg.cpp \
    peripherials/can/packets/ProbegReset.cpp \
    peripherials/can/packets/Time.cpp \
    peripherials/can/packets/Settings.cpp \
    peripherials/can/packets/ContactState.cpp \
    peripherials/can/packets/ContactStateSettings.cpp \
    peripherials/can/packets/ContactStateTime.cpp \
    peripherials/can/packets/J1939.cpp \
    peripherials/can/packets/Control.cpp \
    peripherials/can/packets/LossSave.cpp \
    peripherials/can/packets/IPSState.cpp \
    peripherials/can/packets/TahoSave.cpp \
    peripherials/can/packets/Calibration.cpp \
    peripherials/can/packets/OutSensorSettings.cpp \
    peripherials/can/packets/RpmSettings1.cpp \
    peripherials/can/packets/RpmSettings2.cpp \
    peripherials/can/packets/RpmSettings3.cpp \
    peripherials/can/packets/AdcSettings1.cpp \
    peripherials/can/packets/AdcSettings2.cpp \
    peripherials/can/packets/AdcSettings3.cpp \
    peripherials/can/packets/AdcSettings4.cpp \
    peripherials/can/packets/WorkModeStatus.cpp \
    peripherials/can/packets/Journal.cpp \
    peripherials/can/packets/Request3.cpp \
    peripherials/can/packets/DataGet.cpp

HEADERS += $$HEADERS_PERIPHERIALS_CAN_PACKETS
SOURCES += $$SOURCES_PERIPHERIALS_CAN_PACKETS


#Peripherials/GPIO
HEADERS_PERIPHERIALS_GPIO += \
    peripherials/gpio/gpio.h \
    peripherials/gpio/HardwareButton.h \
    peripherials/gpio/HardwareKeyboard.h

SOURCES_PERIPHERIALS_GPIO += \
    peripherials/gpio/gpio.cpp \
    peripherials/gpio/HardwareButton.cpp \
    peripherials/gpio/HardwareKeyboard.cpp

HEADERS += $$HEADERS_PERIPHERIALS_GPIO
SOURCES += $$SOURCES_PERIPHERIALS_GPIO


#Peripherials/Sensors
HEADERS_PERIPHERIALS_SENSORS += \
    peripherials/sensors/ISensor.h \
    peripherials/sensors/SensorType.h \
    peripherials/sensors/SensorBase.h \
    peripherials/sensors/SensorManager.h \
    peripherials/sensors/SensorADS.h \
    peripherials/sensors/SensorDIG.h \
    peripherials/sensors/SensorOUT.h \
    peripherials/sensors/SensorRPM.h \
    peripherials/sensors/SensorOther.h \
    peripherials/sensors/SensorVirtual.h

SOURCES_PERIPHERIALS_SENSORS += \
    peripherials/sensors/SensorType.cpp \
    peripherials/sensors/SensorBase.cpp \
    peripherials/sensors/SensorManager.cpp \
    peripherials/sensors/SensorADS.cpp \
    peripherials/sensors/SensorDIG.cpp \
    peripherials/sensors/SensorOUT.cpp \
    peripherials/sensors/SensorRPM.cpp \
    peripherials/sensors/SensorOther.cpp \
    peripherials/sensors/SensorVirtual.cpp


HEADERS += $$HEADERS_PERIPHERIALS_SENSORS
SOURCES += $$SOURCES_PERIPHERIALS_SENSORS


#Peripherials/SPI
HEADERS_PERIPHERIALS_SPI += \
    peripherials/spi/TransferSPI.h

SOURCES_PERIPHERIALS_SPI += \
    peripherials/spi/TransferSPI.cpp

HEADERS += $$HEADERS_PERIPHERIALS_SPI
SOURCES += $$SOURCES_PERIPHERIALS_SPI


#widgets/Controls
HEADERS_WIDGETS_CONTROLS += \
    widgets/Controls/ArrowKeyControl.h \
    widgets/Controls/Clock.h \

SOURCES_WIDGETS_CONTROLS += \
    widgets/Controls/ArrowKeyControl.cpp \
    widgets/Controls/Clock.cpp \

FORMS_WIDGETS_CONTROLS += \
    widgets/Controls/ArrowKeyControl.ui \
    widgets/Controls/Clock.ui \

HEADERS += $$HEADERS_WIDGETS_CONTROLS
SOURCES += $$SOURCES_WIDGETS_CONTROLS
FORMS += $$FORMS_WIDGETS_CONTROLS

#Widgets/EventFilters
HEADERS_WIDGETS_EVENTFILTERS += \
    widgets/EventFilters/KeyPressRedirectFilter.h \
    widgets/EventFilters/FocusEventFilter.h \
    widgets/EventFilters/IdleEventFilter.h \
    widgets/EventFilters/ClickSoundEventFilter.h \

SOURCES_WIDGETS_EVENTFILTERS += \
    widgets/EventFilters/KeyPressRedirectFilter.cpp \
    widgets/EventFilters/FocusEventFilter.cpp \
    widgets/EventFilters/IdleEventFilter.cpp \
    widgets/EventFilters/ClickSoundEventFilter.cpp \

HEADERS += $$HEADERS_WIDGETS_EVENTFILTERS
SOURCES += $$SOURCES_WIDGETS_EVENTFILTERS

#Widgets/Panels
HEADERS_WIDGETS_PANELS += \
    widgets/Panels/AlertPanel.h \
    widgets/Panels/AlertPanelItem.h \
    widgets/Panels/StatusPanel.h \
    widgets/Panels/StatusPanelItem.h \

SOURCES_WIDGETS_PANELS += \
    widgets/Panels/AlertPanel.cpp \
    widgets/Panels/AlertPanelItem.cpp \
    widgets/Panels/StatusPanel.cpp \
    widgets/Panels/StatusPanelItem.cpp \

FORMS_WIDGETS_PANELS += \
    widgets/Panels/AlertPanel.ui \
    widgets/Panels/AlertPanelItem.ui \
    widgets/Panels/StatusPanel.ui \
    widgets/Panels/StatusPanelItem.ui \

HEADERS += $$HEADERS_WIDGETS_PANELS
SOURCES += $$SOURCES_WIDGETS_PANELS
FORMS += $$FORMS_WIDGETS_PANELS

#Widgets/VirtualKeyboard
SOURCES_WIDGETS_VIRTUALKEYBOARD += \
    widgets/VirtualKeyboard/BEVirtualKeyboardContext.cpp\
    widgets/VirtualKeyboard/BEVirtualKeyboard.cpp

HEADERS_WIDGETS_VIRTUALKEYBOARD += \
    widgets/VirtualKeyboard/BEVirtualKeyboard.h \
    widgets/VirtualKeyboard/BEVirtualKeyboardContext.h

FORMS_WIDGETS_VIRTUALKEYBOARD += \
   widgets/VirtualKeyboard/BEVirtualKeyboard.ui

lessThan(QT_MAJOR_VERSION, 5): SOURCES += $$SOURCES_WIDGETS_VIRTUALKEYBOARD
lessThan(QT_MAJOR_VERSION, 5): HEADERS += $$HEADERS_WIDGETS_VIRTUALKEYBOARD
lessThan(QT_MAJOR_VERSION, 5): FORMS   += $$FORMS_WIDGETS_VIRTUALKEYBOARD


#Widgets/QCGauge
SOURCES_WIDGETS_QCGAUGE += \
    widgets/QCGauge/qcitem.cpp \
    widgets/QCGauge/qcscale.cpp \
    widgets/QCGauge/qcvalues.cpp \
    widgets/QCGauge/fuelgauge.cpp \
    widgets/QCGauge/qcarc.cpp \
    widgets/QCGauge/qcgauge.cpp \
    widgets/QCGauge/qclabel.cpp \
    widgets/QCGauge/qcimage.cpp \
    widgets/QCGauge/speedgauge.cpp \
    widgets/QCGauge/qcneedle.cpp \
    widgets/QCGauge/qcticks.cpp \
    widgets/QCGauge/qcband.cpp \
    widgets/QCGauge/powergauge.cpp \
    widgets/QCGauge/enginegauge.cpp \
    widgets/QCGauge/fungauge.cpp \
    widgets/QCGauge/drumgauge.cpp

HEADERS_WIDGETS_QCGAUGE += \
    widgets/QCGauge/qcitem.h \
    widgets/QCGauge/qcscale.h \
    widgets/QCGauge/qcvalues.h \
    widgets/QCGauge/fuelgauge.h \
    widgets/QCGauge/qcarc.h \
    widgets/QCGauge/qcgauge.h \
    widgets/QCGauge/qclabel.h \
    widgets/QCGauge/qcimage.h \
    widgets/QCGauge/speedgauge.h \
    widgets/QCGauge/qcneedle.h \
    widgets/QCGauge/qcticks.h \
    widgets/QCGauge/qcband.h \
    widgets/QCGauge/powergauge.h \
    widgets/QCGauge/enginegauge.h \
    widgets/QCGauge/fungauge.h \
    widgets/QCGauge/drumgauge.h

SOURCES += $$SOURCES_WIDGETS_QCGAUGE
HEADERS += $$HEADERS_WIDGETS_QCGAUGE


#Widgets/QLGauge
SOURCES_WIDGETS_QLGAUGE += \
    widgets/QLGauges/qlgauge.cpp \
    widgets/QLGauges/bargauge.cpp

HEADERS_WIDGETS_QLGAUGE += \
    widgets/QLGauges/qlgauge.h \
    widgets/QLGauges/bargauge.h

FORMS_WIDGETS_QLGAUGE += \
    widgets/QLGauges/bargauge.ui \

SOURCES += $$SOURCES_WIDGETS_QLGAUGE
HEADERS += $$HEADERS_WIDGETS_QLGAUGE
FORMS   += $$FORMS_WIDGETS_QLGAUGE

#Widgets/Menu
SOURCES_WIDGETS_MENU += \
    widgets/Menu/menu_main.cpp\
    widgets/Menu/menu_stats.cpp \
    widgets/Menu/menu_stats_alarm.cpp \
    widgets/Menu/menu_stats_alarm_table.cpp \
    widgets/Menu/menu_stats_eventlog.cpp \
    widgets/Menu/menu_stats_eventlog_table.cpp \
    widgets/Menu/menu_stats_general.cpp \
    widgets/Menu/menu_stats_j1939.cpp \
    widgets/Menu/menu_stats_to.cpp\
    widgets/Menu/menu_settings.cpp\
    widgets/Menu/menu_settings_system.cpp \
    widgets/Menu/menu_settings_terminal.cpp \
    widgets/Menu/menu_settings_workmodes.cpp \
    widgets/Menu/menu_calibration.cpp \
    widgets/Menu/menu_calibration_clearance.cpp \
    widgets/Menu/menu_help.cpp \
    widgets/Menu/menu_copy.cpp \
    widgets/Menu/menu_sensors.cpp \
    widgets/Menu/menu_sensors_rotations.cpp \


HEADERS_WIDGETS_MENU += \
    widgets/Menu/menu_main.h \
    widgets/Menu/menu_stats.h \
    widgets/Menu/menu_stats_alarm.h \
    widgets/Menu/menu_stats_alarm_table.h \
    widgets/Menu/menu_stats_eventlog.h \
    widgets/Menu/menu_stats_eventlog_table.h \
    widgets/Menu/menu_stats_general.h \
    widgets/Menu/menu_stats_j1939.h \
    widgets/Menu/menu_stats_to.h\
    widgets/Menu/menu_settings.h\
    widgets/Menu/menu_settings_system.h \
    widgets/Menu/menu_settings_terminal.h \
    widgets/Menu/menu_settings_workmodes.h \
    widgets/Menu/menu_calibration.h \
    widgets/Menu/menu_calibration_clearance.h \
    widgets/Menu/menu_help.h \
    widgets/Menu/menu_copy.h \
    widgets/Menu/menu_sensors.h \
    widgets/Menu/menu_sensors_rotations.h \



FORMS_WIDGETS_MENU += \
    widgets/Menu/menu_main.ui \
    widgets/Menu/menu_stats.ui \
    widgets/Menu/menu_stats_alarm.ui \
    widgets/Menu/menu_stats_alarm_table.ui \
    widgets/Menu/menu_stats_eventlog.ui \
    widgets/Menu/menu_stats_eventlog_table.ui \
    widgets/Menu/menu_stats_general.ui \
    widgets/Menu/menu_stats_j1939.ui \
    widgets/Menu/menu_stats_to.ui\
    widgets/Menu/menu_settings.ui\
    widgets/Menu/menu_settings_system.ui \
    widgets/Menu/menu_settings_terminal.ui \
    widgets/Menu/menu_settings_workmodes.ui \
    widgets/Menu/menu_calibration.ui \
    widgets/Menu/menu_calibration_clearance.ui \
    widgets/Menu/menu_help.ui \
    widgets/Menu/menu_copy.ui \
    widgets/Menu/menu_sensors.ui \
    widgets/Menu/menu_sensors_rotations.ui \


SOURCES += $$SOURCES_WIDGETS_MENU
HEADERS += $$HEADERS_WIDGETS_MENU
FORMS += $$FORMS_WIDGETS_MENU

#Widgets/Windows
SOURCES_WINDGETS_WINDOWS += \
    widgets/Windows/PasswordChangeWindow.cpp \
    widgets/Windows/PasswordEnterWindow.cpp

HEADERS_WINDGETS_WINDOWS += \
    widgets/Windows/PasswordChangeWindow.h \
    widgets/Windows/PasswordEnterWindow.h

FORMS_WINDGETS_WINDOWS += \
    widgets/Windows/PasswordChangeWindow.ui \
    widgets/Windows/PasswordEnterWindow.ui

SOURCES += $$SOURCES_WINDGETS_WINDOWS
HEADERS += $$HEADERS_WINDGETS_WINDOWS
FORMS += $$FORMS_WINDGETS_WINDOWS


#other
SOURCES += \
    main.cpp \
    widgets/mainwindow.cpp \
    SettingsManager.cpp \
    widgets/gaugeswidget.cpp \
    widgets/sensors_statistics_short.cpp \
    widgets/sensors_statistics_full.cpp \
    widgets/warningmessage.cpp \
    widgets/ToInstructions.cpp \
    widgets/SensorType3Screen.cpp \
    widgets/SensorType4Screen.cpp

HEADERS += \
    Config.h \
    SettingsManager.h \
    widgets/mainwindow.h \
    widgets/gaugeswidget.h \
    widgets/sensors_statistics_short.h \
    widgets/sensors_statistics_full.h \
    widgets/warningmessage.h \
    widgets/ToInstructions.h \
    widgets/SensorType3Screen.h \
    widgets/SensorType4Screen.h

FORMS += \
    widgets/mainwindow.ui \
    widgets/gaugeswidget.ui \
    widgets/sensors_statistics_short.ui \
    widgets/sensors_statistics_full.ui \
    widgets/warningmessage.ui \
    widgets/SensorType3Screen.ui \
    widgets/SensorType4Screen.ui \

target.files = skbwest-bius
target.path = $${DESTDIR_TARGET}
INSTALLS = target

sensorrs.path    = $${DESTDIR_TARGET}/data/sensors
sensorrs.files   += ../data/sensors/*
INSTALLS        += sensorrs

sounds.path    = $${DESTDIR_TARGET}/data/sounds
sounds.files   += ../data/sounds/*
INSTALLS       += sounds

icons.path    = $${DESTDIR_TARGET}/data/icons
icons.files   += ../data/icons/*
INSTALLS      += icons


stylesheets.path    = $${DESTDIR_TARGET}/data/stylesheets
stylesheets.files   += ../data/stylesheets/*
INSTALLS      += stylesheets


RESOURCES += \
    icons.qrc \

DEFINES += NO_CAN
