#include "PathHelper.h"
#include "helper.h"
#include <QImage>
#include <QColor>
#include<QDebug>
#include<QFile>
#include <QCoreApplication>

Helper::Helper()
{

}

QPixmap Helper::ScalePixmap(QPixmap img, int size)
{
    return img.scaled(size, size, Qt::KeepAspectRatio, Qt::SmoothTransformation);
}

QPixmap Helper::LoadPixmap(QString fileName)
{
    if(fileName.leftRef(1)!=":")
        fileName = Helper::IconPath(fileName);

    if(!QFile::exists(fileName))
        qDebug()<<"Icon not found:"<<fileName;

    QPixmap pxm(fileName);
    if(pxm.isNull())
        pxm.load(":/media/notfound.png");
    return pxm;
}

QPixmap Helper::LoadPixmap(QString fileName, int size, QColor color)
{
    QPixmap pxm = LoadPixmap(fileName);
    return ScalePixmap(SetPixmapColor(pxm,color),size);
}

QPixmap Helper::LoadPixmap(QString fileName, int size, QString color)
{
    return LoadPixmap(fileName,size, QColor(color));
}

QPixmap Helper::LoadPixmap(QString fileName, int size)
{
    return LoadPixmap(fileName,size,Qt::black);
}

void Helper::SetInRect(const QRectF &out, QRectF &in)
{
 if(out.contains(in))
     return;

 if(in.top()<out.top())
     in.moveTop(out.top());

 if(in.bottom()>out.bottom())
     in.moveBottom(out.bottom());

 if(in.left()<out.left())
     in.moveLeft(out.left());

 if(in.right()>out.right())
     in.moveRight(out.right());
}

QPixmap Helper::SetPixmapColor(const QPixmap& pxm, QColor color)
{
    if(color==Qt::black)
        return pxm;

    QImage tmp = pxm.toImage();

    for(int y = 0; y < tmp.height(); y++)
      for(int x= 0; x < tmp.width(); x++)
      {
          if(qAlpha(tmp.pixel(x,y)) != 0)
            tmp.setPixel(x,y,color.rgba());
      }
    return QPixmap::fromImage(tmp);
}

//sets black to forecolor, transparent to backcolor
QPixmap Helper::SetPixmapColor(const QPixmap& pxm, QColor forecolor, QColor backcolor)
{
    QImage tmp = pxm.toImage();

    for(int y = 0; y < tmp.height(); y++)
      for(int x= 0; x < tmp.width(); x++)
      {
          if(QColor::fromRgba(tmp.pixel(x, y))==Qt::black)
            tmp.setPixel(x,y,forecolor.rgba());
          if(QColor::fromRgba(tmp.pixel(x, y))==Qt::transparent)
            tmp.setPixel(x,y,backcolor.rgba());
      }
    return QPixmap::fromImage(tmp);
}

QString Helper::LoadResource(const QString& name)
{
    QFile file(name);
    if(!file.open(QIODevice::ReadOnly))
    {
        qDebug()<<"Resource file not opened"<<endl;
        return QString();
    }
    QString str = file.readAll();
    file.close();

    return str;
}

QString Helper::IconPath(const QString &name)
{
    return PathHelper::pathAppend(PathHelper::pathAppend(PathHelper::GetDataDirectory(),"/icons/"), name);
}

QColor Helper::dayDisabledColor()
{
    return QColor(230,220,250);
}
