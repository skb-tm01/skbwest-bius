#include "ValueSmoothingHelper.h"
#include<QDebug>

ValueSmoothingHelper::ValueSmoothingHelper(QObject *parent): QObject(parent)
{
    _value     = 0;
    _nextValue = 0;
    _delta     = 0;
    _counter   = 0;

    _slowness = 10;

    _timer.setInterval(50);
    QObject::connect(&_timer, SIGNAL(timeout()), this, SLOT(TryToChangeValue()));
}

void ValueSmoothingHelper::setValue(float value)
{
    _timer.stop();
    _nextValue = value;
    if(_value!=_nextValue)
    {
        _delta = (_nextValue-_value)/(float)_slowness;
        _counter = 0;
        _timer.start();
    }
}

// slowness should be > 0 . default is 10.
void ValueSmoothingHelper::setSlowness(int slowness)
{
    if(slowness <=0)
        return;
    _slowness = slowness;
}

void ValueSmoothingHelper::TryToChangeValue()
{
    if(_counter>=_slowness)
    {
        _value = _nextValue;
        _timer.stop();
     }
    else
    {
        _value+=_delta;
        _counter++;
    }

    if(_value < 0)
        _value = 0;

    emit ValueChanged(_value);
}
