#pragma once

#include <cstdint>

class ByteConversion{
public:
    static uint16_t make_uint16(const uint8_t byte_low, const uint8_t byte_high)
	{
         return   (( byte_high << 8  ) | (byte_low & 0x00FF) );
	}

    static uint32_t make_uint32(const uint16_t byte_low, const uint16_t byte_high)
    {
         return   (( byte_high << 16  ) | (byte_low & 0x0000FFFF) );
    }

    static bool GetBitState(uint8_t* array, uint32_t bit_number)
    {
        int byte = bit_number/8;
        uint8_t bit  = bit_number%8;

        return GetBitState(array[byte], bit);
    }

    static bool GetBitState(uint8_t* array, int32_t bit_number)
    {
        int byte = bit_number/8;
        uint8_t bit  = bit_number%8;

        return GetBitState(array[byte], bit);
    }

    static bool GetBitState(uint8_t byte, uint8_t bit)
    {
        return static_cast<bool>((byte >> bit) & 0x01);
    }
};
