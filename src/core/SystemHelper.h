#pragma once

#include <QString>

class SystemHelper
{
public:
    static QString ExecuteSystemCommand(QString command);
};
