#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cerrno>

#include <QtGlobal>
#include <QDebug>
#include <QFile>

#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
#include <QSound>
#endif


#include "core/AudioHelper.h"
#include "core/SystemHelper.h"
#include "core/PathHelper.h"

#include "SettingsManager.h"

#include "peripherials/PeripherialsManager.h"

void AudioHelper::PlayWaveFile(QString absolutePath, RingingType withRinging)
{
    auto* ptr = PeripherialsManager::instance().AudioWorker_Get();
    if(ptr!=nullptr)
    {
        if(withRinging==RingingType::Error)
        {
            ptr->AddSound(PathHelper::pathAppend(PathHelper::GetDataDirectory(),"sounds/ringing.wav"));
        }
        else if(withRinging==RingingType::Warning)
        {
            ptr->AddSound(PathHelper::pathAppend(PathHelper::GetDataDirectory(),"sounds/notify.wav"));
        }

        if(!QFile::exists(absolutePath))
        {
            qDebug()<<"File "<<absolutePath <<" not exists";
            return;
        }

        if(SettingsManager::settings()->voiceMessages())
        {
            ptr->AddSound(absolutePath);
        }
    }

}

void AudioHelper::PlayWaveFile(QString absolutePath)
{
    if(!QFile::exists(absolutePath))
    {
        qDebug()<<"File "<<absolutePath <<" not exists";
        return;
    }

    #if QT_VERSION < QT_VERSION_CHECK(5, 0, 0) && !defined BUILD_DEVICE
    QSound::play(absolutePath);
    #elif defined __linux__
    QString command = "aplay -q \""+absolutePath+"\"";
    system(command.toStdString().c_str());
    #endif
}

void AudioHelper::PlayWaveFileWhenQueryIsEmpty(QString absolutePath)
{
    if(!QFile::exists(absolutePath))
    {
        qDebug()<<"File "<<absolutePath <<" not exists";
        return;
    }

    auto* ptr = PeripherialsManager::instance().AudioWorker_Get();
    if(ptr!=nullptr)
    {
        ptr->AddSound(absolutePath, true);
    }

}

int AudioHelper::GetAlsaVolume()
{
#ifdef __linux__
    #ifdef BUILD_DEVICE
        return SystemHelper::ExecuteSystemCommand("amixer sget PCM | grep 'Left:' |  awk -F\"[][]\" '/dB/ { print $2 }' | sed 's/%//g'").toInt();
    #else
        return SystemHelper::ExecuteSystemCommand("amixer sget Master | grep 'Left:' |  awk -F\"[][]\" '/dB/ { print $2 }' | sed 's/%//g'").toInt();
    #endif
#endif

    return 0;
}

void AudioHelper::SetAlsaVolume(int volume_level)
{
    if(volume_level<0 || volume_level>100)
        return;

#ifdef __linux__
    #ifdef BUILD_DEVICE
        system(("amixer -q set 'PCM' "+QString::number(volume_level)+"%").toStdString().c_str());
    #else
        system(("amixer -q set 'Master' "+QString::number(volume_level)+"%").toStdString().c_str());
    #endif
    system("alsactl store");
    system("sync -f &");
#endif
}
