#pragma once

#include <QDebug>

/**
 * @brief simple C++11 singleton
 */
template <typename T>
class Singleton
{
public:
        static T& instance()
        {
            static T inst;
            return inst;
        }
protected:
    Singleton() = default;
    ~Singleton() = default;
private:
    Singleton( const Singleton& ) = delete;
    Singleton& operator=( const Singleton& ) = delete;
};
