#include "core/SleepHelper.h"

#ifndef _MSC_VER
#include <unistd.h>
#else
#include <chrono>
#include <thread>
#endif

void SleepHelper::usleep(uint32_t usecs)
{
#ifndef _MSC_VER
    ::usleep(usecs);
#else
    std::this_thread::sleep_for(std::chrono::microseconds(usecs));
#endif
}
