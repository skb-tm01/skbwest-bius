#pragma once
 
#include <QString>

class PathHelper
{
public:
    static QString pathAppend(const QString& path1, const QString& path2);

    static QString GetDataDirectory();

    static QString GetAppDirectory();
private:
    static QString datapath;
};
