#pragma once

#include <QString>


enum RingingType{
    No,
    Warning,
    Error
};

class AudioHelper{
public:
    static void PlayWaveFile(QString absolutePath, RingingType withRinging);
    static void PlayWaveFile(QString absolutePath);
    static void PlayWaveFileWhenQueryIsEmpty(QString absolutePath);

    static int GetAlsaVolume();
    static void SetAlsaVolume(int volume_level);
};
