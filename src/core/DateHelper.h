class DateHelper
{
public:
 static void SetLinuxDate()
{
struct tm time = { 0 };

time.tm_year = Year - 1900;
time.tm_mon  = Month - 1;
time.tm_mday = Day;
time.tm_hour = Hour;
time.tm_min  = Minute;
time.tm_sec  = Second;

if (time.tm_year < 0) time.tm_year = 0;

time_t t = mktime(&time);

if (t != (time_t) -1)
    stime(&t);
}
}

