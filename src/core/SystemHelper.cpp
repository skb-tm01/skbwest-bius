#include <cstdio>

#include <QDebug>

#include "core/SystemHelper.h"


QString SystemHelper::ExecuteSystemCommand(QString command)
{
#ifdef __linux__
     QByteArray bytes;
     char path[2014];

     /* Open the command for reading. */
     FILE* fp = popen(command.toStdString().c_str(), "r");
     if (fp == nullptr) {
       return "";
     }

     /* Read the output a line at a time - output it. */
     while (fgets(path, sizeof(path)-1, fp) != nullptr) {
       bytes.append(path);
     }

    return QString::fromUtf8(bytes);
#else
    qDebug()<<"SystemHelper::ExecuteSystemCommand() works only on Linux";
    return "";
#endif
}
