#ifndef HELPER_H
#define HELPER_H

#include<QRectF>
#include <QPixmap>
#include <QImage>

class Helper
{
public:
    Helper();
    static void SetInRect(const QRectF& out,QRectF& in);
    static QPixmap ScalePixmap(QPixmap img, int size);
    static QPixmap LoadPixmap(QString fileName);
    static QPixmap LoadPixmap(QString fileName, int size);
    static QPixmap LoadPixmap(QString fileName, int size, QString color);
    static QPixmap LoadPixmap(QString fileName, int size, QColor color);
    static QPixmap SetPixmapColor(const QPixmap& pxm, QColor color);
    static QPixmap SetPixmapColor(const QPixmap& pxm, QColor forecolor, QColor backcolor);

    static QString LoadResource(const QString& name);
    static QString IconPath(const QString& name);

    inline static int defIconSize(){return 48;}

    inline static QColor gaugeIconColor(){return QColor(96,96,255);}
    inline static QColor gaugeBlinkingColor(){return QColor(255,10,10);}
    inline static QColor gaugeBlinkingColorY(){return QColor(255,150,10);}

    inline static QColor dayForeColor(){return Qt::black;}
    inline static QColor dayBackColor(){return Qt::white;}
    static QColor dayDisabledColor();
    inline static QColor nightForeColor(){return Qt::white;}
    inline static QColor nightBackColor(){return QColor(32,37,42);}
    inline static QColor nightDisabledColor(){return QColor(56,56,56);}
};

#endif // HELPER_H
