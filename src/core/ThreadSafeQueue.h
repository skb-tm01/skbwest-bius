#pragma once

#include <QMutex>
#include <QMutexLocker>
#include <QObject>
#include <QQueue>

template<class T>
class ThreadSafeQueue {
public:
    ThreadSafeQueue() = default;
    ~ThreadSafeQueue() = default;

    void Enqueue(T object)
    {
        QMutexLocker locker(&queueMutex);
        queue.enqueue(object);
    }

    T Dequeue()
    {
        QMutexLocker locker(&queueMutex);
        return queue.dequeue();
    }

    bool IsEmpty()
    {
        QMutexLocker locker(&queueMutex);
        return queue.isEmpty();
    }

    int Size()
    {
        QMutexLocker locker(&queueMutex);
        return queue.size();
    }

    void Clear()
    {
        QMutexLocker locker(&queueMutex);
        queue.clear();
    }

private:
    mutable QMutex queueMutex;
    QQueue<T> queue;
};
