#pragma once

#include <cstdint>

class SleepHelper
{
public:
    static void usleep(uint32_t usecs);
};
