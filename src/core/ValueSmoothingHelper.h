#ifndef VALUESMOOTHINGHELPER_H
#define VALUESMOOTHINGHELPER_H

#include <QObject>
#include <QTimer>

class ValueSmoothingHelper: public QObject
{
    Q_OBJECT

public:
    ValueSmoothingHelper(QObject *parent = nullptr);

    inline float value(){return _value;}
    void setValue(float value);
    void setSlowness(int slowness);
private:
    float _value;
    float _nextValue;
    float _delta;

    QTimer _timer;
    int _counter;
    float _slowness;
private slots:
    void TryToChangeValue();

signals:
    void ValueChanged(float value);

};

#endif // VALUESMOOTHINGHELPER_H
