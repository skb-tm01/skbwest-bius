#include <QApplication>
#include <QDir>

#include "core/PathHelper.h"

QString PathHelper::datapath = "";

QString PathHelper::pathAppend(const QString& path1, const QString& path2)
{
    return QDir::cleanPath(path1 + QDir::separator() + path2);
}

QString PathHelper::GetDataDirectory()
{
    if(datapath != "")
        return datapath;

    auto dir = QDir(QApplication::applicationDirPath());


    while(!QDir(pathAppend(dir.path(),"/data/")).exists())
    {
       dir.cdUp();
    }

    datapath = pathAppend(dir.path(),"/data/");

    return datapath;
}

QString PathHelper::GetAppDirectory()
{
    return QApplication::applicationDirPath();
}
