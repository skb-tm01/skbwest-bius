#pragma once

#include <cstdint>

#include <QObject>
#include <QSettings>

#define VEHICLE_MODE_COUNT 3

#define SETTINGS_TERMINAL_VOLUME        "terminal/volume"
#define SETTINGS_TERMINAL_BRIGHTNESS    "terminal/brightness"
#define SETTINGS_TERMINAL_LANGUAGE      "terminal/language"
#define SETTINGS_TERMINAL_NIGHTANDDAY   "terminal/nightAndDayMode"
#define SETTINGS_TERMINAL_VOICEMESSAGES "terminal/voice"
#define SETTINGS_TERMINAL_NIGHTDAY_THRESHOLD   "terminal/nightDay_Threshold"
#define SETTINGS_PASSWORD_ALARM "password/alarm"
#define SETTINGS_PASSWORD_SERVICE "password/service"

enum VehicleMode:uint8_t
{
    mode_road = 0,
    mode_field = 1,
    mode_speed = 2
};


enum class EnumLanguge:int
{
    Eng = 0,
    Rus = 1,
    Chi = 2
};

enum class EnumNightDayMode:int
{
    Day = 0,
    Night = 1,
    Auto = 2
};

class SettingsManager : public QObject
{
    Q_OBJECT
public:
    explicit SettingsManager(QObject *parent = nullptr);
    static SettingsManager* settings();

    int brightness();
    void setBrightness(int brightness);

    bool voiceMessages();
    void setVoiceMessages(bool voiceMessages);

    EnumLanguge language();
    void setLanguage(const EnumLanguge &language);

    EnumNightDayMode nightDayMode();
    void setNightDayMode(const EnumNightDayMode &nightDayMode);

    void Deinitialize();

    int getVolume();
    void setVolume(int value);

    inline int nightDayThreshold(){ return _nightDayThreshold;}
    void setNightDayThreshold(int nightDayThreshold);

    EnumNightDayMode currentNightDayMode() const;
    QColor currentForeColor();
    QColor currentBackColor();
    QColor currentDisabledColor();

    int getAlarmPassword();
    int getServicePassword();
    int getAlarmStatsSpecialPassword();

signals:
    void NightDayChanged(EnumNightDayMode mode);

public slots:
    void ApplyIllumination(double illumination);

private:
    static SettingsManager* inst;
    void ApplyBrightness();
    void ApplyNightDayMode(EnumNightDayMode mode);
    void SyncConfig();

    QSettings* _settings;

    int _volume;
    int _brightness;
    bool _voiceMessages;
    EnumLanguge _language;
    EnumNightDayMode _nightDayMode;
    EnumNightDayMode _lastNightDayMode;
    int _nightDayThreshold;
    double _illumination;
};
