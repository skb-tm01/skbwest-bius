#pragma once

#include <atomic>
#include <QObject>

#include "core/ThreadSafeQueue.h"

class AudioWorker : public QObject
{
    Q_OBJECT
public:
    explicit AudioWorker(QObject *parent = 0);

    bool IsPlaying();

signals:
    void finished();
    void error(QString err);

public slots:
    void Run();
    void AddSound(QString str, bool whenEmpty = false);
    void setTerminationFlag();

private:
    ThreadSafeQueue<QString> sounds;

    std::atomic_bool _terminationRequested;

    std::atomic_bool _isPlaying;
};
