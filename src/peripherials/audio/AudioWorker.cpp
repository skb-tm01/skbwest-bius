#include "core/AudioHelper.h"
#include "core/SleepHelper.h"
#include "peripherials/audio/AudioWorker.h"

AudioWorker::AudioWorker(QObject *parent) : QObject(parent)
{
    _terminationRequested = false;
    _isPlaying = false;
}

bool AudioWorker::IsPlaying()
{
    return _isPlaying;
}

void AudioWorker::Run()
{
    while(!_terminationRequested)
    {
        if(!sounds.IsEmpty())
        {
            _isPlaying = true;
            AudioHelper::PlayWaveFile(sounds.Dequeue());
            _isPlaying = false;
        }
        else
        {
            SleepHelper::usleep(50*1000);
        }
    }

    emit finished();
}

void AudioWorker::AddSound(QString str, bool whenEmpty)
{
    if(whenEmpty && (!sounds.IsEmpty() || _isPlaying) )
        return;

    sounds.Enqueue(str);
}

void AudioWorker::setTerminationFlag()
{
    _terminationRequested = true;
}


