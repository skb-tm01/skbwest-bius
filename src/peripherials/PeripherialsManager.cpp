#include <QObject>

#include "3rdparty/jsoncpp/json.h"
#include "peripherials/PeripherialsManager.h"
#include "peripherials/can/CanWorker.h"
#include "peripherials/can/CanBackend_SocketCan.h"

void PeripherialsManager::CAN_Worker_Receiver_Initialize(ICanBackend* can_backend)
{
    can_worker_receiver_thread = new QThread();
    can_worker_receiver_thread->setObjectName("Peripherials/CAN/WorkerReceiver");

    can_worker_receiver_object = new CanWorker();
    can_worker_receiver_object->Init(can_backend);
    can_worker_receiver_object->moveToThread(can_worker_receiver_thread);

    //connect(can_worker, SIGNAL (error(QString)), this, SLOT (errorString(QString)));
    QObject::connect(can_worker_receiver_thread, SIGNAL (started()), can_worker_receiver_object, SLOT (Run()));
    QObject::connect(can_worker_receiver_object, SIGNAL (finished()), can_worker_receiver_thread, SLOT (quit()));
    QObject::connect(can_worker_receiver_object, SIGNAL (finished()), can_worker_receiver_object, SLOT (deleteLater()));
    QObject::connect(can_worker_receiver_thread, SIGNAL (finished()), can_worker_receiver_thread, SLOT (deleteLater()));

    can_worker_receiver_thread->start();
}

CanWorker* PeripherialsManager::CAN_Worker_Get()
{
    return can_worker_receiver_object;
}


void PeripherialsManager::GPIO_Keyboard_Initialize()
{
    _hardKeyb = new HardwareKeyboard();
    _hardKeyb->start();
}

HardwareKeyboard *PeripherialsManager::GPIO_Keyboard_Get()
{
    return _hardKeyb;
}

void PeripherialsManager::AudioWorker_Initialize()
{
    audioworker_thread = new QThread();
    audioworker_thread->setObjectName("Peripherials/Audio/AudioWorker");

    audioworker_object = new AudioWorker();
    audioworker_object->moveToThread(audioworker_thread);

    //connect(can_worker, SIGNAL (error(QString)), this, SLOT (errorString(QString)));
    QObject::connect(audioworker_thread, SIGNAL (started()), audioworker_object, SLOT (Run()));
    QObject::connect(audioworker_object, SIGNAL (finished()), audioworker_thread, SLOT (quit()));
    QObject::connect(audioworker_object, SIGNAL (finished()), audioworker_object, SLOT (deleteLater()));
    QObject::connect(audioworker_thread, SIGNAL (finished()), audioworker_thread, SLOT (deleteLater()));

    audioworker_thread->start();
}

AudioWorker *PeripherialsManager::AudioWorker_Get()
{
    return audioworker_object;
}

IlluminationMonitor *PeripherialsManager::illumMonitor_Get()
{
    return _illumMonitor;
}

void PeripherialsManager::illumMonitor_Init()
{
    _illumMonitor = new IlluminationMonitor();
}
