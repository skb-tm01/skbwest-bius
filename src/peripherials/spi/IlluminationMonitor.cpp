#include "IlluminationMonitor.h"

IlluminationMonitor::IlluminationMonitor(QObject *parent) : QThread(parent)
{
    _tickInterval = 200;
    _accumulateN = 25;
    _run = false;

#ifdef BUILD_DEVICE
    initSPI();
#endif
}

void IlluminationMonitor::stop()
{
    _run = false;
    wait(_tickInterval*4);
}

void IlluminationMonitor::run()
{
    _run = true;

    while (_run)
    {
        if(OnTick())
            break;

        msleep(_tickInterval);
    }
}

void IlluminationMonitor::initSPI()
{
#ifdef BUILD_DEVICE
    _spi0.init("/dev/spidev2.2", SPI_MODE_0, 10000);
    _spi0.setSizeWord(16);
    _spi0.setBuffers((unsigned char*)&tx,(unsigned char*)&rx);
    tx= 0x0000;
    rx = 0x0000;
#endif
}

void IlluminationMonitor::setTickInterval(unsigned int tickInterval)
{
    _tickInterval = tickInterval;
}

bool IlluminationMonitor::OnTick()
{
#ifdef BUILD_DEVICE

    tx = 0xf000;
    rx= 0x0000;
    _spi0.transfer(2);
    int brightness = rx&0x07ff;

    _queue.enqueue (brightness);

#endif

    if(_queue.size()>_accumulateN)
        _queue.dequeue();

    _brightness = 0;

    if(_queue.size()>0)
    {
        QQueue<int>::const_iterator i;
        for (i = _queue.constBegin(); i != _queue.constEnd(); ++i)
            _brightness += *i;

        _brightness/=_queue.size();

        emit BrightnessChanged(2048 - _brightness);
    }

    return false;
}
