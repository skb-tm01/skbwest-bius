#ifndef ILLUMINATIONMONITOR_H
#define ILLUMINATIONMONITOR_H

#include <QThread>
#include <QQueue>

#include "TransferSPI.h"

class IlluminationMonitor : public QThread
{
    Q_OBJECT

public:
    IlluminationMonitor(QObject *parent = nullptr);

    unsigned int getTickInterval() {return _tickInterval;}
    void setTickInterval(unsigned int tickInterval);

    inline double brightness() {return _brightness;}

protected:
    bool OnTick();
    void run();

private:
    volatile bool _run;
    unsigned int _tickInterval;

    double _brightness;



#ifdef BUILD_DEVICE
    transferSPI _spi0;
#endif
    int _accumulateN;
    QQueue<int> _queue;

    unsigned short tx;
    unsigned short rx;

    void initSPI();

public slots:
    void stop();

signals:
    void BrightnessChanged(double brightness);
};

#endif // ILLUMINATIONMONITOR_H
