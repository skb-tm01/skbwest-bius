#include "peripherials/spi/TransferSPI.h"

// haven't error.
#define NO_ERR 				0
// error open device.
#define ERR_SPI_OPEN 			1
// error set mode.
#define ERR_SPI_SET_MODE 		2
// error get mode.
#define ERR_SPI_GET_MODE 		3
// error set bits per word.
#define ERR_SPI_SET_BITS 		4
// error get bits per word.
#define ERR_SPI_GET_BITS 		5
// error set speed.
#define ERR_SPI_SET_SPEED 	6
// error get speed.
#define ERR_SPI_GET_SPEED 	7

#ifdef BUILD_DEVICE

transferSPI::transferSPI()
{
	m_fd = -1;
	m_tr.delay_usecs = 0;	// not use;
	m_tr.speed_hz = 0;		// default.
	m_tr.bits_per_word = 0; // default.
	m_tr.len = 0;			// set in transfer.
    m_tr.cs_change = 0;
}

transferSPI::~transferSPI()
{
    //if(m_fd > 0) close(m_fd);!!!!!!!!!!!!!!!!
}
//*****************************************************************
// inialization: dev - "/dev/spidev4.0"; mode - synchronization CPOL,CPHA;
int transferSPI::init(const char * dev, unsigned int mode, unsigned int speed_hz)
{
	int res = NO_ERR;
	unsigned int bits_per_word = 8;
	m_fd = open(dev, O_RDWR);
	if (m_fd < 0)
	{
		// can't open device.
		res = ERR_SPI_OPEN;
	}
	else
// Mode.
	if (ioctl(m_fd, SPI_IOC_WR_MODE, &mode) == -1)
	{
		// can't set spi mode.
		res = ERR_SPI_SET_MODE;
	}
	else
	if (ioctl(m_fd, SPI_IOC_RD_MODE, &mode) == -1)
	{
		// can't get spi mode.
		res = ERR_SPI_GET_MODE;
	}
	else
// Bit per word.
	if (ioctl(m_fd, SPI_IOC_WR_BITS_PER_WORD, &bits_per_word) == -1)
	{
		// can't set bits per word.
		res = ERR_SPI_SET_BITS;
	}
	else
	if (ioctl(m_fd, SPI_IOC_RD_BITS_PER_WORD, &bits_per_word) == -1)
	{
		// can't get bits per word.
		res = ERR_SPI_GET_BITS;
	}
	else
// Speed.
	if (ioctl(m_fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed_hz) == -1)
	{
		// can't set max speed hz.
		res = ERR_SPI_SET_SPEED;
	}
	else
	if (ioctl(m_fd, SPI_IOC_RD_MAX_SPEED_HZ, &speed_hz) == -1)
	{
		// can't get max speed hz.
		res = ERR_SPI_GET_SPEED;
	}
	return res;
}
//*****************************************************************
// bits per word, by one CS signal.
void transferSPI::setSizeWord(unsigned int bits_pw)
{
	m_tr.bits_per_word = bits_pw;
}
//*****************************************************************
// p_tx - send, p_rx - receive.
void transferSPI::setBuffers(unsigned char * p_tx, unsigned char * p_rx)
{
	m_tr.tx_buf = (unsigned long long)p_tx;
	m_tr.rx_buf = (unsigned long long)p_rx;
}
//*****************************************************************
// len - length in byte for transfer.
bool transferSPI::transfer(unsigned int len)
{
	bool res = true;
	m_tr.len = len;
	if( ioctl(m_fd, SPI_IOC_MESSAGE(1), &m_tr) < 1)
	{
		// error transfer spi.
		res = false;
	}
	return res;
}
#endif
