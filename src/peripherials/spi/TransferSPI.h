#pragma once

// for spi.
#include <iostream>
#include <fcntl.h>

#ifdef BUILD_DEVICE

#include <sys/ioctl.h>
#include <linux/spi/spidev.h>

class transferSPI
{
public:
	transferSPI();
	~transferSPI();

	int init(const char * dev, unsigned int mode, unsigned int speed_hz);
	void setSizeWord(unsigned int bits_pw);
	void setBuffers(unsigned char * p_tx, unsigned char * p_rx);
	bool transfer(unsigned int len);

private:
	int m_fd;
    struct spi_ioc_transfer m_tr;
};

#endif

