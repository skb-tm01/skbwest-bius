#include "HardwareButton.h"

#include<QWidget>
#include<QKeyEvent>
#include<QApplication>
#include<QDebug>

#include <core/AudioHelper.h>
#include <core/PathHelper.h>

HardwareButton::HardwareButton(int code)
{
    _keyCode = code;
    _text = QString();
    _pressed = false;
}

HardwareButton::HardwareButton(QChar symbol)
{
    _keyCode = symbol.unicode();
    _text = symbol;
    _pressed = false;
}

bool HardwareButton::pressed() const
{
    return _pressed;
}

QKeyEvent* HardwareButton::GenerateEvent(bool pressed)
{
    if(_pressed != pressed)
    {
        _pressed = pressed;
        if(_pressed)
            return new QKeyEvent(QEvent::KeyPress, _keyCode, Qt::NoModifier, _text);
        else
            return new QKeyEvent(QEvent::KeyRelease, _keyCode, Qt::NoModifier, _text);
    }

    return nullptr;
}
