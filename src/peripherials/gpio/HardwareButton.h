#include <QKeyEvent>
#include <QString>

class HardwareButton
{
public:
    explicit HardwareButton(QChar symbol);
    explicit HardwareButton(int code);
    explicit HardwareButton(){}

    inline int keyCode() const{return _keyCode;}
    inline QString text() const{return _text;}

    bool pressed() const;
    QKeyEvent* GenerateEvent(bool pressed);

private:
    int _keyCode;
    QString _text;
    bool _pressed;
};
