#ifndef GPIO_H
#define GPIO_H

#include <string>
using namespace std;
/* GPIO Class
 * Purpose: Each object instantiated from this class will control a GPIO pin
 * The GPIO pin number must be passed to the overloaded class constructor
 */

#define GPIO_0 "0"
#define GPIO_1 "1"

class GPIO
{
public:
    explicit GPIO();  // create a GPIO object that controls GPIO4 (default
    explicit GPIO(string x); // create a GPIO object that controls GPIOx, where x is passed to this constructor

    int exportGPIO(); // exports GPIO
    int unexportGPIO(); // unexport GPIO
    int setDir(string dir); // Set GPIO Direction
    int setVal(string val); // Set GPIO Value (putput pins)
    int getVal(string& val); // Get GPIO Value (input/ output pins)
    string getNum(); // return the GPIO number associated with the instance of an object
private:
    string gpionum; // GPIO number associated with the instance of an object
};

#endif // GPIO_H
