#include <fstream>
#include <string>
#include <iostream>
#include <sstream>
#include "gpio.h"

using namespace std;

GPIO::GPIO()
{
    this->gpionum = "4"; //GPIO4 is default
}

GPIO::GPIO(string gnum)
{
    this->gpionum = gnum;  //Instatiate GPIOClass object for GPIO pin number "gnum"
}

int GPIO::exportGPIO()
{
    string export_str = "/sys/class/gpio/export";
    ofstream exportgpio(export_str.c_str()); // Open "export" file. Convert C++ string to C string. Required for all Linux pathnames
    if (!exportgpio)
    {
        //cout << " OPERATION FAILED: Unable to export GPIO"<< this->gpionum <<" ."<< endl;
        return -1;
    }

    exportgpio << this->gpionum ; //write GPIO number to export
    exportgpio.close(); //close export file
    return 0;
}

int GPIO::unexportGPIO()
{
    string unexport_str = "/sys/class/gpio/unexport";
    ofstream unexportgpio(unexport_str.c_str()); //Open unexport file
    if (!unexportgpio)
    {
        //cout << " OPERATION FAILED: Unable to unexport GPIO"<< this->gpionum <<" ."<< endl;
        return -1;
    }

    unexportgpio << this->gpionum ; //write GPIO number to unexport
    unexportgpio.close(); //close unexport file
    return 0;
}

int GPIO::setDir(string dir)
{    
    string setdir_str ="/sys/class/gpio/gpio" + this->gpionum + "/direction";
    ofstream setdirgpio(setdir_str.c_str()); // open direction file for gpio
    if (!setdirgpio)
    {
        //cout << " OPERATION FAILED: Unable to set direction of GPIO"<< this->gpionum <<" ."<< endl;
        return -1;
    }

    setdirgpio << dir; //write direction to direction file
    setdirgpio.close(); // close direction file
    return 0;
}

int GPIO::setVal(string val)
{

    string setval_str = "/sys/class/gpio/gpio" + this->gpionum + "/value";
    ofstream setvalgpio(setval_str.c_str()); // open value file for gpio
    if (!setvalgpio)
    {
        //cout << " OPERATION FAILED: Unable to set the value of GPIO"<< this->gpionum <<" ."<< endl;
        return -1;
    }

    setvalgpio << val ;//write value to value file
    setvalgpio.close();// close value file
    return 0;
}

int GPIO::getVal(string& val)
{

    string getval_str = "/sys/class/gpio/gpio" + this->gpionum + "/value";
    ifstream getvalgpio(getval_str.c_str());// open value file for gpio
    if (!getvalgpio)
    {
        //cout << " OPERATION FAILED: Unable to get value of GPIO"<< this->gpionum <<" ."<< endl;
        return -1;
    }

    getvalgpio >> val ;  //read gpio value

    if(val != GPIO_0)
        val = GPIO_1;
    else
        val = GPIO_0;

    getvalgpio.close(); //close the value file
    return 0;
}

string GPIO::getNum()
{
    return gpionum;
}
