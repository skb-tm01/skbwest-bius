#ifndef HARDWAREKEYBOARD_H
#define HARDWAREKEYBOARD_H

#include <QThread>
#include <QVector>
#include "HardwareButton.h"
#include "gpio.h"

class HardwareKeyboard : public QThread
{
    Q_OBJECT

signals:
    void sig_emitKeyEvent(QKeyEvent* ev);

public:
    HardwareKeyboard(QObject *parent = nullptr);

    unsigned int getTickInterval() const;
    void setTickInterval(unsigned int tickInterval);

protected:
    bool OnTick();
    void run();

private:
    volatile bool _run;
    unsigned int _tickInterval;

    GPIO _gpioData;
    GPIO _gpioLoad;
    GPIO _gpioCLK;

    HardwareButton _F1;
    HardwareButton _F2;
    HardwareButton _F3;
    HardwareButton _F4;
    HardwareButton _F5;
    HardwareButton _F6;
    HardwareButton _F7;

    HardwareButton _Esc ;
    HardwareButton _Enter;
    HardwareButton _Menu ;

    HardwareButton _UpArrow ;
    HardwareButton _DownArrow ;
    HardwareButton _LeftArrow;
    HardwareButton _RightArrow;

    QVector<HardwareButton*> _buttons;

    void initGPIOs();
    void testGPIOs();

public slots:
    void stop();

signals:    


};

#endif // HARDWAREKEYBOARD_H
