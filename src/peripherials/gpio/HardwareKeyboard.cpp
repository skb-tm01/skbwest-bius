#include "HardwareKeyboard.h"

HardwareKeyboard::HardwareKeyboard(QObject *parent) : QThread(parent)
{
    _tickInterval = 50;
    _run = false;

     _gpioData = GPIO("135");
     _gpioLoad = GPIO("114");
     _gpioCLK = GPIO("115");

     initGPIOs();

     _F1 = HardwareButton(Qt::Key_F1);
     _F2 = HardwareButton(Qt::Key_F2);
     _F3 = HardwareButton(Qt::Key_F3);
     _F4 = HardwareButton(Qt::Key_F4);
     _F5 = HardwareButton(Qt::Key_F5);
     _F6 = HardwareButton(Qt::Key_F6);
     _F7 = HardwareButton(Qt::Key_F7);

     _Esc = HardwareButton(Qt::Key_Escape);
     _Enter = HardwareButton(Qt::Key_Enter);
     _Menu = HardwareButton(Qt::Key_Menu);

     _UpArrow = HardwareButton(Qt::Key_Up);
     _DownArrow = HardwareButton(Qt::Key_Down);
     _LeftArrow = HardwareButton(Qt::Key_Left);
     _RightArrow = HardwareButton(Qt::Key_Right);


    _buttons.append(&_F7);
    _buttons.append(NULL);
    _buttons.append(&_F5);
    _buttons.append(&_F6);
    _buttons.append(&_F1);
    _buttons.append(&_F2);
    _buttons.append(&_F3);
    _buttons.append(&_F4);

    _buttons.append(&_Menu);
    _buttons.append(NULL);
    _buttons.append(&_DownArrow);
    _buttons.append(&_Enter);
    _buttons.append(&_Esc);
    _buttons.append(&_UpArrow);
    _buttons.append(&_LeftArrow);
    _buttons.append(&_RightArrow);
}

void HardwareKeyboard::stop()
{
  _run = false;
  wait(_tickInterval*4);
}

void HardwareKeyboard::run()
{
  _run = true;

  while (_run)
  {
    if(OnTick())
      break;

    msleep(_tickInterval);
  }
}

unsigned int HardwareKeyboard::getTickInterval() const
{
    return _tickInterval;
}

void HardwareKeyboard::setTickInterval(unsigned int tickInterval)
{
    _tickInterval = tickInterval;
}

bool HardwareKeyboard::OnTick()
{
    testGPIOs();
    return false;
}

void HardwareKeyboard::initGPIOs()
{
    _gpioData.exportGPIO();
    _gpioLoad.exportGPIO();
    _gpioCLK.exportGPIO();

    _gpioData.setDir("in");
    _gpioLoad.setDir("out");
    _gpioCLK.setDir("out");
}

void HardwareKeyboard::testGPIOs()
{
    _gpioLoad.setVal(GPIO_0);
    _gpioLoad.setVal(GPIO_1);

    string val;
    for (int i = 0; i < _buttons.size(); ++i)
    {
        _gpioData.getVal(val);
        _gpioCLK.setVal(GPIO_1);
        _gpioCLK.setVal(GPIO_0);

        if(_buttons[i]!=nullptr)
        {
            QKeyEvent* ev = _buttons[i]->GenerateEvent(val.compare(GPIO_1) != 0);
            if(ev!=nullptr)
                emit sig_emitKeyEvent(ev);
        }
    }

    _gpioLoad.setVal(GPIO_0);
}

