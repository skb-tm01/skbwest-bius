#pragma once

#include <cstdint>

enum SensorType:uint8_t
{
    DIG = 0x00,
    ADS = 0x40,
    RPM = 0x80,
    OUT = 0xC0,
    OTH = 0xF0,
    UNK = 0xFF
};

extern SensorType RealSensorsTypes[4];
