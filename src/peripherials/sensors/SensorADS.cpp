#include "core/ByteConversion.h"
#include "peripherials/sensors/SensorADS.h"
#include "peripherials/can/packets/ContactState.h"
#include "peripherials/can/packets/Control.h"

SensorADS::SensorADS(QObject *parent) : SensorBase (parent)
{
    _sensorType = SensorType::ADS;
}

void SensorADS::ConnectMainWindow(MainWindow *mainWindow)
{
    SensorBase::ConnectMainWindow(mainWindow);

    if(this->GetId()=="power_failure")
        QObject::connect(this,SIGNAL(onAlertChange(bool)),mainWindow->GetStatusPanel(),SLOT(highlightBattery(bool)));
}
