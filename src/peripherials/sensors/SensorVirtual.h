#pragma once

#include <QObject>
#include <QTimer>

#include "peripherials/sensors/SensorBase.h"

class SensorVirtual : public SensorBase
{
    Q_OBJECT

public:
    explicit SensorVirtual(QObject *parent = nullptr);

    void Init();

private slots:
    void DisplayMsg();

private:
    QTimer* timer;
};
