#pragma once

#include "peripherials/sensors/SensorBase.h"

class SensorADS: public SensorBase
{
    Q_OBJECT

public:
    SensorADS(QObject *parent = nullptr);

    void ConnectMainWindow(MainWindow* mainWindow);
};
