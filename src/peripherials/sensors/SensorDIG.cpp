#include <QDebug>
#include <QString>

#include "3rdparty/jsoncpp/json.h"
#include "core/ByteConversion.h"
#include "peripherials/can/packets/ContactState.h"
#include "peripherials/can/packets/Control.h"
#include "peripherials/sensors/SensorDIG.h"

SensorDIG::SensorDIG(QObject *parent) : SensorBase(parent)
{
    _sensorType = SensorType::DIG;
}

void SensorDIG::ConnectMainWindow(MainWindow *mainWindow)
{
    SensorBase::ConnectMainWindow(mainWindow);

    if(this->GetId()=="hand_brake")
        QObject::connect(this,SIGNAL(onAlertChange(bool)),mainWindow->GetStatusPanel(),SLOT(highlightParkingBreaksIcon(bool)));
    else if(this->GetId()=="high_beam")
        QObject::connect(this,SIGNAL(onAlertChange(bool)),mainWindow->GetStatusPanel(),SLOT(highlightHighbeamIcon(bool)));
}
