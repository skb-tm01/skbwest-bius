#pragma once

#include <QMap>

#include "core/Singleton.h"
#include "peripherials/sensors/ISensor.h"
#include "widgets/mainwindow.h"

class SensorManager : public Singleton<SensorManager>
{
public:
    void InitSensors(MainWindow* w);
    void RequestSensorsData(bool onlyUninitialized = false);

    QList<ISensor*> GetSensors();
    QList<ISensor*> GetSensors(SensorType type);
    ISensor* GetSensor(QString id);

private:
    void InitializeSensorsFromConfig();
    QMap<QString, ISensor*> _sensors;
    MainWindow* _mainWindow;
};
