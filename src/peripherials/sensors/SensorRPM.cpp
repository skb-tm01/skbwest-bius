#include "peripherials/sensors/SensorRPM.h"
#include "peripherials/can/packets/Control.h"
#include "peripherials/can/packets/ContactState.h"

SensorRPM::SensorRPM(QObject *parent) : SensorBase (parent)
{
    _sensorType = SensorType::RPM;
}
