#include <QDebug>

#include "alerts/AlertsManager.h"
#include "peripherials/sensors/SensorBase.h"
#include "widgets/warningmessage.h"
#include "peripherials/can/packets/Control.h"
#include "peripherials/can/packets/ContactState.h"
#include "peripherials/can/packets/Request3.h"
#include "peripherials/can/packets/Taho.h"
#include "peripherials/can/packets/IPSState.h"

SensorBase::SensorBase(QObject *parent) : QObject(parent)
{
    channel_id = 0;
    _isSetControlEnabled = true;
    _alerted = false;
    _underControl = true;
    _broken = false;
    _sensorType = SensorType::UNK;
    _initialized = false;
    milling_rpm = 0;
}

SensorBase::~SensorBase()
{

}

void SensorBase::Init()
{

}

void SensorBase::ConnectMainWindow(MainWindow *)
{

}

void SensorBase::ParseJson(Json::Value json)
{
    id     = QString::fromUtf8(json["id"].asString().c_str());
    channel_id = static_cast<uint8_t>(json["channel_id"].asInt());
    icon_name = QString::fromUtf8(json["icon_name"].asString().c_str());
    can_data_and = false;

    if(json["control_set_enabled"].empty())
        _isSetControlEnabled = true;
    else
        _isSetControlEnabled = json["control_set_enabled"].asBool();

    //name
    Json::Value j_name = json["name"];
    std::vector<std::string> j_name_keys = j_name.getMemberNames();
    for (size_t i = 0; i<j_name_keys.size(); i++)
    {
        _name.insert(j_name_keys[i].c_str(),QString::fromUtf8(j_name[j_name_keys[i]].asString().c_str()));
    }


    //manual_title
    Json::Value j_manual = json["manual"];

    Json::Value j_manualtitle = j_manual["title"];
    std::vector<std::string> j_manualtitle_keys = j_manualtitle.getMemberNames();
    for (size_t i = 0; i<j_manualtitle_keys.size(); i++)
    {
        manual_title.insert(j_manualtitle_keys[i].c_str(),QString::fromUtf8(j_manualtitle[j_manualtitle_keys[i]].asString().c_str()));
    }


    //manual_description
    Json::Value j_manualdescr = j_manual["description"];
    std::vector<std::string> j_manualdescr_keys = j_manualdescr.getMemberNames();
    for (size_t i = 0; i<j_manualdescr_keys.size(); i++)
    {
        manual_description.insert(j_manualdescr_keys[i].c_str(),QString::fromUtf8(j_manualdescr[j_manualdescr_keys[i]].asString().c_str()));
    }

    alert_descr= AlertDescription(json["alert"]);
    alert_descr.SetId(id);
    alert_descr.SetImage(icon_name);

    //can_data
    if(!json["can_data"].empty())
    {
        responses.emplace_back(json["can_data"]["response_id"].asInt(), json["can_data"]["bit"].asInt());
        statuses.push_back(false);
    }

    if(!json["can_data_2"].empty())
    {
        responses.emplace_back(json["can_data_2"]["response_id"].asInt(), json["can_data_2"]["bit"].asInt());
        statuses.push_back(false);
    }

    if(!json["can_data_and"].empty())
        can_data_and = json["can_data_and"].asBool();

    //TODO: extend config and unhardcode this
    if(this->GetId()=="high_grain_loss")
    {
        statuses.push_back(false);
        statuses.push_back(false);
    }
    else if(this->GetId()=="engine_rpm")
    {
        statuses.push_back(false);
    }
}



QString SensorBase::GetId()
{
    return id;
}

uint8_t SensorBase::GetChannelId()
{
    return channel_id;
}

SensorType SensorBase::GetType()
{
    return _sensorType;
}

QString SensorBase::GetName(QString language)
{
    if(!_name.contains(language))
        return "";

    return _name[language];
}

QString SensorBase::GetIconName()
{
    return icon_name;
}

AlertDescription &SensorBase::GetAlertDescription()
{
    return alert_descr;
}

bool SensorBase::IsUnderControl()
{
    return _underControl;
}

bool SensorBase::IsSetControlEnabled()
{
    return _isSetControlEnabled;
}

bool SensorBase::IsBroken()
{
    return _broken;
}

bool SensorBase::IsInitialized()
{
    return _initialized;
}

void SensorBase::SetUnderControl(bool isUnderControl)
{
    uint8_t result = static_cast<uint8_t>((!isUnderControl) | (IsBroken() << 1));
    CanPackets::ControlSet::SendRequest(this, result);
}

bool SensorBase::IsErrorOccured()
{
    return _alerted;
}

void SensorBase::RequestState()
{
    if(_sensorType==SensorType::UNK || _sensorType==SensorType::OTH)
        return;
    CanPackets::ControlGet::SendRequest(this);
}

QString SensorBase::GetManualTitle(QString language)
{
    if(!manual_title.contains(language))
        return "";

    return manual_title[language];
}

QString SensorBase::GetManualDescription(QString language)
{
    if(!manual_description.contains(language))
        return "";

    return manual_description[language];
}

int SensorBase::GetCanBit()
{
    if(responses.empty())
        return -1;

    return responses.front().second;
}

void SensorBase::AddAlert()
{
    /*if(this->id == "straw_separator_axle_rpm") {
        qDebug()<<"SensorBase::AddAlert()";
    }*/

    if(!IsUnderControl()) {
        return;
    }

    auto* alertworker = AlertsManager::instance().Alerts_Worker_Get();
    if(alertworker!=nullptr) {
        if(_alerted == false) {
            alertworker->AddCenterAlert(&alert_descr);
        }

        alertworker->AddTopAlert(&alert_descr);
    }
    
    _alerted = true;
    emit onAlertChange(true);
    emit stateChanged();
}

void SensorBase::RemoveAlert()
{
    auto* alertworker = AlertsManager::instance().Alerts_Worker_Get();
    if(alertworker!=nullptr)
    {
        alertworker->RemoveAlert(this->id);
    }

    if(_alerted)
    {
        emit onAlertChange(false);
        emit stateChanged();
    }
    _alerted = false;

}

void SensorBase::OnCanReceived(CanMessage msg)
{
    if(_sensorType == SensorType::UNK)
        return;

    if(this->GetId()=="engine_rpm")
    {
        OnCanReceived_SPECIAL_EngineRPM(msg);
        if(msg.can_id == CanPackets::ControlGet::response_id)
            OnCanReceived_ControlGet(msg.data);
        return;
    }
    else if(this->GetId()=="high_grain_loss")
    {
        OnCanReceived_SPECIAL_HighGrainLoss(msg);
        return;
    }

    if(msg.can_id == CanPackets::ControlGet::response_id)
        OnCanReceived_ControlGet(msg.data);
    else if(msg.can_id == CanPackets::ControlSet::response_id)
        OnCanReceived_ControlSet(msg.data);
    else if(msg.can_id == CanPacketContactStateResponse::packet_id)
        OnCanReceived_ContactState(msg.data);
    else if(msg.can_id == CanPackets::Request3::response_id)
        OnCanReceived_Request3(msg.data);
    else if(msg.can_id == CanPacketTahoResponse::packet_id)
        OnCanReceived_Taho(msg.data);
    else if(msg.can_id == CanPackets::IPSState::response_id)
        OnCanReceived_IPSState(msg.data);

    ProcessAlert();
}

void SensorBase::ProcessAlert()
{
    if(statuses.size()==0)
        return;

    bool at_least_one = false;
    bool all_true = true;

    for(auto status: statuses)
    {
        if(status)
        {
            at_least_one=true;
        }
        else
        {
            all_true = false;
        }
    }

    if(can_data_and)
    {
        if(all_true)
            AddAlert();
        else
            RemoveAlert();
    }

    if(!can_data_and)
    {
        if(at_least_one || all_true) {
            AddAlert();
        } else {
            RemoveAlert();
        }
    }

    /*if(this->id == "straw_separator_axle_rpm") {
        qDebug()<<"SensorBase::ProcessAlert";
        qDebug()<<"    at_least_one:" << at_least_one;
        qDebug()<<"    all_true:" << all_true;
        qDebug()<<"    can_data_and:" << can_data_and;
    }*/
}

void SensorBase::OnConnectionLost()
{
    RemoveAlert();

    if(_sensorType!=SensorType::UNK)
    {
        _alerted = false;
        emit stateChanged();
    }

}

void SensorBase::OnCanReceived_ControlGet(uint8_t *data)
{
    auto packet = CanPackets::ControlGet::ParseResponse(data);

    if(packet.type != this->GetType() || packet.id != this->GetChannelId())
        return;

    bool _underControl_new = packet.IsUnderControl();
    bool _broken_new = packet.IsBroken();
    bool _initialized_new = true;

    bool changed = _underControl_new!=_underControl || _broken!=_broken_new || _initialized!=_initialized_new;

    _underControl = _underControl_new;
    _broken = _broken_new;
    _initialized = true;

    if(!_underControl)
        RemoveAlert();
    else if(_broken)
        AddAlert();
    else if(changed)
        stateChanged();

}

void SensorBase::OnCanReceived_ControlSet(uint8_t *data)
{
    auto packet = CanPackets::ControlSet::ParseResponse(data);

    if(packet.id == this->GetChannelId() && packet.type == this->GetType())
    {
        RequestState();
    }
}

void SensorBase::OnCanReceived_ContactState(uint8_t *data)
{
    for(size_t i=0;i<responses.size();i++)
    {
        if(responses[i].first != CanPacketContactStateResponse::packet_id)
            continue;

        auto packet = CanPacketContactStateResponse::Parse(data);  
        statuses[i] = ByteConversion::GetBitState(packet.raw_data, responses[i].second);

        /*
        if(this->id == "straw_separator_axle_rpm") {
            qDebug()<<"SensorBase::OnCanReceived_ContactState";
            qDebug()<<"    Target Bit" << responses[i].second;
            qDebug()<<"    Result:" << statuses[i];
        }*/
    }
}

void SensorBase::OnCanReceived_Request3(uint8_t *data)
{
    for(size_t i=0;i<responses.size();i++)
    {
        if(responses[i].first != CanPackets::Request3::response_id)
            continue;

        auto packet = CanPackets::Request3::ParseResponse(data);
        statuses[i] = ByteConversion::GetBitState(packet.raw_data, responses[i].second);
    }
}

void SensorBase::OnCanReceived_Taho(uint8_t *data)
{
    for(size_t i=0;i<responses.size();i++)
    {
        if(responses[i].first != CanPacketTahoResponse::packet_id)
            continue;

        auto packet = CanPacketTahoResponse::Parse(data);
        statuses[i] = ByteConversion::GetBitState(packet.raw_data, responses[i].second);
    }
}

void SensorBase::OnCanReceived_IPSState(uint8_t *data)
{
    for(size_t i=0;i<responses.size();i++)
    {
        if(responses[i].first !=CanPackets::IPSState::response_id)
            continue;

        auto packet = CanPackets::IPSState::ParseResponse(data);
        statuses[i] = ByteConversion::GetBitState(packet.raw_data, responses[i].second);
    }
}

void SensorBase::OnCanReceived_SPECIAL_EngineRPM(CanMessage msg)
{

    if(msg.can_id != CanPacketTahoResponse::packet_id)
        return;

    auto packet = CanPacketTahoResponse::Parse(msg.data);

    if(packet.type == CanPacketTahoType::taho_speed_threshing)
    {
        milling_rpm = packet.rpm;
        return;
    }
    else if(packet.type == CanPacketTahoType::taho_speed_engine)
    {
        if(packet.rpm == 0 && milling_rpm != 0)
            statuses[0]=true;
        else
            statuses[0]=false;

        ProcessAlert();
    }
}

void SensorBase::OnCanReceived_SPECIAL_HighGrainLoss(CanMessage msg)
{
    if(msg.can_id != CanPacketTahoResponse::packet_id)
        return;

    auto packet = CanPacketTahoResponse::Parse(msg.data);

    if(packet.type == CanPacketTahoType::taho_loss_left)
    {
        if(packet.rpm > 70)
            statuses[0]=true;
        else
            statuses[0]=false;
        ProcessAlert();
    }
    else if(packet.type == CanPacketTahoType::taho_loss_right)
    {
        if(packet.rpm > 70)
            statuses[1]=true;
        else
            statuses[1]=false;
        ProcessAlert();
    }
}


void SensorBase::SetErrorOccurred(bool state)
{
    _alerted=state;
}

void SensorBase::SetBroken(bool state)
{
    _broken = state;
}
