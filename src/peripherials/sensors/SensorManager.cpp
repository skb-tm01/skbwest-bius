#include <fstream>

#include <QCoreApplication>
#include <QDebug>
#include <QDir>
#include <QFileInfo>

#include "3rdparty/jsonutils/jsonutils.h"

#include "core/PathHelper.h"

#include "peripherials/PeripherialsManager.h"
#include "peripherials/sensors/SensorBase.h"
#include "peripherials/sensors/SensorManager.h"

#include "peripherials/sensors/SensorADS.h"
#include "peripherials/sensors/SensorDIG.h"
#include "peripherials/sensors/SensorOUT.h"
#include "peripherials/sensors/SensorRPM.h"
#include "peripherials/sensors/SensorOther.h"
#include "peripherials/sensors/SensorVirtual.h"

void SensorManager::InitializeSensorsFromConfig()
{
    auto dir = QDir(PathHelper::pathAppend(PathHelper::GetDataDirectory(),"sensors/"));
    dir.setFilter(QDir::Files | QDir::NoDotAndDotDot | QDir::NoSymLinks);
    QStringList nameFilter("*.json");
    auto fileList = dir.entryList(nameFilter);
    auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();

    for (int i=0; i<fileList.count(); i++)
    {
        std::ifstream t(dir.absoluteFilePath(fileList.at(i)).toStdString());
        std::string str((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());

        auto json = JsonUtils::ParseString(str);
        auto sensor_type = QString::fromStdString(json["type"].asString()).toLower();

        SensorBase* sens = nullptr;
        if(sensor_type=="ads")
            sens = new SensorADS();
        else if(sensor_type=="dig")
            sens = new SensorDIG();
        else if(sensor_type=="out")
            sens = new SensorOUT();
        else if(sensor_type=="rpm")
            sens = new SensorRPM();
        else if(sensor_type=="other")
            sens = new SensorOther();
        else if(sensor_type=="virtual")
            sens = new SensorVirtual();

        if (sens == nullptr)
            continue;

        sens->ParseJson(json);
        sens->Init();

        if(!sens->GetId().isEmpty())
        {
            _sensors.insert(sens->GetId(),sens);

            //connect to can
            if(canworker != nullptr)
            {
                QObject::connect(canworker, SIGNAL(received(CanMessage)),sens,SLOT(OnCanReceived(CanMessage)));
                QObject::connect(canworker, SIGNAL(connectionLost()),sens,SLOT(OnConnectionLost()));
            }
            //connect to mainWindow
            sens->ConnectMainWindow(_mainWindow);
        }
    }
}

void SensorManager::InitSensors(MainWindow* w)
{
    _mainWindow = w;

    InitializeSensorsFromConfig();
}

void SensorManager::RequestSensorsData(bool onlyUninitialized)
{
    for(auto* sensor: _sensors)
    {
        if(sensor->IsInitialized() && onlyUninitialized)
            continue;

        sensor->RequestState();
    }
}

QList<ISensor*> SensorManager::GetSensors()
{
    return _sensors.values();
}

QList<ISensor *> SensorManager::GetSensors(SensorType type)
{
    QList<ISensor*> _sens;
    for(auto* sensor: _sensors)
    {
        if(sensor->GetType() == type)
            _sens.append(sensor);
    }
    return _sens;
}

ISensor *SensorManager::GetSensor(QString id)
{
    if(!_sensors.contains(id))
        return nullptr;

    return _sensors[id];
}
