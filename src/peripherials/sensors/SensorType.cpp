#include "peripherials/sensors/SensorType.h"

SensorType RealSensorsTypes[4] = {
    SensorType::DIG,
    SensorType::ADS,
    SensorType::RPM,
    SensorType::OUT
};

