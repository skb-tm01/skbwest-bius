#include "peripherials/sensors/SensorOUT.h"
#include "peripherials/can/packets/Control.h"
#include "peripherials/can/packets/IPSState.h"

SensorOUT::SensorOUT(QObject *parent) : SensorBase (parent)
{
    _sensorType = SensorType::OUT;
}

