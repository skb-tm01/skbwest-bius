#include <QDebug>
#include <QString>

#include "3rdparty/jsoncpp/json.h"
#include "core/ByteConversion.h"
#include "peripherials/can/packets/ContactState.h"
#include "peripherials/can/packets/Control.h"
#include "peripherials/sensors/SensorOther.h"

SensorOther::SensorOther(QObject *parent) : SensorBase(parent)
{
    _sensorType = SensorType::OTH;

    _initialized = true;
    _broken = false;
    _underControl = true;
}
