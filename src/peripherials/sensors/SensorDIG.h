#pragma once

#include "peripherials/can/CanMessage.h"
#include "peripherials/sensors/SensorBase.h"

class SensorDIG : public SensorBase
{
    Q_OBJECT

public:
    SensorDIG(QObject *parent = nullptr);

    void ConnectMainWindow(MainWindow* mainWindow);
};
