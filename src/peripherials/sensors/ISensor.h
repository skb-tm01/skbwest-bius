#pragma once

#include <QtPlugin>
#include <QString>

#include "alerts/AlertDescription.h"
#include "peripherials/can/CanMessage.h"
#include "peripherials/sensors/SensorType.h"

class ISensor
{
public:
    //JSON
    virtual QString GetId()         = 0;
    virtual uint8_t GetChannelId()    = 0;

    virtual QString GetName(QString language) = 0;

    virtual SensorType GetType()    = 0;

    virtual QString GetIconName()   = 0;

    virtual AlertDescription& GetAlertDescription() = 0;

    virtual QString GetManualTitle(QString language) = 0;
    virtual QString GetManualDescription(QString language) =0;

    //
    virtual bool IsUnderControl()   = 0;
    virtual bool IsInitialized() = 0;
    virtual bool IsErrorOccured()   = 0;
    virtual bool IsBroken() = 0;
    virtual bool IsSetControlEnabled() = 0;

    virtual void RequestState() = 0;
    virtual void SetUnderControl(bool isUnderControl)  = 0;

    virtual int GetCanBit() = 0;



signals:
   virtual void stateChanged() = 0;

public slots:
    virtual void AddAlert() = 0;
    virtual void RemoveAlert() = 0;
    virtual void OnConnectionLost() = 0;
};

Q_DECLARE_INTERFACE(ISensor, "SKBWEST-BIUS.ISensor/1.0")
