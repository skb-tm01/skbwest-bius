#pragma once

#include "peripherials/can/CanMessage.h"
#include "peripherials/sensors/SensorBase.h"

class SensorOther : public SensorBase
{
    Q_OBJECT

public:
    SensorOther(QObject *parent = nullptr);
};
