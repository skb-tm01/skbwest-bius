#include "alerts/AlertsManager.h"
#include "peripherials/PeripherialsManager.h"
#include "peripherials/sensors/SensorVirtual.h"

SensorVirtual::SensorVirtual(QObject *parent) : SensorBase (parent)
{
    _initialized = true;
    _broken = false;
    _underControl = true;
}

void SensorVirtual::Init()
{
    SensorBase::Init();

    if(this->GetId()=="can_connection")
    {
        auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
        if(canworker != nullptr)
        {
            timer = new QTimer();
            timer->setInterval(15000);

            QObject::connect(canworker,SIGNAL(connectionLost()),timer,SLOT(start()));
            QObject::connect(canworker,SIGNAL(connectionRestored()),timer,SLOT(stop()));

            QObject::connect(canworker,SIGNAL(connectionLost()),this,SLOT(AddAlert()));
            QObject::connect(canworker,SIGNAL(connectionRestored()),this,SLOT(RemoveAlert()));

            if(!canworker->IsConnected())
            {
                canworker->ForceConnectionLost();
            }
        }
    }
}

void SensorVirtual::DisplayMsg()
{
    auto* alertworker = AlertsManager::instance().Alerts_Worker_Get();
    if(alertworker!=nullptr)
    {
        alertworker->AddCenterAlert(&GetAlertDescription());
    }
}
