#pragma once

#include <QMap>
#include <QObject>
#include <QString>

#include "3rdparty/jsoncpp/json.h"

#include "alerts/AlertDescription.h"

#include "peripherials/sensors/ISensor.h"
#include "peripherials/sensors/SensorType.h"

#include "widgets/mainwindow.h"

class SensorBase : public QObject, public ISensor
{
    Q_OBJECT
    Q_INTERFACES(ISensor)

public:
    explicit SensorBase(QObject *parent = nullptr);
    ~SensorBase();

    virtual void Init();
    virtual void ConnectMainWindow(MainWindow* mainWindow);
    virtual void ParseJson(Json::Value json);

    QString GetId();
    uint8_t GetChannelId();
    SensorType GetType();
    QString GetName(QString language);
    QString GetIconName();
    AlertDescription& GetAlertDescription();

    bool IsErrorOccured();
    bool IsBroken();
    bool IsInitialized();
    bool IsUnderControl();
    bool IsSetControlEnabled();

    void RequestState();    
    void SetUnderControl(bool isUnderControl);


    QString GetManualTitle(QString language);
    QString GetManualDescription(QString language);


    int GetCanBit();

public slots:
    void AddAlert();
    void RemoveAlert();
    void OnConnectionLost();
    virtual void OnCanReceived(CanMessage msg);

signals:
    void onAlertChange(bool _alerted);

private:
    QString id;
    QString icon_name;
    AlertDescription alert_descr;

    QMap<QString,QString> manual_title;
    QMap<QString,QString> manual_description;
    QMap<QString,QString> _name;

    void ProcessAlert();

signals:
    void stateChanged();

protected:
    void OnCanReceived_ControlGet(uint8_t *data);
    void OnCanReceived_ControlSet(uint8_t *data);
    void OnCanReceived_ContactState(uint8_t *data);
    void OnCanReceived_Request3(uint8_t *data);
    void OnCanReceived_Taho(uint8_t *data);
    void OnCanReceived_IPSState(uint8_t *data);

    //special
    void OnCanReceived_SPECIAL_EngineRPM(CanMessage msg);
    void OnCanReceived_SPECIAL_HighGrainLoss(CanMessage msg);


    void SetErrorOccurred(bool state);
    void SetBroken(bool state);

protected:
    uint8_t channel_id;

    bool _underControl;
    bool _alerted;
    bool _broken;
    bool _initialized;
    bool _isSetControlEnabled;
    SensorType _sensorType;


    std::vector<std::pair<int32_t,int32_t>> responses;
    std::vector<bool> statuses;
    bool can_data_and;

    uint16_t milling_rpm;

};
