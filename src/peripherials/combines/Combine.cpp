#include "Combine.h"

Combine::Combine(uint8_t CANindex, CombineModel model, QString name, CombineStrawEngineType engineType, TempScaleToDisplay tempScale)
{
    _CANindex = CANindex;
    _model = model;
    _name = name;
    _engineType = engineType;
    _tempScale = tempScale;
}
