#pragma once
#include <QString>
#include <QList>
#include "Combine.h"

class CombineKeeper
{
public:
    static CombineKeeper &GetInstance()
    {
       static CombineKeeper instance;
       return instance;
    }
    QList<Combine*> GetCombineList();

    void setCurrentCombine(int CANindex);
    Combine* getCurrentCombine();
    int GetCurrentIndexInList();

    int GetIndexInList(int CANindex);
    int GetCANIndex(int indexInList);
private:
    int currentCombineCANIndex;

    CombineKeeper();
    QList<Combine*> _combines;

    QString GetCombineModelName(int index);
    Combine* GetCombine(QString name);
    Combine* GetCombine(int CANindex);
};




