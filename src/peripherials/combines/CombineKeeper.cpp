#include "CombineKeeper.h"

CombineKeeper::CombineKeeper()
{
    _combines.append(new Combine(0,
                             CombineModel::combine_gs07,
                             "GS-07",
                             CombineStrawEngineType::strawEngine_shaker,
                             TempScaleToDisplay::scale_hydraulicOil
                             ));

    _combines.append(new Combine(1,
                             CombineModel::combine_gs10,
                             "GS-10",
                             CombineStrawEngineType::strawEngine_shaker,
                             TempScaleToDisplay::scale_hydraulicOil
                             ));

    _combines.append(new Combine(2,
                             CombineModel::combine_gs12,
                             "GS-12",
                             CombineStrawEngineType::strawEngine_shaker,
                             TempScaleToDisplay::scale_hydraulicOil
                             ));

    _combines.append(new Combine(3,
                             CombineModel::combine_gs812,
                             "GS-812",
                             CombineStrawEngineType::strawEngine_shaker,
                             TempScaleToDisplay::scale_hydraulicOil
                             ));

    _combines.append(new Combine(4,
                             CombineModel::combine_gs1624,
                             "GS-1624",
                             CombineStrawEngineType::strawEngine_shaker,
                             TempScaleToDisplay::scale_hydraulicOil
                             ));

    _combines.append(new Combine(5,
                             CombineModel::combine_test,
                             "Test",
                             CombineStrawEngineType::strawEngine_shaker,
                             TempScaleToDisplay::scale_hydraulicOil
                             ));

    //6-unknown

    _combines.append(new Combine(7,
                             CombineModel::combine_gs10_2011,
                             "GS-10 2011",
                             CombineStrawEngineType::strawEngine_shaker,
                             TempScaleToDisplay::scale_hydraulicOil
                             ));

    _combines.append(new Combine(8,
                             CombineModel::combine_gs12_2011,
                             "GS-12 2011",
                             CombineStrawEngineType::strawEngine_shaker,
                             TempScaleToDisplay::scale_hydraulicOil
                             ));

    _combines.append(new Combine(9,
                             CombineModel::combine_gs812_2011,
                             "GS-812 2011",
                             CombineStrawEngineType::strawEngine_shaker,
                             TempScaleToDisplay::scale_hydraulicOil
                             ));


    _combines.append(new Combine(10,
                             CombineModel::combine_gs812c_2013_tracks,
                             "GS-812c 2013 Tracks",
                             CombineStrawEngineType::strawEngine_shaker,
                             TempScaleToDisplay::scale_hydraulicOil
                             ));


    _combines.append(new Combine(11,
                             CombineModel::combine_gs812_2013_cop,
                             "GS-812 2013 Stacker",
                             CombineStrawEngineType::strawEngine_shaker,
                             TempScaleToDisplay::scale_hydraulicOil
                             ));

    _combines.append(new Combine(12,
                             CombineModel::combine_gs812_2013_stage_cop,
                             "GS-812 2013 STG Stacker",
                             CombineStrawEngineType::strawEngine_shaker,
                             TempScaleToDisplay::scale_hydraulicOil
                             ));


    _combines.append(new Combine(13,
                             CombineModel::combine_gs812_stage,
                             "GS-812 STG",
                             CombineStrawEngineType::strawEngine_shaker,
                             TempScaleToDisplay::scale_hydraulicOil
                             ));


    _combines.append(new Combine(14,
                             CombineModel::combine_gs12_2011_cop,
                             "GS-12 2011 Stacker",
                             CombineStrawEngineType::strawEngine_shaker,
                             TempScaleToDisplay::scale_hydraulicOil
                             ));

    _combines.append(new Combine(15, //0x0F
                             CombineModel::combine_gs12_cummins,
                             "GS-12 Cummins",
                             CombineStrawEngineType::strawEngine_shaker,
                             TempScaleToDisplay::scale_hydraulicOil
                             ));


    _combines.append(new Combine(16, //0x10
                             CombineModel::combine_gs4118,
                             "GS-4118",
                             CombineStrawEngineType::strawEngine_shaker,
                             TempScaleToDisplay::scale_airFlow
                             ));

    _combines.append(new Combine(17, //0x11
                             CombineModel::combine_kzs3219,
                             QString::fromUtf8("КЗС 3219"),
                             CombineStrawEngineType::strawEngine_separator,
                             TempScaleToDisplay::scale_hydraulicOil
                             ));

    _combines.append(new Combine(18, //0x12
                             CombineModel::combine_kzs3219_lowreductor,
                             QString::fromUtf8("КЗС 3219 пониж. редукт."),
                             CombineStrawEngineType::strawEngine_separator,
                             TempScaleToDisplay::scale_hydraulicOil
                             ));

    currentCombineCANIndex = -1;
    //_combine = nullptr;
}
//not null
QString CombineKeeper::GetCombineModelName(int indexInList)
{
    if(indexInList >=0 && indexInList < _combines.count()) {
        return _combines[indexInList]->getName();
    }
    return "";
}
//not null
QList<Combine*> CombineKeeper::GetCombineList()
{
    return _combines;
}
//can be null
Combine* CombineKeeper::GetCombine(QString name)
{
    for(int i=0;i<_combines.count();i++)
    {
        if(_combines[i]->getName() == name)
        {
            return _combines[i];
        }
    }
    return nullptr;
}
//can be null
Combine* CombineKeeper::GetCombine(int CANindex)
{
    for(int i=0;i<_combines.count();i++)
    {
        if(_combines[i]->getCanIndex() == CANindex)
        {
            return _combines[i];
        }
    }
    return nullptr;
}
//can be null
int CombineKeeper::GetIndexInList(int CANindex)
{
    for(int i=0;i<_combines.count();i++)
    {
        if(_combines[i]->getCanIndex() == CANindex)
        {
            return i;
        }
    }
    return -1;
}
//can be null
int CombineKeeper::GetCANIndex(int indexInList)
{
    if(indexInList >=0 && indexInList < _combines.count()) {
        return _combines[indexInList]->getCanIndex();
    }
    return -1;
}

void CombineKeeper::setCurrentCombine(int CANindex)
{
    currentCombineCANIndex = CANindex;
}
//can be null
Combine *CombineKeeper::getCurrentCombine()
{
    if(currentCombineCANIndex == -1)
        return nullptr;
    else {
        return GetCombine(currentCombineCANIndex);
    }
}

int CombineKeeper::GetCurrentIndexInList()
{
    return GetIndexInList(currentCombineCANIndex);
}
