#pragma once
#include <QString>
#include <QList>

enum CombineModel
{
    combine_gs07                 ,
    combine_gs10                 ,
    combine_gs12                 ,
    combine_gs812                ,
    combine_gs1624               ,
    combine_test                 ,
    //6-unknown                  ,
    combine_gs10_2011            ,
    combine_gs12_2011            ,
    combine_gs812_2011           ,
    combine_gs812c_2013_tracks   ,
    combine_gs812_2013_cop       ,
    combine_gs812_2013_stage_cop ,
    combine_gs812_stage          ,
    combine_gs12_2011_cop        ,
    combine_gs12_cummins         ,
    combine_gs4118               ,
    combine_kzs3219              ,
    combine_kzs3219_lowreductor
};

enum CombineStrawEngineType
{
    strawEngine_separator,
    strawEngine_shaker
};

enum TempScaleToDisplay
{
    scale_hydraulicOil,
    scale_airFlow
};

class Combine
{
public:
    Combine(uint8_t CANindex, CombineModel model, QString name,
            CombineStrawEngineType engineType, TempScaleToDisplay tempScale);
    QString getName() {return _name;}
    uint8_t getCanIndex() {return _CANindex;}
    CombineModel getModel() {return _model;}
    CombineStrawEngineType getEngineType() {return _engineType;}
    TempScaleToDisplay getTempScale() {return _tempScale;}
private:
    uint8_t _CANindex;
    CombineModel _model;
    QString _name;
    CombineStrawEngineType _engineType;
    TempScaleToDisplay _tempScale;
};
