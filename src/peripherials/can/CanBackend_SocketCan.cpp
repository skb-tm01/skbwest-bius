#ifdef BUILD_DEVICE
#include <sys/types.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/uio.h>

#include <net/if.h>

#include <linux/can.h>
#include <linux/can/error.h>
#include <linux/can/raw.h>

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <signal.h>

#endif

#ifdef _MSC_VER
#include <WinSock2.h>
#else
#include <sys/time.h>
#endif

#include <stdlib.h>

#include "peripherials/can/CanBackend_SocketCan.h"

#include <QErrorMessage>
#include <QDebug>

#define INVALID_SOCKET -1

/**
 * @brief Creates SocketCan backend objectCanBackend_SocketCan::CanBackend_SocketCan
 * @param interfaceName specifies CAN interface name
 * @param baudRate specifies connection baudrate
 */
CanBackend_SocketCan::CanBackend_SocketCan(QString interfaceName, uint baudRate)
{
    m_initialized = false;
    m_socket = INVALID_SOCKET;
    baud_rate = baudRate;
    this->interfaceName =interfaceName;
    Init();
    m_initialized = true;
}

void CanBackend_SocketCan::can_interface_down()
{
    QString req("/sbin/ifconfig "+interfaceName +" down");
    system(req.toStdString().c_str());
}
void CanBackend_SocketCan::can_interface_up()
{
    QString req("/sbin/ifconfig "+interfaceName +" up");
    system(req.toStdString().c_str());
}

void CanBackend_SocketCan::can_interface_setbaudrate()
{
    QString req("/sbin/ip link set " + interfaceName + " up type can bitrate " +QString::number(baud_rate) + " restart-ms 100");
    system(req.toStdString().c_str());
}

/**
 * @brief Initializes CAN connection
 * @return  error code, 0 on success, TODO on error
 */
int CanBackend_SocketCan::Init()
{
    can_interface_down();
    can_interface_setbaudrate();
    can_interface_up();

    m_initialized = true;
    return 0;
}

/**
 * @brief CanBackend_SocketCan::Open
 * @return error code, 0 on success, -1 if application was compiled without can support, -2 if not initialized
 */
int CanBackend_SocketCan::Open()
{
#ifdef BUILD_DEVICE

    sockaddr_can addr{};
    ifreq ifr{};

    if(!m_initialized)
        return -2;

    m_socket = socket(PF_CAN, SOCK_RAW, CAN_RAW);

    // Get index for a certain name
    strcpy(ifr.ifr_name, interfaceName.toStdString().c_str());
    if(ioctl(m_socket, SIOCGIFINDEX, &ifr) == -1)
    {
        qWarning() << "CanBackend_SocketCan/Open: error on ioctl";
        return errno;
    }

    addr.can_family = AF_CAN;
    addr.can_ifindex = ifr.ifr_ifindex;

    if(bind(m_socket, reinterpret_cast<struct sockaddr*>(&addr), sizeof(addr)) == -1)
    {
        qWarning() << "CanBackend_SocketCan/Open: error on bind";
        close(m_socket);
        m_socket = INVALID_SOCKET;
        return errno;
    }

    //Configure the socket can layer to report errors
    can_err_mask_t err_mask = ( CAN_ERR_TX_TIMEOUT | CAN_ERR_BUSERROR | CAN_ERR_ACK | CAN_ERR_BUSOFF );
    setsockopt(m_socket, SOL_CAN_RAW, CAN_RAW_ERR_FILTER, &err_mask, sizeof(err_mask));


    //disable CAN loopback
    int loopback = 0; /* 0 = disabled, 1 = enabled (default) */
    setsockopt(m_socket, SOL_CAN_RAW, CAN_RAW_LOOPBACK, &loopback, sizeof(loopback));

    return 0;
#else
    return -1;
#endif
}

/**
 * @brief Close an open connection.
 */
void CanBackend_SocketCan::Close()
{
    #ifdef BUILD_DEVICE
    if(m_initialized)
    {
        shutdown(m_socket, SHUT_RDWR);
        close(m_socket);
        m_socket = INVALID_SOCKET;
    }
    #endif
}


/**
 * @brief Send a message on the CAN bus.
 * @param msg - messsage to send
 * @return 0 -- success, -1 -- not supported, -2 -- initialized, other -- linux ERRNO
 */
int CanBackend_SocketCan::Message_Send(CanMessage msg)
{
    #ifdef BUILD_DEVICE

    if(!m_initialized)
        return -2;

    if(msg.extended)
        msg.can_id |= CAN_EFF_FLAG;


    if(msg.rtr)
        msg.can_id |= CAN_RTR_FLAG;

    can_frame frame{};
    frame.can_id = msg.can_id;
    frame.can_dlc = msg.can_dlc;

    for(int i = 0; i<CAN_MAX_DLEN;i++)
    {
        frame.data[i]=msg.data[i];
    }

    if(write(m_socket, &frame, sizeof(frame)) < 0)
    {
        return errno;
    }

    return 0;
#else
    return -1;
#endif
}


/**
 * @brief  Get a CAN message
 * @details
 * If socket is blocking (default) - this call will block until data is received or until timeout period has expired.
 * If socket is non blocking, it will return false if there is no data or if there is any error.
 * If socket is blocking, it will return false if there is an error or at timeout.
 * @param timeout - will return error after timeout period
 * @return CanMesssage struct
 */
CanMessage CanBackend_SocketCan::Message_Receive(struct timeval timeout)
{
    CanMessage msg{};

    #ifdef BUILD_DEVICE

    int bytesRead;
    int ret;


    if(!m_initialized)
    {
        qWarning() << "CanBackend_SocketCan/Message_Receive: not initialized";
        msg.error = true;
        msg.errorCode = -2;
        return msg;
    }

    // Set up a file descriptor set only containing one socket
    fd_set fds_read{};
    FD_ZERO(&fds_read);
    FD_SET(m_socket, &fds_read);

    // Use select to be able to use a timeout
    ret = select(m_socket+1, &fds_read, nullptr, nullptr, &timeout);
    if(ret<0)
    {
        qWarning() << "CanBackend_SocketCan/Message_Receive: error on select";
        msg.error = true;
        msg.errorCode = errno;
    }
    else if(ret==0)
    {
        msg.err_timeout = true;
    }
    else if(ret > 0)
    {
        can_frame frame{};
        bytesRead = read(m_socket, &frame, sizeof(can_frame));
        if(bytesRead == sizeof(frame))
        {
            msg.can_id = frame.can_id;
            msg.can_dlc = frame.can_dlc;

            for(int i = 0; i<CAN_MAX_DLEN;i++)
            {
                msg.data[i]=frame.data[i];
            }

            msg.error_can = msg.can_id & CAN_ERR_FLAG;
            msg.extended = msg.can_id & CAN_EFF_FLAG;
            msg.rtr = msg.can_id & CAN_RTR_FLAG;

            if(msg.error_can)
                msg.can_id  &= CAN_ERR_MASK;

            if(msg.extended)
                msg.can_id  &= CAN_EFF_MASK;
            else
                msg.can_id &= CAN_SFF_MASK;
        }
        else
        {
            qWarning() << "CanBackend_SocketCan/Message_Receive: error on read";
            msg.error = true;
            msg.errorCode = errno;
        }
    }

    return msg;
#else
    msg.error = true;
    msg.errorCode = -1;
    return msg;
#endif
}

/**
 * @brief Set size of receive buffer. The standard size is usually large enough.
 * @note getsockopt will return twice the size set
 * If settings a larger size than the system supports, the size will set to a lower value than requested
 * @param size - in bytes
 * @return - 0 -- success, -1 -- not supported, -2 not initialized, other -- ERRNO
 */
int CanBackend_SocketCan::Set_ReceiveBufferSize(int size)
{
#ifdef BUILD_DEVICE
    if(!m_initialized)
        return -2;

    int rcvbuf_size = size;

    if(setsockopt(m_socket, SOL_SOCKET, SO_RCVBUF, &rcvbuf_size, sizeof(rcvbuf_size)) < 0)
        return errno;

    return 0;
#else
    return -1;
#endif
}



