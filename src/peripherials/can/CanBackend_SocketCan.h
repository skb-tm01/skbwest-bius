#pragma once

#include <QObject>
#include <QString>

#include "peripherials/can/ICanBackend.h"



class CanBackend_SocketCan : public QObject, public ICanBackend
{
    Q_OBJECT
    Q_INTERFACES(ICanBackend)

public:
    CanBackend_SocketCan(QString interfaceName, uint baudRate);

    int Open();
    void Close();

    int Message_Send(CanMessage msg);
    CanMessage Message_Receive(struct timeval timeout);

    int Set_ReceiveBufferSize(int size);

private:
    int Init();

    void can_interface_down();
    void can_interface_up();
    void can_interface_setbaudrate();

    /**
     * @brief indicates if socket is initialized
     */
    bool m_initialized;

    /**
     * @brief stores CAN interface name
     */
    QString interfaceName;

    /**
     * @brief socket handle
     */
    int m_socket;

    /**
     * @brief connection baudrate
     */
    uint baud_rate;
};
