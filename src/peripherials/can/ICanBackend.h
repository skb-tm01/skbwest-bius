#pragma once

#include <QtPlugin>

#include "peripherials/can/CanMessage.h"

class ICanBackend
{
public:
    virtual int Open() = 0;
    virtual void Close() = 0;

    virtual int Message_Send(CanMessage msg) = 0;
    virtual CanMessage Message_Receive(struct timeval timeout) = 0;

    virtual int Set_ReceiveBufferSize(int size) = 0;
};

Q_DECLARE_INTERFACE(ICanBackend, "SKBWEST-BIUS.ICanBackend/1.0")
