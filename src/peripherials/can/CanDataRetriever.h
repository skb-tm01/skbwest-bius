#ifndef CANDATARETRIEVER_H
#define CANDATARETRIEVER_H

#include <QObject>

#include "peripherials/can/CanMessage.h"

enum RetrieverType{
    FUEL,
    EVENTS
};

class CanDataRetriever : public QObject
{
    Q_OBJECT
public:
    explicit CanDataRetriever(RetrieverType retType,QObject *parent = nullptr);
    void StartExtraction();
    QString GetFilename();
    void SetType(RetrieverType type);
    QByteArray data;

signals:
    void sig_finished();
    void sig_progressChanged(int progress);

public slots:

private slots:
    void OnCanDataMessage(CanMessage msg);
private:
    void OnCanDataMessage_Settings(CanMessage msg);
    void OnCanDataMessage_DataGet(CanMessage msg);


    uint combine_sn;
    RetrieverType ret_type;


    uint16_t start_addr;
    uint16_t offset;
};

#endif // CANDATARETRIEVER_H
