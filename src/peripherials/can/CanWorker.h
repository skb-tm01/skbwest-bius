#pragma once

#include <atomic>
#include <QObject>

#include "core/ThreadSafeQueue.h"
#include "peripherials/can/ICanBackend.h"
#include "peripherials/can/CanMessage.h"

#define RETRIES_COUNT 5
#define NOBIUS_THRESHOLD 10

class CanWorker : public QObject
{
    Q_OBJECT
public:
    explicit CanWorker(QObject *parent = 0);
    void Init(ICanBackend *backend);

    bool IsConnected();

signals:
    void received(CanMessage msg);
    void sent(CanMessage msg);
    void finished();
    void error(QString err);
    void connectionLost();
    void connectionRestored();

public slots:
    void Run();
    void setTerminationFlag();
    void AddMessage(CanMessage msg, bool isPersistent);
    void ForceConnectionLost();

private slots:
    void OnReceived(CanMessage msg);

private:
    bool ReceiveMessage(long timeout);
    int SendMessage(CanMessage msg);

    int _errCount;
    int _noBIUS;
    bool _failOccurred;

    int _receivedCount;


    ThreadSafeQueue<CanMessage> canmessages_once;
    ThreadSafeQueue<CanMessage> canmessages_persistent;

    ICanBackend* _backend;
    std::atomic_bool _terminationRequested;
};
