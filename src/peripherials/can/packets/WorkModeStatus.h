#pragma once

#include <QDebug>

#include <cstdint>

#include <QString>

namespace CanPackets
{
    enum WorkModeChannelId : uint8_t
    {
        wm_chan_beaterdrum     = 2,
        wm_chan_concavegap     = 3,
        wm_chan_blower         = 4,
        wm_chan_chaffer_sieve  = 5,
        wm_chan_shoe_sieve     = 6,
        wm_chan_loss_in_filtat = 0,
        wm_chan_loss_in_straw  = 1
    };

    enum WorkModeStatus : uint16_t
    {
      wait  = 0x0  ,
      minus = 0x1  ,
      plus  = 0x2  ,
      stop  = 0x4  ,
      space = 0x8  ,
      error = 0x10 ,
      off   = 0x20 ,
      time  = 0x40 ,
      ok    = 0x80,
    };

    struct WorkModeData
    {
        WorkModeChannelId channelId;
        uint16_t status;
        uint16_t currentValue;
        uint16_t targetValue;
        uint8_t time;

        QString GetStatusText()
        {
            if(status == 0)
            {
                return QString::fromUtf8("ЖДИТЕ");
            }
            else if(status&WorkModeStatus::minus)
            {
                return QString::fromUtf8("------");
            }
            else if(status&WorkModeStatus::plus)
            {
                return QString::fromUtf8("++++++");
            }
            else if(status&WorkModeStatus::stop)
            {
                return QString::fromUtf8("СТОП");
            }
            else if(status&WorkModeStatus::space)
            {
                return QString::fromUtf8(" ");
            }
            else if(status&WorkModeStatus::error)
            {
                return QString::fromUtf8("ОШИБКА");
            }
            else if(status&WorkModeStatus::off)
            {
                return QString::fromUtf8("ВЫКЛ");
            }
            else if(status&WorkModeStatus::time)
            {
                return QString::fromUtf8("ВРЕМЯ");
            }
            else if(status&WorkModeStatus::ok)
            {
                return QString::fromUtf8("ОК");
            }

            return "UNKNOWN";

        }
    };


    class WorkModeGet{
    public:
        static const uint32_t request_id = 0x019;
        static const uint32_t response_id = 0x419;
        static void SendRequest(WorkModeChannelId channelId);
        static WorkModeData ParseResponse(uint8_t *data);
    };
}
