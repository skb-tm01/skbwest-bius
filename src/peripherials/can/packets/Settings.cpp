#include "core/ByteConversion.h"
#include "peripherials/PeripherialsManager.h"
#include "peripherials/can/CanMessage.h"
#include "peripherials/can/packets/Settings.h"

void CanPacketSettingsGetRequest::Send(CanPacketSettingsType type, bool persistent)
{
    CanMessage msg{};
    msg.can_id = packet_id;
    msg.can_dlc=1;
    msg.data[0]= type;

    auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
    if(canworker!=nullptr)
        canworker->AddMessage(msg, persistent);
}

CanPacketSettingsGetResponseData CanPacketSettingsGetResponse::Parse(uint8_t *data)
{
    CanPacketSettingsGetResponseData msg;
    msg.type = static_cast<CanPacketSettingsType>(data[0]);
    msg.value = ByteConversion::make_uint16(data[1],data[2]);
    return msg;
}

void CanPacketSettingsSetRequest::Send(CanPacketSettingsType type, uint16_t value)
{
   CanMessage msg{};
   msg.can_id = packet_id;
   msg.can_dlc=3;
   msg.data[0]=type;
   msg.data[1]=value&0xFF;
   msg.data[2]=value>>8;

   auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
   if(canworker!=nullptr)
       canworker->AddMessage(msg, false);
}

CanPacketSettingsSetResponseData CanPacketSettingsSetResponse::Parse(uint8_t *data)
{
    CanPacketSettingsSetResponseData msg;
    msg.type = static_cast<CanPacketSettingsType>(data[0]);
    return msg;
}
