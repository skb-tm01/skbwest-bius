#include "peripherials/PeripherialsManager.h"
#include "peripherials/can/packets/TahoCalibration.h"
#include "peripherials/can/CanMessage.h"

#include "core/ByteConversion.h"

void CanPacketAnalogCalibrationRequest::Send(CanPacketTahoType type)
{
   CanMessage msg{};
   msg.can_id = packet_id;
   msg.can_dlc=1;
   msg.data[0]= type;

   auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
   if(canworker!=nullptr)
       canworker->AddMessage(msg, false);
}

CanPacketTahoCalibrationResponseData CanPacketTahoCalibrationResponse::Parse(uint8_t *data)
{
    CanPacketTahoCalibrationResponseData msg;
    msg.type = static_cast<CanPacketTahoType>(data[0]);
    msg.calibr_turns = ByteConversion::make_uint16(data[1],data[2]);
    msg.marks = data[3];
    msg.deviation = data[4];
    return msg;
}
