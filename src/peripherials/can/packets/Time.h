#pragma once

#pragma once

#include <cstdint>
#include <QString>

#pragma pack(push,1)
struct CanPacketTimeResponseData
{
    uint8_t year;
    uint8_t month;
    uint8_t day;
    uint8_t hour;
    uint8_t minute;
    uint8_t second;
};
#pragma pack(pop)
const uint8_t CanPacketTimeResponse_dlc = 6;
static_assert (sizeof(CanPacketTimeResponseData)==CanPacketTimeResponse_dlc,"CanPacketTimeResponseData size error");


class CanPacketTimeRequest
{
public:
    static const uint32_t packet_id = 0x480;
    static void Send();

};

class CanPacketTimeResponse
{
public:
    static const uint32_t packet_id = 0x490;
    static void SendCurrentTime();
    static void Send(CanPacketTimeResponseData data);
};

namespace CanPackets
{
    #pragma pack(push,1)
    struct VersionTimeData
    {
        uint8_t type;
        char data[7];
        uint16_t checksum;
    };
    #pragma pack(pop)

    class VersionTime
    {
    public:
        static const uint32_t request_id = 0x80;
        static const uint32_t response_id = 0x480;

        static void SendRequest(bool secondReq);
        static VersionTimeData ParseResponse(uint8_t* data);
    };
}

