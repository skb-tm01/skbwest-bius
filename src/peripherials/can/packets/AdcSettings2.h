#pragma once

#include <cstdint>

namespace CanPackets
{
    struct AdcSettings2Data
    {
        /**
         * @brief сканирующий канал
         */
        uint8_t contactStateId;

        /**
         * @brief нижнее  ошибочное значение определение неисправности датчика
         */
        uint16_t err_dn;

        /**
         * @brief текущие состояние датчика
         */
        uint16_t now;

        /**
         * @brief неисправность датчика
         */
        uint8_t on_off;

        /**
         * @brief флаг снятия с контроля
         */
        uint8_t status;
    };

    class AdcSettings2Set{
    public:
        static const uint32_t request_id = 0x02A;
        static const uint32_t response_id = 0x42A;
        static void SendRequest(AdcSettings2Data data);
        static void ParseResponse(uint8_t *data);
    };

    class AdcSettings2Get{
    public:
        static const uint32_t request_id = 0x02B;
        static const uint32_t response_id = 0x42B;
        static void SendRequest(uint8_t contactStateId);
        static AdcSettings2Data ParseResponse(uint8_t *data);
    };
}
