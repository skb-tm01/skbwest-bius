#include "Taho.h"

#include "core/ByteConversion.h"
#include "peripherials/PeripherialsManager.h"
#include "peripherials/can/CanMessage.h"

void CanPacketTahoRequest::Send(CanPacketTahoType type, bool persistent)
{
   CanMessage msg{};
   msg.can_id = packet_id;
   msg.can_dlc=1;
   msg.data[0]= type;

   auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
   if(canworker!=nullptr)
       canworker->AddMessage(msg, persistent);
}

CanPacketTahoResponseData CanPacketTahoResponse::Parse(uint8_t *data)
{
    CanPacketTahoResponseData msg;
    msg.type = static_cast<CanPacketTahoType>(data[0]);
    msg.rpm = ByteConversion::make_uint16(data[1],data[2]);
    msg.calibrcoeff = ByteConversion::make_uint16(data[3],data[4]);
    msg.can_motor_error = data[5];
    msg.can_motor_error_lamp = data[6];

    memcpy(msg.raw_data,data,8);

    return msg;
}
