#pragma once

#include <cstdint>


enum CanPacketTahoType:uint8_t
{
    taho_loss_left            =   0, //потери слева
    taho_loss_right           =   1, //потери справа
    taho_speed_current        =   2, //получение текущей скорости
    taho_speed_engine         =   3, //получение оборотов двигателя
    taho_strawsep_speed       =   4, //получение оборотов соломосепаратора
    taho_spikeauger_speed     =   5, //получение оборотов колосового шнека
    //6 - unknown
    taho_grainauger_speed     =   7, //получение оборотов зернового шнека
    //8 - unknown
    taho_drumchopper_speed    =   9, //получение оборотов барабана измельчителя
    taho_speed_fan            =  10, //получение оборотов вентилятора
    taho_speed_threshing      =  11, //получение оборотов молотилки
    //12 - unknown
    taho_shakershaft_speed    =  13, //получение оборотов вала соломотряса
    taho_levelingauger1_speed =  14, //получение оборотов выравнивающего шнека 1
    taho_levelingauger2_speed =  15, //получение оборотов выравнивающего шнека 2, КОПНИТЕЛЬ???
    //16 - unknown
    //17 - unknown
    //18 - unknown
    //19 - unknown
    taho_engine_load          =  20, //получение загрузки двигателя
    taho_unknown_load         = 255,  //какая-то загрузка
};

#pragma pack(push,1)
struct CanPacketTahoResponseData
{
    CanPacketTahoType type;       // сканирующий канал
    uint16_t rpm;                 // обороты/скорость в десятых
    uint16_t calibrcoeff;         // отношение частоты к одной из (обороты двигателя)
    uint8_t can_motor_error;      // статус ошибки связи по кан двигателя
    uint8_t can_motor_error_lamp; // статус ошибок по кан двигателя

    uint8_t raw_data[8];
};
#pragma pack(pop)


class CanPacketTahoRequest
{
public:
    static const uint32_t packet_id = 0x001;
    static void Send(CanPacketTahoType type,bool persistent = false);
};


class CanPacketTahoResponse
{
public:
    static const uint32_t packet_id = 0x401;
    static CanPacketTahoResponseData Parse(uint8_t* data);
};
