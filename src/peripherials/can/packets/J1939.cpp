#include "peripherials/PeripherialsManager.h"
#include "peripherials/can/CanMessage.h"
#include "peripherials/can/packets/J1939.h"

void CanPackets::J1939Get::SendRequest(uint8_t Id)
{
   CanMessage msg{};
   msg.can_id = request_id;
   msg.can_dlc=1;
   msg.data[0]= Id;

   auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
   if(canworker!=nullptr)
       canworker->AddMessage(msg, false);
}

CanPackets::J1939Data CanPackets::J1939Get::ParseResponse(uint8_t *data)
{
    J1939Data msg{};
    msg.Id = data[0];
    msg.SPN = (data[1] << 24) | (data[2] << 16) | (data[3] << 8) | data[4]; //TODO: check endianess
    msg.FMI = data[5];
    return msg;
}
