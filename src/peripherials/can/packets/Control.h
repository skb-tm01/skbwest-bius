#pragma once

#include <cstdint>

#include "core/ByteConversion.h"

#include "peripherials/sensors/SensorType.h"
#include "peripherials/sensors/ISensor.h"


namespace CanPackets
{
    struct ControlGetData
    {
        SensorType type;
        uint8_t id;

        uint8_t control_state;

        bool IsUnderControl()
        {
            return !ByteConversion::GetBitState(control_state,0);
        }

        bool IsBroken()
        {
            return ByteConversion::GetBitState(control_state,1);
        }
    };

    struct ControlSetData
    {
        SensorType type;
        uint8_t id;
    };

    class ControlSet{
    public:
        static const uint32_t request_id = 0x014;
        static const uint32_t response_id = 0x414;
        static void SendRequest(ISensor* sensor, uint8_t state);
        static void SendRequest(uint8_t sensor, uint8_t state);
        static ControlSetData ParseResponse(uint8_t *data);
    };

    class ControlGet{
    public:
        static const uint32_t request_id = 0x015;
        static const uint32_t response_id = 0x415;
        static void SendRequest(ISensor* sensor);
        static void SendRequest(uint8_t sensor);
        static ControlGetData ParseResponse(uint8_t *data);
    };
}
