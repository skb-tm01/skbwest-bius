#pragma once

#include <cstdint>

enum CanPacketProbegType:uint8_t
{
    //Data[0]<100 – возвр. аварийная статистика
    //Data[0]>149 – возвр. сообщение об ошибке


    alarm_shaker_stuck                   = 20, //забивание соломотряса
    alarm_discharge_auger_on             = 22,
    alarm_hydraulic_powercyl_oil_temp    = 25,
    alarm_oiltank_oil_level              = 26,
    alarm_hydraulicblock_overlevel       = 27,
    alarm_engine_oil_pressure            = 28,
    alarm_hydraulic_movementpar_oil_temp = 29,
    alarm_engine_coolant_temp            = 30,
    alarm_engine_coolant_level           = 38,
    alarm_grain_tank_opened              = 40,
    alarm_coiled_auger_low_speed         = 55,
    alarm_grain_auger_low_speed          = 57,
    alarm_drum_chopper_low_speed         = 59,
    alarm_cleaning_fan_low_speed         = 60,
    alarm_drum_low_speed                 = 61, //обороты молотильного барабана ниже запомненных
    alarm_shaker_low_speed               = 63, //обороты солмотряса ниже допустимых
    alarm_stacker_low_speed              = 65, //обороты вала копнителя ниже допустимых

    probeg_distance_temp            = 100, // возвр. временный пробег комбайна
    probeg_distance_total           = 101, // возвр. суммарный пробег комбайна
    probeg_area_temp                = 102, // временная убранная площадь
    probeg_area_total               = 103, // суммарная убранная площадь
    probeg_combinetime_temp         = 104, // временная наработка комбайна
    probeg_combinetime_total        = 105, // суммарная наработка комбайна
    probeg_enginetime_to_eto        = 106, // время работы двигателя до ТО  ETO
    probeg_time_to_to1              = 107, // время работы комбайна до ТО    TO-1
    probeg_enginetime_total         = 108, // суммарная наработка двигателя
    probeg_enginetime_total_can     = 109, // суммарная наработка двигателя / moto_time_can
    probeg_enginetime_total_litr    = 110, // суммарная наработка двигателя / moto_litr_can
    probeg_enginetime_temp_litr     = 111, // moto_litr_can_temp / 0
    probeg_enginetime_total_wo_load = 112, // суммарная наработка двигателя без учетом загрузки
    probeg_enginetime_total_wi_load = 113, // суммарная наработка двигателя с учетом загрузки
    probeg_poweralarm_total         = 114, // суммарная авария напряжения бортсети
    //115 unknown
    probeg_enginetime_before_eto    = 116, // время работы двигателя до ТО  ETO
    probeg_enginetime_before_to1    = 117, // время работы комбайна до ТО    TO-1
    probeg_enginetime_before_to2    = 118, // время работы комбайна до ТО    TO-2
    probeg_canalarm_count           = 149  // motor_can_error_count
};

#pragma pack(push,1)
struct CanPacketProbegResponseData
{
    CanPacketProbegType type;
    uint32_t value;
};
#pragma pack(pop)
const uint8_t CanPacketProbegResponse_dlc=5;
static_assert (sizeof(CanPacketProbegResponseData)==CanPacketProbegResponse_dlc,"CanPacketProbegResponse size error");


class CanPacketProbegRequest
{
public:
    static const uint32_t packet_id = 0x007;
    static void Send(CanPacketProbegType type);
};


class CanPacketProbegResponse
{
public:
    static const uint32_t packet_id = 0x407; //1031 in dec
    static CanPacketProbegResponseData Parse(uint8_t* data);
};


