#include "core/ByteConversion.h"
#include "peripherials/PeripherialsManager.h"
#include "peripherials/can/packets/ContactStateSettings.h"

namespace CanPackets
{
    void ContactStateSettingsSet::SendRequest(ContactStateSettingsData data)
    {
       CanMessage msg{};
       msg.can_id  = request_id;
       msg.can_dlc = 8;
       msg.data[0] = data.contactStateId;
       msg.data[1] = data.adc_value & 0xff;
       msg.data[2] = data.adc_value >> 8;
       msg.data[3] = data.adc_threshold & 0xff;
       msg.data[4] = data.adc_threshold >> 8;
       msg.data[5] = data.current_state;
       msg.data[6] = data.type;
       msg.data[7] = data.on_control;

       auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
       if(canworker!=nullptr)
           canworker->AddMessage(msg, false);
    }

    void ContactStateSettingsSet::ParseResponse(uint8_t *)
    {
        return;
    }

    void ContactStateSettingsGet::SendRequest(uint8_t contactStateId)
    {
       CanMessage msg{};
       msg.can_id = request_id;
       msg.can_dlc=1;
       msg.data[0]= contactStateId;

       auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
       if(canworker!=nullptr)
           canworker->AddMessage(msg, false);
    }

    ContactStateSettingsData ContactStateSettingsGet::ParseResponse(uint8_t *data)
    {
        ContactStateSettingsData msg;
        msg.contactStateId = data[0];
        msg.adc_value      = ByteConversion::make_uint16(data[1],data[2]);
        msg.adc_threshold  = ByteConversion::make_uint16(data[3],data[4]);
        msg.current_state  = data[5];
        msg.type           = data[6];
        msg.on_control     = data[7];
        return msg;
    }
}
