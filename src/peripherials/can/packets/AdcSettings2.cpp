#include "peripherials/can/packets/AdcSettings2.h"
#include "peripherials/PeripherialsManager.h"
#include "core/ByteConversion.h"

void CanPackets::AdcSettings2Set::SendRequest(CanPackets::AdcSettings2Data data)
{
       CanMessage msg{};
       msg.can_id  = request_id;
       msg.can_dlc = 7;
       msg.data[0] = data.contactStateId;
       msg.data[1] = data.err_dn & 0xFF;
       msg.data[2] = data.err_dn >> 8;
       msg.data[3] = data.now & 0xFF;
       msg.data[4] = data.now >> 8;
       msg.data[5] = data.on_off;
       msg.data[6] = data.status;

       auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
       if(canworker!=nullptr)
           canworker->AddMessage(msg, false);
}

void CanPackets::AdcSettings2Set::ParseResponse(uint8_t *)
{
    return;
}

void CanPackets::AdcSettings2Get::SendRequest(uint8_t contactStateId)
{
       CanMessage msg{};
       msg.can_id = request_id;
       msg.can_dlc=1;
       msg.data[0]= contactStateId;

       auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
       if(canworker!=nullptr)
           canworker->AddMessage(msg, false);
}

CanPackets::AdcSettings2Data CanPackets::AdcSettings2Get::ParseResponse(uint8_t *data)
{
    AdcSettings2Data msg;
    msg.contactStateId = data[0];
    msg.err_dn = ByteConversion::make_uint16(data[1],data[2]);
    msg.now = ByteConversion::make_uint16(data[3],data[4]);
    msg.on_off = data[5];
    msg.status = data[6];
    return msg;
}
