#include "peripherials/can/packets/DataGet.h"

#include "peripherials/PeripherialsManager.h"
#include "peripherials/can/CanMessage.h"

void CanPackets::DataGet::SendRequest(uint16_t req_id)
{
    CanMessage msg{};
    msg.can_id  = request_id;
    msg.can_dlc = 3;
    msg.data[0] = 0;
    msg.data[1] = req_id&0xFF;
    msg.data[2] = req_id>>8;

    auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
    if(canworker!=nullptr)
        canworker->AddMessage(msg, false);
}

void CanPackets::DataGet::ParseResponse(uint8_t *)
{

}
