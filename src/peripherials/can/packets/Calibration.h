#pragma once

#include <cstdint>

namespace CanPackets
{
    enum CalibrationType: uint8_t{
       clearance     = 0x41,
       lower_sieves  = 0x47,
       higher_siever = 0x48,
    };

    struct CalibrationInfoData{
        CalibrationType type;
        uint16_t adcval;
    };

    class CalibrationInfoGet{
    public:
        static const uint32_t request_id = 0x029;
        static const uint32_t response_id = 0x429;
        static void SendRequest(CalibrationType type);
        static CalibrationInfoData ParseResponse(uint8_t *data);
    };


    class CalibrationSave{
    public:
        static const uint32_t request_id = 0x030;
        static const uint32_t response_id = 0x430;
        static void SendRequest(CalibrationType type, uint16_t min, uint16_t max);
        static void ParseResponse(uint8_t *data);
    };
}
