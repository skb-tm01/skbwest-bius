#pragma once

#include <cstdint>

namespace CanPackets
{
    struct J1939Data
    {
        uint8_t Id;

        uint32_t SPN;

        uint8_t FMI;
    };

    class J1939Get{
    public:
        static const uint32_t request_id = 0x011;
        static const uint32_t response_id = 0x411;
        static void SendRequest(uint8_t Id);
        static J1939Data ParseResponse(uint8_t *data);
    };
}
