#include "peripherials/can/packets/RpmSettings3.h"
#include "peripherials/PeripherialsManager.h"
#include "core/ByteConversion.h"

void CanPackets::RpmSettings3Set::SendRequest(CanPackets::RpmSettings3Data data)
{
       CanMessage msg{};
       msg.can_id  = request_id;
       msg.can_dlc = 8;
       msg.data[0] = data.contactStateId;
       msg.data[1] = data.proskal;
       msg.data[2] = data.status;
       msg.data[3] = data.coeff & 0xFF;
       msg.data[4] = data.coeff >> 8;
       msg.data[5] = data.tms100;
       msg.data[6] = data.lo_rpm & 0xFF;
       msg.data[7] = data.lo_rpm >> 8;

       auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
       if(canworker!=nullptr)
           canworker->AddMessage(msg, false);
}

void CanPackets::RpmSettings3Set::ParseResponse(uint8_t *)
{
    return;
}

void CanPackets::RpmSettings3Get::SendRequest(uint8_t contactStateId)
{
       CanMessage msg{};
       msg.can_id = request_id;
       msg.can_dlc=1;
       msg.data[0]= contactStateId;

       auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
       if(canworker!=nullptr)
           canworker->AddMessage(msg, false);
}

CanPackets::RpmSettings3Data CanPackets::RpmSettings3Get::ParseResponse(uint8_t *data)
{
    RpmSettings3Data msg;
    msg.contactStateId = data[0];
    msg.proskal = data[1];
    msg.status = data[2];
    msg.coeff = ByteConversion::make_uint16(data[3],data[4]);
    msg.tms100 = data[5];
    msg.lo_rpm = ByteConversion::make_uint16(data[6],data[7]);
    return msg;
}
