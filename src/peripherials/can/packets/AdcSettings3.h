#pragma once

#include <cstdint>

namespace CanPackets
{
    struct AdcSettings3Data
    {
        /**
         * @brief сканирующий канал
         */
        uint8_t contactStateId;

        uint16_t val;
    };

    class AdcSettings3Set{
    public:
        static const uint32_t request_id = 0x02C;
        static const uint32_t response_id = 0x42C;
        static void SendRequest(AdcSettings3Data data);
        static void ParseResponse(uint8_t *data);
    };

    class AdcSettings3Get{
    public:
        static const uint32_t request_id = 0x02D;
        static const uint32_t response_id = 0x42D;
        static void SendRequest(uint8_t contactStateId);
        static AdcSettings3Data ParseResponse(uint8_t *data);
    };
}
