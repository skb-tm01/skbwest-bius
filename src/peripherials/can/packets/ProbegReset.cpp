#include "peripherials/PeripherialsManager.h"
#include "peripherials/can/CanMessage.h"
#include "peripherials/can/packets/ProbegReset.h"

void CanPacketProbegResetRequest::Send(CanPacketProbegResetType type)
{
   CanMessage msg{};
   msg.can_id = packet_id;
   msg.can_dlc=1;
   msg.data[0]= type;

   auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
   if(canworker!=nullptr)
       canworker->AddMessage(msg, false);
}

CanPacketProbegResetType CanPacketProbegResetResponse::Parse(uint8_t *data)
{
    return static_cast<CanPacketProbegResetType>(data[0]);
}
