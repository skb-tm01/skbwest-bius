#include "peripherials/PeripherialsManager.h"
#include "peripherials/can/CanMessage.h"
#include "peripherials/can/packets/LossSave.h"

namespace CanPackets
{
    void LossSave::SendRequest()
    {
       CanMessage msg{};
       msg.can_id = request_id;
       msg.can_dlc=1;
       msg.data[0]= 0;

       auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
       if(canworker!=nullptr)
           canworker->AddMessage(msg, false);
    }

    void LossSave::ParseResponse(uint8_t *)
    {
        return;
    }
}
