#include "peripherials/can/packets/RpmSettings2.h"
#include "peripherials/PeripherialsManager.h"
#include "core/ByteConversion.h"

void CanPackets::RpmSettings2Set::SendRequest(CanPackets::RpmSettings2Data data)
{
       CanMessage msg{};
       msg.can_id  = request_id;
       msg.can_dlc = 8;
       msg.data[0] = data.contactStateId;
       msg.data[1] = data.s1_hz & 0xFF;
       msg.data[2] = data.s1_hz >> 8;
       msg.data[3] = data.ca_rpm & 0xFF;
       msg.data[4] = data.ca_rpm >> 8;
       msg.data[5] = data.ca_k & 0xFF;
       msg.data[6] = data.ca_k >> 8 ;
       msg.data[7] = data.percent;

       auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
       if(canworker!=nullptr)
           canworker->AddMessage(msg, false);
}

void CanPackets::RpmSettings2Set::ParseResponse(uint8_t *)
{
    return;
}

void CanPackets::RpmSettings2Get::SendRequest(uint8_t contactStateId)
{
       CanMessage msg{};
       msg.can_id = request_id;
       msg.can_dlc=1;
       msg.data[0]= contactStateId;

       auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
       if(canworker!=nullptr)
           canworker->AddMessage(msg, false);
}

CanPackets::RpmSettings2Data CanPackets::RpmSettings2Get::ParseResponse(uint8_t *data)
{
    RpmSettings2Data msg;
    msg.contactStateId = data[0];
    msg.s1_hz   = ByteConversion::make_uint16(data[1],data[2]);
    msg.ca_rpm  = ByteConversion::make_uint16(data[3],data[4]);
    msg.ca_k    = ByteConversion::make_uint16(data[5],data[6]);
    msg.percent = data[7];
    return msg;
}
