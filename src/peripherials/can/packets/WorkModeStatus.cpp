#include "peripherials/can/packets/WorkModeStatus.h"
#include "peripherials/can/CanMessage.h"
#include "peripherials/PeripherialsManager.h"
#include "core/ByteConversion.h"

namespace CanPackets {

    void WorkModeGet::SendRequest(WorkModeChannelId channelId)
    {
        CanMessage msg{};
        msg.can_id  = request_id;
        msg.can_dlc = 1;
        msg.data[0] = channelId;

        auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
        if(canworker!=nullptr)
           canworker->AddMessage(msg, false);
    }

    WorkModeData WorkModeGet::ParseResponse(uint8_t *data)
    {
        WorkModeData msg{};
        msg.channelId = static_cast<WorkModeChannelId>(data[0]);
        msg.status = ByteConversion::make_uint16(data[1], data[2]);
        msg.currentValue = ByteConversion::make_uint16(data[3],data[4]);
        msg.targetValue = ByteConversion::make_uint16(data[5],data[6]);
        msg.time = data[7];

        return msg;
    }
}
