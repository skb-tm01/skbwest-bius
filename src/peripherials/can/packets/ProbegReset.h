#pragma once

#include <cstdint>

enum CanPacketProbegResetType:uint8_t
{
    reset_all_temp_stats     =  0, // очистка всего
    reset_engine_eto_time    =  1, // очистка времени работы двигателя до ТО  ETO
    reset_combine_to1_time   =  2, // время работы комбайна до ТО    TO-1
    reset_combine_work_time  =  3, // суммарная наработка комбайна
    reset_engine_eto_time_2  =  4, // время работы двигателя до ТО  ETO
    reset_combine_to1_time_2 =  5, // время работы комбайна до ТО    TO-1
    reset_combine_to2_time   =  6, // время работы комбайна до ТО    TO-2а
    //7-9 unknown
    reset_all_total_stats    =  10, // суммарной сброс статистики
    reset_alarm_stats        =  11, // аварийной сброс статистики
    //12-48 unknown
    reset_unknown_1          =  49,
    reset_unknown_2          =  50,
    reset_factory            =  51
};

class CanPacketProbegResetRequest
{
public:
    static const uint32_t packet_id = 0x008;
    static void Send(CanPacketProbegResetType type);
};


class CanPacketProbegResetResponse
{
public:
    static const uint32_t packet_id = 0x408;
    static CanPacketProbegResetType Parse(uint8_t* data);
};
