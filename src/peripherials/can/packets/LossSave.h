#pragma once

#include <cstdint>

namespace CanPackets
{
    class LossSave{
    public:
        static const uint32_t request_id = 0x009;
        static const uint32_t response_id = 0x409;
        static void SendRequest();
        static void ParseResponse(uint8_t *data);
    };
}
