#pragma once

#include <cstdint>

namespace CanPackets
{
    struct RpmSettings3Data
    {
        /**
         * @brief сканирующий канал
         */
        uint8_t contactStateId;

        /**
         * @brief флаг указывает что в данный момент проскальзывание
         */
        uint8_t proskal;

        /**
         * @brief флаг снятия с контроля
         */
        uint8_t status;

        /**
         * @brief множитель по оборотам   1000 =1,000
         */
        uint16_t coeff;

        /**
         * @brief tms100
         */
        uint8_t tms100;

        /**
         * @brief нижний порог оборотов
         */
        uint16_t lo_rpm;
    };

    class RpmSettings3Set{
    public:
        static const uint32_t request_id = 0x036;
        static const uint32_t response_id = 0x436;
        static void SendRequest(RpmSettings3Data data);
        static void ParseResponse(uint8_t *data);
    };

    class RpmSettings3Get{
    public:
        static const uint32_t request_id = 0x037;
        static const uint32_t response_id = 0x437;
        static void SendRequest(uint8_t contactStateId);
        static RpmSettings3Data ParseResponse(uint8_t *data);
    };
}
