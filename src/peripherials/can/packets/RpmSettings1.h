#pragma once

#include <cstdint>

namespace CanPackets
{
    struct RpmSettings1Data
    {
        /**
         * @brief сканирующий канал
         */
        uint8_t contactStateId;

        /**
         * @brief количество меток на один оборот
         */
        uint8_t points;

        /**
         * @brief длительность импульса
         */
        uint16_t d_pulse;

        /**
         * @brief частота в герцах
         */
        uint16_t d_hz;

        /**
         * @brief  частота в оборотах в минуту
         */
        uint16_t d_rpm;
    };


    class RpmSettings1Set{
    public:
        static const uint32_t request_id = 0x032;
        static const uint32_t response_id = 0x432;
        static void SendRequest(RpmSettings1Data data);
        static void ParseResponse(uint8_t *data);
    };

    class RpmSettings1Get{
    public:
        static const uint32_t request_id = 0x033;
        static const uint32_t response_id = 0x433;
        static void SendRequest(uint8_t contactStateId);
        static RpmSettings1Data ParseResponse(uint8_t *data);
    };
}
