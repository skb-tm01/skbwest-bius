#pragma once

#include <cstdint>

namespace CanPackets
{
    struct ContactStateTimeData
    {
        /**
         * @brief Сканирующий канал
         */
        uint8_t contactStateId;

        /**
         * @brief время которое нужно ждать до события
         */
        uint16_t timeToEvent;

        /**
         * @brief счетчик времени
         */
        uint16_t timeCounter;

        /**
         * @brief зависимость от другого канала
         * @remark 0xff - нет завсисимости
         */
        uint8_t dependency;

        /**
         * @brief значение превышения зависимости
         */
        uint16_t dependency_excess;
    };

    class ContactStateTimeSet{
    public:
        static const uint32_t request_id = 0x020;
        static const uint32_t response_id = 0x420;
        static void SendRequest(ContactStateTimeData data);
        static void ParseResponse(uint8_t *data);
    };

    class ContactStateTimeGet{
    public:
        static const uint32_t request_id = 0x021;
        static const uint32_t response_id = 0x421;
        static void SendRequest(uint8_t contactStateId);
        static ContactStateTimeData ParseResponse(uint8_t *data);
    };


}
