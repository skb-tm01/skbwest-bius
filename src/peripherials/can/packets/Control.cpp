#include "peripherials/PeripherialsManager.h"
#include "peripherials/can/packets/Control.h"

namespace CanPackets
{
    //Get

    void ControlGet::SendRequest(ISensor *sensor)
    {
        SendRequest(sensor->GetChannelId() | sensor->GetType());
    }

    void ControlGet::SendRequest(uint8_t sensor)
    {
        CanMessage msg{};
        msg.can_id  = request_id;
        msg.can_dlc = 1;
        msg.data[0] = sensor;

        auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
        if(canworker!=nullptr)
            canworker->AddMessage(msg, false);
    }

    ControlGetData ControlGet::ParseResponse(uint8_t *data)
    {
        ControlGetData msg{};
        msg.type = static_cast<SensorType>(data[0] & 0xC0);
        msg.id = data[0] & 0x3F;
        msg.control_state = data[1];
        return msg;
    }

    //Set

    void ControlSet::SendRequest(ISensor *sensor, uint8_t state)
    {
        SendRequest(sensor->GetChannelId() | sensor->GetType(), state);
    }

    void ControlSet::SendRequest(uint8_t sensor, uint8_t state)
    {
        CanMessage msg{};
        msg.can_id  = request_id;
        msg.can_dlc = 2;
        msg.data[0] = sensor;
        msg.data[1] = state;

        qDebug()<<"ControlSet::SendRequest(): ID:" << msg.data[0] << "STATE: " << msg.data[1];

        auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
        if(canworker!=nullptr)
            canworker->AddMessage(msg, false);
    }

    ControlSetData ControlSet::ParseResponse(uint8_t *data)
    {
        ControlSetData msg{};
        msg.type = static_cast<SensorType>(data[0] & 0xC0);
        msg.id = data[0] & 0x3F;

        return msg;
    }

}
