#pragma once

#include <cstdint>

namespace CanPackets
{
    struct RpmSettings2Data
    {
        /**
         * @brief сканирующий канал
         */
        uint8_t contactStateId;


        /**
         * @brief количество импульсов за предыдущую секунду
         */
        uint16_t s1_hz;


        /**
         * @brief  калибровачные обороты
         */
        uint16_t ca_rpm;


        /**
         * @brief калибровачные обороты
         */
        uint16_t ca_k;

        /**
         * @brief допустимое отклонение в процентах
         */
        uint8_t percent;
    };


    class RpmSettings2Set{
    public:
        static const uint32_t request_id = 0x034;
        static const uint32_t response_id = 0x434;
        static void SendRequest(RpmSettings2Data data);
        static void ParseResponse(uint8_t *data);
    };

    class RpmSettings2Get{
    public:
        static const uint32_t request_id = 0x035;
        static const uint32_t response_id = 0x435;
        static void SendRequest(uint8_t contactStateId);
        static RpmSettings2Data ParseResponse(uint8_t *data);
    };
}
