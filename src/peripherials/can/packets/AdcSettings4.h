#pragma once

#include <cstdint>

namespace CanPackets
{
    struct AdcSettings4Data
    {
        /**
         * @brief сканирующий канал
         */
        uint8_t contactStateId;
        uint8_t point_id;
        uint16_t adc_point;
        uint16_t adc_point_data;
    };

    class AdcSettings4Set{
    public:
        static const uint32_t request_id = 0x02E;
        static const uint32_t response_id = 0x42E;
        static void SendRequest(AdcSettings4Data data);
        static void ParseResponse(uint8_t *data);
    };

    class AdcSettings4Get{
    public:
        static const uint32_t request_id = 0x02F;
        static const uint32_t response_id = 0x42F;
        static void SendRequest(uint8_t contactStateId, uint8_t point_id);
        static AdcSettings4Data ParseResponse(uint8_t *data);
    };
}
