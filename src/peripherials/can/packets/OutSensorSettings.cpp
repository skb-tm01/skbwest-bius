#include "core/ByteConversion.h"
#include "peripherials/PeripherialsManager.h"
#include "peripherials/can/CanMessage.h"
#include "peripherials/can/packets/OutSensorSettings.h"

namespace CanPackets
{
    void OutSensorSettingsSet::SendRequest(OutSensorSettingsData data)
    {
        CanMessage msg{};
        msg.can_id = request_id;
        msg.can_dlc = 8;
        msg.data[0] = data.contactStateId;
        msg.data[1] = data.on_off;
        msg.data[2] = data.status;
        msg.data[3] = data.time_s;
        msg.data[4] = data.t_3ms&0xFF;
        msg.data[5] = data.t_3ms>>8;
        msg.data[6] = data.ton_3ms&0xFF;
        msg.data[7] = data.t_3ms>>8;

        auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
        if(canworker!=nullptr)
            canworker->AddMessage(msg, false);
    }

    void OutSensorSettingsSet::ParseResponse(uint8_t *)
    {
        return;
    }

    void OutSensorSettingsGet::SendRequest(uint8_t contactStateId)
    {
       CanMessage msg{};
       msg.can_id = request_id;
       msg.can_dlc = 1;
       msg.data[0] = contactStateId;

       auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
       if(canworker!=nullptr)
           canworker->AddMessage(msg, false);
    }

    OutSensorSettingsData OutSensorSettingsGet::ParseResponse(uint8_t *data)
    {
        OutSensorSettingsData msg{};
        msg.contactStateId = data[0];
        msg.on_off  = data[1];
        msg.status  = data[2];
        msg.time_s  = data[3];
        msg.t_3ms   = ByteConversion::make_uint16(data[4],data[5]);
        msg.ton_3ms = ByteConversion::make_uint16(data[6],data[7]);
        return msg;
    }

}
