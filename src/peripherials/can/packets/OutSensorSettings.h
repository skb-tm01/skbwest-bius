#pragma once

#include <cstdint>

namespace CanPackets
{
    struct OutSensorSettingsData
    {
        /**
         * @brief Сканирующий канал
         */
        uint8_t contactStateId;

        /**
         * @brief текущие состояние датчика
         */
        uint8_t on_off;

        /**
         * @brief флаг снятия с контроля
         */
        uint8_t status;

        /**
         * @brief время управления автоматического режима
         */
        uint8_t time_s;

        /**
         * @brief Время ШИМ
         */
        uint16_t t_3ms;

        /**
         * @brief время включенного состояния шим
         */
        uint16_t ton_3ms;
    };


    class OutSensorSettingsSet{
    public:
        static const uint32_t request_id = 0x03C;
        static const uint32_t response_id = 0x43C;
        static void SendRequest(OutSensorSettingsData data);
        static void ParseResponse(uint8_t *data);
    };

    class OutSensorSettingsGet{
    public:
        static const uint32_t request_id = 0x03D;
        static const uint32_t response_id = 0x43D;
        static void SendRequest(uint8_t contactStateId);
        static OutSensorSettingsData ParseResponse(uint8_t *data);
    };
}
