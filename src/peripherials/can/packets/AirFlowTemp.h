#pragma once

#include "Analog.h"
#include <cstdint>

namespace CanPackets
{
    struct CanPacketAirFlowTempResponseData{
        uint8_t type;
        uint8_t air_flow_temp; //in Celsius
    };
    const uint8_t CanPacketAirFlowTempResponseData_dlc = 0x002;
    static_assert (sizeof(CanPacketAirFlowTempResponseData)==CanPacketAirFlowTempResponseData_dlc,"CanPacketAirFlowTempResponseData size error");

    class AirFlowTemp{
    public:
        static const uint32_t request_id = 0x12;
        static const uint32_t response_id = 0x412;

        static void SendRequest(uint8_t type, bool persistent = false); //type should be zero
        static CanPacketAirFlowTempResponseData ParseResponse(uint8_t* data);
    };
}
