#include "peripherials/PeripherialsManager.h"
#include "peripherials/can/CanMessage.h"
#include "peripherials/can/packets/IPSState.h"

namespace CanPackets
{
    void IPSState::SendRequest(bool persistent)
    {    
       CanMessage msg{};
       msg.can_id = request_id;
       msg.can_dlc=1;
       msg.data[0]= 0;

       auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
       if(canworker!=nullptr)
           canworker->AddMessage(msg, persistent);
    }

    IPSStateData IPSState::ParseResponse(uint8_t *data)
    {
        IPSStateData msg;
        memcpy(msg.raw_data,data,5);
        return msg;
    }
}
