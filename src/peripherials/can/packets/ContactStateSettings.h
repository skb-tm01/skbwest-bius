
#pragma once

#include <cstdint>

namespace CanPackets
{
    struct ContactStateSettingsData
    {
        /**
         * @brief Сканирующий канал
         */
        uint8_t contactStateId;

        /**
         * @brief значение считанное с ацп преобразователя
         * @remark не учитывается при SettingsSet
         * @remark ADC
         */
        uint16_t adc_value;

        /**
         * @brief пороговое значение для контактного датчика
         * @remark LEVEl
         */
        uint16_t adc_threshold;

        /**
         * @brief текущие состояние датчика
         * @remark не учитывается при SettingsSet
         * @remark ON_OFF
         */
        uint8_t current_state;

        /**
         * @brief тип контактного датчика (подпор / нагрузка)
         * @remark UP_DN
         */
        uint8_t type;

        /**
         * @brief флаг снятия с контроля
         * @remark STATUS
         */
        uint8_t on_control;
    };


    class ContactStateSettingsSet{
    public:
        static const uint32_t request_id = 0x01E;
        static const uint32_t response_id = 0x41E;
        static void SendRequest(ContactStateSettingsData data);
        static void ParseResponse(uint8_t *data);
    };

    class ContactStateSettingsGet{
    public:
        static const uint32_t request_id = 0x01F;
        static const uint32_t response_id = 0x41F;
        static void SendRequest(uint8_t contactStateId);
        static ContactStateSettingsData ParseResponse(uint8_t *data);
    };
}
