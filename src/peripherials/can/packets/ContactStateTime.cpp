#include "core/ByteConversion.h"
#include "peripherials/PeripherialsManager.h"
#include "peripherials/can/CanMessage.h"
#include "peripherials/can/packets/ContactStateTime.h"

namespace CanPackets {
    void ContactStateTimeSet::SendRequest(CanPackets::ContactStateTimeData data)
    {
   CanMessage msg{};
   msg.can_id  = request_id;
   msg.can_dlc = 8;
   msg.data[0] = data.contactStateId;
   msg.data[1] = data.timeToEvent & 0xff;
   msg.data[2] = data.timeToEvent >> 8;
   msg.data[3] = data.timeCounter & 0xff;
   msg.data[4] = data.timeCounter >> 8;
   msg.data[5] = data.dependency;
   msg.data[6] = data.dependency_excess & 0xff;
   msg.data[7] = data.dependency_excess >> 8;

   auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
   if(canworker!=nullptr)
       canworker->AddMessage(msg, false);
    }

    void ContactStateTimeSet::ParseResponse(uint8_t *)
    {
        return;
    }

    void ContactStateTimeGet::SendRequest(uint8_t contactStateId)
    {
   CanMessage msg{};
   msg.can_id = request_id;
   msg.can_dlc=1;
   msg.data[0]= contactStateId;

   auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
   if(canworker!=nullptr)
       canworker->AddMessage(msg, false);
    }

    ContactStateTimeData ContactStateTimeGet::ParseResponse(uint8_t *data)
    {
        ContactStateTimeData msg;
        msg.contactStateId    = data[0];
        msg.timeToEvent       = ByteConversion::make_uint16(data[1], data[2]);
        msg.timeCounter       = ByteConversion::make_uint16(data[3], data[4]);
        msg.dependency        = data[5];
        msg.dependency_excess = ByteConversion::make_uint16(data[6], data[7]);
        return msg;
    }
}
