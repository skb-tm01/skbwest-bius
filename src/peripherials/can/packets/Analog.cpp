#include <cstring>

#include "core/ByteConversion.h"
#include "peripherials/PeripherialsManager.h"
#include "peripherials/can/CanMessage.h"
#include "peripherials/can/packets/Analog.h"

void CanPacketAnalogRequest::Send(CanPacketAnalogRequestData data, bool persistent)
{
    CanMessage msg{};
    msg.can_id = packet_id;
    msg.can_dlc = CanPacketAnalogRequestData_dlc;
    memcpy(&msg.data,&data,sizeof(data));

    auto* cansender = PeripherialsManager::instance().CAN_Worker_Get();
    if(cansender != nullptr)
        cansender->AddMessage(msg,persistent);
}

void CanPacketAnalogRequest::Send(CanPacketAnalogType type, bool persistent)
{
    CanPacketAnalogRequestData data{};
    data.type=type;
    Send(data, persistent);
}

CanPacketAnalogResponseData CanPacketAnalogResponse::Parse(uint8_t* data)
{
    CanPacketAnalogResponseData res{};
    res.type=static_cast<CanPacketAnalogType>(data[0]);
    res.state = ByteConversion::make_uint16(data[1],data[2]);
    res.adc =  ByteConversion::make_uint16(data[3],data[4]);
    return res;
}
