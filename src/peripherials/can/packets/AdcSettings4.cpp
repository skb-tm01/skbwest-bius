#include "peripherials/can/packets/AdcSettings4.h"
#include "peripherials/PeripherialsManager.h"
#include "core/ByteConversion.h"

void CanPackets::AdcSettings4Set::SendRequest(CanPackets::AdcSettings4Data data)
{
       CanMessage msg{};
       msg.can_id  = request_id;
       msg.can_dlc = 6;
       msg.data[0] = data.contactStateId;
       msg.data[1] = data.point_id;
       msg.data[2] = data.adc_point & 0xFF;
       msg.data[3] = data.adc_point >> 8;
       msg.data[4] = data.adc_point_data & 0xFF;
       msg.data[5] = data.adc_point_data >> 8;

       auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
       if(canworker!=nullptr)
           canworker->AddMessage(msg, false);
}

void CanPackets::AdcSettings4Set::ParseResponse(uint8_t *)
{
    return;
}

void CanPackets::AdcSettings4Get::SendRequest(uint8_t contactStateId, uint8_t point_id)
{
       CanMessage msg{};
       msg.can_id = request_id;
       msg.can_dlc=2;
       msg.data[0]= contactStateId;
       msg.data[1]= point_id;

       auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
       if(canworker!=nullptr)
           canworker->AddMessage(msg, false);
}

CanPackets::AdcSettings4Data CanPackets::AdcSettings4Get::ParseResponse(uint8_t *data)
{
    AdcSettings4Data msg;
    msg.contactStateId = data[0];
    msg.point_id = data[1];
    msg.adc_point =  ByteConversion::make_uint16(data[2],data[3]);
    msg.adc_point_data = ByteConversion::make_uint16(data[4],data[5]);
    return msg;
}
