#include "peripherials/PeripherialsManager.h"
#include "peripherials/can/CanMessage.h"
#include "peripherials/can/packets/ContactState.h"


void CanPacketContactStateRequest::Send(bool persistent)
{
   CanMessage msg{};
    msg.can_id = packet_id;
    msg.can_dlc=1;
    msg.data[0]= 0;

    auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
    if(canworker!=nullptr)
        canworker->AddMessage(msg, persistent);
}

CanPacketContactStateData CanPacketContactStateResponse::Parse(uint8_t *data)
{
    CanPacketContactStateData msg;
    memcpy(msg.raw_data,data,8);
    msg.byte_7 = data[7];
    return msg;
}
