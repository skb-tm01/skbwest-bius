#pragma once

#include <cstdint>

namespace CanPackets
{
    enum TahoSaveType: uint8_t{
       all       = 0, //запомнить все коэффициенты
       molotilka = 1, // запомнить обороты молотилки, нигде не используется
       fan       = 2, // запомнить обороты вентилятора, нигде не использутся
    };

    class TahoSave{
    public:
        static const uint32_t request_id = 0x00A;
        static const uint32_t response_id = 0x40A;
        static void SendRequest(TahoSaveType type);
        static TahoSaveType ParseResponse(uint8_t *data);
    };
}
