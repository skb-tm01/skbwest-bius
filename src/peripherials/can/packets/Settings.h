#pragma once

#include <cstdint>

enum CanPacketSettingsEngineType:uint8_t
{
    engine_with_electronic_control = 0,
    engine_without_electronic_control =1
};

enum CanPacketSettignsGrainType:uint8_t
{
    grain_wheat     = 0, //пшеница
    grain_oats      = 1, //овёс
    grain_alfalfa   = 2, //люцерна
    grain_buckwheat = 3, //гречиха
    grain_corn      = 4, //кукуруза
    grain_barley    = 5, //ячмень
    grain_rye       = 6, //рожь
    grain_clover    = 7, //клевер
    grain_colza     = 8, //рапс
    grain_soy       = 9, //соя
    grain_chickpea  =10, //нут
    grain_sunflower =11, //подсолнух
};

enum CanPacketSettingsType:uint8_t
{
    settings_diameter         =  0, //диаметр
    settings_width            =  1, //ширина
    settings_motor_por        =  2, //?
    settings_barab_por        =  3, //?
    settings_k_motor          =  4, // коэффициент двигателя
    settings_k_speed          =  5, // коэффициент по скорости
    settings_combine_model_1  =  6, // модель комбайна
    settings_wtf              =  7, // дробная часть ширины жатки ввода вывода
    settings_combine_sn       =  8, // номер комбайна
    settings_password_1       =  9, // пароль для изменения настроек ввода вывода
    settings_motor_eto        = 10, // ETO_motor_ee
    settings_motor_to1        = 11, // TO1_motor_ee
    settings_motor_to2        = 12, // TO2_motor_ee
    settings_password_2       = 13, // пароль для сброса статистик ввода вывода
    settings_engine_type      = 14, // тип двигателя
    settings_combine_model_3  = 15, // модель комбайна
    //16 -unknown
    settings_auto_man         = 17, // auto_man, 0 - свои, 1- рекомендуемый, 2 - ручной,
    settings_vid_zerno        = 18, // vid_zerno
    settings_test_poteri      = 19, // тип потерь зерна, если ответ 0-по площади, 1-по времени
    //settings_sv_potery_level1 = 20, // sv_potery_level1+vid_now*svoi_num
    //settings_sv_potery_level2 = 21, // sv_potery_level2+vid_now*svoi_num
    //settings_sb_baraban       = 22, // sv_baraban+vid_now*svoi_num
    //settings_sv_barab_zazor   = 23, // sv_barab_zazor+vid_now*svoi_num
    //settings_sv_ventiol       = 24, // sv_ventil+vid_now*svoi_num
    //settings_sv_zazor_up      = 25, // sv_zazor_up+vid_now*svoi_num)
    //settings_sv_zazor_down    = 26, // sv_zazor_down+vid_now*svoi_num

    settings_modes_6 = 20,
    settings_modes_7 = 21,
    settings_modes_1 = 22,
    settings_modes_2 = 23,
    settings_modes_3 = 24,
    settings_modes_4 = 25,
    settings_modes_5 = 26,

    settings_fuel_sensor_type = 50,

    settings_temperature_rev = 51,
    settings_timeinterval_rev = 52,
};

#pragma pack(push,1)
struct CanPacketSettingsGetResponseData
{
    CanPacketSettingsType type;
    uint16_t value;
};
#pragma pack(pop)
const uint8_t CanPacketSettingsGetResponse_dlc=3;
static_assert (sizeof(CanPacketSettingsGetResponseData)==CanPacketSettingsGetResponse_dlc,"CanPacketTahoResponse size error");


#pragma pack(push,1)
struct CanPacketSettingsSetResponseData
{
    CanPacketSettingsType type;
};
#pragma pack(pop)
const uint8_t CanPacketSettingsSetResponse_dlc=1;
static_assert (sizeof(CanPacketSettingsSetResponseData)==CanPacketSettingsSetResponse_dlc,"CanPacketSettingsSetResponse size error");

class CanPacketSettingsGetRequest
{
public:
    static const uint32_t packet_id = 0x00D; //13 in dec
    static void Send(CanPacketSettingsType type, bool persistent = false);
};


class CanPacketSettingsGetResponse
{
public:
    static const uint32_t packet_id = 0x40D; //1037 in dec
    static CanPacketSettingsGetResponseData Parse(uint8_t* data);
};

class CanPacketSettingsSetRequest
{
public:
    static const uint32_t packet_id = 0x00C; //12 in dec
    static void Send(CanPacketSettingsType type, uint16_t value);
};

class CanPacketSettingsSetResponse
{
public:
    static const uint32_t packet_id = 0x40C; //1036 in dec
    static CanPacketSettingsSetResponseData Parse(uint8_t* data);
};

