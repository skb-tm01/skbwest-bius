#pragma once

#include <cstdint>

namespace CanPackets
{
    class DataGet{
    public:
        static const uint32_t request_id = 0x099;
        static const uint32_t response_id = 0x499;
        static void SendRequest(uint16_t req_id);
        static void ParseResponse(uint8_t* data);
    };
}
