#include "peripherials/can/packets/RpmSettings1.h"
#include "peripherials/PeripherialsManager.h"
#include "core/ByteConversion.h"

void CanPackets::RpmSettings1Set::SendRequest(CanPackets::RpmSettings1Data data)
{
       CanMessage msg{};
       msg.can_id  = request_id;
       msg.can_dlc = 8;
       msg.data[0] = data.contactStateId;
       msg.data[1] = data.points;
       msg.data[2] = data.d_pulse & 0xFF;
       msg.data[3] = data.d_pulse >> 8;
       msg.data[4] = data.d_hz & 0xFF;
       msg.data[5] = data.d_hz >> 8;
       msg.data[6] = data.d_rpm & 0xFF;
       msg.data[7] = data.d_rpm >> 8;

       auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
       if(canworker!=nullptr)
           canworker->AddMessage(msg, false);
}

void CanPackets::RpmSettings1Set::ParseResponse(uint8_t *)
{
    return;
}

void CanPackets::RpmSettings1Get::SendRequest(uint8_t contactStateId)
{
       CanMessage msg{};
       msg.can_id = request_id;
       msg.can_dlc=1;
       msg.data[0]= contactStateId;

       auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
       if(canworker!=nullptr)
           canworker->AddMessage(msg, false);
}

CanPackets::RpmSettings1Data CanPackets::RpmSettings1Get::ParseResponse(uint8_t *data)
{
    RpmSettings1Data msg;
    msg.contactStateId = data[0];
    msg.points = data[1];
    msg.d_pulse = ByteConversion::make_uint16(data[2],data[3]);
    msg.d_hz = ByteConversion::make_uint16(data[4],data[5]);
    msg.d_rpm  = ByteConversion::make_uint16(data[6],data[7]);
    return msg;
}
