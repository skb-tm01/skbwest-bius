#include "core/ByteConversion.h"
#include "peripherials/PeripherialsManager.h"
#include "peripherials/can/packets/Probeg.h"

void CanPacketProbegRequest::Send(CanPacketProbegType type)
{
   CanMessage msg{};
   msg.can_id = packet_id;
   msg.can_dlc=1;
   msg.data[0]= type;

   auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
   if(canworker!=nullptr)
       canworker->AddMessage(msg, false);
}

CanPacketProbegResponseData CanPacketProbegResponse::Parse(uint8_t *data)
{
    CanPacketProbegResponseData msg;
    msg.type = static_cast<CanPacketProbegType>(data[0]);
    msg.value = ByteConversion::make_uint32(ByteConversion::make_uint16(data[1],data[2]), ByteConversion::make_uint16(data[3],data[4]));
    return msg;
}
