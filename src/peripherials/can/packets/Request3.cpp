#include "peripherials/PeripherialsManager.h"
#include "peripherials/can/CanMessage.h"
#include "peripherials/can/packets/Request3.h"

void CanPackets::Request3::SendRequest(bool isPersistent)
{
    CanMessage msg{};
     msg.can_id = request_id;
     msg.can_dlc=1;
     msg.data[0]= 0;

     auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
     if(canworker!=nullptr)
         canworker->AddMessage(msg, isPersistent);
}

CanPackets::Request3Data CanPackets::Request3::ParseResponse(uint8_t *data)
{
    Request3Data msg;
    memcpy(msg.raw_data,data,8);
    return msg;
}
