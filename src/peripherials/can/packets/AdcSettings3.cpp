#include "peripherials/can/packets/AdcSettings3.h"
#include "peripherials/PeripherialsManager.h"
#include "core/ByteConversion.h"

void CanPackets::AdcSettings3Set::SendRequest(CanPackets::AdcSettings3Data data)
{
       CanMessage msg{};
       msg.can_id  = request_id;
       msg.can_dlc = 3;
       msg.data[0] = data.contactStateId;
       msg.data[1] = data.val & 0xFF;
       msg.data[2] = data.val >> 8;

       auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
       if(canworker!=nullptr)
           canworker->AddMessage(msg, false);
}

void CanPackets::AdcSettings3Set::ParseResponse(uint8_t *)
{
    return;
}

void CanPackets::AdcSettings3Get::SendRequest(uint8_t contactStateId)
{
       CanMessage msg{};
       msg.can_id = request_id;
       msg.can_dlc=1;
       msg.data[0]= contactStateId;

       auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
       if(canworker!=nullptr)
           canworker->AddMessage(msg, false);
}

CanPackets::AdcSettings3Data CanPackets::AdcSettings3Get::ParseResponse(uint8_t *data)
{
    AdcSettings3Data msg;
    msg.contactStateId = data[0];
    msg.val =  ByteConversion::make_uint16(data[1],data[2]);
    return msg;
}
