#include "peripherials/PeripherialsManager.h"
#include "peripherials/can/CanMessage.h"
#include "peripherials/can/packets/TahoSave.h"

namespace CanPackets
{
    void TahoSave::SendRequest(TahoSaveType type)
    {
       CanMessage msg{};
       msg.can_id = request_id;
       msg.can_dlc=1;
       msg.data[0]= type;

       auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
       if(canworker!=nullptr)
           canworker->AddMessage(msg, false);
    }

    TahoSaveType TahoSave::ParseResponse(uint8_t *data)
    {
        return static_cast<TahoSaveType>(data[0]);
    }
}
