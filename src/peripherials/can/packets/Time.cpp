#include "peripherials/PeripherialsManager.h"
#include "peripherials/can/packets/Time.h"
#include "peripherials/can/CanMessage.h"

#include <QDateTime>

void CanPacketTimeResponse::SendCurrentTime()
{
   auto current_time = QDateTime::currentDateTime();
   CanPacketTimeResponseData msg{};

   msg.year   = static_cast<uint8_t>(current_time.date().year() % 100);
   msg.month  = static_cast<uint8_t>(current_time.date().month());
   msg.day    = static_cast<uint8_t>(current_time.date().day());
   msg.hour   = static_cast<uint8_t>(current_time.time().hour());
   msg.minute = static_cast<uint8_t>(current_time.time().minute());
   msg.second = static_cast<uint8_t>(current_time.time().second());

   Send(msg);
}

void CanPacketTimeResponse::Send(CanPacketTimeResponseData data)
{
    CanMessage msg{};
    msg.can_id = packet_id;
    msg.can_dlc = CanPacketTimeResponse_dlc;
    memcpy(&msg.data,&data,sizeof(data));

    auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
    if(canworker!=nullptr)
        canworker->AddMessage(msg, false);
}

void CanPackets::VersionTime::SendRequest(bool secondReq)
{
    CanMessage msg{};
    msg.can_id = request_id;
    msg.can_dlc = 1;
    msg.data[0] = secondReq;

    auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
    if(canworker!=nullptr)
        canworker->AddMessage(msg, false);
}

CanPackets::VersionTimeData CanPackets::VersionTime::ParseResponse(uint8_t* data)
{
    VersionTimeData msg{};
    msg.type = data[0];
    if(msg.type == 0)
    {
        memcpy(&msg.data,data+1,7);
    }
    else
    {
        memcpy(&msg.data,data+1,4);
    }

    msg.checksum = (data[7]<<8)+data[6];

    return msg;
}
