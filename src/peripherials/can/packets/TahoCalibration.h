#pragma once

#include "peripherials/can/packets/Taho.h"
#include <cstdint>

#pragma pack(push, 1)
struct CanPacketTahoCalibrationResponseData
{
    CanPacketTahoType type;
    uint16_t calibr_turns; //калибровочные обороты
    uint8_t  marks;        //количество меток на один оборот
    uint8_t  deviation;    //допустимое отклонение в процентах
};
#pragma pack(pop)

/**
 * @brief The CanPacketAnalogRequest class
 */
class CanPacketAnalogCalibrationRequest
{
public:
    static const uint32_t packet_id = 0x010;
    static void Send(CanPacketTahoType type);
};


/**
 * @brief The CanPacketAnalogResponse class
 */
class CanPacketTahoCalibrationResponse
{
public:
    static const uint32_t packet_id = 0x410;
    static CanPacketTahoCalibrationResponseData Parse(uint8_t* data);
};



