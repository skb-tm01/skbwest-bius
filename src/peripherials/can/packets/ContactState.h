#pragma once

#include <QDebug>
#include <cstdint>

#pragma pack(push, 1)
struct CanPacketContactStateData
{
    uint8_t raw_data[8];

    //b7
    union
    {
        struct
        {
            uint8_t analog_8:1;
            uint8_t analog_9:1;
            uint8_t analog_10:1;
            uint8_t on_work_mode:1;
            uint8_t diesel_on:1;
            uint8_t eto_required:1;
            uint8_t to1_required:1;
            uint8_t unk_7_7:1;      //(time_sekcia > 59)
         };
        uint8_t byte_7;
    };


};
#pragma pack(pop)


class CanPacketContactStateRequest
{
public:
    static const uint32_t packet_id = 0x004;
    static void Send(bool persistent = false);
};


class CanPacketContactStateResponse
{
public:
    static const uint32_t packet_id = 0x404;
    static CanPacketContactStateData Parse(uint8_t* data);
};

