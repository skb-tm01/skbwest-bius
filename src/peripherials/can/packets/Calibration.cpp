#include "core/ByteConversion.h"

#include "peripherials/can/packets/Calibration.h"
#include "peripherials/PeripherialsManager.h"
#include "peripherials/can/CanMessage.h"

void CanPackets::CalibrationInfoGet::SendRequest(CanPackets::CalibrationType type)
{
   CanMessage msg{};
   msg.can_id = request_id;
   msg.can_dlc=1;
   msg.data[0]= type;

   auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
   if(canworker!=nullptr)
       canworker->AddMessage(msg, false);
}

CanPackets::CalibrationInfoData CanPackets::CalibrationInfoGet::ParseResponse(uint8_t *data)
{
    CalibrationInfoData msg;
    msg.type = static_cast<CalibrationType>(data[0]);
    msg.adcval =ByteConversion::make_uint16(data[1],data[2]);
    return msg;
}

void CanPackets::CalibrationSave::SendRequest(CanPackets::CalibrationType type, uint16_t min, uint16_t max)
{
   CanMessage msg{};
   msg.can_id = request_id;
   msg.can_dlc = 8;
   msg.data[0] = type;
   msg.data[1] = min & 0xff;
   msg.data[2] = min >> 8;
   msg.data[3] = max & 0xff;
   msg.data[4] = max >> 8;

   auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
   if(canworker!=nullptr)
       canworker->AddMessage(msg, false);
}

void CanPackets::CalibrationSave::ParseResponse(uint8_t *)
{
    return;
}

