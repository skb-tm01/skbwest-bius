#pragma once

#include <cstdint>

namespace CanPackets
{
    #pragma pack(push, 1)
    struct IPSStateData
    {
        uint8_t raw_data[5];
    };
    #pragma pack(pop)


    class IPSState
    {
    public:
        static const uint32_t request_id = 0x070;
        static const uint32_t response_id = 0x470;

        static void SendRequest(bool persistent = false);
        static IPSStateData ParseResponse(uint8_t* data);
    };

}
