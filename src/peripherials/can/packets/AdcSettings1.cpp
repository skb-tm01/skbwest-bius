#include "peripherials/can/packets/AdcSettings1.h"
#include "peripherials/PeripherialsManager.h"
#include "core/ByteConversion.h"

void CanPackets::AdcSettings1Set::SendRequest(CanPackets::AdcSettings1Data data)
{
       CanMessage msg{};
       msg.can_id  = request_id;
       msg.can_dlc = 7;
       msg.data[0] = data.contactStateId;
       msg.data[1] = data.adc & 0xFF;
       msg.data[2] = data.adc >> 8;
       msg.data[3] = data.calibr_zero & 0xFF;
       msg.data[4] = data.calibr_zero >> 8;
       msg.data[5] = data.err_up& 0xFF;
       msg.data[6] = data.err_up >> 8;

       auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
       if(canworker!=nullptr)
           canworker->AddMessage(msg, false);
}

void CanPackets::AdcSettings1Set::ParseResponse(uint8_t *)
{
    return;
}

void CanPackets::AdcSettings1Get::SendRequest(uint8_t contactStateId)
{
       CanMessage msg{};
       msg.can_id = request_id;
       msg.can_dlc=1;
       msg.data[0]= contactStateId;

       auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
       if(canworker!=nullptr)
           canworker->AddMessage(msg, false);
}

CanPackets::AdcSettings1Data CanPackets::AdcSettings1Get::ParseResponse(uint8_t *data)
{
    AdcSettings1Data msg;
    msg.contactStateId = data[0];
    msg.adc = ByteConversion::make_uint16(data[1],data[2]);
    msg.calibr_zero = ByteConversion::make_uint16(data[3],data[4]);
    msg.err_up = ByteConversion::make_uint16(data[5],data[6]);
    return msg;
}
