#pragma once

#include <cstdint>

namespace CanPackets
{
    class Journal{
    public:
        static const uint32_t request_id = 0x09A;
        static const uint32_t response_id = 0x49A;
        static void SendRequest(uint16_t req_id);

    };
}
