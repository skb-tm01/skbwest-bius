#pragma once

#include <cstdint>

/**
 * @brief Contains types of available sensors for ANALOG request
 */
enum CanPacketAnalogType:uint8_t
{
    can_analog_type_oilpressure       = 0, //давление масла в двигателе
    can_analog_type_concaveclearance  = 1, //зазор подбарабанья
    can_analog_type_hydraulicpressure = 2, //получение давления масла в двигателе
    can_analog_type_hydraulic_oiltemp = 3, // получение температуры масла в г х_ч
    //4-unknown
    can_analog_type_fuel_level        = 5, // получение уровня топлива
    can_analog_type_engine_temp       = 6, // получение температуры двигателя
    can_analog_type_sieves_lower      = 7, // нижн решета
    can_analog_type_sieves_higher     = 8, // верх решета
    can_analog_type_power             = 9  // получение +24???
};

#pragma pack(push, 1)
struct CanPacketAnalogRequestData
{
    CanPacketAnalogType type;
};
#pragma pack(pop)
const uint8_t CanPacketAnalogRequestData_dlc = 0x001;
static_assert (sizeof(CanPacketAnalogRequestData)==CanPacketAnalogRequestData_dlc,"CanPacketAnalogRequestData size error");


#pragma pack(push, 1)
struct CanPacketAnalogResponseData
{
    CanPacketAnalogType type;
    uint16_t state; //текущие состояние датчика
    uint16_t adc;   //значение считанное с ацп преобразователя
};
#pragma pack(pop)
const uint8_t CanPacketAnalogResponseData_dlc = 0x005;
static_assert (sizeof(CanPacketAnalogResponseData)==CanPacketAnalogResponseData_dlc,"CanPacketAnalogResponseData size error");

/**
 * @brief The CanPacketAnalogRequest class
 */
class CanPacketAnalogRequest
{
public:
    static const uint32_t packet_id = 0x002;

    static void Send(CanPacketAnalogRequestData data, bool persistent = false);
    static void Send(CanPacketAnalogType type, bool persistent = false);
};


/**
 * @brief The CanPacketAnalogResponse class
 */
class CanPacketAnalogResponse
{
public:
    static const uint32_t packet_id = 0x402;
    static CanPacketAnalogResponseData Parse(uint8_t* data);
};



