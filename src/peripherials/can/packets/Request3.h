#pragma once

#include <cstdint>



namespace CanPackets
{
    struct Request3Data
    {
        uint8_t raw_data[8];
    };

    class Request3{
    public:
        static const uint32_t request_id = 0x003;
        static const uint32_t response_id = 0x403;
        static void SendRequest(bool isPersistent);
        static Request3Data ParseResponse(uint8_t *data);
    };
}
