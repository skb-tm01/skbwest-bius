#include <cstring>
#include "AirFlowTemp.h"
#include "peripherials/can/CanMessage.h"
#include "peripherials/PeripherialsManager.h"

void CanPackets::AirFlowTemp::SendRequest(uint8_t type, bool persistent)
{
    CanMessage msg{};
    msg.can_id = request_id;
    msg.can_dlc = 1;
    msg.data[0] = type;

    auto* cansender = PeripherialsManager::instance().CAN_Worker_Get();
    if(cansender != nullptr) {
        cansender->AddMessage(msg, persistent);
    }
}

CanPackets::CanPacketAirFlowTempResponseData CanPackets::AirFlowTemp::ParseResponse(uint8_t* data)
{
    CanPacketAirFlowTempResponseData res{};
    res.type=data[0];
    res.air_flow_temp=data[1];
    return res;
}
