#pragma once

#include <cstdint>

namespace CanPackets
{
    struct AdcSettings1Data
    {
        /**
         * @brief сканирующий канал
         */
        uint8_t contactStateId;

        /**
         * @brief значение считанное с ацп преобразователя
         */
        uint16_t adc;

        /**
         * @brief калибровачное значение нуля
         */
        uint16_t calibr_zero;

        /**
         * @brief верхнее ошибочное значение определение неисправности датчика
         */
        uint16_t err_up;
    };

    class AdcSettings1Set{
    public:
        static const uint32_t request_id = 0x028;
        static const uint32_t response_id = 0x428;
        static void SendRequest(AdcSettings1Data data);
        static void ParseResponse(uint8_t *data);
    };

    class AdcSettings1Get{
    public:
        static const uint32_t request_id = 0x029;
        static const uint32_t response_id = 0x429;
        static void SendRequest(uint8_t contactStateId);
        static AdcSettings1Data ParseResponse(uint8_t *data);
    };
}
