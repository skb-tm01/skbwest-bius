#include "CanDataRetriever.h"

#include "peripherials/PeripherialsManager.h"
#include "peripherials/can/CanWorker.h"
#include "peripherials/can/packets/Settings.h"
#include "peripherials/can/packets/DataGet.h"

#include <cstring>

CanDataRetriever::CanDataRetriever(RetrieverType retType, QObject *parent) : QObject(parent)
{
    combine_sn = 0;
    ret_type = retType;
    data.resize(0x8000);

    auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
    if(canworker!=nullptr)
    {
        QObject::connect(canworker,SIGNAL(received(CanMessage)),this,SLOT(OnCanDataMessage(CanMessage)));
        CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_combine_sn,false);
    }

}

void CanDataRetriever::StartExtraction()
{
    offset=0;
    start_addr =0;

    if(ret_type==RetrieverType::FUEL)
        start_addr = 0x8000;

    CanPackets::DataGet::SendRequest(start_addr + offset);
}

void CanDataRetriever::OnCanDataMessage(CanMessage msg)
{
    if(msg.can_id == CanPacketSettingsGetResponse::packet_id)
        OnCanDataMessage_Settings(msg);

    if(msg.can_id == CanPackets::DataGet::response_id)
        OnCanDataMessage_DataGet(msg);

}

void CanDataRetriever::OnCanDataMessage_Settings(CanMessage msg)
{
    CanPacketSettingsGetResponseData packet = CanPacketSettingsGetResponse::Parse(msg.data);
    if(packet.type != CanPacketSettingsType::settings_combine_sn)
        return;

    combine_sn = packet.value;

}

void CanDataRetriever::OnCanDataMessage_DataGet(CanMessage msg)
{
    memcpy(&data.data()[offset],msg.data,8);
    offset+=8;

    emit sig_progressChanged(offset);

    if(offset<0x8000)
    {
        CanPackets::DataGet::SendRequest(start_addr + offset);
    }
    else
    {
        emit sig_finished();
    }
}

QString CanDataRetriever::GetFilename()
{
    char sn_04u[5]{};
    sprintf(sn_04u,"%04u",combine_sn);
    auto fileName = QString::fromLatin1(sn_04u);

    if(ret_type == RetrieverType::FUEL)
        fileName += "FUEL";
    else
        fileName += "STAT";

    fileName+= ".bin";

    return fileName;
}

void CanDataRetriever::SetType(RetrieverType type)
{
    ret_type = type;
}

