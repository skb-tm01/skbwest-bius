#include <QDebug>

#include "core/SleepHelper.h"
#include "peripherials/can/CanWorker.h"
#include "peripherials/can/packets/Time.h"

#ifndef _MSC_VER
#include <sys/time.h>
#else
#include <WinSock2.h>
#undef SendMessage
#endif

CanWorker::CanWorker(QObject *parent) : QObject(parent)
{
    _backend = nullptr;
    _terminationRequested = false;
    _errCount = 0;
    _failOccurred = false;
    _receivedCount =0;
    _noBIUS = 0;
}

/**
 * @brief Setup worker thread
 * @param backend - pointer to CAN backend instance
 */
void CanWorker::Init(ICanBackend *backend)
{
    _failOccurred = false;
    _errCount = 0;
    _backend = backend;
}

bool CanWorker::IsConnected()
{
    return !_failOccurred;
}

/**
 * @brief
 * This function will be excuted in an own thread when start is called from parent thread
 * It will wait for data from CAN (blocking) and signal to GUI when data arrives
 */
void CanWorker::Run()
{
    while(!_terminationRequested)
    {
        if(!canmessages_once.IsEmpty())
        {
            SendMessage(canmessages_once.Dequeue());
            if(ReceiveMessage(300*1000))
            {
                _receivedCount++;
            }
        }

        //process persitent message queue
        else if(!canmessages_persistent.IsEmpty())
        {
            CanMessage msg = canmessages_persistent.Dequeue();
            SendMessage(msg);
            canmessages_persistent.Enqueue(msg);
            if(ReceiveMessage(300*1000))
            {
                _receivedCount++;
            }
        }

        //try to receive message even if we don't send anything to CAN bus
        else
        {
            if(ReceiveMessage(100*1000))
                _receivedCount++;
        }

        if(_receivedCount%10==0)
        {
            _receivedCount = 0;
            SleepHelper::usleep(90*1000);
        }

    }

    emit finished();
}

void CanWorker::setTerminationFlag()
{
    _terminationRequested = true;
}

void CanWorker::OnReceived(CanMessage msg)
{
    switch(msg.can_id)
    {
    case CanPacketTimeRequest::packet_id:
        CanPacketTimeResponse::SendCurrentTime();
        break;
    default:
        break;
    }
}

bool CanWorker::ReceiveMessage(long timeout_usec)
{
    bool ret = false;
    struct timeval tv;
    tv.tv_sec = 0;
    tv.tv_usec = timeout_usec;

    CanMessage msg = _backend->Message_Receive(tv);

    if(msg.can_id<0x400)
        _noBIUS++;
    else
        _noBIUS = 0;

    if(!msg.err_timeout && !msg.error_can && !msg.error && (_noBIUS == 0))
    {
        if(_failOccurred)
        {
            qDebug()<<"CONNECTION RESTORED";
            emit connectionRestored();
        }
        _failOccurred = false;
        _errCount = 0;

        emit received(msg);
        OnReceived(msg);
        ret = true;
    }
    else
    {
        _errCount++;
        if(!_failOccurred && (_errCount > RETRIES_COUNT || _noBIUS > NOBIUS_THRESHOLD))
        {
            _failOccurred=true;

            qDebug()<<"CONNECTION LOST";
            emit connectionLost();
        }
        ret = false;
    }

    if(msg.error)
    {
        emit error(QString("Error detected, error code: " + QString::number(msg.errorCode)));
        //qDebug()<<"Error detected, error code: " + QString::number(msg.errorCode);
    }

    if(msg.error_can)
    {
        emit error(QString("CAN Error detected, error code: ") + QString::number(msg.error_can));
        qDebug()<<QString("CAN Error detected, error code: ") + QString::number(msg.error_can);
    }

    return ret;
}

int CanWorker::SendMessage(CanMessage msg)
{
    if(_backend == nullptr)
        return -1;

    int result = _backend->Message_Send(msg);
    emit sent(msg);
    return result;
}

void CanWorker::AddMessage(CanMessage msg, bool isPersistent)
{
    if(isPersistent)
        canmessages_persistent.Enqueue(msg);
    else
        canmessages_once.Enqueue(msg);
}

void CanWorker::ForceConnectionLost()
{
    emit connectionLost();
}


