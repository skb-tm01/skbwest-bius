#pragma once

#include <cstdint>

#include <QMetaType>

#ifndef CAN_MAX_DLEN
#define CAN_MAX_DLEN 8
#endif


/**
 * @brief The CanMessage struct
 */
#pragma pack(push,1)
struct CanMessage
{

    /**
     * @brief 32 bit CAN_ID + EFF/RTR/ERR flags
     */
    uint32_t can_id;

    /**
     * @brief frame payload length in byte (0 .. CAN_MAX_DLEN)
     */
    uint8_t    can_dlc;

    uint8_t data[CAN_MAX_DLEN];

    /**
     * @brief extended - set to tru to send an extended frame
     */
    bool extended;

    /**
     * @brief rtr  - set to true to send a remote request (rtr)
     */
    bool rtr;

    /**
     * @brief error - set to true if any error occurred
     */
    bool error;

    /**
     * @brief error_can - error in can bus
     */
    bool error_can;

    /**
     * @brief errorCode - will be set to an error code
     */
    int  errorCode;

    /**
     * @brief nodata - set to true if timeout occurred
     */
    bool err_timeout;
};
#pragma pack(pop)

Q_DECLARE_METATYPE(CanMessage)
Q_DECLARE_METATYPE(CanMessage*)
