#pragma once

#include <QThread>

#include "core/Singleton.h"

#include "peripherials/audio/AudioWorker.h"
#include "peripherials/can/CanWorker.h"
#include "peripherials/gpio/HardwareKeyboard.h"
#include "peripherials/spi/IlluminationMonitor.h"


class PeripherialsManager : public Singleton<PeripherialsManager>
{
public:
    void CAN_Worker_Receiver_Initialize(ICanBackend* can_backend);
    CanWorker* CAN_Worker_Get();

    void GPIO_Keyboard_Initialize();
    HardwareKeyboard* GPIO_Keyboard_Get();

    void AudioWorker_Initialize();
    AudioWorker* AudioWorker_Get();

    void illumMonitor_Init();
    IlluminationMonitor* illumMonitor_Get();

private:
    QThread* can_worker_receiver_thread;
    CanWorker* can_worker_receiver_object;

    QThread* audioworker_thread;
    AudioWorker* audioworker_object;

    //keyboard
    HardwareKeyboard* _hardKeyb;

    //illumination
    IlluminationMonitor* _illumMonitor;
};
