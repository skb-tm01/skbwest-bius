#include <cstdlib>

#include <QApplication>
#include <QDebug>

#include "core/AudioHelper.h"
#include "core/helper.h"
#include "core/PathHelper.h"

#include "SettingsManager.h"

SettingsManager* SettingsManager::inst = nullptr;

SettingsManager::SettingsManager(QObject *parent): QObject(parent)
{
    _settings = new QSettings(PathHelper::pathAppend(QCoreApplication::applicationDirPath(),"config.ini"), QSettings::IniFormat, this);

    _volume = _settings->value(SETTINGS_TERMINAL_VOLUME,50).toInt();
    _brightness = _settings->value(SETTINGS_TERMINAL_BRIGHTNESS,30).toInt();

    _voiceMessages = _settings->value(SETTINGS_TERMINAL_VOICEMESSAGES, true).toBool();

    _language = static_cast<EnumLanguge>(_settings->value(SETTINGS_TERMINAL_LANGUAGE, static_cast<int>(EnumLanguge::Rus)).toInt());
    _nightDayMode = static_cast<EnumNightDayMode>(_settings->value(SETTINGS_TERMINAL_NIGHTANDDAY, static_cast<int>(EnumNightDayMode::Day)).toInt());
    _nightDayThreshold = _settings->value(SETTINGS_TERMINAL_NIGHTDAY_THRESHOLD,1024).toInt();

    _illumination = 1024;
    _lastNightDayMode = EnumNightDayMode::Auto;

    ApplyBrightness();
    ApplyNightDayMode(currentNightDayMode());
}

SettingsManager* SettingsManager::settings()
{
    if(inst == nullptr)
    {
        inst = new SettingsManager(QApplication::instance());
    }
    return inst;
}

void SettingsManager::Deinitialize()
{
    _settings->sync();
     delete _settings;
    _settings = nullptr;
}

int SettingsManager::getVolume()
{
#ifdef __linux
    return AudioHelper::GetAlsaVolume();
#endif
    return 0;
}

void SettingsManager::setVolume(int value)
{
#ifdef __linux
    return AudioHelper::SetAlsaVolume(value);
#endif
}

int SettingsManager::brightness()
{
    return _brightness;
}

void SettingsManager::setBrightness(int brightness)
{
    if(_brightness==brightness)
        return;

    _brightness = brightness;
    _settings->setValue(SETTINGS_TERMINAL_BRIGHTNESS, _brightness);
      SyncConfig();
    ApplyBrightness();
}

void SettingsManager::ApplyBrightness()
{
#ifdef BUILD_DEVICE
    system((QString("echo ")+QString::number(200 - _brightness) + QString(" > /sys/class/backlight/pwm-backlight.0/brightness")).toStdString().c_str());
#endif
}

bool SettingsManager::voiceMessages()
{
    return _voiceMessages;
}

void SettingsManager::setVoiceMessages(bool voiceMessages)
{
    if(_voiceMessages==voiceMessages)
        return;

    _voiceMessages = voiceMessages;
    _settings->setValue(SETTINGS_TERMINAL_VOICEMESSAGES, voiceMessages);
    SyncConfig();
}

EnumLanguge SettingsManager::language()
{
    return _language;
}

void SettingsManager::setLanguage(const EnumLanguge &languge)
{
    if(_language==languge)
        return;

    _language = languge;
    _settings->setValue(SETTINGS_TERMINAL_LANGUAGE, static_cast<int>(languge));
      SyncConfig();
}

EnumNightDayMode SettingsManager::nightDayMode()
{
    return _nightDayMode;
}

void SettingsManager::setNightDayMode(const EnumNightDayMode &nightDayMode)
{
    if(_nightDayMode==nightDayMode)
        return;

    _nightDayMode = nightDayMode;
    _settings->setValue(SETTINGS_TERMINAL_NIGHTANDDAY, static_cast<int>(nightDayMode));

    ApplyNightDayMode(currentNightDayMode());
    SyncConfig();
}

EnumNightDayMode SettingsManager::currentNightDayMode() const
{
    if(_nightDayMode==EnumNightDayMode::Auto)
        return (_illumination>_nightDayThreshold)?EnumNightDayMode::Day:EnumNightDayMode::Night;
    return _nightDayMode;
}

QColor SettingsManager::currentForeColor()
{
    return (currentNightDayMode()==EnumNightDayMode::Day) ? Helper::dayForeColor():Helper::nightForeColor();
}

QColor SettingsManager::currentBackColor()
{
    return (currentNightDayMode()==EnumNightDayMode::Day) ? Helper::dayBackColor():Helper::nightBackColor();
}

QColor SettingsManager::currentDisabledColor()
{
    return (currentNightDayMode()==EnumNightDayMode::Day) ? Helper::dayDisabledColor():Helper::nightDisabledColor();
}

int SettingsManager::getAlarmPassword()
{
    return _settings->value(SETTINGS_PASSWORD_ALARM,0).toInt();
}

int SettingsManager::getServicePassword()
{
    return _settings->value(SETTINGS_PASSWORD_SERVICE,0).toInt();
}

int SettingsManager::getAlarmStatsSpecialPassword()
{
    return 7788;
}

void SettingsManager::ApplyNightDayMode(EnumNightDayMode mode)
{
    if(_lastNightDayMode==mode)
        return;

    _lastNightDayMode=mode;

    QString checkbox_stylesheet(
        "QCheckBox::indicator:checked { image: url(\"%1/icons/checkbox_checked_%2.svg\"); }"
        "QCheckBox::indicator:unchecked { image: url(\"%1/icons/checkbox_unchecked_%2.svg\"); }"
    );
    QString checkbox_stylesheet_day = checkbox_stylesheet.arg(PathHelper::GetDataDirectory(),"day");
    QString checkbox_stylesheet_night = checkbox_stylesheet.arg(PathHelper::GetDataDirectory(),"night");

    if(_lastNightDayMode==EnumNightDayMode::Day)
    {
        qApp->setStyleSheet(
            Helper::LoadResource(PathHelper::pathAppend(PathHelper::GetDataDirectory(),"stylesheets/App.qss")) +
            Helper::LoadResource(PathHelper::pathAppend(PathHelper::GetDataDirectory(),"stylesheets/AppDay.qss")) +
            checkbox_stylesheet_day
        );
        emit NightDayChanged(_lastNightDayMode);
    }
    else if(_lastNightDayMode==EnumNightDayMode::Night)
    {
        qApp->setStyleSheet(
            Helper::LoadResource(PathHelper::pathAppend(PathHelper::GetDataDirectory(),"stylesheets/App.qss")) +
            Helper::LoadResource(PathHelper::pathAppend(PathHelper::GetDataDirectory(),"stylesheets/AppNight.qss")) +
            checkbox_stylesheet_night
        );
        emit NightDayChanged(_lastNightDayMode);
    }    
    else
        qDebug()<<"Need to check Night/Day mode (Now is Auto)";
}

void SettingsManager::setNightDayThreshold(int nightDayThreshold)
{
    if(_nightDayThreshold == nightDayThreshold)
        return;

    _nightDayThreshold = nightDayThreshold;

    _settings->setValue(SETTINGS_TERMINAL_NIGHTDAY_THRESHOLD, _nightDayThreshold);
    SyncConfig();

    ApplyNightDayMode(currentNightDayMode());
}

void SettingsManager::ApplyIllumination(double illumination)
{
    _illumination = illumination;

    if(_nightDayMode!=EnumNightDayMode::Auto)
        return;

    ApplyNightDayMode(currentNightDayMode());
}

void SettingsManager::SyncConfig()
{
     _settings->sync();
#ifdef __linux__
     system("sync -f &");
#endif
}
