#pragma once

#include <QDialog>
#include <QKeyEvent>
#include <QString>

namespace Ui {
    class PasswordEnterWindowWidget;
}

class PasswordEnterWindowWidget : public QDialog
{
    Q_OBJECT

public:
    explicit PasswordEnterWindowWidget(QWidget *parent = 0);
    ~PasswordEnterWindowWidget();
    int password();
    void SetTitle(QString title);

protected:
    void keyPressEvent(QKeyEvent* e);

private:
    Ui::PasswordEnterWindowWidget *ui;
};
