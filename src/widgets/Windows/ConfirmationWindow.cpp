#include "widgets/EventFilters/FocusEventFilter.h"
#include "widgets/Windows/ConfirmationWindow.h"

#include "ui_ConfirmationWindow.h"

#include <QPushButton>
#include "widgets/WindowManager.h"

ConfirmationWindow::ConfirmationWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ConfirmationWindow)
{
    ui->setupUi(this);

    setWindowFlags(Qt::Window
                   | Qt::FramelessWindowHint
                   | Qt::WindowTitleHint);

    ui->buttonBox->button(QDialogButtonBox::Ok)->setText(QString::fromUtf8("Ввод. ОК"));
    ui->buttonBox->button(QDialogButtonBox::Cancel)->setText(QString::fromUtf8("Esc. Отмена"));

    ui->buttonBox->button(QDialogButtonBox::Ok)->setStyleSheet("color: black; background-color: lime");
    ui->buttonBox->button(QDialogButtonBox::Cancel)->setStyleSheet("color: black; background-color: blue");
}

ConfirmationWindow::~ConfirmationWindow()
{
    delete ui;
}

void ConfirmationWindow::SetTitle(QString title)
{
    ui->title->setText(title);
}

void ConfirmationWindow::SetText(QString title)
{
    ui->label->setText(title);
}

void ConfirmationWindow::SetCancelVisibility(bool isVisible)
{
    ui->buttonBox->button(QDialogButtonBox::Cancel)->setVisible(isVisible);
}

void ConfirmationWindow::keyPressEvent(QKeyEvent *e)
{
    switch(e->key())
    {
    case Qt::Key_Escape:
        this->close();
        //  WindowManager::getInstance()->CloseMe(this);
        break;
    }
}
