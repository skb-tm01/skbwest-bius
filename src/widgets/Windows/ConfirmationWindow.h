#pragma once

#include <QDialog>
#include <QString>
#include <QKeyEvent>

namespace Ui {
    class ConfirmationWindow;
}

class ConfirmationWindow : public QDialog
{
    Q_OBJECT

public:
    explicit ConfirmationWindow(QWidget *parent = nullptr);
    ~ConfirmationWindow();

    void SetTitle(QString title);

    void SetText(QString title);

    void SetCancelVisibility(bool isVisible);

protected:
    void keyPressEvent(QKeyEvent* e);

private:
    Ui::ConfirmationWindow *ui;
};
