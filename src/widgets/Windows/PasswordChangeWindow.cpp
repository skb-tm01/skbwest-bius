#include "widgets/Windows/PasswordChangeWindow.h"

#include "ui_PasswordChangeWindow.h"
#include "widgets/WindowManager.h"
#include <QPushButton>

PasswordChangeWindowWidget::PasswordChangeWindowWidget(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PasswordChangeWindowWidget)
{
    ui->setupUi(this);
    setWindowFlags(Qt::Window
                   | Qt::FramelessWindowHint
                   | Qt::WindowTitleHint);

    ui->buttonBox->button(QDialogButtonBox::Ok)->setText(QString::fromUtf8("ОК"));
    ui->buttonBox->button(QDialogButtonBox::Cancel)->setText(QString::fromUtf8("Отмена"));

    ui->buttonBox->button(QDialogButtonBox::Ok)->setStyleSheet("color: black; background-color: lime");
    ui->buttonBox->button(QDialogButtonBox::Cancel)->setStyleSheet("color: black; background-color: blue");
}

PasswordChangeWindowWidget::~PasswordChangeWindowWidget()
{
    delete ui;
}

int PasswordChangeWindowWidget::oldPassword()
{
    return ui->password_old->number();
}


int PasswordChangeWindowWidget::newPassword()
{
    return ui->password_new->number();
}


void PasswordChangeWindowWidget::SetTitle(QString title)
{
    ui->title->setText(title);
}

void PasswordChangeWindowWidget::keyPressEvent(QKeyEvent *e)
{
    switch(e->key())
    {
    case Qt::Key_Escape:
        this->close();
        //WindowManager::getInstance()->CloseMe(this);
        break;
    }
}

