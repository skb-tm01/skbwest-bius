#include "widgets/EventFilters/FocusEventFilter.h"
#include "widgets/Windows/PasswordEnterWindow.h"

#include "ui_PasswordEnterWindow.h"
#include "widgets/WindowManager.h"
#include <QPushButton>

PasswordEnterWindowWidget::PasswordEnterWindowWidget(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PasswordEnterWindowWidget)
{
    ui->setupUi(this);

   auto* eventFilter = new FocusEventFilter(this);
   ui->buttonBox->button(QDialogButtonBox::Ok)->installEventFilter(eventFilter);
   ui->buttonBox->button(QDialogButtonBox::Cancel)->installEventFilter(eventFilter);

    setWindowFlags(Qt::Window
                   | Qt::FramelessWindowHint
                   | Qt::WindowTitleHint);


    ui->buttonBox->button(QDialogButtonBox::Ok)->setText(QString::fromUtf8("ОК"));
    ui->buttonBox->button(QDialogButtonBox::Cancel)->setText(QString::fromUtf8("Отмена"));

    ui->buttonBox->button(QDialogButtonBox::Ok)->setStyleSheet("color: black; background-color: lime");
    ui->buttonBox->button(QDialogButtonBox::Cancel)->setStyleSheet("color: black; background-color: blue");
}

PasswordEnterWindowWidget::~PasswordEnterWindowWidget()
{
    delete ui;
}

int PasswordEnterWindowWidget::password()
{
    return ui->passwordBox->number();
}

void PasswordEnterWindowWidget::SetTitle(QString title)
{
    ui->title->setText(title);
}

void PasswordEnterWindowWidget::keyPressEvent(QKeyEvent *e)
{
    switch(e->key())
    {
    case Qt::Key_Escape:
        this->close();
        //WindowManager::getInstance()->CloseMe(this);
        break;
    }
}
