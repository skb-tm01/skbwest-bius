#pragma once

#include <QDialog>
#include <QKeyEvent>

namespace Ui {
    class PasswordChangeWindowWidget;
}

class PasswordChangeWindowWidget : public QDialog
{
    Q_OBJECT

public:
    explicit PasswordChangeWindowWidget(QWidget *parent = 0);
    ~PasswordChangeWindowWidget();
    int newPassword();
    int oldPassword();
    void SetTitle(QString title);

protected:
    void keyPressEvent(QKeyEvent* e);

private:
    Ui::PasswordChangeWindowWidget *ui;
};
