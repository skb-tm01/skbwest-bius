#include "BEVirtualKeyboard.h"

BEVirtualKeyboard::BEVirtualKeyboard(QWidget *parent)
	: QWidget(parent, Qt::Tool | Qt::WindowStaysOnTopHint)
{
    setAttribute(Qt::WA_GroupLeader,true);
	setupUi(this);

	lastFocusedWidget = NULL;

    connect(qApp, SIGNAL(focusChanged(QWidget*,QWidget*)), this, SLOT(saveFocusWidget(QWidget*,QWidget*)));
	
	connect(_Btn_1, SIGNAL(clicked()),this, SLOT(OnClickedSymbolButton()));
	connect(_Btn_2, SIGNAL(clicked()),this, SLOT(OnClickedSymbolButton()));
	connect(_Btn_3, SIGNAL(clicked()),this, SLOT(OnClickedSymbolButton()));
	connect(_Btn_4, SIGNAL(clicked()),this, SLOT(OnClickedSymbolButton()));
	connect(_Btn_5, SIGNAL(clicked()),this, SLOT(OnClickedSymbolButton()));
	connect(_Btn_6, SIGNAL(clicked()),this, SLOT(OnClickedSymbolButton()));
	connect(_Btn_7, SIGNAL(clicked()),this, SLOT(OnClickedSymbolButton()));
	connect(_Btn_8, SIGNAL(clicked()),this, SLOT(OnClickedSymbolButton()));
	connect(_Btn_9, SIGNAL(clicked()),this, SLOT(OnClickedSymbolButton()));
	connect(_Btn_0, SIGNAL(clicked()),this, SLOT(OnClickedSymbolButton()));

	connect(_Btn_Comma, SIGNAL(clicked()),this, SLOT(OnClickedSymbolButton()));

	connect(_Btn_BackSpace, SIGNAL(clicked()),this, SLOT(OnClickedSymbolButton()));

      connect(_Btn_UpArrow, SIGNAL(clicked()),this, SLOT(OnClickedSymbolButton()));
      connect(_Btn_DownArrow, SIGNAL(clicked()),this, SLOT(OnClickedSymbolButton()));
      connect(_Btn_LeftArrow, SIGNAL(clicked()),this, SLOT(OnClickedSymbolButton()));
      connect(_Btn_RightArrow, SIGNAL(clicked()),this, SLOT(OnClickedSymbolButton()));
      connect(_BtnClose, SIGNAL(clicked()),this, SLOT(OnClickedSymbolButton()));

}

BEVirtualKeyboard::~BEVirtualKeyboard()
{

}

bool BEVirtualKeyboard::event(QEvent *e)
{
    if(e->type() == QEvent::WindowActivate && lastFocusedWidget != nullptr)
        lastFocusedWidget->activateWindow();
	return QWidget::event(e);
}

void BEVirtualKeyboard::saveFocusWidget(QWidget * /*oldFocus*/, QWidget *newFocus)
{
    if(newFocus == nullptr)
        return;

    if (!this->isAncestorOf(newFocus))
        lastFocusedWidget = newFocus;

    if(isVisible())
        this->hide();
}

//! [2]

//! [3]

void BEVirtualKeyboard::OnClickedSymbolButton()
{
	QWidget *w  = qobject_cast<QWidget*>(sender());
    if(w==nullptr)
		return;

    QVariant prop = w->property("keyCode");
	int keyCode = 0;
	QString text;
	if(prop.isValid())
	{
		keyCode = qvariant_cast<int>(prop);
	}
	else
	{
		QChar chr = qvariant_cast<QChar>(w->property("buttonValue"));
		keyCode = chr.unicode();
		text = chr;
	}	
	emit characterGenerated(keyCode,text);
}

