#include "BEVirtualKeyboard.h"
#include "BEVirtualKeyboardContext.h"

#include <QDesktopWidget>
#include <QKeyEvent>
#include <QPointer>

BEVirtualKeyboardContext::BEVirtualKeyboardContext()
	: QInputContext()
{
	_Keyboard = new BEVirtualKeyboard();
	connect(_Keyboard, SIGNAL(characterGenerated(int,QString)), SLOT(sendCharacter(int,QString)));
}

BEVirtualKeyboardContext::~BEVirtualKeyboardContext()
{
	delete _Keyboard;
}
#include<QDebug>
bool BEVirtualKeyboardContext::filterEvent(const QEvent* event)
{
    if (event->type() == QEvent::RequestSoftwareInputPanel)
    {
		updatePosition();
		_Keyboard->show();
		return true;
    }
    else if (event->type() == QEvent::CloseSoftwareInputPanel)
    {
		_Keyboard->hide();
		return true;
	}
    else if (event->type() == QEvent::KeyPress)
    {
        QKeyEvent *ke = static_cast<QKeyEvent*>(const_cast<QEvent*>(event));
        if (ke->key() == Qt::Key_Escape)
        {
            _Keyboard->hide();
            return true;
        }
    }

	return false;
}

//! [1]

QString BEVirtualKeyboardContext::identifierName()
{
	return "BEVirtualKeyboardContext";
}

void BEVirtualKeyboardContext::reset()
{
}

bool BEVirtualKeyboardContext::isComposing() const
{
	return false;
}

QString BEVirtualKeyboardContext::language()
{
	return "ru_RU";
}

//! [2]

void BEVirtualKeyboardContext::sendCharacter(int keyCode, QString text)
{
	QPointer<QWidget> w = focusWidget();

	if (!w)
		return;

	QKeyEvent keyPress(QEvent::KeyPress, keyCode, Qt::NoModifier, text);
	QApplication::sendEvent(w, &keyPress);

	if (!w)
		return;

	QKeyEvent keyRelease(QEvent::KeyRelease, keyCode, Qt::NoModifier, QString());
	QApplication::sendEvent(w, &keyRelease);
}

//! [2]

//! [3]

void BEVirtualKeyboardContext::updatePosition()
{
	QWidget *widget = focusWidget();
	if (!widget)
		return;

	/*QRect widgetRect = widget->rect();
	QPoint panelPos = QPoint(widgetRect.left(), widgetRect.bottom() + 2);
	panelPos = widget->mapToGlobal(panelPos);*/

  QPoint panelPos = LocatePanelPosition(widget, _Keyboard);

	_Keyboard->move(panelPos);
}

QPoint BEVirtualKeyboardContext::LocatePanelPosition(QWidget* sourceControl, QWidget* shownPanel)
{
  QRect sourceRect = sourceControl->frameGeometry();  
  QPoint sourceLocation = qobject_cast<QWidget*>(sourceControl->parent())->mapToGlobal(sourceRect.topLeft());

  QRect panelRect = shownPanel->frameGeometry();  
  
  QRect workingArea = QApplication::desktop()->screenGeometry(sourceControl);

  int margin = 3;

  QPoint destLocation;
  if(sourceLocation.x() > (workingArea.x() + workingArea.width() / 2))
    destLocation.setX(sourceLocation.x() - panelRect.width() - margin);
  else
    destLocation.setX(sourceLocation.x() + sourceRect.width() + margin);

  destLocation.setY(sourceLocation.y() - panelRect.height() / 2);

  if(destLocation.y() < margin)
    destLocation.setY(margin);
  if(destLocation.y() + panelRect.height() > workingArea.y() + workingArea.height() - margin)
    destLocation.setY(workingArea.y() + workingArea.height() - margin - panelRect.height());
  return destLocation;
}
