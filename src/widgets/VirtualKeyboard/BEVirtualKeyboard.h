#ifndef BEVIRTUALKEYBOARD_H
#define BEVIRTUALKEYBOARD_H

#include <QWidget>
#include <QSignalMapper>
#include "ui_BEVirtualKeyboard.h"

class BEVirtualKeyboard : public QWidget, public Ui::BEVirtualKeyboard
{
	Q_OBJECT

public:
	BEVirtualKeyboard(QWidget *parent = 0);
	~BEVirtualKeyboard();


signals:
	void characterGenerated(int keyCode, QString text);

protected:
	bool event(QEvent *e);

private slots:
	void saveFocusWidget(QWidget *oldFocus, QWidget *newFocus);
	void OnClickedSymbolButton();

private:
	QWidget *lastFocusedWidget;
	QSignalMapper signalMapper;
};

#endif // BEVIRTUALKEYBOARD_H
