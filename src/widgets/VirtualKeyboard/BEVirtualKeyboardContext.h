#pragma once

#include <QtGlobal>
#include <QInputContext>

class BEVirtualKeyboard;
class BEVirtualKeyboardContext : public QInputContext
{
	Q_OBJECT

public:
	BEVirtualKeyboardContext();
	~BEVirtualKeyboardContext();

	bool filterEvent(const QEvent* event);

	QString identifierName();
	QString language();

	bool isComposing() const;

	void reset();

	private slots:
		void sendCharacter(int keyCode, QString text);

private:
	void updatePosition();
  QPoint LocatePanelPosition(QWidget* sourceControl, QWidget* shownPanel);

private:
	BEVirtualKeyboard *_Keyboard;
	
};
