#include <QFocusEvent>
#include <QPainter>
#include <QPen>
#include <QPixmap>

#include "alerts/AlertDescription.h"

#include "core/AudioHelper.h"
#include "core/helper.h"

#include "widgets/ToInstructions.h"
#include "widgets/SensorStateInstructions.h"
#include "widgets/sensors_statistics_full.h"

#include "widgets/SensorWidget.h"
#include "ui_SensorWidget.h"
#include "widgets/WindowManager.h"

SensorWidget::SensorWidget(QWidget *parent, ISensor* s) :
    QFrame(parent),
    ui(new Ui::SensorWidget)
{
    ui->setupUi(this);
    _sensor = s;

    is_day = true;

    _pixmap = Helper::LoadPixmap(_sensor->GetIconName());
    _alertPixmap = Helper::LoadPixmap("warning.svg",34);
    _selected = false;

    _parent = parent;
}

SensorWidget::~SensorWidget()
{
    delete ui;
}

#include <QDebug>

void SensorWidget::ShowSensorInfo(bool showServiceMenu)
{
    auto *w = new SensorStateInstructions(this);
    auto* pw = window();
    w->resize(pw->size());
    w->move(pw->pos());
    w->SetServiceMenuButtonVisiblity(showServiceMenu);
    WindowManager::getInstance()->Open(_parent,w);
    AudioHelper::PlayWaveFile(_sensor->GetAlertDescription().GetSoundFile("ru"),RingingType::No);
}

void SensorWidget::mousePressEvent(QMouseEvent* event)
{
    if(!isSelected())
        Select();
    else
        ShowSensorInfo(sensors_statistics_full::Instance->ServiceMenuState());

    event->accept();
}

void SensorWidget::paintEvent(QPaintEvent *event)
{
    QColor forecolor = is_day?Helper::dayForeColor():Helper::nightForeColor();

    bool on_control = _sensor->IsUnderControl();

    int iconSize = 64;

    QPixmap icon = Helper::ScalePixmap(_pixmap, iconSize);
    icon = Helper::SetPixmapColor(icon, forecolor);

    QRect iconRect((width()-icon.width())/2, (height()-icon.height())/2, icon.width(), icon.height());
    QRect iconPlace((width()-iconSize+2)/2, (height()-iconSize+2)/2, iconSize-2, iconSize-2);

    QPainter painter(this);
    painter.drawPixmap(iconRect, icon);

    if(!on_control)
    {
        QColor pencolor(qRgb(0, 0, 255));
        QPen pen(pencolor, 5, Qt::SolidLine, Qt::RoundCap);
        painter.setPen(pen);
        painter.drawLine(iconPlace.topLeft(), iconPlace.bottomRight());
        painter.drawLine(iconPlace.bottomLeft(), iconPlace.topRight());
    }

    bool isBroken = _sensor->IsBroken();

    if(isBroken)
    {
        QPoint alertPos(width()-_alertPixmap.width()-3,3);
        painter.drawPixmap(alertPos, _alertPixmap);
    }
}

void SensorWidget::setDayNight(bool day_now)
{
    is_day = day_now;
    update();
}

void SensorWidget::setSelected(bool value)
{
    if(_selected==value)
        return;
    _selected=value;
    selectedChanged(this);

    if(_selected)
        setStyleSheet("SensorWidget{border-color: lime; border-width: 6px; }");

    else
        setStyleSheet("");
}


