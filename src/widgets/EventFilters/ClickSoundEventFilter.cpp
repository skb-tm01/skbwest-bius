#include <QCoreApplication>
#include <QEvent>

#include "core/AudioHelper.h"
#include "core/PathHelper.h"
#include "widgets/EventFilters/ClickSoundEventFilter.h"

ClickSoundEventFilter::ClickSoundEventFilter(QObject *parent) : QObject(parent)
{

}

bool ClickSoundEventFilter::eventFilter(QObject *, QEvent *event)
{
    if(event->type() == QEvent::KeyPress || event->type() == QEvent::MouseButtonPress)
    {
        AudioHelper::PlayWaveFileWhenQueryIsEmpty(PathHelper::pathAppend(PathHelper::GetDataDirectory(),"sounds/key_press.wav"));
    }

    return false;
}
