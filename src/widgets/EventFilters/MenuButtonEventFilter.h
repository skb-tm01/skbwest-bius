#ifndef MENUBUTTONEVENTFILTER_H
#define MENUBUTTONEVENTFILTER_H

#include <QObject>
#include <QDebug>
#include <QKeyEvent>

class MenuButtonEventFilter : public QObject
{
    Q_OBJECT
public:
    explicit MenuButtonEventFilter(QObject *parent = nullptr);

protected:
  bool eventFilter(QObject *obj, QEvent *ev);

signals:
    void MenuButtonPressed();

};

#endif // MENUBUTTONEVENTFILTER_H
