#include "IdleEventFilter.h"
#include <QDebug>
#include <QDateTime>

IdleEventFilter* IdleEventFilter::instance = nullptr;

IdleEventFilter::IdleEventFilter(QObject* parent) : QObject(parent)
{
    onPause = false;
    QObject::connect(&_timer, SIGNAL(timeout()), this, SIGNAL(IdleOccurred()));
    IdleEventFilter::instance = this;
}

int IdleEventFilter::getIdleTime()
{
    return _timer.interval();
}

void IdleEventFilter::setIdleTime(int value)
{
    _timer.setInterval(value);
}

void IdleEventFilter::Pause()
{
    onPause = true;
    _timer.stop();
}

void IdleEventFilter::Resume()
{
    onPause =false;
    _timer.start();
}

bool IdleEventFilter::eventFilter(QObject *, QEvent *ev)
{
  if(ev->type() == QEvent::KeyPress || ev->type() == QEvent::MouseButtonPress)
  {
      if(!onPause)
          _timer.start();
  }

  return false;
}
