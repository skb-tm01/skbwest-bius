#pragma once

#include <QObject>
#include <QVector>

#include "core/Singleton.h"

class KeyPressRedirectFilter : public QObject
{
    Q_OBJECT
public:
    explicit KeyPressRedirectFilter(QObject* redirectTarget, QObject *parent = 0);
    explicit KeyPressRedirectFilter(QObject* redirectTarget, QVector<int> keys_to_catch, QObject *parent = 0);
protected:
    bool eventFilter(QObject *obj, QEvent *event);
private:
    QObject* _target;
    QVector<int> _keys_include;
};

