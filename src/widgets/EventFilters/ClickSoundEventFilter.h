#pragma once

#include <QObject>

class ClickSoundEventFilter: public QObject
{
    Q_OBJECT

public:
    ClickSoundEventFilter(QObject* parent = nullptr);

protected:
    bool eventFilter(QObject *obj, QEvent *event);
};
