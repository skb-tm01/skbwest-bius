#include "widgets/EventFilters/KeyPressRedirectFilter.h"

#include <QApplication>
#include <QEvent>
#include <QKeyEvent>

KeyPressRedirectFilter::KeyPressRedirectFilter(QObject *redirectTarget, QObject *parent): QObject(parent)
{
    _target = redirectTarget;
}

KeyPressRedirectFilter::KeyPressRedirectFilter(QObject *redirectTarget, QVector<int> keys_to_catch, QObject *parent)
{
    _target = redirectTarget;
    _keys_include = keys_to_catch;
}

bool KeyPressRedirectFilter::eventFilter(QObject *, QEvent *event)
{
    if(event->type()!=QEvent::KeyPress)
        return false;

    QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);

    if(keyEvent->key() == Qt::Key_Return || keyEvent->key() == Qt::Key_Enter)
        return false;

    if(!_keys_include.empty()){
        if(!_keys_include.contains(keyEvent->key())){
                return false;
        }
    }

    QApplication::sendEvent(_target,new QKeyEvent(keyEvent->type(),keyEvent->key(),keyEvent->modifiers()));

    return true;
}
