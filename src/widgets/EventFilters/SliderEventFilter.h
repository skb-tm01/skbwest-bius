#pragma once

#include <QObject>
#include <QWidget>

class SliderEventFilter : public QWidget
{
    Q_OBJECT
public:
    explicit SliderEventFilter(QWidget *parent = nullptr);

protected:
    bool eventFilter(QObject *obj, QEvent *event);
};
