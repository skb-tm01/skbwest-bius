#pragma once

#include <QObject>

#include "core/Singleton.h"

class FocusEventFilter : public QObject
{
    Q_OBJECT
public:
    explicit FocusEventFilter(QObject *parent = 0): QObject(parent) {};
protected:
    bool eventFilter(QObject *obj, QEvent *event);
};

