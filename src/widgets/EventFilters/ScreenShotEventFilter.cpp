#include "ScreenShotEventFilter.h"

#include <QApplication>
#include <QKeyEvent>
#include <QDesktopWidget>
#include <QDateTime>
#include <QDir>
#include <QPixmap>
#include <QScreen>

#include "core/PathHelper.h"

ScreenShotEventFilter::ScreenShotEventFilter(QObject *parent) : QObject(parent)
{
    dir = QDir(PathHelper::pathAppend(PathHelper::GetAppDirectory(),"screenshots/"));
    if(!dir.exists())
        dir.mkpath(".");
}

bool ScreenShotEventFilter::eventFilter(QObject *obj, QEvent *ev)
{
    if(ev->type()!=QEvent::KeyPress)
        return false;

    QKeyEvent *keyEvent = static_cast<QKeyEvent *>(ev);

    if(keyEvent->key() != Qt::Key_F7)
        return false;

    auto img = QPixmap::grabWindow(QApplication::desktop()->winId());
    img.save(PathHelper::pathAppend(dir.absolutePath(),
                                    QDateTime::currentDateTimeUtc().toString().replace(":","").replace(" ","_")+".png"));

    return false;
}
