#pragma once

#include <QDir>
#include <QObject>

class ScreenShotEventFilter : public QObject
{
    Q_OBJECT
public:
    explicit ScreenShotEventFilter(QObject *parent = nullptr);

protected:
  bool eventFilter(QObject *obj, QEvent *ev);
signals:

private:
  QDir dir;

public slots:
};
