#ifndef IDLEEVENTFILTER_H
#define IDLEEVENTFILTER_H

#include <QObject>
#include <QEvent>
#include <QTimer>

class IdleEventFilter : public QObject
{
    Q_OBJECT
public:
    IdleEventFilter(QObject* parent = nullptr);
    int getIdleTime();
    void setIdleTime(int value);
    void Pause();
    void Resume();

    static IdleEventFilter* instance;

protected:
  bool eventFilter(QObject *obj, QEvent *ev);

signals:
  void IdleOccurred();

private:
  bool onPause;
  QTimer _timer;

};

#endif // IDLEEVENTFILTER_H
