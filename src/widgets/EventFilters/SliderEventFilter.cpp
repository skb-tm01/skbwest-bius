#include "SliderEventFilter.h"

#include <QApplication>
#include <QKeyEvent>
#include <QSlider>

SliderEventFilter::SliderEventFilter(QWidget *parent) : QWidget(parent)
{

}

bool SliderEventFilter::eventFilter(QObject *obj, QEvent *event)
{
    QSlider* cb = qobject_cast<QSlider*>(obj);
    if(cb==nullptr)
        return false;

    if(event->type()!=QEvent::KeyPress)
        return false;

    QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);


    switch(keyEvent->key())
    {
        case Qt::Key_Up:
            QApplication::instance()->postEvent(obj, new QKeyEvent(QEvent::KeyPress,Qt::Key_Tab,Qt::ShiftModifier));
            return true;
        case Qt::Key_Down:
            QApplication::instance()->postEvent(obj, new QKeyEvent(QEvent::KeyPress,Qt::Key_Tab,Qt::NoModifier));
            return true;
        default:
            break;
    }

    return false;
}
