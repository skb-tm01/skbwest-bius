#pragma once

#include <QObject>

class MousePressRedirectFilter : public QObject
{
    Q_OBJECT
public:
    explicit MousePressRedirectFilter(QObject* redirectTarget, QObject *parent = 0);
protected:
    bool eventFilter(QObject *obj, QEvent *event);
private:
    QObject* _target;
};

