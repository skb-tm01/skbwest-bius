#include "widgets/EventFilters/mousepressredirectfilter.h"

#include <QApplication>
#include <QEvent>
#include <QMouseEvent>
#include <QDebug>

MousePressRedirectFilter::MousePressRedirectFilter(QObject *redirectTarget, QObject *parent): QObject(parent)
{
    _target = redirectTarget;
}

bool MousePressRedirectFilter::eventFilter(QObject *, QEvent *event)
{
    if(event->type()!=QEvent::MouseButtonPress && event->type() != QEvent::MouseButtonRelease) {
        return false;
    }

    QMouseEvent *mouseEvent = static_cast<QMouseEvent*>(event);
    QApplication::sendEvent(_target,new QMouseEvent(*mouseEvent));
    return true;
}
