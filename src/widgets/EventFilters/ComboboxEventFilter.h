#pragma once

#include <QObject>

class ComboboxEventFilter : public QObject
{
    Q_OBJECT
public:
    explicit ComboboxEventFilter(QObject *parent = nullptr);

signals:

protected:
    bool eventFilter(QObject *obj, QEvent *event);
};
