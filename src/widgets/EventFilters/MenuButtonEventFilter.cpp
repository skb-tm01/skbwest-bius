#include "MenuButtonEventFilter.h"


MenuButtonEventFilter::MenuButtonEventFilter(QObject *parent) : QObject(parent)
{

}


bool MenuButtonEventFilter::eventFilter(QObject *obj, QEvent *ev)
{
    if(obj == nullptr) {
        return false;
    }

    if(ev->type()!=QEvent::KeyPress) {
        return false;
    }

    if(obj->objectName() != "MainWindow" && !obj->objectName().startsWith("Menu")) {
        return false;
    }


  QKeyEvent *keyEvent = static_cast<QKeyEvent *>(ev);
  if(keyEvent->key() == Qt::Key_Menu)
  {
        qDebug() << "MENU FILTER" << obj->objectName();
        emit MenuButtonPressed();
  }
  return false;
}
