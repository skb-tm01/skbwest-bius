#include "CheckBoxEventFilter.h"

#include <QCheckBox>
#include <QKeyEvent>


CheckBoxEventFilter::CheckBoxEventFilter(QObject *parent) : QObject(parent)
{

}

bool CheckBoxEventFilter::eventFilter(QObject *obj, QEvent *event)
{
    QCheckBox* cb = qobject_cast<QCheckBox*>(obj);
    if(cb==nullptr)
        return false;

    if(event->type()!=QEvent::KeyPress)
        return false;

    QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);


    switch(keyEvent->key())
    {
        case Qt::Key_Return:
        case Qt::Key_Enter:
            cb->click();
            return true;
        default:
            break;
    }

    return false;
}
