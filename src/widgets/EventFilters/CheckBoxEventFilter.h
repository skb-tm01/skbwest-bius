#pragma once
#include <QObject>

class CheckBoxEventFilter : public QObject
{
    Q_OBJECT
public:
    explicit CheckBoxEventFilter(QObject *parent = nullptr);

signals:

protected:
    bool eventFilter(QObject *obj, QEvent *event);
};
