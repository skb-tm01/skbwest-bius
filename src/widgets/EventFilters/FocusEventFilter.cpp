#include "FocusEventFilter.h"
#include <QEvent>
#include <QWidget>
#include "../../SettingsManager.h"

bool FocusEventFilter::eventFilter(QObject *obj, QEvent *event)
{
    auto* widget = dynamic_cast<QWidget*>(obj);
    if (widget == NULL)
        return false;

    if (event->type() == QEvent::FocusIn)
    {
        widget->setStyleSheet(""
                              "QPushButton{font-weight: bold;  border: 3px solid lime; background-color: #bdbdbd;} "
                              "QFrame{ border-width: 3px; }"
                              "QComboBox { border: 3px solid lime; background-color: #03c503; }"
                              "QSlider::handle:horizontal { background: lime;}"
                              "QCheckBox { border: 3px solid lime; }"
                              );
    }
    else if(event->type() == QEvent::FocusOut)
    {
        QColor backColor = SettingsManager::settings()->currentBackColor();
        widget->setStyleSheet("QPushButton{} QFrame{ border-width: 1px; }"
                              "QComboBox { border: 3px solid grey; background-color: " + backColor.name() + "; }"
                              "QCheckBox { border: 3px solid " + backColor.name() + ";}"
                              );
    }

    return false;
}
