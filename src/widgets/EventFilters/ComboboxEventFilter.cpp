#include "ComboboxEventFilter.h"

#include <QApplication>
#include <QComboBox>
#include <QKeyEvent>

ComboboxEventFilter::ComboboxEventFilter(QObject *parent) : QObject(parent)
{

}

bool ComboboxEventFilter::eventFilter(QObject *obj, QEvent *event)
{
    QComboBox* cb = qobject_cast<QComboBox*>(obj);
    if(cb==nullptr)
        return false;

    if(event->type()!=QEvent::KeyPress)
        return false;

    QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);


    switch(keyEvent->key())
    {
        case Qt::Key_Return:
        case Qt::Key_Enter:
            cb->showPopup();
            return true;
        case Qt::Key_Left:
        case Qt::Key_Up:
            QApplication::instance()->postEvent(obj, new QKeyEvent(QEvent::KeyPress,Qt::Key_Tab,Qt::ShiftModifier));
            return true;
        case Qt::Key_Right:
        case Qt::Key_Down:
            QApplication::instance()->postEvent(obj, new QKeyEvent(QEvent::KeyPress,Qt::Key_Tab,Qt::NoModifier));
            return true;
        default:
            break;
    }

    return false;
}
