#include "widgets/Controls/Clock.h"

#include <QtGui>
#include <QTimer>

#include "ui_Clock.h"

Clock::Clock(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Clock)
{
    ui->setupUi(this);
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(showTime()));
    timer->start(5000);
    showTime();
}

Clock::~Clock()
{
    delete ui;
}

void Clock::showTime()
{
    QTime time = QTime::currentTime();
    QString t = time.toString("hh:mm");
    ui->labelTime->setText(t);
    QDate date = QDate::currentDate();
    QString d = date.toString("dd.MM.yyyy");
    ui->labelDate->setText(d);
}

