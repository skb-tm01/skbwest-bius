#include <QDebug>
#include <QKeyEvent>
#include <QPainter>
#include <QSpacerItem>

#include "widgets/Controls/ArrowKeyControl.h"
#include "widgets/EventFilters/FocusEventFilter.h"

#include "ui_ArrowKeyControl.h"

#define STYLESHEET_SELECTED_ACTIVE "QLabel {font: %1px; background-color: #189FF5 }"
#define STYLESHEET_SELECTED_NOTACTIVE "QLabel {font: %1px }"
#define STYLESHEET_DESELECTED  "QLabel {font: %1px; }"
#define STYLESHEET_FOCUS_ON "ArrowKeyControl{ border: 3px solid lime; }"
#define STYLESHEET_FOCUS_OFF ""

///
/// Ctors/Dtors
///

///
/// \brief ArrowKeyControl::ArrowKeyControl
/// \param parent
///
ArrowKeyControl::ArrowKeyControl(QWidget *parent) : QFrame(parent), ui(new Ui::ArrowKeyControl)
{
    setAttribute(Qt::WA_InputMethodEnabled);

    ui->setupUi(this);

    this->installEventFilter(this);

    _base = 0;
    _digitCapacity = 0;
    _selectedPosition = 0;
    _leftmostBitIndex = 0;
    _isEditable = false;
    _fontsize = 36;
    _limitHigh = 0;
    _limitLow = 0;

    _longPressTimer_KeyUp.setInterval(2000);
    _longPressTimer_KeyDown.setInterval(2000);
    _longPressTimer_KeyUp.setSingleShot(true);
    _longPressTimer_KeyDown.setSingleShot(true);

    QObject::connect(&_longPressTimer_KeyUp, SIGNAL(timeout()), this, SIGNAL(longArrowUp()));
    QObject::connect(&_longPressTimer_KeyDown, SIGNAL(timeout()), this, SIGNAL(longArrowDown()));

    setFocusPolicy(Qt::StrongFocus);

    init();
}

///
/// \brief ArrowKeyControl::~ArrowKeyControl
///
ArrowKeyControl::~ArrowKeyControl()
{
    delete ui;
}


///
/// Initializers
///

///
/// \brief ArrowKeyControl::init
/// \param digitCapacity
/// \param base
///
void ArrowKeyControl::init(int digitCapacity, int base)
{
    _base = base;
    _digitCapacity = qBound(1,digitCapacity,digitCapacity);
    _limitHigh = powerBase(digitCapacity) - 1;
    _limitLow = 0;

    //resize vectors
    for(auto* label: _digits)
    {
        delete label;
    }
    _digits.resize(_digitCapacity);
    _digitsAsInt.resize(_digitCapacity);

    auto fontsize = QString::number(_fontsize);
    for (int i = 0; i < _digitCapacity; i++)
    {
       _digits[i] = new QLabel(this);
       _digits[i]->setStyleSheet(QString(STYLESHEET_DESELECTED).arg(fontsize));
       _digits[i]->setSizePolicy(QSizePolicy(QSizePolicy::Policy::Expanding,QSizePolicy::Policy::Preferred));
       _digits[i]->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
       _digits[i]->setVisible(true);
       _digits[i]->installEventFilter(this);
       _digits[i]->setText("0");
    }

    for (int i = _digitCapacity-1; i >= 0; i--)
    {
        ui->horizontalLayout_2->addWidget(_digits[i]);
    }

    select(_digitCapacity-1);
    setEditable(false);
    computeLeftmostBitIndex();
}

///
/// Getters
///

///
/// \brief ArrowKeyControl::digitCapacity
/// \return
///
int ArrowKeyControl::digitCapacity()
{
    return _digitCapacity;
}

///
/// \brief ArrowKeyControl::leftmostDigitIndex
/// \return
///
int ArrowKeyControl::leftmostDigitIndex()
{
    return _leftmostBitIndex;
}

///
/// \brief ArrowKeyControl::number
/// \return
///
int ArrowKeyControl::number()
{
    if(_leftmostBitIndex<0)
        return -1;

    int number=0;

    for (int i = 0; i < _digitCapacity; i++)
    {
        number += _digitsAsInt[i]*powerBase(i);
    }
    return number;
}

///
/// \brief ArrowKeyControl::selectedPosition
/// \return
///
int ArrowKeyControl::selectedPosition()
{
    return _selectedPosition;
}

bool ArrowKeyControl::isBeingEdited()
{
    return _isEditable;
}

int ArrowKeyControl::limitLow()
{
    return _limitLow;
}

int ArrowKeyControl::limitHigh()
{
    return _limitHigh;
}

///
/// Setters
///

///
/// \brief ArrowKeyControl::setEditable
/// \param setEditable
///
void ArrowKeyControl::setEditable(bool setEditable)
{
    _isEditable = setEditable;
    UpdateStylesheets();
    update();
}

///
/// \brief ArrowKeyControl::setNumber
/// \param value
///
void ArrowKeyControl::setNumber(int value)
{
    clearNumber();

    if(value<0)
        return;

    if((powerBase(_digitCapacity+1)-1)<value)
        return;

    if(value>_limitHigh)
        value = _limitHigh;
    if(value<_limitLow)
        value = _limitLow;

    for(int i=0;;i++)
    {
        if (value<=0)
            break;

        _digitsAsInt[i] = value%_base;
        value = value - _digitsAsInt[i];
        value = value/_base;
    }

    updateAllLabels();
    computeLeftmostBitIndex();
    checkLimit();
}

void ArrowKeyControl::setFontSize(int value)
{
    _fontsize = value;
    UpdateStylesheets();
}

void ArrowKeyControl::setLimitLow(int value)
{
    _limitLow = value;
    checkLimit();
}

void ArrowKeyControl::setLimitHigh(int value)
{
    _limitHigh = value;
    checkLimit();
}

///
/// EVENTS
///

///
/// \brief ArrowKeyControl::focusInEvent
/// \param event
///
void ArrowKeyControl::focusInEvent(QFocusEvent * event)
{
    UpdateStylesheets();
    QWidget::focusInEvent(event);
}

///
/// \brief ArrowKeyControl::focusOutEvent
/// \param event
///
void ArrowKeyControl::focusOutEvent(QFocusEvent * event)
{
    setEditable(false);
    UpdateStylesheets();
    QWidget::focusOutEvent(event);
}

///
/// \brief ArrowKeyControl::keyPressEvent
/// \param e
///
void ArrowKeyControl::keyPressEvent(QKeyEvent* e)
{
    switch(e->key())
    {
    case Qt::Key_Left:

        if(!_isEditable)
            QApplication::instance()->postEvent(this, new QKeyEvent(QEvent::KeyPress,Qt::Key_Tab,Qt::ShiftModifier));
        else
            select(_selectedPosition+1);
        break;
    case Qt::Key_Right:
        if(!_isEditable)
            QApplication::instance()->postEvent(this, new QKeyEvent(QEvent::KeyPress,Qt::Key_Tab,Qt::NoModifier));
        else
            select(_selectedPosition-1);
        break;
    case Qt::Key_Up:
        _longPressTimer_KeyUp.start();
        if(_isEditable)
            increment(selectedPosition());
        else
            QApplication::instance()->postEvent(this, new QKeyEvent(QEvent::KeyPress,Qt::Key_Tab,Qt::ShiftModifier));
        break;
    case Qt::Key_Down:
        _longPressTimer_KeyDown.start();
        if(!_isEditable)
            QApplication::instance()->postEvent(this, new QKeyEvent(QEvent::KeyPress,Qt::Key_Tab,Qt::NoModifier));
        else
            decrement(selectedPosition());
        break;
    case Qt::Key_Return:
    case Qt::Key_Enter:
        if(_isEditable)
        {
            emit returnPressed();
            setEditable(false);
        }
        else
        {
            setEditable(true);
        }
        QApplication::sendEvent(this, new QEvent(QEvent::CloseSoftwareInputPanel));
        break;
    case Qt::Key_Escape:
        if(_isEditable)
            emit returnPressed();
        break;
    case Qt::Key_Backspace:
        backspacePressed();
        break;
    case Qt::Key_0:
        numKeyPressed(0);
        break;
    case Qt::Key_1:
        numKeyPressed(1);
        break;
    case Qt::Key_2:
        numKeyPressed(2);
        break;
    case Qt::Key_3:
        numKeyPressed(3);
        break;
    case Qt::Key_4:
        numKeyPressed(4);
        break;
    case Qt::Key_5:
        numKeyPressed(5);
        break;
    case Qt::Key_6:
        numKeyPressed(6);
        break;
    case Qt::Key_7:
        numKeyPressed(7);
        break;
    case Qt::Key_8:
        numKeyPressed(8);
        break;
    case Qt::Key_9:
        numKeyPressed(9);
        break;
    }
}

void ArrowKeyControl::keyReleaseEvent(QKeyEvent *e)
{
    switch(e->key())
    {
    case Qt::Key_Up:
        _longPressTimer_KeyUp.stop();
        break;
    case Qt::Key_Down:
        _longPressTimer_KeyDown.stop();
        break;
    case Qt::Key_Escape:
        QApplication::sendEvent(this, new QEvent(QEvent::CloseSoftwareInputPanel));

        if(_isEditable)
        {
            qDebug()  << "ArrowKeyControl::keyReleaseEvent()->send editable false";
            emit returnPressed();
            setEditable(false);
        }
        else
        {
            if(parent()!=nullptr){
                qDebug() << "ArrowKeyControl::keyReleaseEvent()->send ekey";
                QApplication::sendEvent(parent(),new QKeyEvent(QEvent::KeyPress,e->key(),e->modifiers(),e->text()));
            }
        }
    }
}

///
/// \brief ArrowKeyControl::mousePressEvent
/// \param event
///
void ArrowKeyControl::mousePressEvent(QMouseEvent *event)
{
    if(_isEditable)
    {
        QEvent event(QEvent::RequestSoftwareInputPanel);
        QApplication::sendEvent(this, &event);
    }
    else
    {
        setEditable(true);
    }

    QWidget::mousePressEvent(event);
}

///
/// EVENT FILTER
///

///
/// \brief ArrowKeyControl::eventFilter
/// \param obj
/// \param ev
/// \return
///
bool ArrowKeyControl::eventFilter(QObject *obj, QEvent *ev)
{
    if(ev->type()==QEvent::MouseButtonPress)
    {
        auto* selected_label = qobject_cast<QLabel*>(obj);
        if(selected_label==nullptr)
            return false;

        for(int i =0;i<_digits.size();i++)
        {
            if (_digits[i]==selected_label)
            {
                select(i);
                return false;
            }
        }
    }
    else if(ev->type()==QEvent::FocusIn)
    {
        if(qobject_cast<ArrowKeyControl*>(obj)!=nullptr)
            this->setStyleSheet(STYLESHEET_FOCUS_ON);
    }
    else if(ev->type()==QEvent::FocusOut)
    {
        if(qobject_cast<ArrowKeyControl*>(obj)!=nullptr)
            this->setStyleSheet(STYLESHEET_FOCUS_OFF);
    }
    return false;
}


///
/// Private functions
///

///
/// \brief ArrowKeyControl::increment
/// \param position
///
void ArrowKeyControl::increment(int position)
{
    if (position<0 || position >= _digitCapacity)
        return;

    if (_digitsAsInt[position] >= _base-1 )
        _digitsAsInt[position]=0;
    else
        _digitsAsInt[position]++;

    updateAllLabels();
    computeLeftmostBitIndex();
    checkLimit();
}

///
/// \brief ArrowKeyControl::decrement
/// \param position
///
void ArrowKeyControl::decrement(int position)
{
    if (position<0 || position >= _digitCapacity)
        return;

    if(_digitsAsInt[position]<=0)
        _digitsAsInt[position]=_base-1;
    else
        _digitsAsInt[position]--;

    updateAllLabels();
    computeLeftmostBitIndex();
    checkLimit();
}


///
/// \brief ArrowKeyControl::clearNumber
///

void ArrowKeyControl::clearNumber()
{
    for (int i = 0; i < _digitCapacity; i++)
    {
       _digitsAsInt[i] = 0;
    }
    updateAllLabels();

    _leftmostBitIndex=0;
}

void ArrowKeyControl::numKeyPressed(int num)
{
    if (!_isEditable)
        return;

    if ( num<0 || num>(_base-1))
        return;

    if ((_leftmostBitIndex==(_digitCapacity-1)) && (_selectedPosition==-1))
        return;

    // если он старший и уменьшили до 0
    // пересчитать индекс старшего

    if(_selectedPosition == -1)
    {
        //стоим на нулевой(cursor) позиции - все сдвигаем влево на позицию
        // и вводим число на свободную позицию
        // фокус не меняется
        leftShift();
        _digitsAsInt[0]=static_cast<uint8_t>(num);
        _digits[0]->setText(QString::number(_digitsAsInt[0]));

        computeLeftmostBitIndex();
    }
    else
    {
        // стоим не на нулевой позиции - заменяем число
        // фокус сдвигаем вправо
        _digitsAsInt[_selectedPosition]=static_cast<uint8_t>(num);
        _digits[_selectedPosition]->setText(QString::number(_digitsAsInt[_selectedPosition]));
        select(_selectedPosition-1);
    }
    computeLeftmostBitIndex();
    if (num==0)
    {
        _digits[0]->setVisible(true);
    }

    checkLimit();
}

///
/// сдвигает разряды в сторону старших, 0 разряд = 0
void ArrowKeyControl::leftShift()
{
    for (int i = _digitCapacity-1 ; i > 0; i--)
    {
        _digitsAsInt[i]= _digitsAsInt[i-1];
    }
    _digitsAsInt[0] = 0;

    updateAllLabels();
    computeLeftmostBitIndex();
}

///
/// \brief ArrowKeyControl::backspacePressed
///
void ArrowKeyControl::backspacePressed()
{
    _digitsAsInt[_selectedPosition] = 0;
    updateAllLabels();
    computeLeftmostBitIndex();
}

///
/// \brief ArrowKeyControl::updateAllLabels
///
void ArrowKeyControl::updateAllLabels()
{
    for (int i = 0; i < _digitCapacity; i++)
        _digits[i]->setText(QString::number(_digitsAsInt[i],_base).toUpper());
}

/**
 * @brief Computes leftmost bit for widget and stores it in _leftmostBitIndex variable
 */
void ArrowKeyControl::computeLeftmostBitIndex()
{
    for (int i = _digitCapacity-1; i>=0; i--)
    {
        if(_digitsAsInt[i]!=0)
        {
            _leftmostBitIndex = i;
            return;
        }
    }

    _leftmostBitIndex = 0;
}

void ArrowKeyControl::checkLimit()
{
    if(number()>_limitHigh)
        setNumber(_limitHigh);

    if(number()<_limitLow)
        setNumber(_limitLow);
}


void ArrowKeyControl::UpdateStylesheets()
{
    for(int i=0;i<_digits.size();i++)
    {
        auto fontsize = QString::number(_fontsize);
        if(!hasFocus())
            _digits[i]->setStyleSheet(QString(STYLESHEET_DESELECTED).arg(fontsize));
        else if(_isEditable)
            _digits[i]->setStyleSheet(i==_selectedPosition
                                      ?QString(STYLESHEET_SELECTED_ACTIVE).arg(fontsize)
                                      :QString(STYLESHEET_DESELECTED).arg(fontsize));
        else
            _digits[i]->setStyleSheet(i==_selectedPosition
                                      ?QString(STYLESHEET_SELECTED_NOTACTIVE).arg(fontsize)
                                      :QString(STYLESHEET_DESELECTED).arg(fontsize));
    }
}


int ArrowKeyControl::powerBase(int i)
{
    if (i<0)
        return 0;

    if (i==0)
        return 1;

    int result = _base;
    for(int j=1;j<i;j++)
    {
        result = result*_base;
    }
    return result;
}

void ArrowKeyControl::select(int position)
{
    if (position>=_digitCapacity)
    {
        select(0);
        return;
    }

    if (position<0)
    {
        select(_digitCapacity-1);
        return;
    }

    if (position==_selectedPosition)
        return;

    _selectedPosition = position;

    UpdateStylesheets();
    updateAllLabels();
    computeLeftmostBitIndex();
}



