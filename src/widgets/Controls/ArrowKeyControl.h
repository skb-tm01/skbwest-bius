#pragma once

#include <QKeyEvent>
#include <QLabel>
#include <QWidget>
#include <QTimer>

namespace Ui
{
    class ArrowKeyControl;
}

class ArrowKeyControl : public QFrame
{
    Q_OBJECT

signals:
    void returnPressed();
    void longArrowUp();
    void longArrowDown();

public:
    //ctors/dtors
    explicit ArrowKeyControl(QWidget *parent = nullptr);
    ~ArrowKeyControl();

    //initializer
    void init(int digitCapacity=4, int base=10);

    //getters
    int digitCapacity();
    int leftmostDigitIndex();
    int number();
    int selectedPosition();
    bool isBeingEdited();

    int limitLow();
    int limitHigh();

    //setters
    void setEditable(bool setEditable);
    void setNumber(int value);
    void setFontSize(int value);
    void setLimitLow(int value);
    void setLimitHigh(int value);

protected:
    //events
    void keyPressEvent(QKeyEvent* e);
    void keyReleaseEvent(QKeyEvent* e);
    void focusInEvent(QFocusEvent * event);
    void focusOutEvent(QFocusEvent * event);
    void mousePressEvent(QMouseEvent *event);


    //eventfilter
    bool eventFilter(QObject *obj, QEvent *ev);

private:
    Ui::ArrowKeyControl *ui;

    void increment(int position);
    void decrement(int position);

    void select(int position);
    int powerBase(int i);

    void numKeyPressed(int num);
    void backspacePressed();
    void leftShift();
    void updateAllLabels();
    void clearNumber();
    void computeLeftmostBitIndex();
    void checkLimit();


    void UpdateStylesheets();

    //fields
    QVector<QLabel*> _digits;
    QVector<uint8_t> _digitsAsInt;

    QTimer _longPressTimer_KeyUp;
    QTimer _longPressTimer_KeyDown;

    int  _selectedPosition;
    int  _leftmostBitIndex;
    int  _digitCapacity;
    bool _isEditable;
    int  _base;
    int _fontsize;
    int _limitLow;
    int _limitHigh;
};
