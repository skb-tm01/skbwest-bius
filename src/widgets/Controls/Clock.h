#pragma once

#include <QWidget>

namespace Ui {
    class Clock;
}

class Clock : public QWidget
{
    Q_OBJECT

public:
    explicit Clock(QWidget *parent = 0);
    ~Clock();

private slots:
    void showTime();

private:
    Ui::Clock *ui;
};
