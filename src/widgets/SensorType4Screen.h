#pragma once

#include <QWidget>
#include <QKeyEvent>
#include <QTimer>


#include "peripherials/sensors/ISensor.h"
#include "peripherials/can/CanMessage.h"

namespace Ui {
class SensorType4Screen;
}

class SensorType4Screen : public QWidget
{
    Q_OBJECT

public:
    explicit SensorType4Screen(ISensor* sensor, QWidget *parent = 0);
    ~SensorType4Screen();
protected:
    void keyPressEvent(QKeyEvent* e);
private slots:
    void on_Btn1Close_clicked();

    //can
    void OnCanMessage(CanMessage msg);
    void OnCanConnectionLost();
    void OnCanConnectionRestore();
    void SendSettings();


    void on_akc_onoff_returnPressed();
    void on_akc_status_returnPressed();
    void on_akc_times_returnPressed();
    void on_akc_t3ms_returnPressed();
    void on_akc_ton3ms_returnPressed();

private:

    ISensor* _sensor;
    Ui::SensorType4Screen *ui;
    QTimer timer;
};
