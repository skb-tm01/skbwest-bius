#pragma once

#include "qcgauge.h"

class QCNeedle;
class QCLabel;
class QCArc;
class QCTicks;
class QCValues;
class QCBand;

class FunGauge : public QCGauge
{
    Q_OBJECT
public:
    explicit FunGauge(QWidget *parent = nullptr);

    inline QString units() const;
    void setUnits(const QString &units);
    void SwitchRanges(float value);

    void setGaugeColor(const QColor &gaugeColor);
    void setBands(float redStart, float middle, float greenEnd);

public slots:
    void setCurrentValue(float value);

private:
    bool _extendedRange;

    QCBand* _redBand;
    QCBand* _greenBand;
    QCLabel* _labelValue;
    QCLabel* _labelUnits;
    QCArc* _inArc;
    QCArc* _outArc;

};
