#include "qcband.h"
#include"qcgauge.h"

QCBand::QCBand(QCGauge *parent) : QCScale(parent)
{
    _beginValue = 10;
    _endValue =90;
    _rWidth = 5;
}

QPainterPath QCBand::createSubBand(float from, float sweep)
{
    QRectF rect = adjustRect(_parent->gaugeFullRect(),_rPos);
    QPainterPath path;
    path.arcMoveTo(rect,from);
    path.arcTo(rect,from,sweep);
    return path;
}

float QCBand::beginValue() const
{
    return _beginValue;
}

void QCBand::setBeginValue(float beginValue)
{
    _beginValue = beginValue;
}

float QCBand::endValue() const
{
    return _endValue;
}

void QCBand::setEndValue(float endValue)
{
    _endValue = endValue;
}

void QCBand::setBand(float beginValue, float endValue)
{
    if(beginValue<_minValue)
        beginValue = _minValue;
    else if(beginValue> _maxValue)
        beginValue = _maxValue;

    if(endValue > _maxValue)
        endValue = _maxValue;

    _beginValue = beginValue;
    _endValue = endValue;
    update();
}

void QCBand::draw(QPainter *painter)
{
    float r = _parent->radius();
    QPen pen;
    pen.setCapStyle(Qt::FlatCap);
    pen.setWidthF(rPosToPoints(r,_rWidth));
    painter->setBrush(Qt::NoBrush);

    float beg = getDegFromValue(_beginValue);
    float sweep = getDegFromValue(_endValue) - beg;

    QPainterPath path = createSubBand(beg,sweep);
    pen.setColor(_color);
    painter->setPen(pen);
    painter->drawPath(path);

}

void QCBand::setrWidth(float rWidth)
{
    _rWidth = rWidth;
    update();
}
