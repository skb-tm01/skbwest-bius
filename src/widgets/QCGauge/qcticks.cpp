#include "qcticks.h"
#include"qcgauge.h"


QCTicks::QCTicks(QCGauge *parent) : QCScale(parent)
{
    _step = 10;
    _subTicks = false;

    _rLength = 5;
    _rWidth = 3;
}


void QCTicks::draw(QPainter *painter)
{
    painter->setPen(_color);

    float r = _parent->radius();

    float origin = rPosToPoints(r,_rPos);
    float length = rPosToPoints(r,_rLength);
    float width = rPosToPoints(r,_rWidth);

    for(float val = _minValue;val<=_maxValue;val+=_step)
    {
        float deg = getDegFromValue(val);
        QPointF start = _parent->point(origin, deg);
        QPointF end = _parent->point(origin+length, deg);

        QPen pen;
        pen.setColor(_color);
        if(_subTicks)
            pen.setWidthF(width*0.5);
        else
            pen.setWidthF(width);

        painter->setPen(pen);
        painter->drawLine(start,end);
    }
}

void QCTicks::setStep(float step)
{
    _step = step;
    update();
}

void QCTicks::setSubDegree(bool b)
{
    _subTicks = b;
    update();
}

void QCTicks::setrLength(float rLength)
{
    _rLength = rLength;
    update();
}

void QCTicks::setrWidth(float rWidth)
{
    _rWidth = rWidth;
    update();
}
