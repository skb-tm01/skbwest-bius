#pragma once

#include "qcgauge.h"

class QCNeedle;
class QCLabel;
class QCArc;
class QCTicks;
class QCValues;

class EngineGauge : public QCGauge
{
    Q_OBJECT
public:
    explicit EngineGauge(QWidget *parent = nullptr);

    void setGaugeColor(const QColor &gaugeColor);

    inline QString units() const;
    void setUnits(const QString &units);


public slots:
    void setCurrentValue(float value);

private:
    QCLabel* _labelValue;
    QCLabel* _labelUnits;
    QCLabel* _labelMult;
    QCArc* _inArc;
    QCArc* _outArc;

};
