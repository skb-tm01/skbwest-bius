#include "enginegauge.h"
#include "qcarc.h"
#include"qcvalues.h"
#include"qcticks.h"
#include"qcimage.h"
#include"qclabel.h"
#include"qcneedle.h"
#include"qcband.h"
#include "core/helper.h"

EngineGauge::EngineGauge(QWidget *parent) : QCGauge(parent)
{
    _centerPos = BottomCenter;

    auto margins = contentsMargins();
    margins.setBottom(15);
    setContentsMargins(margins);

    auto redBand = new QCBand(this);
    redBand->setrPos(0.975f);
    redBand->setrWidth(0.04f);
    redBand->setDegreeRange(180,0);
    redBand->setValueRange(0,3);
    redBand->setBand(2,3);
    redBand->setColor(Qt::red);
    mItems.append(redBand);

    _outArc = new QCArc(this);
    _outArc->setrPos(1);
    _outArc->setrWidth(5);
    _outArc->setDegreeRange(180,0);
    mItems.append(_outArc);

    _inArc = new QCArc(this);
    _inArc->setrPos(0.6f);
    _inArc->setrWidth(2);
    _inArc->setDegreeRange(180,0);
    mItems.append(_inArc);

    _ticks = new QCTicks(this);
    _ticks->setrPos(0.95f);
    _ticks->setrLength(0.05f);
    _ticks->setDegreeRange(180,0);
    _ticks->setValueRange(0,3);
    _ticks->setStep(0.5f);
    mItems.append(_ticks);

    _labelMult = new QCLabel(this);
    _labelMult->setrPos(0.89f);
    _labelMult->setAngle(90);
    _labelMult->setrFontSize(16);
    _labelMult->setBoldFont(false);
    _labelMult->setText(QString::fromUtf8("x1000"));
    mItems.append(_labelMult);

    _image = new QCImage(this);
    _image->setrPos(0.73f);
    _image->setPixmap(Helper::LoadPixmap("image94.svg",Helper::defIconSize()));
    _image->setDefaultColor(Helper::gaugeIconColor());
    _image->setBlinkedColor(Helper::gaugeBlinkingColor());
    _image->setAngle(90);
    mItems.append(_image);

    _vals = new QCValues(this);
    _vals->setrPos(0.83f);
    _vals->setDegreeRange(180,0);
    _vals->setValueRange(0,3);
    _vals->setStep(1);
    _vals->setrFontSize(38);
    mItems.append(_vals);

    _labelUnits = new QCLabel(this);
    _labelUnits->setrPos(0.10f);
    _labelUnits->setAngle(90);
    _labelUnits->setrFontSize(26);
    mItems.append(_labelUnits);

    _labelValue = new QCLabel(this);
    _labelValue->setrPos(0.30f);
    _labelValue->setAngle(90);
    _labelValue->setrFontSize(50);
    mItems.append(_labelValue);

    _needle = new QCTrapezeNeedle(this);
    _needle->setrPos(0.605f);
    _needle->setrLength(0.34f);
    _needle->setrWidth(9);
    _needle->setDegreeRange(180,0);
    _needle->setValueRange(0,3000);
    _needle->setColor(Qt::red);
    mItems.append(_needle);

    setGaugeColor(Qt::black);
    setUnits(QString::fromUtf8("об/мин"));
    setCurrentValue(0);
}

void EngineGauge::setGaugeColor(const QColor &gaugeColor)
{
    QCGauge::setGaugeColor(gaugeColor);

    _outArc->setColor(_gaugeColor);
    _inArc->setColor(_gaugeColor);
    _ticks->setColor(_gaugeColor);
    _vals->setColor(_gaugeColor);

    _labelValue->setColor(_gaugeColor);
    _labelUnits->setColor(_gaugeColor);
    _labelMult->setColor(_gaugeColor);
}

QString EngineGauge::units() const
{
    return _labelUnits->text();
}

void EngineGauge::setUnits(const QString &units)
{
    _labelUnits->setText(units);
}

void EngineGauge::setCurrentValue(float value)
{
    _needle->setCurrentValue(value);
    _labelValue->setText(QString::number(value,'f',0));
}

