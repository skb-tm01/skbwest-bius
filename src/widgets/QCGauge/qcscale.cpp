#include "qcscale.h"
#include "qcgauge.h"

QCScale::QCScale(QCGauge *parent) : QCItem(parent)
{
    _minDegree = _parent->minAngle();
    _maxDegree = _parent->maxAngle();
    _minValue = 0;
    _maxValue = 100;
}

void QCScale::setValueRange(float minValue, float maxValue)
{
    if(!(minValue<maxValue))
        throw( InvalidValueRange);
    _minValue = minValue;
    _maxValue = maxValue;
    update();
}

void QCScale::setDegreeRange(float minDegree, float maxDegree)
{
    _minDegree = minDegree;
    _maxDegree = maxDegree;
    update();
}

float QCScale::getDegFromValue(float v)
{
    float a = (_maxDegree-_minDegree)/(_maxValue-_minValue);
    float b = -a*_minValue+_minDegree;
    return a*v+b;
}
