#ifndef QCNEEDLE_H
#define QCNEEDLE_H

#include "qcscale.h"

class QCNeedle : public QCScale
{
    Q_OBJECT
public:
    explicit QCNeedle(QCGauge *parent);

    inline float currentValue(){return _currentValue;}
    void setCurrentValue(float value);

    inline float rLength() {return _rLength;}
    void setrLength(float rLength);

    inline float rWidth() {return _rWidth;}
    void setrWidth(float rWidth);

protected:

    float _rLength;
    float _rWidth;
    float _currentValue;

    void createFeatherNeedle(float r);
    void createAttitudeNeedle(float r);
    void createCompassNeedle(float r);    
};

class QCTriangleNeedle : public QCNeedle
{
    Q_OBJECT
public:
    explicit QCTriangleNeedle(QCGauge *parent);
    virtual void draw(QPainter *);
};

class QCRhombusleNeedle : public QCNeedle
{
    Q_OBJECT
public:
    explicit QCRhombusleNeedle(QCGauge *parent);
    virtual void draw(QPainter *);
};

class QCTrapezeNeedle : public QCNeedle
{
    Q_OBJECT
public:
    explicit QCTrapezeNeedle(QCGauge *parent);
    virtual void draw(QPainter *);

    inline float aspect() {return _aspect;}
    void setAspect(float aspect);

private:
    float _aspect;
};

#endif // QCNEEDLE_H
