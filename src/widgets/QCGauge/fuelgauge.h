#pragma once

#include "qcgauge.h"

class QCNeedle;
class QCTicks;
class QCValues;

class FuelGauge : public QCGauge
{
    Q_OBJECT
public:
    explicit FuelGauge(QWidget *parent = nullptr);

    void setGaugeColor(const QColor &gaugeColor);

public slots:
    void setCurrentValue(float value);

};
