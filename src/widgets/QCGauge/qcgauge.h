#pragma once

#include <QColor>
#include <QWidget>

#include "qcimage.h"
#include "qcitem.h"
#include "qcneedle.h"
#include "qcticks.h"
#include "qcvalues.h"

enum EnumCenterPos
{
    CenterLeft,
    CenterCenter,
    CenterRight,
    BottomLeft,
    BottomCenter,
    BottomRight
};

class QCGauge : public QWidget
{
    Q_OBJECT
public:
    explicit QCGauge(QWidget *parent = nullptr);
    float radius();
    float minAngle();
    float maxAngle();

    QPointF center();
    QPointF point(float rpos, float deg);
    QRectF gaugeRect();
    QSizeF gaugeSize();
    QRectF gaugeFullRect();

    static double DegreesToRadians(double deg);
    static double RadiansToDegrees(double rad);


    void setBlinkingState(bool enabled);



protected:
    virtual QSize minimumSizeHint() const;

    void addItem(QCItem* item, float position);
    int removeItem(QCItem* item);
    QList <QCItem*> items();
    QList <QCItem*> mItems;

    EnumCenterPos _centerPos;    

signals:

public slots:

private:    
    void paintEvent(QPaintEvent *);

protected:
    //needle
    float currentValue();
    QCNeedle* _needle;

    //gauge color
    QColor _gaugeColor;
    QColor gaugeColor();
    virtual void setGaugeColor(const QColor &gaugeColor);

    //ticks
    QCTicks* _ticks;

    //vals
    QCValues* _vals;

    //image
    QCImage* _image;
};
