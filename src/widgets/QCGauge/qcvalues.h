#ifndef QCVALUES_H
#define QCVALUES_H

#include"qcscale.h"

class QCValues : public QCScale
{
    Q_OBJECT
public:
    explicit QCValues(QCGauge *parent);

    void draw(QPainter*);
    void setStep(float step);

    inline float rFontSize() {return _rFontSize;}
    void setrFontSize(float rFontSize);

    inline bool boldFont() {return _boldFont;}
    void setBoldFont(bool boldFont);

    inline bool correctPos(){return _correctPos;}
    void setCorrectPos(bool correctPos);

private:
    float _step;

    float _rFontSize;
    bool _boldFont;
    bool _correctPos;
};

#endif // QCVALUES_H
