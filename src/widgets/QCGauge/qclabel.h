#ifndef QCLABEL_H
#define QCLABEL_H

#include "qcitem.h"

class QCLabel : public QCItem
{
    Q_OBJECT
public:
    explicit QCLabel(QCGauge *parent);
    virtual void draw(QPainter *);
    void setAngle(float);
    float angle();
    void setText(const QString &text, bool repaint = true);
    QString text();


    inline float rFontSize() {return _rFontSize;}
    void setrFontSize(float rFontSize);

    inline bool boldFont() {return _boldFont;}
    void setBoldFont(bool boldFont);

private:
    float mAngle;
    float _rFontSize;
    bool _boldFont;
    QString _text;
};

#endif // QCLABEL_H
