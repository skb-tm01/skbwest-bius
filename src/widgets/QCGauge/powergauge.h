#pragma once

#include "qcgauge.h"

class QCNeedle;
class QCTicks;
class QCValues;

class PowerGauge : public QCGauge
{
    Q_OBJECT
public:
    explicit PowerGauge(QWidget *parent = nullptr);

    void setGaugeColor(const QColor &gaugeColor);


public slots:
    void setCurrentValue(float value);

};

