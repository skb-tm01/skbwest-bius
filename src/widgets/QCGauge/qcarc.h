#ifndef QCARC_H
#define QCARC_H

#include "qcscale.h"

class QCArc : public QCScale
{
    Q_OBJECT
public:
    explicit QCArc(QCGauge *parent);
    void draw(QPainter*);

    inline float rWidth() {return _rWidth;}
    void setrWidth(float rWidth);

protected:
    float _rWidth;
};

#endif // QCARC_H
