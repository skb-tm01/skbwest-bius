#ifndef QCTICKS_H
#define QCTICKS_H

#include "qcscale.h"

class QCTicks : public QCScale
{
    Q_OBJECT
public:
    explicit QCTicks(QCGauge *parent);

    void draw(QPainter *painter);
    void setStep(float step);
    void setSubDegree(bool );

    inline float rLength() {return _rLength;}
    void setrLength(float rLength);

    inline float rWidth() {return _rWidth;}
    void setrWidth(float rWidth);

protected:
    float _rLength;
    float _rWidth;

    float _step;
    bool _subTicks;
};

#endif // QCTICKS_H
