#include "speedgauge.h"
#include "qcarc.h"
#include"qcvalues.h"
#include"qcticks.h"
#include"qcimage.h"
#include"qclabel.h"
#include"qcneedle.h"
#include"core/helper.h"

SpeedGauge::SpeedGauge(QWidget *parent) : QCGauge(parent)
{
    _centerPos = BottomCenter;
    auto margins = contentsMargins();
    margins.setBottom(15);
    setContentsMargins(margins);

    _outArc = new QCArc(this);
    _outArc->setrPos(1);
    _outArc->setrWidth(5);
    _outArc->setDegreeRange(180,0);
    mItems.append(_outArc);

    _inArc = new QCArc(this);
    _inArc->setrPos(0.6f);
    _inArc->setrWidth(2);
    _inArc->setDegreeRange(180,0);
    mItems.append(_inArc);

    _ticks = new QCTicks(this);
    _ticks->setrPos(0.95f);
    _ticks->setrLength(0.05f);
    _ticks->setDegreeRange(180,0);

    _ticks->setValueRange(0,30);
    _ticks->setStep(5);
    mItems.append(_ticks);

    _image = new QCImage(this);
    _image->setrPos(0.73f);
    _image->setPixmap(Helper::LoadPixmap("image101.svg",Helper::defIconSize()));
    _image->setDefaultColor(Helper::gaugeIconColor());
    _image->setBlinkedColor(Helper::gaugeBlinkingColor());
    _image->setAngle(90);
    mItems.append(_image);

    _vals = new QCValues(this);
    _vals->setrPos(0.80f);
    _vals->setDegreeRange(180,0);
    _vals->setValueRange(0,30);
    _vals->setStep(10);
    _vals->setrFontSize(38);
    mItems.append(_vals);

    _labelUnits = new QCLabel(this);
    _labelUnits->setrPos(0.10f);
    _labelUnits->setAngle(90);
    _labelUnits->setrFontSize(26);
    mItems.append(_labelUnits);

    _labelValue = new QCLabel(this);
    _labelValue->setrPos(0.30f);
    _labelValue->setAngle(90);
    _labelValue->setrFontSize(50);
    mItems.append(_labelValue);

    _needle = new QCTrapezeNeedle(this);
    _needle->setrPos(0.605f);
    _needle->setrLength(0.34f);
    _needle->setrWidth(9);
    _needle->setDegreeRange(180,0);
    _needle->setValueRange(0,30);
    _needle->setColor(Qt::red);
    mItems.append(_needle);

    setGaugeColor(Qt::black);
    setUnits(QString::fromUtf8("км/ч"));
    setCurrentValue(0);
}

void SpeedGauge::setGaugeColor(const QColor &gaugeColor)
{
    QCGauge::setGaugeColor(gaugeColor);

    _outArc->setColor(_gaugeColor);
    _inArc->setColor(_gaugeColor);
    _ticks->setColor(_gaugeColor);
    _vals->setColor(_gaugeColor);

    _labelValue->setColor(_gaugeColor);
    _labelUnits->setColor(_gaugeColor);
}

QString SpeedGauge::units() const
{
    return _labelUnits->text();
}

void SpeedGauge::setUnits(const QString &units)
{
    _labelUnits->setText(units);
}

void SpeedGauge::setCurrentValue(float value)
{
    _needle->setCurrentValue(value);
    _labelValue->setText(QString::number(value,'f',1));
}
