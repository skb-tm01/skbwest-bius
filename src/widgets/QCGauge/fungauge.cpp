#include "fungauge.h"

#include "qcarc.h"
#include"qcvalues.h"
#include"qcticks.h"
#include"qcimage.h"
#include"qclabel.h"
#include"qcneedle.h"
#include"qcband.h"
#include "core/helper.h"

FunGauge::FunGauge(QWidget *parent) : QCGauge(parent)
{
    _extendedRange = false;
    _centerPos = BottomRight;

    int lim = _extendedRange?3000:1000;

    auto margins = contentsMargins();
    margins.setRight(30);
    margins.setBottom(15);
    setContentsMargins(margins);

    const float begDeg = 180;
    const float endDeg = 90;

    _redBand = new QCBand(this);
    _redBand->setrPos(0.975f);
    _redBand->setrWidth(0.04f);
    _redBand->setDegreeRange(begDeg,endDeg);
    _redBand->setValueRange(0,lim);
    _redBand->setColor(Qt::red);
    mItems.append(_redBand);

    _greenBand = new QCBand(this);
    _greenBand->setrPos(0.975f);
    _greenBand->setrWidth(0.04f);
    _greenBand->setDegreeRange(begDeg,endDeg);
    _greenBand->setValueRange(0,lim);
    _greenBand->setColor(Qt::green);
    mItems.append(_greenBand);
    setBands(0,lim/8,lim/4*3);

    _outArc = new QCArc(this);
    _outArc->setrPos(1);
    _outArc->setrWidth(5);
    _outArc->setDegreeRange(begDeg,endDeg);
    mItems.append(_outArc);

    _inArc = new QCArc(this);
    _inArc->setrPos(0.6f);
    _inArc->setrWidth(2);
    _inArc->setDegreeRange(begDeg,endDeg);
    mItems.append(_inArc);

    _ticks = new QCTicks(this);
    _ticks->setrPos(0.95f);
    _ticks->setrLength(0.05f);
    _ticks->setDegreeRange(begDeg,endDeg);
    _ticks->setValueRange(0,lim);
    _ticks->setStep(lim/8);
    mItems.append(_ticks);

    _image = new QCImage(this);
    _image->setrPos(1.2f);
    _image->setPixmap(Helper::LoadPixmap("image131.svg",Helper::defIconSize()));
    _image->setDefaultColor(Helper::gaugeIconColor());
    _image->setBlinkedColor(Helper::gaugeBlinkingColor());
    _image->setAngle(135);
    mItems.append(_image);

    _vals = new QCValues(this);
    _vals->setrPos(0.85f);
    _vals->setDegreeRange(begDeg,endDeg);
    _vals->setValueRange(0,lim);
    _vals->setStep(lim/2);
    _vals->setrFontSize(22);
    mItems.append(_vals);

    _labelUnits = new QCLabel(this);
    _labelUnits->setrPos(0.25f);
    _labelUnits->setAngle(155);
    _labelUnits->setrFontSize(18);
    mItems.append(_labelUnits);

    _labelValue = new QCLabel(this);
    _labelValue->setrPos(0.35f);
    _labelValue->setAngle(130);
    _labelValue->setrFontSize(38);
    mItems.append(_labelValue);

    _needle = new QCTrapezeNeedle(this);
    _needle->setrPos(0.605f);
    _needle->setrLength(0.34f);
    _needle->setrWidth(8);
    _needle->setDegreeRange(begDeg,endDeg);
    _needle->setValueRange(0,lim);
    _needle->setColor(Qt::red);
    mItems.append(_needle);

    setGaugeColor(Qt::black);
    setUnits(QString::fromUtf8("об/мин"));
    setCurrentValue(0);
}

void FunGauge::setGaugeColor(const QColor &gaugeColor)
{
    QCGauge::setGaugeColor(gaugeColor);

    _outArc->setColor(_gaugeColor);
    _inArc->setColor(_gaugeColor);
    _ticks->setColor(_gaugeColor);
    _vals->setColor(_gaugeColor);

    _labelValue->setColor(_gaugeColor);
    _labelUnits->setColor(_gaugeColor);
}

void FunGauge::setBands(float redStart, float middle, float greenEnd)
{
    _redBand->setBand(redStart,middle);
    _greenBand->setBand(middle,greenEnd);
}

QString FunGauge::units() const
{
    return _labelUnits->text();
}

void FunGauge::setUnits(const QString &units)
{
    _labelUnits->setText(units);
}

void FunGauge::setCurrentValue(float value)
{
    SwitchRanges(value);

    _needle->setCurrentValue(value);
    _labelValue->setText(QString::number(value,'f',0));
}

void FunGauge::SwitchRanges(float value)
{
    if(!_extendedRange && value < 980) //не надо переключаться
        return;

    if(_extendedRange && value > 940)  //не надо переключаться
        return;

    _extendedRange = !_extendedRange;

    int lim = _extendedRange?3000:1000;

    _ticks->setValueRange(0,lim);
    _ticks->setStep(lim/8);
    _redBand->setValueRange(0,lim);
    _greenBand->setValueRange(0,lim);
    _vals->setValueRange(0,lim);
    _vals->setStep(lim/2);
    _needle->setValueRange(0,lim);
}
