#include "fuelgauge.h"
#include "qcarc.h"
#include "qcband.h"
#include "qcticks.h"
#include "qcvalues.h"
#include"qcneedle.h"
#include"qclabel.h"
#include"qcimage.h"
#include"core/helper.h"

#include <QTimer>

FuelGauge::FuelGauge(QWidget *parent) : QCGauge(parent)
{
    _centerPos = BottomCenter;

    auto margins = contentsMargins();
    margins.setLeft(15);
    margins.setRight(30);
    setContentsMargins(margins);

    _ticks = new QCTicks(this);
    _ticks->setrPos(0.85f);
    _ticks->setrLength(0.1f);
    _ticks->setDegreeRange(150,30);
    _ticks->setValueRange(0,100);
    _ticks->setStep(25);
    mItems.append(_ticks);

    auto redBand = new QCBand(this);
    redBand->setrPos(0.95f);
    redBand->setDegreeRange(150,30);
    redBand->setValueRange(0,100);
    redBand->setBand(0,25);
    redBand->setColor(Qt::red);
    mItems.append(redBand);

    auto greenBand = new QCBand(this);
    greenBand->setrPos(0.95f);
    greenBand->setDegreeRange(150,30);
    greenBand->setValueRange(0,100);
    greenBand->setBand(25,100);
    greenBand->setColor(Qt::green);
    mItems.append(greenBand);


    _vals = new QCValues(this);
    _vals->setrPos(1.21f);
    _vals->setDegreeRange(150,30);
    _vals->setValueRange(0,100);
    _vals->setStep(100);
    _vals->setrFontSize(24);
    _vals->setBoldFont(false);
    mItems.append(_vals);

    _image = new QCImage(this);
    _image->setrPos(0.55f);
    _image->setPixmap(Helper::LoadPixmap("image81.svg",Helper::defIconSize()));
    _image->setDefaultColor(Helper::gaugeIconColor());
    _image->setBlinkedColor(Helper::gaugeBlinkingColor());
    _image->setAngle(90);
    mItems.append(_image);

    _needle = new QCTrapezeNeedle(this);
    _needle->setrLength(0.9f);
    _needle->setDegreeRange(150,30);
    _needle->setValueRange(0,100);    
    _needle->setColor(Qt::red);
    mItems.append(_needle);

    setCurrentValue(0);
}

void FuelGauge::setCurrentValue(float value)
{
    _needle->setCurrentValue(value);    
}

void FuelGauge::setGaugeColor(const QColor &gaugeColor)
{
    QCGauge::setGaugeColor(gaugeColor);

    _ticks->setColor(_gaugeColor);
    _vals->setColor(_gaugeColor);
}
