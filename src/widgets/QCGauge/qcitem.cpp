#include "qcitem.h"
#include<qmath.h>
#include"qcgauge.h"


QCItem::QCItem(QCGauge *parent) :
    QObject(parent)
{
    _parent = parent;
    _rPos = 0.5f;
    _color = Qt::black;
}

void QCItem::update()
{
    _parent->update();
}

QRectF QCItem::rect()
{
    return mRect;
}

void QCItem::setrPos(float rpos)
{
    _rPos = rpos;
    update();
}

void QCItem::setColor(const QColor &color)
{
    _color = color;
    update();
}

QRectF QCItem::adjustRect(const QRectF& rect, float rpos)
{
    float radius = _parent->radius();
    float offset =   radius - rPosToPoints(radius,rpos);
    QRectF tmpRect = rect.adjusted(offset,offset,-offset,-offset);
    return tmpRect;
}

float QCItem::rPosToPoints(float r, float rpos)
{
    if(rpos<=0)
        return 0.0f;
    if(rpos>=1.45)
        return rpos;

    return r*rpos;
}
