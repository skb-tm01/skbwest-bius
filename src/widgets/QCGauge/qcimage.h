#pragma once

#include <QPixmap>
#include <QTimer>

#include "qcitem.h"

class QCImage : public QCItem
{
    Q_OBJECT
public:
    explicit QCImage(QCGauge *parent);

    virtual void draw(QPainter *);

    void setAngle(float);
    float angle();

    void setPixmap(const QPixmap &pixmap, bool repaint = true);

    void setDefaultColor(QColor color);
    void setBlinkedColor(QColor color);

    void setBlinkingState(bool enabled);

private slots:
    void blinkedTimerTimeout();

private:
    float mAngle;
    QPixmap _image;
    QPixmap _image_default;
    QPixmap _image_blinked;
    QTimer* _timer;

    bool _blinkingEnabled;
    bool _blinkingState;

    QColor _defaultColor;
    QColor _blinkedColor;

    void initTimer();
};
