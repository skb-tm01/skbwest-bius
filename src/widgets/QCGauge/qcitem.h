#ifndef QCITEM_H
#define QCITEM_H

#include <QObject>
#include <QPainter>
#include <QWidget>

class QCGauge;

class QCItem : public QObject
{
    Q_OBJECT
public:
    explicit QCItem(QCGauge *parent);

    virtual void draw(QPainter *) = 0;

    void setrPos(float rpos);
    inline float rPos(){return _rPos;}
    QRectF rect();
    enum Error{InvalidValueRange};

    inline QColor color(){return _color;}
    void setColor(const QColor& color);

protected:
    QRectF adjustRect(const QRectF& rect, float rpos);
    float rPosToPoints(float r, float rpos);

    void update();

    QCGauge* _parent;
    float _rPos;

    QColor _color;

private:
    QRectF mRect;        
};

#endif // QCITEM_H
