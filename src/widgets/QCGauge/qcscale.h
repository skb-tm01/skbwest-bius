#ifndef QCSCALE_H
#define QCSCALE_H

#include "qcitem.h"

class QCScale : public QCItem
{
    Q_OBJECT
public:
    explicit QCScale(QCGauge *parent);

    void setValueRange(float minValue,float maxValue);
    void setDegreeRange(float minDegree,float maxDegree);

protected:
    float getDegFromValue(float);

    float _minValue;
    float _maxValue;
    float _minDegree;
    float _maxDegree;

};

#endif // QCSCALE_H
