#ifndef DRUMGAUGE_H
#define DRUMGAUGE_H

#include"qcgauge.h"

class QCNeedle;
class QCLabel;
class QCArc;
class QCTicks;
class QCValues;
class QCBand;

class DrumGauge : public QCGauge
{
    Q_OBJECT
public:
    explicit DrumGauge(QWidget *parent = nullptr);

    void setGaugeColor(const QColor &gaugeColor);
    void setBands(float redStart, float middle, float greenEnd);

    inline QString units() const;
    void setUnits(const QString &units);

public slots:
    void setCurrentValue(float value);

    void SwitchRanges(float value);

private:

    QCBand* _redBand;
    QCBand* _greenBand;

    QCLabel* _labelValue;
    QCLabel* _labelUnits;
    QCArc* _inArc;
    QCArc* _outArc;

    bool _extendedRange;
};

#endif // DRUMGAUGE_H
