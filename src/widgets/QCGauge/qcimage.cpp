#include "qcimage.h"
#include "qcgauge.h"

#include "core/helper.h"

QCImage::QCImage(QCGauge *parent) : QCItem(parent)
{
    mAngle = 270;

    _blinkingEnabled = false;
    _blinkingState = false;

    _timer = nullptr;
}

void QCImage::draw(QPainter *painter)
{
    QPixmap* img = nullptr;

    if(_blinkingState)
        img = &_image_blinked;
    else
        img = &_image_default;

    if(img->isNull())
        return;

    QPointF imgCenter = _parent->point(rPosToPoints(_parent->radius(), _rPos), mAngle);

    QSize sz = img->size();
    QRectF imgRect(QPointF(0,0), sz );
    imgRect.moveCenter(imgCenter);

    painter->drawPixmap(imgRect,*img,QRectF(QPointF(0,0), sz ));
}

void QCImage::setAngle(float a)
{
    mAngle = a;
    update();
}

float QCImage::angle()
{
    return mAngle;
}

void QCImage::setPixmap(const QPixmap &image, bool repaint)
{
    _image = image;

    setDefaultColor(_defaultColor);
    setBlinkedColor(_blinkedColor);

    if(repaint)
        update();
}

void QCImage::setDefaultColor(QColor color)
{
    _defaultColor = color;
    _image_default = Helper::SetPixmapColor(_image,_defaultColor);
}

void QCImage::setBlinkedColor(QColor color)
{
    _blinkedColor = color;
    _image_blinked = Helper::SetPixmapColor(_image,_blinkedColor);
}

void QCImage::setBlinkingState(bool enabled)
{
    if(_blinkingEnabled==enabled)
        return;

    _blinkingEnabled = enabled;
    if(enabled)
    {
        initTimer();
        _timer->start();
    }
    else
    {
        if(_timer!=nullptr)
            _timer->stop();

        _blinkingEnabled = false;
        _blinkingState = false;
        update();
    }
}

void QCImage::blinkedTimerTimeout()
{
    _blinkingState = !_blinkingState;
    update();
}

void QCImage::initTimer()
{
    if(_timer!=nullptr)
        return;

    _timer = new QTimer();
    _timer->setInterval(1000);
    QObject::connect(_timer,SIGNAL(timeout()),this,SLOT(blinkedTimerTimeout()));
}


