#include "3rdparty/flickcharm/flickcharm.h"
#include "peripherials/can/packets/ProbegReset.h"
#include "widgets/EventFilters/FocusEventFilter.h"
#include "widgets/Windows/ConfirmationWindow.h"
#include "widgets/WindowManager.h"
#include "ToInstructions.h"
#include "ui_ToInstructions.h"

ToInstructions::ToInstructions(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ToInstructions)
{
    setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    ui->setupUi(this);

    ui->Btn2To->setVisible(false);

    auto* eventFilter = new FocusEventFilter(this);

    instructions = new QLabel(this);
    instructions->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
    instructions->setWordWrap(true);
    SetFontSize(18);

    instructions->setAlignment(Qt::AlignTop | Qt::AlignLeft);
    ui->scrollArea->setWidget(instructions);

    FlickCharm *flickCharm = new FlickCharm(this);
    flickCharm->activateOn(ui->scrollArea);
    //FlickCharm disables scrollbar so enables it again
    ui->scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);

    ui->Btn1Close->installEventFilter(eventFilter);
    ui->Btn2To->installEventFilter(eventFilter);
}

void ToInstructions::SetData(ISensor *sensor, QString title)
{
    if(sensor==nullptr)
        return;

    if(title == "")
        SetData(sensor->GetManualTitle("ru"),sensor->GetManualDescription("ru"));
    else
        SetData(title, sensor->GetManualDescription("ru"));
    this->id = sensor->GetId();

    if(id == "do_to_1" || id == "do_to_2" || id == "do_eto" )
    {
        ui->Btn2To->setVisible(true);
    }
    else {
        ui->Btn2To->setVisible(false);
    }
}

void ToInstructions::SetData(QString title, QString text)
{
    ui->label_2->setText(title);
    instructions->setText(text);
}

void ToInstructions::SetFontSize(int size)
{
    instructions->setStyleSheet("QLabel{font: "+QString::number(size)+"pt;}");
}

void ToInstructions::Close()
{
    WindowManager::getInstance()->CloseMe(this);
}

void ToInstructions::keyPressEvent(QKeyEvent* e)
{
    if(e==nullptr)
        return;

    switch(e->key())
    {
    case Qt::Key_F1:
        on_Btn2To_clicked();
        break;
    case Qt::Key_Escape:
        WindowManager::getInstance()->CloseMe(this);
        break;
    }
}

ToInstructions::~ToInstructions()
{
    delete ui;
}

void ToInstructions::on_Btn1Close_clicked()
{
    WindowManager::getInstance()->CloseMe(this);
}


void ToInstructions::on_Btn2To_clicked()
{
    auto * w = new ConfirmationWindow(this);
    w->SetTitle(QString::fromUtf8("Предупреждение"));
    w->SetText(QString::fromUtf8("Вы уверены, что хотите сбросить интервал техобслуживания?"));
    if (w->exec() == QDialog::Accepted)
    {
       if(id=="do_eto")
           CanPacketProbegResetRequest::Send(CanPacketProbegResetType::reset_engine_eto_time_2);
       else if(id=="do_to_1")
           CanPacketProbegResetRequest::Send(CanPacketProbegResetType::reset_combine_to1_time_2);
       else if(id=="do_to_2")
           CanPacketProbegResetRequest::Send(CanPacketProbegResetType::reset_combine_to2_time);

       auto* w2 =  new ConfirmationWindow(this);
       w2->SetTitle(QString::fromUtf8("Информация"));
       w2->SetText(QString::fromUtf8("Интервал техобслуживания сброшен"));
       w2->SetCancelVisibility(false);
       w2->exec();
    }
}
