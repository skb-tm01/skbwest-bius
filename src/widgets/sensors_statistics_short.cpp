#include "sensors_statistics_short.h"
#include "ui_sensors_statistics_short.h"
#include "widgets/EventFilters/FocusEventFilter.h"
#include "peripherials/sensors/SensorManager.h"
#include "peripherials/can/packets/Settings.h"
#include "peripherials/PeripherialsManager.h"
#include "peripherials/can/packets/Time.h"
#include "alerts/AlertsManager.h"
#include "Config.h"
#include "widgets/WindowManager.h"
#include "peripherials/combines/CombineKeeper.h"
#include "peripherials/sensors/SensorType.h"
#include "peripherials/sensors/ISensor.h"
#include "peripherials/sensors/SensorManager.h"
#include <QDebug>

sensors_statistics_short::sensors_statistics_short(QWidget *parent, EnumSensorsStatisticsMode mode) :
    QWidget(parent),
    ui(new Ui::sensors_statistics_short)
{
    setAttribute(Qt::WA_DeleteOnClose);
    setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    ui->setupUi(this);

    _mode = mode;
    model = -1;
    checksum = -1;

    _canworker = PeripherialsManager::instance().CAN_Worker_Get();
    if(_canworker!=nullptr)
    {
        QObject::connect(_canworker,SIGNAL(received(CanMessage)),this,SLOT(OnCanMessage(CanMessage)));
    }

    ui->label_version_terminal->setText(SKBWEST_BIUS_VERSION);

    if(_mode==EnumSensorsStatisticsMode::Menu)
    {
        auto* eventFilter = new FocusEventFilter(this);
        ui->BtnClose->installEventFilter(eventFilter);
        ui->BtnClose->setVisible(true);
        ui->labelTestInProgess->setText("");
        ui->LabelCaption->setText(QString::fromUtf8("СТАТИСТИКА ПО ДАТЧИКАМ"));

        if(_canworker!=nullptr && !_canworker->IsConnected())
        {
            OnConnectionLost();
        }
        QObject::connect(_canworker, SIGNAL(connectionRestored()), this, SLOT(OnConnectionRestore()));
        UpdateStats();
    }
    else
    {
        ui->BtnClose->setVisible(false);
        ui->labelTestInProgess->setText(QString::fromUtf8("ВЫПОЛНЯЕТСЯ ТЕСТИРОВАНИЕ ДАТЧИКОВ"));
        ui->labelPleaseWait->setText(QString::fromUtf8("ПОЖАЛУЙСТА ПОДОЖДИТЕ..."));
        _timer = new QTimer(this);
        _timer->setInterval(1000);
        _time = 0;
        connect(_timer,SIGNAL(timeout()),this,SLOT(OnTimer()));
        _timer->start();

        if(_canworker != nullptr)
        {
            QObject::connect(_canworker, SIGNAL(connectionRestored()), this, SLOT(OnConnectionRestore()));
        }

        QList<ISensor *> sensors;
        for(SensorType type: RealSensorsTypes)
        {
            sensors.append(SensorManager::instance().GetSensors(type));
        }
        ui->label_total->setText(QString::number(sensors.count()));
    }
    if(_canworker != nullptr)
        QObject::connect(_canworker, SIGNAL(connectionLost()), this, SLOT(OnConnectionLost()));

    CanPackets::VersionTime::SendRequest(false);
    CanPackets::VersionTime::SendRequest(true);
    CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_combine_model_3);
}

sensors_statistics_short::~sensors_statistics_short()
{
    delete ui;
}

void sensors_statistics_short::on_BtnClose_clicked()
{
    closeInternal();
}

const int ReqTime = 3;
const int CloseTime = 5;

void sensors_statistics_short::OnTimer()
{
    if(_canworker!=nullptr && _canworker->IsConnected() == false)
    {
        qDebug()<<"false";
        closeInternal();
        return;
    }

    _time++;
    if(_time<=ReqTime)
    {
        return;
    }
    else if(_time<=CloseTime)
    {
        ui->labelTestInProgess->setStyleSheet("QLabel{color: green;}");
        ui->labelTestInProgess->setText(QString::fromUtf8("ТЕСТИРОВАНИЕ ЗАВЕРШЕНО"));

        UpdateStats();

        ui->labelPleaseWait->setText("");
    }
    else
    {
        closeInternal();
    }
}

void sensors_statistics_short::OnConnectionLost()
{
    if(_mode == EnumSensorsStatisticsMode::OnStart)
    {
        closeInternal();
        return;
    }

    ui->label_broken->setText("---");
    ui->label_checked->setText("---");
    ui->label_notundercontrol->setText("---");
}

void sensors_statistics_short::OnConnectionRestore()
{
    UpdateStats();
}

void sensors_statistics_short::OnCanMessage(CanMessage msg)
{
    if(msg.can_id == CanPacketSettingsGetResponse::packet_id)
    {
        auto packet = CanPacketSettingsGetResponse::Parse(msg.data);
        if(packet.type==CanPacketSettingsType::settings_combine_model_3)
        {
            model=packet.value;
            UpdateBIOVersion();
            CombineKeeper::GetInstance().setCurrentCombine(packet.value);
        }
    }
    else if(msg.can_id == CanPackets::VersionTime::response_id)
    {
        auto packet = CanPackets::VersionTime::ParseResponse(msg.data);

        //for packet type 1 get first 7 bytes
        //for packet type 2 get first 4 bytes
        //received string is not NULL-terminated!
        char received_string[9]{};
        memcpy(received_string,packet.data,packet.type==0?7:4);

        if(packet.type==0)
        {
            date1=QString::fromLatin1(received_string);
        }
        else
        {
            date2=QString::fromLatin1(received_string);
            checksum=packet.checksum;
        }

        UpdateBIOVersion();

    }
}

void sensors_statistics_short::keyPressEvent(QKeyEvent* e)
{
    if(e->key() == Qt::Key_Escape)
    {
       closeInternal();
    }
}

void sensors_statistics_short::UpdateStats()
{
    QList<ISensor *> sensors;
    for(SensorType type: RealSensorsTypes)
    {
        sensors.append(SensorManager::instance().GetSensors(type));
    }

    int broken = 0;
    int initialized = 0;
    int notundercontrol =0;

    ui->label_total->setText(QString::number(sensors.count()));
    for(auto* sensor : sensors)
    {
        if(sensor->IsInitialized())
            initialized++;
     //   else
      //      qDebug()<<sensor->GetId();

        if(sensor->IsBroken())
            broken++;

        if(!sensor->IsUnderControl())
            notundercontrol++;
    }

    ui->label_broken->setText(QString::number(broken));

    ui->label_checked->setText(QString::number(initialized));

    ui->label_notundercontrol->setText(QString::number(notundercontrol));
}

void sensors_statistics_short::closeInternal()
{
    if(_mode==EnumSensorsStatisticsMode::OnStart) {
       _timer->stop();

       //mark all broken sensors as already displayed
       auto sensors = SensorManager::instance().GetSensors();
       auto* alertsWorker = AlertsManager::instance().Alerts_Worker_Get();

       for(auto* sensor : sensors)
       {
           if(sensor->IsBroken()) {
                alertsWorker->MarkAsDisplayed(sensor->GetId());
                qDebug() << "sensorstatistics_short: alertsWorker->MarkAsDisplayed(->" << sensor->GetName("ru");
           }
       }

       qDebug() << "sensorstatistics_short: alertworker->ClearAlerts(true);";
       alertsWorker->ClearAlerts(true);
    }

    emit OnClosing();
    qApp->processEvents();
    WindowManager::getInstance()->CloseMe(this);
}

void sensors_statistics_short::UpdateBIOVersion()
{
    ui->label_version_bio->setText(QString("sw-")+date1 + date2 + " sum=" + QString::number(checksum) + " m="+QString::number(model));
}
