#pragma once

#include <QKeyEvent>
#include <QWidget>

#include "peripherials/can/CanDataRetriever.h"

namespace Ui {
    class MenuCopyWidget;
}

class MenuCopyWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MenuCopyWidget(QWidget *parent = 0);
    ~MenuCopyWidget();


protected:
    void keyPressEvent(QKeyEvent* e);

private slots:
    void on_Btn1FuelConsumption_clicked();
    void on_Btn2EventLog_clicked();
    void on_Btn3Close_clicked();

    void finished();
    void onProgressChanged(int progress);

private:
    Ui::MenuCopyWidget *ui;
    CanDataRetriever *retriever;

    void PerformCopy(RetrieverType type);
};
