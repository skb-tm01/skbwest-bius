#include <QDir>

#include "core/SystemHelper.h"

#include "widgets/EventFilters/FocusEventFilter.h"
#include "widgets/EventFilters/IdleEventFilter.h"
#include "widgets/Menu/menu_copy.h"
#include "widgets/Windows/ConfirmationWindow.h"

#include "ui_menu_copy.h"
#include "widgets/WindowManager.h"

MenuCopyWidget::MenuCopyWidget(QWidget *parent) : QWidget(parent), ui(new Ui::MenuCopyWidget)
{
    setAttribute(Qt::WA_DeleteOnClose);
    setWindowFlags(Qt::Window | Qt::FramelessWindowHint);

    ui->setupUi(this);

    auto* eventFilter = new FocusEventFilter(this);
    ui->Btn3Close->installEventFilter(eventFilter);
    ui->Btn1FuelConsumption->installEventFilter(eventFilter);
    ui->Btn2EventLog->installEventFilter(eventFilter);
    ui->progressBar->setVisible(false);
    retriever = nullptr;
}

MenuCopyWidget::~MenuCopyWidget()
{
    delete ui;
}

void MenuCopyWidget::keyPressEvent(QKeyEvent* e)
{
    switch(e->key())
    {
    case Qt::Key_Escape:
        IdleEventFilter::instance->Resume();
        WindowManager::getInstance()->CloseMe(this);
        break;
    case Qt::Key_F1:
        ui->Btn1FuelConsumption->click();
        break;
    case Qt::Key_F2:
        ui->Btn2EventLog->click();
        break;
    }
}

void MenuCopyWidget::PerformCopy(RetrieverType type)
{
    bool folder_exists = QDir("/mnt/mmc").exists();
    bool folder_mounted = false;

    if(folder_exists)
    {
    #ifdef __linux__
        if(SystemHelper::ExecuteSystemCommand("mount | grep /mnt/mmc").size() > 2)
            folder_mounted = true;
    #endif
    }

    if(!folder_exists || !folder_mounted)
    {
        auto* w = new ConfirmationWindow(this);
        w->SetCancelVisibility(false);
        w->SetTitle(QString::fromUtf8(""));
        w->SetText(QString::fromUtf8("USB накопитель отсутствует"));
        w->exec();
        return;
    }

    IdleEventFilter::instance->Pause();
    ui->Btn1FuelConsumption->setEnabled(false);
    ui->Btn2EventLog->setEnabled(false);
    ui->progressBar->setVisible(true);

    if(retriever==nullptr)
    {
        retriever = new CanDataRetriever(type,this);
        QObject::connect(retriever,SIGNAL(sig_finished()),this,SLOT(finished()));
        QObject::connect(retriever,SIGNAL(sig_progressChanged(int)),this,SLOT(onProgressChanged(int)));
    }
    retriever->SetType(type);
    retriever->StartExtraction();
}

void MenuCopyWidget::on_Btn1FuelConsumption_clicked()
{
    PerformCopy(RetrieverType::FUEL);
}

void MenuCopyWidget::on_Btn2EventLog_clicked()
{
    PerformCopy(RetrieverType::EVENTS);
}

void MenuCopyWidget::on_Btn3Close_clicked()
{
    IdleEventFilter::instance->Resume();
    WindowManager::getInstance()->CloseMe(this);
}

void MenuCopyWidget::finished()
{
    IdleEventFilter::instance->Resume();
    ui->progressBar->setVisible(false);
    ui->Btn1FuelConsumption->setEnabled(true);
    ui->Btn2EventLog->setEnabled(true);

    QFile file("/mnt/mmc/"+retriever->GetFilename());
    if (file.open(QIODevice::ReadWrite) )
    {
        file.write(retriever->data,0x8000);
        file.close();
    }

    auto* w = new ConfirmationWindow(this);
    w->SetCancelVisibility(false);
    w->SetTitle(QString::fromUtf8(""));
    w->SetText(QString::fromUtf8("Копирование успешно выполнено"));
    w->exec();
    return;
}

void MenuCopyWidget::onProgressChanged(int progress)
{
    ui->progressBar->setValue(progress);
}
