#include "3rdparty/flickcharm/flickcharm.h"
#include "peripherials/PeripherialsManager.h"
#include "peripherials/can/packets/Journal.h"
#include "widgets/EventFilters/FocusEventFilter.h"
#include "widgets/Menu/menu_stats_eventlog.h"

#include "ui_menu_stats_eventlog.h"
#include "widgets/WindowManager.h"
#include <widgets/Menu/menu_stats_eventlog_table.h>
#include <QWidget>
#include <QScrollBar>
#include <QKeyEvent>
#include "widgets/EventFilters/KeyPressRedirectFilter.h"

MenuStatsEventlogWidget::MenuStatsEventlogWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MenuStatsEventlogWidget)
{
    setAttribute(Qt::WA_DeleteOnClose);
    setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    ui->setupUi(this);
    auto* eventFilter = new FocusEventFilter(this);
    ui->Btn1Close->installEventFilter(eventFilter);
    counter = 0;


    auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
    if(canworker!=nullptr)
    {
        connect(canworker,SIGNAL(received(CanMessage)),this,SLOT(OnCanMessage(CanMessage)));
        connect(canworker,SIGNAL(connectionLost()),this,SLOT(OnCanConnectionLost()));
        connect(canworker,SIGNAL(connectionRestored()),this,SLOT(OnCanConnectionRestore()));
        OnCanConnectionRestore();
    }

    CanPackets::Journal::SendRequest(counter);

    QVector<int> buttons;
    buttons.append(Qt::Key_Up);
    buttons.append(Qt::Key_Down);

    auto* keyRedirectFilter = new  KeyPressRedirectFilter(window(), buttons,this);
    for(auto* button: this->findChildren<QPushButton *>())
    {
        button->installEventFilter(keyRedirectFilter);
    }
}

void MenuStatsEventlogWidget::keyPressEvent(QKeyEvent* e)
{
    switch(e->key())
    {
    case Qt::Key_Escape:
        WindowManager::getInstance()->CloseMe(this);
        break;
    case Qt::Key_Down:
        ScrollDown();
        break;
    case Qt::Key_Up:
        ScrollUp();
        break;
    }
}

MenuStatsEventlogWidget::~MenuStatsEventlogWidget()
{
    delete ui;
}

void MenuStatsEventlogWidget::on_Btn1Close_clicked()
{
    WindowManager::getInstance()->CloseMe(this);
}

void MenuStatsEventlogWidget::OnCanMessage(CanMessage msg)
{
    if(msg.can_id!=CanPackets::Journal::response_id)
        return;

    if(msg.data[0]==0xFF && msg.data[7]==0xFF)
        return;

    int day = msg.data[1];
    int month = msg.data[0]&0x0f;
    int year = (msg.data[0]>>4)+10;
    int hour = msg.data[2];
    int minute = msg.data[3];
    int rotate = ((unsigned short)(msg.data[6])<<8)+msg.data[5];
    int H_num=msg.data[7];
    int L_num =msg.data[4]&0x7f;
    int status = msg.data[4]>>7;

    ui->widget->ChangeItem(counter, 0, QString::number(day).rightJustified(2,'0')+"."+QString::number(month).rightJustified(2,'0')+"."+
                                             QString::number(year).rightJustified(2,'0')+ + " / " + QString::number(hour).rightJustified(2,'0') + ":" + QString::number(minute).rightJustified(2,'0'));
    ui->widget->ChangeItem(counter, 1, QString::number(rotate));
    ui->widget->ChangeItem(counter, 2, QString("(")+QString::number(H_num)+") "+QString::number(L_num));
    ui->widget->ChangeItem(counter, 3, QString::number(status));

    counter++;
    CanPackets::Journal::SendRequest(counter);
}

void MenuStatsEventlogWidget::ScrollUp()
{
    qDebug("SCROLL UP");
    ui->widget->ScrollUp();
}

void MenuStatsEventlogWidget::ScrollDown()
{
    qDebug("SCROLL DOWN");
    ui->widget->ScrollDown();
}

void MenuStatsEventlogWidget::OnCanConnectionLost()
{
    counter=0;
}

void MenuStatsEventlogWidget::OnCanConnectionRestore()
{
    counter = 0;
    CanPackets::Journal::SendRequest(counter);
}
