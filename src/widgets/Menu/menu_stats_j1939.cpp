#include <QString>
#include <QTimer>

#include "peripherials/can/packets/J1939.h"
#include "widgets/EventFilters/FocusEventFilter.h"
#include "widgets/Menu/menu_stats_j1939.h"

#include "ui_menu_stats_j1939.h"
#include "widgets/WindowManager.h"

using namespace CanPackets;

MenuStatsJ1939Widget::MenuStatsJ1939Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MenuStatsJ1939Widget)
{
    setAttribute(Qt::WA_DeleteOnClose);
    setWindowFlags(Qt::Window | Qt::FramelessWindowHint);

    ui->setupUi(this);
    auto* eventFilter = new FocusEventFilter(this);
    ui->Btn1Close->installEventFilter(eventFilter);

    auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
    if(canworker!=nullptr)
    {
        connect(canworker,SIGNAL(received(CanMessage)),this,SLOT(OnCanMessage(CanMessage)));
        connect(canworker,SIGNAL(connectionLost()),this,SLOT(OnCanConnectionLost()));
        connect(canworker,SIGNAL(connectionRestored()),this,SLOT(OnCanConnectionRestore()));
        OnCanConnectionRestore();
    }

    auto* timer = new QTimer(this);
    QObject::connect(timer,SIGNAL(timeout()), this, SLOT(OnCanConnectionRestore()));
    timer->setInterval(500);
    timer->start();
}

MenuStatsJ1939Widget::~MenuStatsJ1939Widget()
{
    delete ui;
}

void MenuStatsJ1939Widget::OnCanMessage(CanMessage msg)
{
    if(msg.can_id != J1939Get::response_id)
        return;

    auto packet = J1939Get::ParseResponse(msg.data);
    switch(packet.Id)
    {
    case 0:
        ui->label_FMI1_Value->setText(QString::number(packet.FMI));
        ui->label_SPN1_Value->setText(QString::number(packet.SPN));
        break;
    case 1:
        ui->label_FMI2_Value->setText(QString::number(packet.FMI));
        ui->label_SPN2_Value->setText(QString::number(packet.SPN));
        break;
    case 2:
        ui->label_FMI3_Value->setText(QString::number(packet.FMI));
        ui->label_SPN3_Value->setText(QString::number(packet.SPN));
        break;
    case 3:
        ui->label_FMI4_Value->setText(QString::number(packet.FMI));
        ui->label_SPN4_Value->setText(QString::number(packet.SPN));
        break;
    case 4:
        ui->label_FMI5_Value->setText(QString::number(packet.FMI));
        ui->label_SPN5_Value->setText(QString::number(packet.SPN));
        break;
    case 5:
        ui->label_FMI6_Value->setText(QString::number(packet.FMI));
        ui->label_SPN6_Value->setText(QString::number(packet.SPN));
        break;
    }
}

void MenuStatsJ1939Widget::OnCanConnectionLost()
{
    ui->label_FMI1_Value->setText("---");
    ui->label_FMI2_Value->setText("---");
    ui->label_FMI3_Value->setText("---");
    ui->label_FMI4_Value->setText("---");
    ui->label_FMI5_Value->setText("---");
    ui->label_FMI6_Value->setText("---");

    ui->label_SPN1_Value->setText("---");
    ui->label_SPN2_Value->setText("---");
    ui->label_SPN3_Value->setText("---");
    ui->label_SPN4_Value->setText("---");
    ui->label_SPN5_Value->setText("---");
    ui->label_SPN6_Value->setText("---");
}

void MenuStatsJ1939Widget::OnCanConnectionRestore()
{
    for(int i=0;i<6;i++)
    {
        J1939Get::SendRequest(i);
    }
}

void MenuStatsJ1939Widget::keyPressEvent(QKeyEvent* e)
{
    switch(e->key())
    {
    case Qt::Key_Escape:
        WindowManager::getInstance()->CloseMe(this);
        break;
    }
}

void MenuStatsJ1939Widget::on_Btn1Close_clicked()
{
    WindowManager::getInstance()->CloseMe(this);
    delete this;
}

