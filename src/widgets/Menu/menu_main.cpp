#include <QDebug>
#include <QKeyEvent>
#include <QMessageBox>

#include "core/AudioHelper.h"
#include "core/PathHelper.h"

#include "widgets/EventFilters/FocusEventFilter.h"

#include "widgets/Menu/menu_calibration.h"
#include "widgets/Menu/menu_copy.h"
#include "widgets/Menu/menu_main.h"
#include "widgets/Menu/menu_help.h"
#include "widgets/Menu/menu_settings.h"
#include "widgets/Menu/menu_sensors.h"
#include "widgets/Menu/menu_stats.h"
#include "widgets/WindowManager.h"

#include "ui_menu_main.h"

MenuMain::MenuMain(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MenuMain)
{
    setAttribute(Qt::WA_DeleteOnClose);
    setWindowFlags(Qt::Window | Qt::FramelessWindowHint);

    ui->setupUi(this);

    auto* eventFilter = new FocusEventFilter(this);
    ui->Btn1Statistics->installEventFilter(eventFilter);
    ui->Btn2Help->installEventFilter(eventFilter);
    ui->Btn3Settings->installEventFilter(eventFilter);
    ui->Btn4Calibration->installEventFilter(eventFilter);
    ui->Btn5Sensorstate->installEventFilter(eventFilter);
    ui->Btn6Datacopy->installEventFilter(eventFilter);
    ui->Btn7Close->installEventFilter(eventFilter);
}

MenuMain::~MenuMain()
{
    delete ui;
}

void MenuMain::keyPressEvent(QKeyEvent* e)
{
    switch(e->key())
    {
    case Qt::Key_Escape:
        WindowManager::getInstance()->CloseMe(this);
        break;
    case Qt::Key_F1:
        ui->Btn1Statistics->click();
        break;
    case Qt::Key_F2:
        ui->Btn2Help->click();
        break;
    case Qt::Key_F3:
        ui->Btn3Settings->click();
        break;
    case Qt::Key_F4:
        ui->Btn4Calibration->click();
        break;
    case Qt::Key_F5:
        ui->Btn5Sensorstate->click();
        break;
    case Qt::Key_F6:
        ui->Btn6Datacopy->click();
        break;
    }
}

void MenuMain::on_Btn1Statistics_clicked()
{
    auto* w = new MenuStatsWidget(this);
    w->move(pos());
    w->resize(size());
    WindowManager::getInstance()->Open(this,w);
}

void MenuMain::on_Btn2Help_clicked()
{
    auto* w = new MenuHelpWidget(this);
    w->move(pos());
    w->resize(size());
    WindowManager::getInstance()->Open(this,w);
}

void MenuMain::on_Btn3Settings_clicked()
{
    MenuSettingsWidget *w = new MenuSettingsWidget(this);
    w->move(pos());
    w->resize(size());
    WindowManager::getInstance()->Open(this,w);
}

void MenuMain::on_Btn4Calibration_clicked()
{
    auto* w = new MenuCalibrationWidget(this);
    w->move(pos());
    w->resize(size());
    WindowManager::getInstance()->Open(this,w);
}

void MenuMain::on_Btn5Sensorstate_clicked()
{
    auto *w = new MenuSensorsWidget(this);
    w->resize(size());
    w->move(pos());
    WindowManager::getInstance()->Open(this,w);
}

void MenuMain::on_Btn6Datacopy_clicked()
{
    auto *w = new MenuCopyWidget(this);
    w->resize(size());
    w->move(pos());
    WindowManager::getInstance()->Open(this,w);
}

void MenuMain::on_Btn7Close_clicked()
{
    WindowManager::getInstance()->CloseMe(this);
}
