#pragma once

#include <QKeyEvent>
#include <QMouseEvent>
#include <QPushButton>
#include <QString>
#include <QWidget>

#include "SettingsManager.h"
#include "core/helper.h"
#include "peripherials/can/CanMessage.h"
#include "peripherials/can/packets/Taho.h"

namespace Ui
{
    class MenuSensorsRotationsWidget;
}

class MenuSensorsRotationsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MenuSensorsRotationsWidget(QWidget *parent = 0);
    ~MenuSensorsRotationsWidget();
    void SetClosable(bool visibility);
    void Initialize_CAN();
    void FocusButton();

protected:
    void keyPressEvent(QKeyEvent* e);
    bool eventFilter(QObject *obj, QEvent *event);
private slots:

    void OnCanMessage(CanMessage msg);

    void OnCanConnectionLost();

    void OnCanConnectionRestore();

    void ApplyNightDay(EnumNightDayMode mode);


    void on_Btn9SaveRotations_clicked();

    void on_Btn10Close_clicked();

    void mousePressEvent(QMouseEvent * event);
    void mouseReleaseEvent(QMouseEvent * event);
private:
    bool _canInitiailized;
    bool _isClosable;
    Ui::MenuSensorsRotationsWidget *ui;

    QPushButton* buttons[9];
    QPixmap button_icons[9];
    QString button_description[9];
    CanPacketTahoType button_tahorequest[9];

    QString engine_label_focused_stylesheet;
    QColor btn_focused_forecolor;
    QColor btn_focused_backcolor;
    QColor btn_unfocused_forecolor;
    QColor btn_unfocused_backcolor;
    QPushButton *focused_button;
   // int focused_button_index;

    int mousePressX;
    int mousePressY;
signals:
    void LeftSideButtonClicked();
    void RightSideButtonClicked();
};
