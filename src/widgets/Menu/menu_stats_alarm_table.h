#pragma once

#include <QColor>
#include <QKeyEvent>
#include <QVector>
#include <QWidget>

#include "peripherials/combines/CombineKeeper.h"

namespace Ui {
    class MenuStatsAlarmTableWidget;
}
class QLabel;
class MenuStatsAlarmTableWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MenuStatsAlarmTableWidget(QWidget *parent = 0);
    ~MenuStatsAlarmTableWidget();
    bool eventFilter(QObject *object, QEvent *event);

    void setTime(int rowIndex, double hours);
    static const int rowCount = 18;
    void setStrawEngineType(CombineStrawEngineType engineType);

private:
    void setColor(int rowIndex, QString color);
    static const int colCount = 3;
    static const int ICON_COLUMN = 0;
    static const int DESC_COLUMN = 1;
    static const int TIME_COLUMN = 2;
    QVector<QVector<QLabel*>> table;
    QPixmap icons[rowCount];
    double time[rowCount];
    Ui::MenuStatsAlarmTableWidget *ui;
    void setInitText();

    bool strawEngineRed;
};

