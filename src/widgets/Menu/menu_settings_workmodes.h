#pragma once

#include <QKeyEvent>
#include <QWidget>
#include <QTimer>
#include <cstdint>

#include "peripherials/can/CanMessage.h"
#include "peripherials/can/packets/Settings.h"
#include "SettingsManager.h"

enum WorkModes: uint8_t
{
    workmode_own = 0,
    workmode_recommended = 1,
    workmode_handoperated = 2
};

namespace Ui {
    class MenuSettingsWorkmodesWidget;
}

class MenuSettingsWorkmodesWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MenuSettingsWorkmodesWidget(QWidget *parent = 0);
    ~MenuSettingsWorkmodesWidget();

protected:
    void keyPressEvent(QKeyEvent* e);

private slots:
    //daynight
    void ApplyNightDay(EnumNightDayMode mode);

    //can
    void OnCanMessage(CanMessage msg);
    void OnCanMessage_Control(CanMessage msg);
    void OnCanConnectionLost();
    void OnCanConnectionRestore();

    //timer
    void OnTimerTimeout();

    //buttons
    void on_Btn1Close_clicked();
    void on_comboBox_grainType_currentIndexChanged(int index);
    void on_comboBox_lossType_currentIndexChanged(int index);
    void on_comboBox_mode_currentIndexChanged(int index);

    void on_akc_threshing_drum_our_returnPressed();

    void on_akc_concave_gap_our_returnPressed();

    void on_akc_blower_rpm_our_returnPressed();

    void on_akc_chaffer_sieve_our_returnPressed();

    void on_akc_shover_sieve_our_returnPressed();

    void on_akc_loss_in_filtration_our_returnPressed();

    void on_akc_loss_in_straw_our_returnPressed();

    void on_CheckBoxBeaterDrum_clicked(bool checked);

    void on_CheckBoxConcaveGap_clicked(bool checked);

    void on_CheckBoxBlower_clicked(bool checked);

    void on_CheckBoxChafferSieve_clicked(bool checked);

    void on_CheckBoxShoeSieve_clicked(bool checked);

private:
    void ChangeModeControl(WorkModes modes);
    void SetGrainImage(CanPacketSettignsGrainType grainType);

    Ui::MenuSettingsWorkmodesWidget *ui;
    QTimer _timer;

    bool checkbox_1_1;
    bool checkbox_1_2;

    bool checkbox_2_1;
    bool checkbox_2_2;

    bool checkbox_3_1;
    bool checkbox_3_2;

    bool checkbox_4_1;
    bool checkbox_4_2;

    bool checkbox_5_1;
    bool checkbox_5_2;

};
