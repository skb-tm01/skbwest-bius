#include <QDebug>
#include <QTime>

#include "core/AudioHelper.h"
#include "core/PathHelper.h"
#include "core/helper.h"

#include "peripherials/PeripherialsManager.h"
#include "peripherials/spi/IlluminationMonitor.h"

#include "widgets/EventFilters/CheckBoxEventFilter.h"
#include "widgets/EventFilters/ComboboxEventFilter.h"
#include "widgets/EventFilters/SliderEventFilter.h"

#include "widgets/EventFilters/FocusEventFilter.h"
#include "widgets/Menu/menu_settings_terminal.h"

#include "SettingsManager.h"
#include "ui_menu_settings_terminal.h"
#include "widgets/WindowManager.h"

MenuSettingsTerminalWidget::MenuSettingsTerminalWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MenuSettingsTerminalWidget)
{
    setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    ui->setupUi(this);

    setTabOrder(ui->voiceMessagesCheckBox, ui->sliderVolume);
    setTabOrder(ui->sliderVolume, ui->sliderBrightness);
    setTabOrder(ui->sliderBrightness,ui->time_hours);
    setTabOrder(ui->time_hours,ui->time_min);
    setTabOrder(ui->time_min,ui->date_day);
    setTabOrder(ui->date_day,ui->date_month);
    setTabOrder(ui->date_month,ui->date_year);
    setTabOrder(ui->date_year,ui->languageSetting);
    setTabOrder(ui->languageSetting,ui->nightAndDayMode);
    setTabOrder(ui->nightAndDayMode,ui->sliderNightAndDayModeThreshold);
    setTabOrder(ui->sliderNightAndDayModeThreshold,ui->Btn1Close);

    ui->time_hours->init(2);
    ui->time_hours->setLimitHigh(23);

    ui->time_min->init(2);
    ui->time_min->setLimitHigh(59);

    ui->date_day->init(2);
    ui->date_day->setLimitLow(1);
    ui->date_day->setLimitHigh(31);

    ui->date_month->init(2);
    ui->date_month->setLimitLow(1);
    ui->date_month->setLimitHigh(12);

    ui->date_year->init(4);
    ui->date_year->setLimitLow(1970);
    ui->date_year->setLimitHigh(2037);


    ui->sliderBrightness->setValue(SettingsManager::settings()->brightness());
    ui->voiceMessagesCheckBox->setCheckState(SettingsManager::settings()->voiceMessages() ? Qt::CheckState::Checked : Qt::CheckState::Unchecked);
    ui->languageSetting->setCurrentIndex(static_cast<int>(SettingsManager::settings()->language()));
    ui->nightAndDayMode->setCurrentIndex(static_cast<int>(SettingsManager::settings()->nightDayMode()));
    ui->sliderNightAndDayModeThreshold->setValue(SettingsManager::settings()->nightDayThreshold());
    ui->sliderNightAndDayModeThreshold->setEnabled(SettingsManager::settings()->nightDayMode()==EnumNightDayMode::Auto);

    showTime();

#ifdef __linux__
    ui->sliderVolume->setEnabled(true);
    ui->sliderVolume->setValue(SettingsManager::settings()->getVolume());
#endif
    ApplyNightDay(SettingsManager::settings()->currentNightDayMode());
    QObject::connect(SettingsManager::settings(),SIGNAL(NightDayChanged(EnumNightDayMode)), this, SLOT(ApplyNightDay(EnumNightDayMode)));

    QObject::connect(ui->date_day,SIGNAL(returnPressed()),this,SLOT(onChange()));
    QObject::connect(ui->date_month,SIGNAL(returnPressed()),this,SLOT(onChange()));
    QObject::connect(ui->date_year,SIGNAL(returnPressed()),this,SLOT(onChange()));
    QObject::connect(ui->time_hours,SIGNAL(returnPressed()),this,SLOT(onChange()));
    QObject::connect(ui->time_min,SIGNAL(returnPressed()),this,SLOT(onChange()));

    QObject::connect(PeripherialsManager::instance().illumMonitor_Get(), SIGNAL (BrightnessChanged(double)), this, SLOT (OnIlliminationChange(double)));

    auto* eventFilter = new FocusEventFilter(this);
    ui->Btn1Close->installEventFilter(eventFilter);

    auto* cbEventFilter = new ComboboxEventFilter(this);
    for(auto* item: this->findChildren<QComboBox*>())
    {
        item->installEventFilter(eventFilter);
        item->installEventFilter(cbEventFilter);
    }

    auto* sliderEventFilter = new SliderEventFilter(this);
    for(auto* item: this->findChildren<QSlider*>())
    {
        item->installEventFilter(eventFilter);
        item->installEventFilter(sliderEventFilter);
    }

    auto* checkEventFilter = new CheckBoxEventFilter(this);
    for(auto* item: this->findChildren<QCheckBox*>())
    {
        item->installEventFilter(eventFilter);
        item->installEventFilter(checkEventFilter);
    }
}

MenuSettingsTerminalWidget::~MenuSettingsTerminalWidget()
{
    delete ui;
}

void MenuSettingsTerminalWidget::keyPressEvent(QKeyEvent* e)
{
    AudioHelper::PlayWaveFile(PathHelper::pathAppend(QCoreApplication::applicationDirPath(),"data/sounds/key_press.wav"),RingingType::No);
    switch(e->key())
    {
    case Qt::Key_Escape:
        WindowManager::getInstance()->CloseMe(this);
        break;
    }
}

void MenuSettingsTerminalWidget::on_Btn1Close_clicked()
{
    WindowManager::getInstance()->CloseMe(this);
}


void MenuSettingsTerminalWidget::on_sliderVolume_valueChanged(int value)
{
    AudioHelper::SetAlsaVolume(value);
    AudioHelper::PlayWaveFileWhenQueryIsEmpty(PathHelper::pathAppend(QCoreApplication::applicationDirPath(),"data/sounds/key_press.wav"));
}

void MenuSettingsTerminalWidget::on_sliderBrightness_valueChanged(int value)
{
    SettingsManager::settings()->setBrightness(value);
}

void MenuSettingsTerminalWidget::on_voiceMessagesCheckBox_stateChanged(int arg1)
{
    SettingsManager::settings()->setVoiceMessages(arg1);
}

void MenuSettingsTerminalWidget::on_languageSetting_currentIndexChanged(int index)
{
    SettingsManager::settings()->setLanguage(static_cast<EnumLanguge>(index));
}

void MenuSettingsTerminalWidget::on_nightAndDayMode_currentIndexChanged(int index)
{
    SettingsManager::settings()->setNightDayMode(static_cast<EnumNightDayMode>(index));
    ui->sliderNightAndDayModeThreshold->setEnabled(SettingsManager::settings()->nightDayMode()==EnumNightDayMode::Auto);
}

void MenuSettingsTerminalWidget::on_sliderNightAndDayModeThreshold_valueChanged(int value)
{
     SettingsManager::settings()->setNightDayThreshold(value);
}

void MenuSettingsTerminalWidget::showTime()
{
    QTime time = QTime::currentTime();
    ui->time_hours->setNumber(time.hour());
    ui->time_min->setNumber(time.minute());

    QDate date = QDate::currentDate();
    ui->date_day->setNumber(date.day());
    ui->date_month->setNumber(date.month());
    ui->date_year->setNumber(date.year());
}

void MenuSettingsTerminalWidget::ChangeDate()
{
#ifndef _WIN32
    struct tm time {};

    time.tm_year = ui->date_year->number() - 1900;
    time.tm_mon  = ui->date_month->number() - 1;
    time.tm_mday = ui->date_day->number();
    time.tm_hour = ui->time_hours->number();
    time.tm_min  = ui->time_min->number();
    time.tm_sec  = 0;

    if (time.tm_year < 0) time.tm_year = 0;

    time_t t = mktime(&time);

    if (t != (time_t) -1)
        stime(&t);

    #ifdef BUILD_DEVICE
    system("hwclock --rtc=/dev/rtc1 -w");
    #endif
#endif
}


void MenuSettingsTerminalWidget::onChange()
{
    ChangeDate();
}

void MenuSettingsTerminalWidget::OnIlliminationChange(double val)
{
    ui->label_illum_val->setText(QString::number(ui->sliderNightAndDayModeThreshold->value()/2048.0*100.0,'f',1)+"/ E="+QString::number(val/2048.0*100.0,'f',1));
}

void MenuSettingsTerminalWidget::ApplyNightDay(EnumNightDayMode mode)
{
    int iconsize = 48;

    QColor foreColor = (mode==EnumNightDayMode::Day)?Qt::black:Qt::white;
    ui->label->setPixmap(Helper::LoadPixmap("moon.svg",iconsize,foreColor));
    ui->label_2->setPixmap(Helper::LoadPixmap("sun.svg",iconsize,foreColor));

    ui->label_volume_high->setPixmap(Helper::LoadPixmap("volume_high.svg",iconsize,foreColor));
    ui->label_volume_low->setPixmap(Helper::LoadPixmap("volume_low.svg",iconsize,foreColor));

    ui->label_brightness_high->setPixmap(Helper::LoadPixmap("brightness_high.svg",iconsize,foreColor));
    ui->label_brightness_low->setPixmap(Helper::LoadPixmap("brightness_low.svg",iconsize,foreColor));

}
