#include <cmath>
#include <QMessageBox>

#include "peripherials/can/packets/Probeg.h"
#include "peripherials/can/packets/ProbegReset.h"
#include "widgets/EventFilters/FocusEventFilter.h"
#include "widgets/Menu/menu_stats_general.h"
#include "widgets/Windows/ConfirmationWindow.h"

#include "ui_menu_stats_general.h"
#include "widgets/WindowManager.h"

MenuStatsGeneralWidget::MenuStatsGeneralWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MenuStatsGeneralWidget)
{
    setAttribute(Qt::WA_DeleteOnClose);
    setWindowFlags(Qt::Window | Qt::FramelessWindowHint);

    ui->setupUi(this);
    auto* eventFilter = new FocusEventFilter(this);
    ui->Btn1Reset->installEventFilter(eventFilter);
    ui->Btn2Close->installEventFilter(eventFilter);

    _combinetime_temp = 0;
    _area_temp = 0;

    auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
    if(canworker!=nullptr)
    {
        connect(canworker,SIGNAL(received(CanMessage)),this,SLOT(OnCanMessage(CanMessage)));
        connect(canworker,SIGNAL(connectionLost()),this,SLOT(OnCanConnectionLost()));
        connect(canworker,SIGNAL(connectionRestored()),this,SLOT(OnCanConnectionRestore()));
        OnCanConnectionRestore();
    }
}

MenuStatsGeneralWidget::~MenuStatsGeneralWidget()
{
    delete ui;
}

void MenuStatsGeneralWidget::OnCanMessage(CanMessage msg)
{
    if(msg.can_id != CanPacketProbegResponse::packet_id)
        return;

    auto packet = CanPacketProbegResponse::Parse(msg.data);
    switch(packet.type)
    {
        case CanPacketProbegType::probeg_distance_temp:
            ui->label_distance_temp->setText(QString::number(packet.value/10.0,'f',1));
            break;
        case CanPacketProbegType::probeg_distance_total:
            ui->label_distance_total->setText(QString::number(packet.value/10.0,'f',1));
            break;
        case CanPacketProbegType::probeg_area_temp:
        {
            _area_temp = packet.value/10.0;
            ui->label_area_temp->setText(QString::number(_area_temp,'f',1));

            double time = _combinetime_temp;
            if(_combinetime_temp>=0)
            {
                if(packet.value==0)
                    ui->label_performance_temp->setText("---");
                else
                {
                    double performance = 10*packet.value/time;
                    if(std::isinf(performance))
                        ui->label_performance_temp->setText(QString::number(0,'f',1));
                    else
                        ui->label_performance_temp->setText(QString::number(performance,'f',1));
                }
            }
            break;
        }
        case CanPacketProbegType::probeg_area_total:
            ui->label_area_total->setText(QString::number(packet.value/10.0,'f',1));
            break;
        case CanPacketProbegType::probeg_combinetime_temp:
        {
            _combinetime_temp = packet.value/10.0;
            ui->label_combinetime_temp->setText(QString::number(_combinetime_temp,'f',1));

            auto area = _area_temp;
            if(_area_temp>=0)
            {
                if(packet.value==0)
                    ui->label_performance_temp->setText("---");
                else
                {
                    double performance = 10*area/packet.value;
                    if (std::isinf(performance))
                        ui->label_performance_temp->setText(QString::number(0,'f',1));
                    else
                        ui->label_performance_temp->setText(QString::number(performance,'f',1));
                }
            }
            break;
        }
        case CanPacketProbegType::probeg_combinetime_total:
            ui->label_combinetime_total->setText(QString::number(packet.value/10.0,'f',1));
            break;
        case CanPacketProbegType::probeg_enginetime_total_can:
            ui->label_enginetime_total->setText(QString::number(packet.value/10.0,'f',1));
            break;
        default:
            break;
    }
}

void MenuStatsGeneralWidget::OnCanConnectionLost()
{
    ui->label_area_temp->setText("---");
    ui->label_area_total->setText("---");

    ui->label_distance_temp->setText("---");
    ui->label_distance_total->setText("---");

    ui->label_combinetime_temp->setText("---");
    ui->label_combinetime_total->setText("---");

    ui->label_enginetime_temp->setText("---");
    ui->label_enginetime_total->setText("---");

    ui->label_performance_temp->setText("---");
    ui->label_performance_total->setText("---");
}

void MenuStatsGeneralWidget::OnCanConnectionRestore()
{
    CanPacketProbegRequest::Send(CanPacketProbegType::probeg_distance_temp);
    CanPacketProbegRequest::Send(CanPacketProbegType::probeg_distance_total);
    CanPacketProbegRequest::Send(CanPacketProbegType::probeg_area_temp); 
    CanPacketProbegRequest::Send(CanPacketProbegType::probeg_area_total); 
    CanPacketProbegRequest::Send(CanPacketProbegType::probeg_combinetime_temp); 
    CanPacketProbegRequest::Send(CanPacketProbegType::probeg_combinetime_total);
    CanPacketProbegRequest::Send(CanPacketProbegType::probeg_enginetime_total_can);
}

void MenuStatsGeneralWidget::keyPressEvent(QKeyEvent* e)
{
    switch(e->key())
    {
    case Qt::Key_F1:
        ui->Btn1Reset->click();
        break;
    case Qt::Key_F2:
        ui->Btn2Close->click();
        break;
    case Qt::Key_Escape:
        WindowManager::getInstance()->CloseMe(this);
        break;
    }
}

void MenuStatsGeneralWidget::on_Btn1Reset_clicked()
{
    auto* w = new ConfirmationWindow(this);
    w->SetTitle(QString::fromUtf8("Предупреждение"));
    w->SetText(QString::fromUtf8("Вы уверены, что хотите сбросить статистику?"));
    if (w->exec() == QDialog::Accepted)
    {
       OnCanConnectionLost();
       CanPacketProbegResetRequest::Send(CanPacketProbegResetType::reset_all_temp_stats);
       OnCanConnectionRestore();

       auto* w2 = new ConfirmationWindow(this);
       w2->SetTitle(QString::fromUtf8("Информация"));
       w2->SetText(QString::fromUtf8("Статистика была сброшена"));
       w2->SetCancelVisibility(false);
       w2->exec();
    }
}

void MenuStatsGeneralWidget::on_Btn2Close_clicked()
{
    WindowManager::getInstance()->CloseMe(this);
}
