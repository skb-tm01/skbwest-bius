#pragma once

#include <QWidget>
#include <QKeyEvent>

#include "peripherials/can/CanMessage.h"
#include "widgets/Menu/menu_stats_alarm_table.h"
#include "widgets/WindowManager.h"
namespace Ui {
class MenuStatsAlarmWidget;
}


class MenuStatsAlarmWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MenuStatsAlarmWidget(QWidget *parent = 0);
    ~MenuStatsAlarmWidget();
protected:
    void keyPressEvent(QKeyEvent* e);
private slots:
    void on_Btn1Close_clicked();

    void OnCanMessage(CanMessage msg);
    void OnCanConnectionLost();
    void OnCanConnectionRestore();

    void ScrollUp();
    void ScrollDown();
private:
    Ui::MenuStatsAlarmWidget *ui;
    MenuStatsAlarmTableWidget *_table;
};
