#pragma once

#include <QKeyEvent>
#include <QWidget>

namespace Ui {
    class MenuSensorsWidget;
}

class MenuSensorsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MenuSensorsWidget(QWidget *parent = nullptr);
    ~MenuSensorsWidget();

protected:
    void keyPressEvent(QKeyEvent* e);

private slots:
    void on_Btn1EngineRotations_clicked();
    void on_Btn2SensorsStatisticsFull_clicked();
    void on_Btn3SensorsStatisticsShort_clicked();

    void on_Btn4Close_clicked();

private:
    Ui::MenuSensorsWidget *ui;
};
