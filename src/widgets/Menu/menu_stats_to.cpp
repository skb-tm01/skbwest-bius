#include "core/helper.h"

#include "widgets/EventFilters/FocusEventFilter.h"
#include "widgets/Menu/menu_stats_to.h"
#include "widgets/ToInstructions.h"
#include "peripherials/sensors/SensorManager.h"

#include "SettingsManager.h"
#include "widgets/WindowManager.h"
#include "ui_menu_stats_to.h"

MenuStatsToWidget::MenuStatsToWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MenuStatsToWidget)
{
    setAttribute(Qt::WA_DeleteOnClose);
    setWindowFlags(Qt::Window | Qt::FramelessWindowHint);

    ui->setupUi(this);
    auto* eventFilter = new FocusEventFilter(this);

    ui->Btn1EtoManual->installEventFilter(eventFilter);
    ui->Btn2To1Manual->installEventFilter(eventFilter);
    ui->Btn3To2Manual->installEventFilter(eventFilter);
    ui->Btn4Close->installEventFilter(eventFilter);

    auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
    if(canworker!=nullptr)
    {
        connect(canworker,SIGNAL(received(CanMessage)),this,SLOT(OnCanMessage(CanMessage)));
        connect(canworker,SIGNAL(connectionLost()),this,SLOT(OnCanConnectionLost()));
        connect(canworker,SIGNAL(connectionRestored()),this,SLOT(OnCanConnectionRestore()));

        OnCanConnectionRestore();
    }

    ApplyNightDay();
    QObject::connect(SettingsManager::settings(),SIGNAL(NightDayChanged(EnumNightDayMode)), this, SLOT(ApplyNightDay()));
}

void MenuStatsToWidget::ApplyNightDay()
{
    QPixmap bookIcon = Helper::LoadPixmap("book_1.svg");
    QColor forecolor;

    auto nightDayMode = SettingsManager::settings()->currentNightDayMode();
    if (nightDayMode==EnumNightDayMode::Day)
        forecolor = Qt::black;
    else
        forecolor = Qt::white;

    bookIcon = Helper::SetPixmapColor(bookIcon, forecolor);

    ui->Btn1EtoManual->setIcon(bookIcon);
    ui->Btn2To1Manual->setIcon(bookIcon);
    ui->Btn3To2Manual->setIcon(bookIcon);
}

MenuStatsToWidget::~MenuStatsToWidget()
{
    delete ui;
}

void MenuStatsToWidget::OnCanMessage(CanMessage msg)
{
    switch(msg.can_id )
    {
        case  CanPacketProbegResponse::packet_id:
        {
            auto packet = CanPacketProbegResponse::Parse(msg.data);
            switch(packet.type)
            {
                case CanPacketProbegType::probeg_enginetime_before_eto:
                    ui->ETO_time->setText(QString::number(packet.value/10.0)+ QString::fromUtf8(" ч."));
                    break;
                case CanPacketProbegType::probeg_enginetime_before_to1:
                    ui->TO1_time->setText(QString::number(packet.value/10.0)+ QString::fromUtf8(" ч."));
                    break;
                case CanPacketProbegType::probeg_enginetime_before_to2:
                    ui->TO2_time->setText(QString::number(packet.value/10.0)+ QString::fromUtf8(" ч."));
                    break;
                default:
                    break;
            }
            break;
        }
    default:
        break;
    }
}

void MenuStatsToWidget::OnCanConnectionLost()
{
    ui->ETO_time->setText("---");
    ui->TO1_time->setText("---");
    ui->TO2_time->setText("---");
}

void MenuStatsToWidget::OnCanConnectionRestore()
{
    CanPacketProbegRequest::Send(CanPacketProbegType::probeg_enginetime_before_eto);
    CanPacketProbegRequest::Send(CanPacketProbegType::probeg_enginetime_before_to1);
    CanPacketProbegRequest::Send(CanPacketProbegType::probeg_enginetime_before_to2);
}

void MenuStatsToWidget::keyPressEvent(QKeyEvent* e)
{
    switch(e->key())
    {
    case Qt::Key_F1:
        ui->Btn1EtoManual->click();
        break;
    case Qt::Key_F2:
        ui->Btn2To1Manual->click();
        break;
    case Qt::Key_F3:
        ui->Btn3To2Manual->click();
        break;
    case Qt::Key_Escape:
        WindowManager::getInstance()->CloseMe(this);
        break;
    }
}

void MenuStatsToWidget::on_Btn1EtoManual_clicked()
{
    QString title = QString::fromUtf8("Ежесменное техническое обслуживание\n");
    auto sensor = SensorManager::instance().GetSensor("do_eto");
    auto *w = new ToInstructions(this);
    w->SetData(sensor, title);
    w->move(pos());
    w->resize(size());
    WindowManager::getInstance()->Open(this,w);
}


void MenuStatsToWidget::on_Btn2To1Manual_clicked()
{
    QString title = QString::fromUtf8("Первое техническое обслуживание\n");
    auto sensor = SensorManager::instance().GetSensor("do_to_1");
    auto *w = new ToInstructions(this);
    w->SetData(sensor, title);
    w->move(pos());
    w->resize(size());
    WindowManager::getInstance()->Open(this,w);
}

void MenuStatsToWidget::on_Btn3To2Manual_clicked()
{
    QString title = QString::fromUtf8("Второе техническое обслуживание\n");
    auto sensor = SensorManager::instance().GetSensor("do_to_2");
    auto *w = new ToInstructions(this);
    w->SetData(sensor, title);
    w->move(pos());
    w->resize(size());
    WindowManager::getInstance()->Open(this,w);
}

void MenuStatsToWidget::on_Btn4Close_clicked()
{
    WindowManager::getInstance()->CloseMe(this);
}
