#include "core/AudioHelper.h"
#include "core/PathHelper.h"
#include "core/helper.h"

#include "peripherials/PeripherialsManager.h"
#include "peripherials/can/packets/Analog.h"
#include "peripherials/can/packets/Control.h"
#include "peripherials/can/packets/Taho.h"
#include "peripherials/can/packets/WorkModeStatus.h"

#include "widgets/EventFilters/CheckBoxEventFilter.h"
#include "widgets/EventFilters/FocusEventFilter.h"
#include "widgets/EventFilters/ComboboxEventFilter.h"
#include "widgets/EventFilters/IdleEventFilter.h"

#include "widgets/WindowManager.h"
#include "widgets/Menu/menu_settings_workmodes.h"
#include "ui_menu_settings_workmodes.h"

MenuSettingsWorkmodesWidget::MenuSettingsWorkmodesWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MenuSettingsWorkmodesWidget)
{
    checkbox_1_1 = false;
    checkbox_1_1 = false;

    checkbox_2_1 = false;
    checkbox_2_2 = false;

    checkbox_3_1 = false;
    checkbox_3_2 = false;

    checkbox_4_1 = false;
    checkbox_4_2 = false;

    checkbox_5_1 = false;
    checkbox_5_2 = false;

    setAttribute(Qt::WA_DeleteOnClose);
    setWindowFlags(Qt::Window | Qt::FramelessWindowHint);

    ui->setupUi(this);

    setTabOrder(ui->comboBox_mode, ui->comboBox_grainType);
    setTabOrder(ui->comboBox_grainType, ui->comboBox_lossType);
    setTabOrder(ui->comboBox_lossType,ui->CheckBoxBeaterDrum);

    setTabOrder(ui->CheckBoxBeaterDrum,ui->CheckBoxConcaveGap);
    setTabOrder(ui->CheckBoxConcaveGap,ui->CheckBoxBlower);
    setTabOrder(ui->CheckBoxBlower,ui->CheckBoxChafferSieve);
    setTabOrder(ui->CheckBoxChafferSieve,ui->CheckBoxShoeSieve);
    setTabOrder(ui->CheckBoxShoeSieve,ui->akc_threshing_drum_our);

    setTabOrder(ui->akc_threshing_drum_our,ui->akc_concave_gap_our);
    setTabOrder(ui->akc_concave_gap_our,ui->akc_blower_rpm_our);
    setTabOrder(ui->akc_blower_rpm_our,ui->akc_chaffer_sieve_our);
    setTabOrder(ui->akc_chaffer_sieve_our,ui->akc_shover_sieve_our);
    setTabOrder(ui->akc_shover_sieve_our,ui->akc_loss_in_filtration_our);
    setTabOrder(ui->akc_loss_in_filtration_our,ui->akc_loss_in_straw_our);
    setTabOrder(ui->akc_loss_in_straw_our,ui->Btn1Close);

    auto* eventFilter = new FocusEventFilter(this);
    ui->Btn1Close->installEventFilter(eventFilter);

    ApplyNightDay(SettingsManager::settings()->currentNightDayMode());
    QObject::connect(SettingsManager::settings(),SIGNAL(NightDayChanged(EnumNightDayMode)), this, SLOT(ApplyNightDay(EnumNightDayMode)));

    auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
    if(canworker!=nullptr)
    {
        connect(canworker,SIGNAL(received(CanMessage)),this,SLOT(OnCanMessage(CanMessage)));
        connect(canworker,SIGNAL(connectionLost()),this,SLOT(OnCanConnectionLost()));
        connect(canworker,SIGNAL(connectionRestored()),this,SLOT(OnCanConnectionRestore()));
        OnCanConnectionRestore();
    }

    OnTimerTimeout();
    QObject::connect(&_timer,SIGNAL(timeout()),this,SLOT(OnTimerTimeout()));
    _timer.start(5000);

    IdleEventFilter::instance->Pause();

    auto* cbEventFilter = new ComboboxEventFilter(this);
    for(auto* item: this->findChildren<QComboBox*>())
    {
        item->installEventFilter(eventFilter);
        item->installEventFilter(cbEventFilter);
    }

    auto* checkEventFilter = new CheckBoxEventFilter(this);
    for(auto* item: this->findChildren<QCheckBox*>())
    {
        item->installEventFilter(eventFilter);
        item->installEventFilter(checkEventFilter);
    }
}

MenuSettingsWorkmodesWidget::~MenuSettingsWorkmodesWidget()
{
    delete ui;
}

void MenuSettingsWorkmodesWidget::keyPressEvent(QKeyEvent* e)
{
    switch(e->key())
    {
    case Qt::Key_Escape:
        IdleEventFilter::instance->Resume();
        WindowManager::getInstance()->CloseMe(this);
        break;
    }
}

void MenuSettingsWorkmodesWidget::on_Btn1Close_clicked()
{
    IdleEventFilter::instance->Resume();
    WindowManager::getInstance()->CloseMe(this);
}

void MenuSettingsWorkmodesWidget::ApplyNightDay(EnumNightDayMode mode)
{
    int iconsize = 48;
    int largeiconsize = 124;

    QColor foreColor = (mode==EnumNightDayMode::Day)?Qt::black:Qt::white;
    ui->l_1_threshing_drum_image->setPixmap(Helper::LoadPixmap("image121.svg",iconsize,foreColor));
    ui->label_3->setPixmap(Helper::LoadPixmap("image115.svg",iconsize,foreColor));
    ui->label_4->setPixmap(Helper::LoadPixmap("image131.svg",iconsize,foreColor));
    ui->label_5->setPixmap(Helper::LoadPixmap("image107.svg",iconsize,foreColor));
    ui->label_6->setPixmap(Helper::LoadPixmap("image108.svg",iconsize,foreColor));
    ui->label_32->setPixmap(Helper::LoadPixmap("image127.svg",iconsize,foreColor));
    ui->label_31->setPixmap(Helper::LoadPixmap("strawshaker_losses.svg",iconsize,foreColor));
    ui->label_plantImage->setPixmap(Helper::LoadPixmap("image136.svg",largeiconsize,foreColor));
    ui->label_engine_sign->setPixmap(Helper::LoadPixmap("image94.svg",iconsize,foreColor));
    ui->label_circles_1->setPixmap(Helper::LoadPixmap("image129.svg",iconsize,foreColor));
    ui->label_circles_2->setPixmap(Helper::LoadPixmap("image129.svg",iconsize,foreColor));
}

void MenuSettingsWorkmodesWidget::OnCanMessage(CanMessage msg)
{
    switch(msg.can_id )
    {
        case CanPackets::ControlGet::response_id:
            OnCanMessage_Control(msg);
            break;
     case CanPacketTahoResponse::packet_id:
    {
        auto packet = CanPacketTahoResponse::Parse(msg.data);
        if(packet.type == CanPacketTahoType::taho_speed_threshing)
        {
            ui->l_1_threshing_drum_current->setText(QString::number(packet.rpm));
        }
        else if(packet.type == CanPacketTahoType::taho_speed_fan)
        {
            ui->Label3BlowerRPMCur ->setText(QString::number(packet.rpm));
        }
        else if(packet.type == CanPacketTahoType::taho_loss_left)
        {
            ui->Label6LossInFiltrationChCur->setText(QString::number(packet.rpm));
        }
        else if(packet.type == CanPacketTahoType::taho_loss_right)
        {
            ui->Label7LossInStrawChCur->setText(QString::number(packet.rpm));
        }
        else if(packet.type == CanPacketTahoType::taho_speed_engine)
        {
            ui->label_Engine->setText(QString::number(packet.rpm));
        }
        break;
    }

    case CanPacketAnalogResponse::packet_id:
   {
       auto packet = CanPacketAnalogResponse::Parse(msg.data);
       if(packet.type == CanPacketAnalogType::can_analog_type_concaveclearance)
       {
           ui->Label2ConcaveGapCur->setText(QString::number(packet.state));
       }
       else if(packet.type == CanPacketAnalogType::can_analog_type_sieves_lower)
       {
           ui->Label4ChafferSieveCur->setText(QString::number(packet.state/10.0f,'f',0));
       }
       else if(packet.type == CanPacketAnalogType::can_analog_type_sieves_higher)
       {
           ui->Label5ShoeSieveCur->setText(QString::number(packet.state/10.0f,'f',0));
       }
       break;
   }


     case CanPacketSettingsGetResponse::packet_id:
    {
        auto packet = CanPacketSettingsGetResponse::Parse(msg.data);
        if(packet.type == CanPacketSettingsType::settings_vid_zerno)
        {
            SetGrainImage(static_cast<CanPacketSettignsGrainType>(packet.value));
            if(ui->comboBox_grainType->currentIndex() != packet.value)
                ui->comboBox_grainType->setCurrentIndex(packet.value);
        }
        else if(packet.type == CanPacketSettingsType::settings_test_poteri)
        {
            if(ui->comboBox_lossType->currentIndex() != packet.value)
                ui->comboBox_lossType->setCurrentIndex(packet.value);
        }
        else if(packet.type == CanPacketSettingsType::settings_auto_man)
        {
            if(ui->comboBox_mode->currentIndex() != packet.value)
                ui->comboBox_mode->setCurrentIndex(packet.value);

            ChangeModeControl(static_cast<WorkModes>(packet.value));
        }
        else if(packet.type == CanPacketSettingsType::settings_modes_1)
        {
            ui->akc_threshing_drum_our->setNumber(packet.value);
        }
        else if(packet.type == CanPacketSettingsType::settings_modes_2)
        {
            ui->akc_concave_gap_our->setNumber(packet.value);
        }
        else if(packet.type == CanPacketSettingsType::settings_modes_3)
        {
            ui->akc_blower_rpm_our->setNumber(packet.value);
        }
        else if(packet.type == CanPacketSettingsType::settings_modes_4)
        {
            ui->akc_chaffer_sieve_our->setNumber(packet.value);
        }
        else if(packet.type == CanPacketSettingsType::settings_modes_5)
        {
            ui->akc_shover_sieve_our->setNumber(packet.value);
        }
        else if(packet.type == CanPacketSettingsType::settings_modes_6)
        {
            ui->akc_loss_in_filtration_our->setNumber(packet.value);
        }
        else if(packet.type == CanPacketSettingsType::settings_modes_7)
        {
            ui->akc_loss_in_straw_our->setNumber(packet.value);
        }
        break;
    }


    case CanPackets::WorkModeGet::response_id:
    {
        auto packet = CanPackets::WorkModeGet::ParseResponse(msg.data);

        switch(packet.channelId)
        {
            case CanPackets::WorkModeChannelId::wm_chan_beaterdrum:
                ui->l_1_threshing_drum_status->setText(packet.GetStatusText());
                ui->l_1_threshing_drum_recommended->setText(QString::number(packet.targetValue));
                break;
            case CanPackets::WorkModeChannelId::wm_chan_concavegap:
                ui->Label2ConcaveGapStat->setText(packet.GetStatusText());
                ui->Label2ConcaveGapRec->setText(QString::number(packet.targetValue));
                break;
            case CanPackets::WorkModeChannelId::wm_chan_blower:
                ui->Label3BlowerRPMStat->setText(packet.GetStatusText());
                ui->Label3BlowerRPMRec->setText(QString::number(packet.targetValue));
                break;
            case CanPackets::WorkModeChannelId::wm_chan_chaffer_sieve:
                ui->Label4ChafferSieveStat->setText(packet.GetStatusText());
                ui->Label4ChafferSieveRec->setText(QString::number(packet.targetValue));
                break;
            case CanPackets::WorkModeChannelId::wm_chan_shoe_sieve:
                ui->Label5ShoeSieveStat->setText(packet.GetStatusText());
                ui->Label5ShoeSieveRec->setText(QString::number(packet.targetValue));
                break;
            case CanPackets::WorkModeChannelId::wm_chan_loss_in_filtat:
                ui->Label6LossInFiltrationChStat->setText(packet.GetStatusText());
                ui->Label6LossInFiltrationChRec->setText(QString::number(packet.targetValue));
                break;
            case CanPackets::WorkModeChannelId::wm_chan_loss_in_straw:
                ui->Label7LossInStrawChStat->setText(packet.GetStatusText());
                ui->Label7LossInStrawChRec->setText(QString::number(packet.targetValue));
                break;
            default:
                break;
            }
        }
    default:
        break;
    }
}

void MenuSettingsWorkmodesWidget::OnCanMessage_Control(CanMessage msg)
{
    auto packet = CanPackets::ControlGet::ParseResponse(msg.data);
    if(packet.type!= SensorType::OUT)
        return;

    if(packet.id==1)
    {
        checkbox_1_1=packet.control_state&1?true:false;
    }
    else if(packet.id==2)
    {
        checkbox_1_2=packet.control_state&1?true:false;
    }
    else if(packet.id==8)
    {
        checkbox_2_1=packet.control_state&1?true:false;
    }
    else if(packet.id==9)
    {
        checkbox_2_2=packet.control_state&1?true:false;
    }
    else if(packet.id==4)
    {
        checkbox_3_1=packet.control_state&1?true:false;
    }
    else if(packet.id==6)
    {
        checkbox_3_2=packet.control_state&1?true:false;
    }
    else if(packet.id==3)
    {
        checkbox_4_1=packet.control_state&1?true:false;
    }
    else if(packet.id==5)
    {
        checkbox_4_2=packet.control_state&1?true:false;
    }
    else if(packet.id==0)
    {
        checkbox_5_1=packet.control_state&1?true:false;
    }
    else if(packet.id==7)
    {
        checkbox_5_2=packet.control_state&1?true:false;
    }

    if(!(checkbox_1_1||checkbox_1_2))
        ui->CheckBoxBeaterDrum->setCheckState(Qt::CheckState::Checked);
    else
        ui->CheckBoxBeaterDrum->setCheckState(Qt::CheckState::Unchecked);

    if(!(checkbox_2_1||checkbox_2_2))
        ui->CheckBoxConcaveGap->setCheckState(Qt::CheckState::Checked);
    else
        ui->CheckBoxConcaveGap->setCheckState(Qt::CheckState::Unchecked);

    if(!(checkbox_3_1||checkbox_3_2))
        ui->CheckBoxBlower->setCheckState(Qt::CheckState::Checked);
    else
        ui->CheckBoxBlower->setCheckState(Qt::CheckState::Unchecked);

    if(!(checkbox_4_1||checkbox_4_2))
        ui->CheckBoxChafferSieve->setCheckState(Qt::CheckState::Checked);
    else
        ui->CheckBoxChafferSieve->setCheckState(Qt::CheckState::Unchecked);

    if(!(checkbox_5_1||checkbox_5_2))
        ui->CheckBoxShoeSieve->setCheckState(Qt::CheckState::Checked);
    else
        ui->CheckBoxShoeSieve->setCheckState(Qt::CheckState::Unchecked);
}



void MenuSettingsWorkmodesWidget::OnCanConnectionLost()
{

}

void MenuSettingsWorkmodesWidget::OnCanConnectionRestore()
{
    CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_auto_man);
    CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_vid_zerno);
    CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_test_poteri);

    CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_modes_1);
    CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_modes_2);
    CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_modes_3);
    CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_modes_4);
    CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_modes_5);
    CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_modes_6);
    CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_modes_7);


    CanPackets::ControlGet::SendRequest(0xC0+0);
    CanPackets::ControlGet::SendRequest(0xC0+1);
    CanPackets::ControlGet::SendRequest(0xC0+2);
    CanPackets::ControlGet::SendRequest(0xC0+3);
    CanPackets::ControlGet::SendRequest(0xC0+4);
    CanPackets::ControlGet::SendRequest(0xC0+5);
    CanPackets::ControlGet::SendRequest(0xC0+6);
    CanPackets::ControlGet::SendRequest(0xC0+7);
    CanPackets::ControlGet::SendRequest(0xC0+8);
    CanPackets::ControlGet::SendRequest(0xC0+9);
}

void MenuSettingsWorkmodesWidget::OnTimerTimeout()
{
    CanPackets::WorkModeGet::SendRequest(CanPackets::WorkModeChannelId::wm_chan_beaterdrum);
    CanPackets::WorkModeGet::SendRequest(CanPackets::WorkModeChannelId::wm_chan_blower);
    CanPackets::WorkModeGet::SendRequest(CanPackets::WorkModeChannelId::wm_chan_chaffer_sieve);
    CanPackets::WorkModeGet::SendRequest(CanPackets::WorkModeChannelId::wm_chan_concavegap);
    CanPackets::WorkModeGet::SendRequest(CanPackets::WorkModeChannelId::wm_chan_loss_in_filtat);
    CanPackets::WorkModeGet::SendRequest(CanPackets::WorkModeChannelId::wm_chan_loss_in_straw);
    CanPackets::WorkModeGet::SendRequest(CanPackets::WorkModeChannelId::wm_chan_shoe_sieve);
}

void MenuSettingsWorkmodesWidget::SetGrainImage(CanPacketSettignsGrainType grainType)
{

    int largeiconsize = 124;

    QColor foreColor = SettingsManager::settings()->currentForeColor();

    switch(grainType)
    {
        case CanPacketSettignsGrainType::grain_alfalfa:
            ui->label_plantImage->setPixmap(Helper::LoadPixmap("alfalfa.svg",largeiconsize,foreColor));
            break;
        case CanPacketSettignsGrainType::grain_barley:
            ui->label_plantImage->setPixmap(Helper::LoadPixmap("barley.svg",largeiconsize,foreColor));
            break;
        case CanPacketSettignsGrainType::grain_buckwheat:
            ui->label_plantImage->setPixmap(Helper::LoadPixmap("buckwheat.svg",largeiconsize,foreColor));
            break;
        case CanPacketSettignsGrainType::grain_chickpea:
            ui->label_plantImage->setPixmap(Helper::LoadPixmap("chickpea.svg",largeiconsize,foreColor));
            break;
        case CanPacketSettignsGrainType::grain_clover:
            ui->label_plantImage->setPixmap(Helper::LoadPixmap("clover.svg",largeiconsize,foreColor));
            break;
        case CanPacketSettignsGrainType::grain_colza:
            ui->label_plantImage->setPixmap(Helper::LoadPixmap("colza.svg",largeiconsize,foreColor));
            break;
        case CanPacketSettignsGrainType::grain_corn:
            ui->label_plantImage->setPixmap(Helper::LoadPixmap("corn.svg",largeiconsize,foreColor));
            break;
        case CanPacketSettignsGrainType::grain_oats:
            ui->label_plantImage->setPixmap(Helper::LoadPixmap("oats.svg",largeiconsize,foreColor));
            break;
        case CanPacketSettignsGrainType::grain_rye:
            ui->label_plantImage->setPixmap(Helper::LoadPixmap("rye.svg",largeiconsize,foreColor));
            break;
        case CanPacketSettignsGrainType::grain_soy:
            ui->label_plantImage->setPixmap(Helper::LoadPixmap("soy.svg",largeiconsize,foreColor));
            break;
        case CanPacketSettignsGrainType::grain_sunflower:
            ui->label_plantImage->setPixmap(Helper::LoadPixmap("sunflower.svg",largeiconsize,foreColor));
            break;
        case CanPacketSettignsGrainType::grain_wheat:
            ui->label_plantImage->setPixmap(Helper::LoadPixmap("wheat.svg",largeiconsize,foreColor));
            break;
        default:
            break;
    }
}

void MenuSettingsWorkmodesWidget::on_comboBox_grainType_currentIndexChanged(int index)
{
    //Note: there is periodic event which check this. There is why you do not need to call OnConnectionRestore()
    CanPacketSettingsSetRequest::Send(CanPacketSettingsType::settings_vid_zerno, index);
}

void MenuSettingsWorkmodesWidget::on_comboBox_lossType_currentIndexChanged(int index)
{

    //Note: there is periodic event which check this. There is why you do not need to call OnConnectionRestore()
    //Todo: add OnConnectionRestore() when implement CAN request optimization
    CanPacketSettingsSetRequest::Send(CanPacketSettingsType::settings_test_poteri, index);
}

void MenuSettingsWorkmodesWidget::on_comboBox_mode_currentIndexChanged(int index)
{
    CanPacketSettingsSetRequest::Send(CanPacketSettingsType::settings_auto_man, index);
    OnCanConnectionRestore();
}

void MenuSettingsWorkmodesWidget::ChangeModeControl(WorkModes modes)
{
    if(modes==WorkModes::workmode_handoperated)
    {
        ui->CheckBoxBeaterDrum->setVisible(false);
        ui->CheckBoxBlower->setVisible(false);
        ui->CheckBoxChafferSieve->setVisible(false);
        ui->CheckBoxConcaveGap->setVisible(false);
        ui->CheckBoxShoeSieve->setVisible(false);
        ui->label_instruction->setVisible(false);
    }
    else
    {
        ui->CheckBoxBeaterDrum->setVisible(true);
        ui->CheckBoxBlower->setVisible(true);
        ui->CheckBoxChafferSieve->setVisible(true);
        ui->CheckBoxConcaveGap->setVisible(true);
        ui->CheckBoxShoeSieve->setVisible(true);
        ui->label_instruction->setVisible(true);
    }
}

void MenuSettingsWorkmodesWidget::on_akc_threshing_drum_our_returnPressed()
{
     CanPacketSettingsSetRequest::Send(CanPacketSettingsType::settings_modes_1,ui->akc_threshing_drum_our->number());
}

void MenuSettingsWorkmodesWidget::on_akc_concave_gap_our_returnPressed()
{
    CanPacketSettingsSetRequest::Send(CanPacketSettingsType::settings_modes_2,ui->akc_concave_gap_our->number());
}

void MenuSettingsWorkmodesWidget::on_akc_blower_rpm_our_returnPressed()
{

    CanPacketSettingsSetRequest::Send(CanPacketSettingsType::settings_modes_3,ui->akc_blower_rpm_our->number());
}

void MenuSettingsWorkmodesWidget::on_akc_chaffer_sieve_our_returnPressed()
{

    CanPacketSettingsSetRequest::Send(CanPacketSettingsType::settings_modes_4,ui->akc_chaffer_sieve_our->number());
}

void MenuSettingsWorkmodesWidget::on_akc_shover_sieve_our_returnPressed()
{

    CanPacketSettingsSetRequest::Send(CanPacketSettingsType::settings_modes_5,ui->akc_shover_sieve_our->number());
}

void MenuSettingsWorkmodesWidget::on_akc_loss_in_filtration_our_returnPressed()
{

    CanPacketSettingsSetRequest::Send(CanPacketSettingsType::settings_modes_6,ui->akc_loss_in_filtration_our->number());
}

void MenuSettingsWorkmodesWidget::on_akc_loss_in_straw_our_returnPressed()
{

    CanPacketSettingsSetRequest::Send(CanPacketSettingsType::settings_modes_7,ui->akc_loss_in_straw_our->number());
}


void MenuSettingsWorkmodesWidget::on_CheckBoxBeaterDrum_clicked(bool checked)
{
    qDebug()<<"CLICK";

    CanPackets::ControlSet::SendRequest(0xC0+1,checked?0:1);
    CanPackets::ControlSet::SendRequest(0xC0+2,checked?0:1);
}

void MenuSettingsWorkmodesWidget::on_CheckBoxConcaveGap_clicked(bool checked)
{
    CanPackets::ControlSet::SendRequest(0xC0+8,checked?0:1);
    CanPackets::ControlSet::SendRequest(0xC0+9,checked?0:1);
}

void MenuSettingsWorkmodesWidget::on_CheckBoxBlower_clicked(bool checked)
{
    CanPackets::ControlSet::SendRequest(0xC0+4,checked?0:1);
    CanPackets::ControlSet::SendRequest(0xC0+6,checked?0:1);
}

void MenuSettingsWorkmodesWidget::on_CheckBoxChafferSieve_clicked(bool checked)
{
    CanPackets::ControlSet::SendRequest(0xC0+3,checked?0:1);
    CanPackets::ControlSet::SendRequest(0xC0+5,checked?0:1);
}

void MenuSettingsWorkmodesWidget::on_CheckBoxShoeSieve_clicked(bool checked)
{
    CanPackets::ControlSet::SendRequest(0xC0+0,checked?0:1);
    CanPackets::ControlSet::SendRequest(0xC0+7,checked?0:1);
}
