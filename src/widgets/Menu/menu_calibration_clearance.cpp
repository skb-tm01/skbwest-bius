#include <QEvent>
#include <QMessageBox>

#include "peripherials/PeripherialsManager.h"
#include "peripherials/can/packets/Analog.h"

#include "widgets/EventFilters/FocusEventFilter.h"
#include "widgets/Menu/menu_calibration_clearance.h"
#include "widgets/Windows/ConfirmationWindow.h"

#include "ui_menu_calibration_clearance.h"
#include "widgets/WindowManager.h"

MenuCalibrationClearanceWidget::MenuCalibrationClearanceWidget(CanPackets::CalibrationType type, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MenuCalibrationClearanceWidget)
{
    _type = type;
    min = -1;
    max = -1;
    request2_id = 255;
    setmin = true;

    setAttribute(Qt::WA_DeleteOnClose);
    setWindowFlags(Qt::Window | Qt::FramelessWindowHint);

    ui->setupUi(this);
    ui->Btn3Save->setEnabled(false);

    if(_type == CanPackets::CalibrationType::clearance)
    {
        request2_id = 1;
        ui->title->setText(QString::fromUtf8("ЗАЗОР ПОДБАРАБАНЬЯ"));
    }
    else if(_type == CanPackets::CalibrationType::lower_sieves)
    {
        request2_id = 7;
        ui->title->setText(QString::fromUtf8("НИЖНИЕ РЕШЕТА"));
    }
    else if(_type == CanPackets::CalibrationType::higher_siever)
    {
        request2_id = 8;
        ui->title->setText(QString::fromUtf8("ВЕРХНИЕ РЕШЕТА"));
    }

    auto* eventFilter = new FocusEventFilter(this);
    ui->Btn4Close->installEventFilter(eventFilter);
    ui->Btn1Minimum->installEventFilter(eventFilter);
    ui->Btn2Maximum->installEventFilter(eventFilter);
    ui->Btn3Save->installEventFilter(eventFilter);

    auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
    if(canworker!=nullptr)
    {
        connect(canworker,SIGNAL(received(CanMessage)),this,SLOT(OnCanMessage(CanMessage)));
        connect(canworker,SIGNAL(connectionLost()),this,SLOT(OnCanConnectionLost()));
        connect(canworker,SIGNAL(connectionRestored()),this,SLOT(OnCanConnectionRestore()));
        OnCanConnectionRestore();
    }

    QObject::connect(&timer,SIGNAL(timeout()),this,SLOT(TimerTimeout()));
    timer.start(500);
}

MenuCalibrationClearanceWidget::~MenuCalibrationClearanceWidget()
{
    delete ui;
}

void MenuCalibrationClearanceWidget::TimerTimeout()
{
    CanPacketAnalogRequest::Send(static_cast<CanPacketAnalogType>(request2_id));
}

void MenuCalibrationClearanceWidget::OnCanMessage(CanMessage msg)
{
    if(msg.can_id == CanPacketAnalogResponse::packet_id)
    {
        auto packet = CanPacketAnalogResponse::Parse(msg.data);
        if(packet.type != request2_id)
            return;
        ui->label_value->setText(QString::number(packet.adc));
        if(_type == CanPackets::CalibrationType::clearance)
        {
            ui->label_absolute->setText(QString::number(packet.state)+QString::fromUtf8(" мм."));
        }
        else
        {
            ui->label_absolute->setText(QString::number(packet.state/10.0,'f',1)+QString::fromUtf8(" мм."));
        }
    }
    else if(msg.can_id == CanPackets::CalibrationInfoGet::response_id)
    {
        auto packet = CanPackets::CalibrationInfoGet::ParseResponse(msg.data);
        if(packet.type != _type)
            return;

        if(setmin)
        {
            min=packet.adcval;
            ui->label_min->setText(QString::number(min));
        }
        else
        {
            max=packet.adcval;
            ui->label_max->setText(QString::number(max));
        }

        if(max>-1 && min>-1)
            ui->Btn3Save->setEnabled(true);
    }
    else if(msg.can_id == CanPackets::CalibrationSave::response_id)
    {
        auto* w = new ConfirmationWindow(this);
        w->SetTitle(QString::fromUtf8("Информация"));
        w->SetText(QString::fromUtf8("Калибровка успешно произведена"));
        w->SetCancelVisibility(false);
        w->exec();
    }
}

void MenuCalibrationClearanceWidget::OnCanConnectionLost()
{

}

void MenuCalibrationClearanceWidget::OnCanConnectionRestore()
{

}

void MenuCalibrationClearanceWidget::keyPressEvent(QKeyEvent* e)
{
    switch(e->key())
    {
    case Qt::Key_F1:
        if(max>-1 && min>-1)
            on_Btn3Save_clicked();
        break;
    case Qt::Key_Escape:
        WindowManager::getInstance()->CloseMe(this);
        break;
    }
}

void MenuCalibrationClearanceWidget::on_Btn1Minimum_clicked()
{
    this->setmin=true;
    CanPackets::CalibrationInfoGet::SendRequest(_type);
}

void MenuCalibrationClearanceWidget::on_Btn2Maximum_clicked()
{
    this->setmin=false;
    CanPackets::CalibrationInfoGet::SendRequest(_type);
}

void MenuCalibrationClearanceWidget::on_Btn3Save_clicked()
{
    CanPackets::CalibrationSave::SendRequest(_type,static_cast<uint16_t>(min),static_cast<uint16_t>(max));
}

void MenuCalibrationClearanceWidget::on_Btn4Close_clicked()
{
    timer.stop();
    WindowManager::getInstance()->CloseMe(this);
    deleteLater();
}
