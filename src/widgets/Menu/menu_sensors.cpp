#include "widgets/flowwindow.h"
#include "widgets/sensors_statistics_full.h"
#include "widgets/sensors_statistics_short.h"

#include "widgets/EventFilters/FocusEventFilter.h"
#include "widgets/Menu/menu_sensors.h"
#include "widgets/Menu/menu_sensors_rotations.h"

#include "ui_menu_sensors.h"
#include "widgets/WindowManager.h"
MenuSensorsWidget::MenuSensorsWidget(QWidget *parent) : QWidget(parent), ui(new Ui::MenuSensorsWidget)
{
    setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    ui->setupUi(this);

    auto* eventFilter = new FocusEventFilter(this);
    ui->Btn4Close->installEventFilter(eventFilter);
    ui->Btn1EngineRotations->installEventFilter(eventFilter);
    ui->Btn2SensorsStatisticsFull->installEventFilter(eventFilter);
    ui->Btn3SensorsStatisticsShort->installEventFilter(eventFilter);
}

MenuSensorsWidget::~MenuSensorsWidget()
{
    delete ui;
}

void MenuSensorsWidget::keyPressEvent(QKeyEvent* e)
{
    switch(e->key())
    {
    case Qt::Key_Escape:
        WindowManager::getInstance()->CloseMe(this);
        break;
    case Qt::Key_F1:
        ui->Btn1EngineRotations->click();
        break;
    case Qt::Key_F2:
        ui->Btn2SensorsStatisticsFull->click();
        break;
    case Qt::Key_F3:
        ui->Btn3SensorsStatisticsShort->click();
        break;
    }
}

void MenuSensorsWidget::on_Btn1EngineRotations_clicked()
{
    auto *w = new MenuSensorsRotationsWidget(this);
    w->resize(size());
    w->move(pos());
    WindowManager::getInstance()->Open(this,w);
    w->SetClosable(true);
}

void MenuSensorsWidget::on_Btn2SensorsStatisticsFull_clicked()
{
    auto * w = new sensors_statistics_full(this);
    w->resize(size());
    w->move(pos());    
    WindowManager::getInstance()->Open(this,w);
}

void MenuSensorsWidget::on_Btn3SensorsStatisticsShort_clicked()
{
    auto * w = new sensors_statistics_short(this);
    w->resize(size());
    w->move(pos());
    WindowManager::getInstance()->Open(this,w);
}

void MenuSensorsWidget::on_Btn4Close_clicked()
{
    WindowManager::getInstance()->CloseMe(this);
}
