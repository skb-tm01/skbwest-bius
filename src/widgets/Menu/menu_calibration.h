#pragma once

#include <QWidget>
#include <QKeyEvent>

namespace Ui {
class MenuCalibrationWidget;
}

class MenuCalibrationWidget : public QWidget
{
    Q_OBJECT

public:
    // parent = MainWidget
    explicit MenuCalibrationWidget(QWidget *parent = 0);
    ~MenuCalibrationWidget();

protected:
    void keyPressEvent(QKeyEvent* e);

private slots:
    void on_Btn1Clearance_clicked();

    void on_Btn2Lowersieves_clicked();

    void on_Btn3Highersieves_clicked();

    void on_Btn4Close_clicked();

private:
    Ui::MenuCalibrationWidget *ui;
};
