#pragma once

#include <QWidget>
#include <QString>
#include <QKeyEvent>

#include "peripherials/can/CanMessage.h"


namespace Ui {
class MenuSettingsWidget;
}

class MenuSettingsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MenuSettingsWidget(QWidget *parent = 0);
    ~MenuSettingsWidget();

protected:
    void keyPressEvent(QKeyEvent* e);

private slots:
    void on_Btn1SettingsSystem_clicked();

    void on_Btn2SettingsWorkmodes_clicked();

    void on_Btn3SettingsTerminal_clicked();

    void on_Btn5Close_clicked();

    void OnCanMessage(CanMessage msg);
    void OnCanConnectionLost();



private:
    int _password;

    Ui::MenuSettingsWidget *ui;
};
