#pragma once

#include <QKeyEvent>
#include <QWidget>

#include "SettingsManager.h"

namespace Ui {
    class MenuHelpWidget;
}

class MenuHelpWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MenuHelpWidget(QWidget *parent = 0);
    ~MenuHelpWidget();

protected:
    void keyPressEvent(QKeyEvent* e);
    bool eventFilter(QObject *obj, QEvent *event);

private slots:
    void on_Btn1Close_clicked();
    void on_Btn2Backward_clicked();
    void on_Btn3Forward_clicked();

    void ScrollUp();
    void ScrollDown();

    void ApplyNightDay(EnumNightDayMode mode);

private:
    Ui::MenuHelpWidget *ui;
};
