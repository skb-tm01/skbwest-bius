#pragma once

#include <QKeyEvent>
#include <QWidget>

#include "peripherials/PeripherialsManager.h"
#include "peripherials/can/CanMessage.h"
#include "peripherials/can/packets/Probeg.h"

namespace Ui {
class MenuStatsToWidget;
}


class MenuStatsToWidget : public QWidget
{
    Q_OBJECT

public:
    // parent = MainWidget
    explicit MenuStatsToWidget(QWidget *parent = 0);
    ~MenuStatsToWidget();

protected:
    void keyPressEvent(QKeyEvent* e);

private slots:
    void OnCanMessage(CanMessage msg);
    void OnCanConnectionLost();
    void OnCanConnectionRestore();
    void on_Btn1EtoManual_clicked();
    void on_Btn2To1Manual_clicked();
    void on_Btn3To2Manual_clicked();
    void on_Btn4Close_clicked();
    void ApplyNightDay();
private:
    Ui::MenuStatsToWidget *ui;
};
