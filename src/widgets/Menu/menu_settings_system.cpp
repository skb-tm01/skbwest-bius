#include <QDialog>
#include <QMessageBox>
#include <QListView>
#include <QDebug>
#include "core/AudioHelper.h"
#include "core/PathHelper.h"

#include "peripherials/can/packets/ProbegReset.h"

#include "widgets/EventFilters/FocusEventFilter.h"
#include "widgets/EventFilters/CheckBoxEventFilter.h"
#include "widgets/EventFilters/ComboboxEventFilter.h"

#include "widgets/Menu/menu_settings_system.h"
#include "widgets/Windows/PasswordChangeWindow.h"
#include "widgets/Windows/PasswordEnterWindow.h"
#include "widgets/Windows/ConfirmationWindow.h"

#include "SettingsManager.h"

#include "ui_menu_settings_system.h"

#include "peripherials/combines/CombineKeeper.h"
#include "peripherials/combines/Combine.h"
#include "widgets/WindowManager.h"

MenuSettingsSystemWidget::MenuSettingsSystemWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MenuSettingsSystemWidget)
{
    setAttribute(Qt::WA_DeleteOnClose);
    setWindowFlags(Qt::Window | Qt::FramelessWindowHint);

    _password1 = -1;
    _password2 = -1;
    _fuelSensType_initialized =false;
    _combineType_initialized =false;
    _reaperType_initialized = false;
    _engineType_initialized = false;

    ui->setupUi(this);

    //taborder
    setTabOrder(ui->combineType, ui->reaperType);
    setTabOrder(ui->reaperType, ui->fuelSensType);
    setTabOrder(ui->fuelSensType, ui->engineType);
    setTabOrder(ui->engineType, ui->inputcontrol_serialNumber);
    setTabOrder(ui->inputcontrol_serialNumber,ui->inputcontrol_time_eto);
    setTabOrder(ui->inputcontrol_time_eto,ui->inputcontrol_time_to1);
    setTabOrder(ui->inputcontrol_time_to1,ui->inputcontrol_time_to2);
    setTabOrder(ui->inputcontrol_time_to2, ui->inputcontrol_coeff_engine);
    setTabOrder(ui->inputcontrol_coeff_engine,ui->inputcontrol_coeff_speed);
    setTabOrder(ui->inputcontrol_coeff_speed, ui->inputcontrol_wheel_diameter);
    setTabOrder(ui->inputcontrol_wheel_diameter, ui->chk_temperature_rev);
    setTabOrder(ui->chk_temperature_rev, ui->inputcontrol_temperature_rev);
    setTabOrder(ui->inputcontrol_temperature_rev, ui->chk_timeinterval_rev);
    setTabOrder(ui->chk_timeinterval_rev, ui->inputcontrol_timeinterval_rev);
    setTabOrder(ui->inputcontrol_timeinterval_rev, ui->Btn1ClearTotalstats);
    setTabOrder(ui->Btn1ClearTotalstats,ui->Btn2ClearAlarmstats);
    setTabOrder(ui->Btn2ClearAlarmstats,ui->Btn3FactoryReset);
    setTabOrder(ui->Btn3FactoryReset,ui->Btn4ChangePassword1);
    setTabOrder(ui->Btn4ChangePassword1,ui->Btn5ChangePassword2);
    setTabOrder(ui->Btn5ChangePassword2,ui->Btn6Close);

    //combine type combo
    auto combines = CombineKeeper::GetInstance().GetCombineList();
    for(int i=0;i<combines.count();i++)
    {
        ui->combineType->addItem(combines[i]->getName(), i);
    }

    //reaper type combo
    //ui->reaperType->setView(new QListView(ui->reaperType));
    for(int i =5;i<9;i++)
    {
        for(int j=0;j<10;j++)
        {
            ui->reaperType->addItem(QString::number(i)+"."+QString::number(j)+QString::fromUtf8(" метров"), i*100+j*10);
        }
    }
    ui->reaperType->addItem(QString("9.0") + QString::fromUtf8(" метров"), 900);

    //ui->engineType->setView(new QListView(ui->engineType))

    ui->fuelSensType->addItem(QString::fromUtf8("0: резистивный"));
    ui->fuelSensType->addItem(QString::fromUtf8("1: емкостной"));

    ui->inputcontrol_temperature_rev->setLimitLow(TEMPERATURE_REV_OFF);
    ui->inputcontrol_temperature_rev->setLimitHigh(TEMPERATURE_REV_MAX);
    ui->inputcontrol_timeinterval_rev->setLimitLow(TIMEINTERVAL_REV_OFF);
    ui->inputcontrol_timeinterval_rev->setLimitHigh(TIMEINTERVAL_REV_MAX);

    auto* eventFilter = new FocusEventFilter(this);
    ui->Btn6Close->installEventFilter(eventFilter);
    ui->Btn4ChangePassword1->installEventFilter(eventFilter);
    ui->Btn5ChangePassword2->installEventFilter(eventFilter);
    ui->Btn2ClearAlarmstats->installEventFilter(eventFilter);
    ui->Btn1ClearTotalstats->installEventFilter(eventFilter);
    ui->Btn3FactoryReset->installEventFilter(eventFilter);

    ui->chk_temperature_rev->installEventFilter(eventFilter);
    ui->chk_timeinterval_rev->installEventFilter(eventFilter);

    for(auto* item: this->findChildren<QComboBox*>())
    {
        item->installEventFilter(eventFilter);
    }

    for(auto* item: this->findChildren<ArrowKeyControl*>())
    {
        item->setMinimumSize(180,44);
        item->setMaximumSize(180,44);
    }

    auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
    if(canworker!=nullptr)
    {
        connect(canworker,SIGNAL(received(CanMessage)),this,SLOT(OnCanMessage(CanMessage)));
        connect(canworker,SIGNAL(connectionLost()),this,SLOT(OnCanConnectionLost()));
        connect(canworker,SIGNAL(connectionRestored()),this,SLOT(OnCanConnectionRestore()));
        OnCanConnectionRestore();
    }

    auto* cbEventFilter = new ComboboxEventFilter(this);
    for(auto* item: this->findChildren<QComboBox*>())
    {
        item->installEventFilter(eventFilter);
        item->installEventFilter(cbEventFilter);
    }


    auto* checkEventFilter = new CheckBoxEventFilter(this);
    for(auto* item: this->findChildren<QCheckBox*>())
    {
        item->installEventFilter(eventFilter);
        item->installEventFilter(checkEventFilter);
    }
}

MenuSettingsSystemWidget::~MenuSettingsSystemWidget()
{
    delete ui;
}

void MenuSettingsSystemWidget::OnCanMessage(CanMessage msg)
{
    if(msg.can_id == CanPacketSettingsGetResponse::packet_id)
    {
        auto packet = CanPacketSettingsGetResponse::Parse(msg.data);
        switch(packet.type)
        {
            case CanPacketSettingsType::settings_combine_sn:
                if(!ui->inputcontrol_serialNumber->hasFocus())
                    ui->inputcontrol_serialNumber->setNumber(packet.value);
                break;
            case CanPacketSettingsType::settings_diameter:
                if(!ui->inputcontrol_wheel_diameter->hasFocus())
                    ui->inputcontrol_wheel_diameter->setNumber(packet.value);
                break;
            case CanPacketSettingsType::settings_k_speed:
                if(!ui->inputcontrol_coeff_speed->hasFocus())
                    ui->inputcontrol_coeff_speed->setNumber(packet.value);
                break;
            case CanPacketSettingsType::settings_k_motor:
                if(!ui->inputcontrol_coeff_engine->hasFocus())
                    ui->inputcontrol_coeff_engine->setNumber(packet.value);
                break;
            case CanPacketSettingsType::settings_motor_eto:
                if(!ui->inputcontrol_time_eto->hasFocus())
                    ui->inputcontrol_time_eto->setNumber(packet.value);
                break;
            case CanPacketSettingsType::settings_motor_to1:
                if(!ui->inputcontrol_time_to1->hasFocus())
                    ui->inputcontrol_time_to1->setNumber(packet.value);
                break;
            case CanPacketSettingsType::settings_motor_to2:
                if(!ui->inputcontrol_time_to2->hasFocus())
                    ui->inputcontrol_time_to2->setNumber(packet.value);
                break;
            case CanPacketSettingsType::settings_temperature_rev:

                qDebug() << "CAN settings_temperature_rev packet received";
                if(!ui->inputcontrol_temperature_rev->hasFocus()
                        && !ui->chk_temperature_rev->hasFocus())
                {
                    qDebug() << "CAN settings_temperature_rev !hasfocus";
                    qDebug() << "setting value "<< packet.value;
                    if(packet.value == TEMPERATURE_REV_OFF)
                    {  
                        ui->inputcontrol_temperature_rev->setNumber(packet.value);
                        ui->chk_temperature_rev->setCheckState(Qt::Unchecked);
                    }
                    else
                    {             
                        ui->inputcontrol_temperature_rev->setNumber(packet.value);
                        ui->chk_temperature_rev->setCheckState(Qt::Checked);
                    }
                }
                break;

            case CanPacketSettingsType::settings_timeinterval_rev:
                qDebug() << "CAN settings_timeinterval_rev packet received";
                if(!ui->inputcontrol_timeinterval_rev->hasFocus()
                        && !ui->chk_timeinterval_rev->hasFocus())
                {
                    qDebug() << "CAN settings_timeinterval_rev !hasfocus";
                    qDebug() << "setting value "<< packet.value;
                    if(packet.value == TIMEINTERVAL_REV_OFF)
                    {                       
                        ui->inputcontrol_timeinterval_rev->setNumber(packet.value);
                        ui->chk_timeinterval_rev->setCheckState(Qt::Unchecked);
                    }
                    else
                    {
                        ui->inputcontrol_timeinterval_rev->setNumber(packet.value);
                        ui->chk_timeinterval_rev->setCheckState(Qt::Checked);
                    }
                }
                break;

            case CanPacketSettingsType::settings_engine_type:
                if(!ui->engineType->hasFocus())
                    ui->engineType->setCurrentIndex(packet.value);
                _engineType_initialized = true;
                break;
            case CanPacketSettingsType::settings_combine_model_3:{
                CombineKeeper::GetInstance().setCurrentCombine(packet.value);
                auto index = CombineKeeper::GetInstance().GetCurrentIndexInList();
                if(ui->combineType->currentIndex() != index)
                {
                    ui->combineType->setCurrentIndex(index);
                }
                _combineType_initialized = true;
                break;
        }
            case CanPacketSettingsType::settings_width:
                if(!ui->reaperType->hasFocus())
                    ui->reaperType->setCurrentIndex( ui->reaperType->findData(packet.value));
                 _reaperType_initialized = true;
                break;
            case CanPacketSettingsType::settings_password_1:
                _password1 = packet.value;
                break;
            case CanPacketSettingsType::settings_password_2:
                _password2 = packet.value;
                break;
            case CanPacketSettingsType::settings_fuel_sensor_type:
                if(!ui->fuelSensType->hasFocus())
                    ui->fuelSensType->setCurrentIndex(packet.value);
                _fuelSensType_initialized = true;
                break;
            default:
                break;
        }
    }
}

void MenuSettingsSystemWidget::OnCanConnectionLost()
{
    //pushButton: disable
    ui->Btn4ChangePassword1->setEnabled(false);
    ui->Btn5ChangePassword2->setEnabled(false);
    ui->Btn2ClearAlarmstats->setEnabled(false);
    ui->Btn1ClearTotalstats->setEnabled(false);
    ui->Btn3FactoryReset->setEnabled(false);
}

void MenuSettingsSystemWidget::OnCanConnectionRestore()
{
    ui->Btn4ChangePassword1->setEnabled(true);
    ui->Btn5ChangePassword2->setEnabled(true);
    ui->Btn2ClearAlarmstats->setEnabled(true);
    ui->Btn1ClearTotalstats->setEnabled(true);
    ui->Btn3FactoryReset->setEnabled(true);

    CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_diameter);
    CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_k_speed);
    CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_motor_eto);
    CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_motor_to1);
    CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_motor_to2);
    CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_engine_type);
    CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_combine_model_3);
    CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_width);
    CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_password_1);
    CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_password_2);
    CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_combine_sn);
    CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_k_motor);
    CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_fuel_sensor_type);
    CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_temperature_rev);
    CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_timeinterval_rev);
}

void MenuSettingsSystemWidget::on_combineType_currentIndexChanged(int index)
{
    if(_combineType_initialized) {
        int CANindex = CombineKeeper::GetInstance().GetCANIndex(index);
        if (CANindex == -1)
        {
            _combineType_initialized = false;
            return;
        }
        CanPacketSettingsSetRequest::Send(CanPacketSettingsType::settings_combine_model_3,static_cast<uint16_t>(CANindex));
        OnCanConnectionRestore();
    }
}

void MenuSettingsSystemWidget::on_fuelSensType_currentIndexChanged(int index)
{
    if(_fuelSensType_initialized) {
        CanPacketSettingsSetRequest::Send(CanPacketSettingsType::settings_fuel_sensor_type,static_cast<uint8_t>(index));
    }
}

void MenuSettingsSystemWidget::on_reaperType_currentIndexChanged(int index)
{
    if(_reaperType_initialized) {
        CanPacketSettingsSetRequest::Send(CanPacketSettingsType::settings_width,static_cast<uint16_t>(ui->reaperType->itemData(index).toInt()));
    }
}

void MenuSettingsSystemWidget::on_engineType_currentIndexChanged(int index)
{
    if(_engineType_initialized) {
        CanPacketSettingsSetRequest::Send(CanPacketSettingsType::settings_engine_type,static_cast<uint8_t>(index));
    }
}

void MenuSettingsSystemWidget::on_inputcontrol_wheel_diameter_returnPressed()
{
    CanPacketSettingsSetRequest::Send(
        CanPacketSettingsType::settings_diameter,
        static_cast<uint16_t>(qBound(0, ui->inputcontrol_wheel_diameter->number(), static_cast<int>(UINT16_MAX)))
    );
    CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_diameter);
}

void MenuSettingsSystemWidget::on_inputcontrol_coeff_speed_returnPressed()
{
    CanPacketSettingsSetRequest::Send(
        CanPacketSettingsType::settings_k_speed,
        static_cast<uint16_t>(qBound(0, ui->inputcontrol_coeff_speed->number(), static_cast<int>(UINT16_MAX)))
    );

    CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_k_speed);
}

void MenuSettingsSystemWidget::on_inputcontrol_coeff_engine_returnPressed()
{
    CanPacketSettingsSetRequest::Send(
        CanPacketSettingsType::settings_k_motor,
        static_cast<uint16_t>(qBound(0, ui->inputcontrol_coeff_engine->number(), static_cast<int>(UINT16_MAX)))
    );

    CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_k_motor);
}

void MenuSettingsSystemWidget::on_inputcontrol_serialNumber_returnPressed()
{
    CanPacketSettingsSetRequest::Send(
        CanPacketSettingsType::settings_combine_sn,
        static_cast<uint16_t>(qBound(0, ui->inputcontrol_serialNumber->number(), static_cast<int>(UINT16_MAX)))
    );

    CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_combine_sn);
}

void MenuSettingsSystemWidget::on_inputcontrol_time_eto_returnPressed()
{
    CanPacketSettingsSetRequest::Send(
        CanPacketSettingsType::settings_motor_eto,
        static_cast<uint16_t>(qBound(0, ui->inputcontrol_time_eto->number(), static_cast<int>(UINT16_MAX)))
    );

    CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_motor_eto);
}

void MenuSettingsSystemWidget::on_inputcontrol_time_to1_returnPressed()
{
    CanPacketSettingsSetRequest::Send(
        CanPacketSettingsType::settings_motor_to1,
        static_cast<uint16_t>(qBound(0, ui->inputcontrol_time_to1->number(), static_cast<int>(UINT16_MAX)))
    );

    CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_motor_to1);
}

void MenuSettingsSystemWidget::on_inputcontrol_time_to2_returnPressed()
{
    CanPacketSettingsSetRequest::Send(
        CanPacketSettingsType::settings_motor_to2,
        static_cast<uint16_t>(qBound(0, ui->inputcontrol_time_to2->number(), static_cast<int>(UINT16_MAX)))
    );

    CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_motor_to2);
}

void MenuSettingsSystemWidget::on_inputcontrol_temperature_rev_returnPressed()
{
    CanPacketSettingsSetRequest::Send(
        CanPacketSettingsType::settings_temperature_rev,
        static_cast<uint16_t>(qBound(0, ui->inputcontrol_temperature_rev->number(), static_cast<int>(UINT16_MAX)))
    );

    CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_temperature_rev);
}

void MenuSettingsSystemWidget::on_inputcontrol_timeinterval_rev_returnPressed()
{
    CanPacketSettingsSetRequest::Send(
        CanPacketSettingsType::settings_timeinterval_rev,
        static_cast<uint16_t>(qBound(0, ui->inputcontrol_timeinterval_rev->number(), static_cast<int>(UINT16_MAX)))
    );

    CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_timeinterval_rev);
}

void MenuSettingsSystemWidget::keyPressEvent(QKeyEvent* e)
{
    switch(e->key())
    {
        case Qt::Key_Escape:
            WindowManager::getInstance()->CloseMe(this);
            break;
    }
}

void MenuSettingsSystemWidget::on_Btn1ClearTotalstats_clicked()
{
    #ifndef NO_CAN
    if(_password2==-1)
    {
        auto* w = new ConfirmationWindow(this);
        w->SetText(QString::fromUtf8("Блок контроля недоступен."));
        w->SetTitle(QString::fromUtf8("Ошибка"));
        w->SetCancelVisibility(false);
        //WindowManager::getInstance()->Open(this,w);
        w->exec();

        CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_password_2);
        return;
    }
    #endif
    auto* passwordWindow = new PasswordEnterWindowWidget(this);
    passwordWindow->SetTitle(QString::fromUtf8("Введите пароль 2"));
    int code = passwordWindow->exec();

    if(code == QDialog::Accepted)
    {
        if(passwordWindow->password() == _password2 || passwordWindow->password() == SettingsManager::settings()->getServicePassword())
        {
            CanPacketProbegResetRequest::Send(CanPacketProbegResetType::reset_all_total_stats);

            auto* w = new ConfirmationWindow(this);
            w->SetText(QString::fromUtf8("Суммарная статистика сброшена."));
            w->SetTitle(QString::fromUtf8("Успех"));
            w->SetCancelVisibility(false);
            w->exec();
            return;
        }
        else
        {
            auto* w = new ConfirmationWindow(this);
            w->SetText(QString::fromUtf8("Неверный пароль 2."));
            w->SetTitle(QString::fromUtf8("Ошибка"));
            w->SetCancelVisibility(false);
            w->exec();
            return;
        }
    }
}

void MenuSettingsSystemWidget::on_Btn2ClearAlarmstats_clicked()
{
    #ifndef NO_CAN
    if(_password2==-1)
    {
        auto* w = new ConfirmationWindow(this);
        w->SetText(QString::fromUtf8("Блок контроля недоступен."));
        w->SetTitle(QString::fromUtf8("Ошибка"));
        w->SetCancelVisibility(false);
        //WindowManager::getInstance()->Open(this,w);
        w->exec();

        CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_password_2);
        return;
    }
    #endif

    auto* passwordWindow = new PasswordEnterWindowWidget(this);
    passwordWindow->SetTitle(QString::fromUtf8("Введите пароль"));
    int code = passwordWindow->exec();

    if(code == QDialog::Accepted)
    {
        if(passwordWindow->password() == SettingsManager::settings()->getAlarmStatsSpecialPassword())
        {
            CanPacketProbegResetRequest::Send(CanPacketProbegResetType::reset_alarm_stats);

            auto* w = new ConfirmationWindow(this);
            w->SetText(QString::fromUtf8("Аварийная статистика сброшена"));
            w->SetTitle(QString::fromUtf8("Успех"));
            w->SetCancelVisibility(false);
            w->exec();
            return;
        }
        else
        {
            auto* w = new ConfirmationWindow(this);
            w->SetText(QString::fromUtf8("Неверный пароль"));
            w->SetTitle(QString::fromUtf8("Ошибка"));
            w->SetCancelVisibility(false);
            w->exec();
            return;
        }
    }
}

void MenuSettingsSystemWidget::on_Btn3FactoryReset_clicked()
{
    #ifndef NO_CAN
    if(_password2==-1)
    {
        auto* w = new ConfirmationWindow(this);
        w->SetText(QString::fromUtf8("Блок контроля недоступен."));
        w->SetTitle(QString::fromUtf8("Ошибка"));
        w->SetCancelVisibility(false);
        //WindowManager::getInstance()->Open(this,w);
        w->exec();

        CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_password_2);
        return;
    }
    #endif

    auto* passwordWindow = new PasswordEnterWindowWidget(this);
    passwordWindow->SetTitle(QString::fromUtf8("Введите пароль 2"));
    int code = passwordWindow->exec();


    if(code == QDialog::Accepted)
    {
        if(passwordWindow->password() == _password2 || passwordWindow->password() == SettingsManager::settings()->getServicePassword())
        {
            CanPacketProbegResetRequest::Send(CanPacketProbegResetType::reset_factory);

            auto* w = new ConfirmationWindow(this);
            w->SetText(QString::fromUtf8("Сброс до заводских настроек произведен."));
            w->SetTitle(QString::fromUtf8("Успех"));
            w->SetCancelVisibility(false);
            w->exec();
            return;
        }
        else
        {
            auto* w = new ConfirmationWindow(this);
            w->SetText(QString::fromUtf8("Неверный пароль 2."));
            w->SetTitle(QString::fromUtf8("Ошибка"));
            w->SetCancelVisibility(false);
            w->exec();
            return;
        }
    }
}

void MenuSettingsSystemWidget::on_Btn4ChangePassword1_clicked()
{
#ifndef NO_CAN
    if(_password1==-1)
    {
        auto* w = new ConfirmationWindow(this);
        w->SetText(QString::fromUtf8("Блок контроля недоступен."));
        w->SetTitle(QString::fromUtf8("Ошибка"));
        w->SetCancelVisibility(false);
        //WindowManager::getInstance()->Open(this,w);
        w->exec();

        CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_password_1);
        return;
    }
#endif
    auto* passChanger = new PasswordChangeWindowWidget(this);
    passChanger->SetTitle(QString::fromUtf8("Смена пароля 1"));

    int code = passChanger->exec();
    if(code == QDialog::Accepted)
    {
        if(passChanger->oldPassword() == _password1 || passChanger->oldPassword() == SettingsManager::settings()->getServicePassword())
        {
            CanPacketSettingsSetRequest::Send(CanPacketSettingsType::settings_password_1, static_cast<uint16_t>(passChanger->newPassword()));

            auto* w = new ConfirmationWindow(this);
            w->SetText(QString::fromUtf8("Пароль 1 изменен."));
            w->SetTitle(QString::fromUtf8("Успех"));
            w->SetCancelVisibility(false);
            w->exec();
            _password1 = -1;
            CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_password_1);
            return;
        }
        else
        {
            auto* w = new ConfirmationWindow(this);
            w->SetText(QString::fromUtf8("Неверный пароль 1."));
            w->SetTitle(QString::fromUtf8("Ошибка"));
            w->SetCancelVisibility(false);
            w->exec();
            return;
        }
    }
}

void MenuSettingsSystemWidget::on_Btn5ChangePassword2_clicked()
{
    #ifndef NO_CAN
    if(_password2==-1)
    {
        auto* w = new ConfirmationWindow(this);
        w->SetText(QString::fromUtf8("Блок контроля недоступен."));
        w->SetTitle(QString::fromUtf8("Ошибка"));
        w->SetCancelVisibility(false);
        //WindowManager::getInstance()->Open(this,w);
        w->exec();

        CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_password_2);
        return;
    }
    #endif

    auto* passChanger = new PasswordChangeWindowWidget(this);
    passChanger->SetTitle(QString::fromUtf8("Смена пароля 2"));

    int code = passChanger->exec();
    if(code == QDialog::Accepted)
    {
        if(passChanger->oldPassword() == _password2 || passChanger->oldPassword() == SettingsManager::settings()->getServicePassword())
        {
            CanPacketSettingsSetRequest::Send(CanPacketSettingsType::settings_password_2, static_cast<uint16_t>(passChanger->newPassword()));

            auto* w = new ConfirmationWindow(this);
            w->SetText(QString::fromUtf8("Пароль 2 изменен."));
            w->SetTitle(QString::fromUtf8("Успех"));
            w->SetCancelVisibility(false);
            w->exec();

            _password2 = -1;
            CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_password_2);
            return;
        }
        else
        {
            auto* w = new ConfirmationWindow(this);
            w->SetText(QString::fromUtf8("Неверный пароль 2."));
            w->SetTitle(QString::fromUtf8("Ошибка"));
            w->SetCancelVisibility(false);
            w->exec();

            return;
        }
    }
}

void MenuSettingsSystemWidget::on_Btn6Close_clicked()
{
    WindowManager::getInstance()->CloseMe(this);
    delete this;
}

void MenuSettingsSystemWidget::on_chk_temperature_rev_stateChanged(int state)
{
    switch(state) {
    case Qt::Unchecked:
        ui->inputcontrol_temperature_rev->setNumber(TEMPERATURE_REV_OFF);
        ui->inputcontrol_temperature_rev->setEnabled(false);
        on_inputcontrol_temperature_rev_returnPressed();
        break;
    case Qt::Checked:
        if(ui->inputcontrol_temperature_rev->number() == TEMPERATURE_REV_OFF)
            ui->inputcontrol_temperature_rev->setNumber(TEMPERATURE_REV_MIN);
        ui->inputcontrol_temperature_rev->setEnabled(true);
        on_inputcontrol_temperature_rev_returnPressed();
        break;
    }
}

void MenuSettingsSystemWidget::on_chk_timeinterval_rev_stateChanged(int state)
{
    switch(state) {
    case Qt::Unchecked:
        ui->inputcontrol_timeinterval_rev->setNumber(TIMEINTERVAL_REV_OFF);
        ui->inputcontrol_timeinterval_rev->setEnabled(false);
        on_inputcontrol_timeinterval_rev_returnPressed();
        break;
    case Qt::Checked:
        if(ui->inputcontrol_temperature_rev->number() == TIMEINTERVAL_REV_OFF)
            ui->inputcontrol_timeinterval_rev->setNumber(TIMEINTERVAL_REV_MIN);
        ui->inputcontrol_timeinterval_rev->setEnabled(true);
        on_inputcontrol_timeinterval_rev_returnPressed();
        break;
    }
}
