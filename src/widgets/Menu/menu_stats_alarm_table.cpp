#include "widgets/Menu/menu_stats_alarm_table.h"
#include <QLabel>
#include "ui_menu_stats_alarm_table.h"
#include "core/helper.h"

MenuStatsAlarmTableWidget::MenuStatsAlarmTableWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MenuStatsAlarmTableWidget)
{
    setAttribute(Qt::WA_DeleteOnClose);
    setWindowFlags(Qt::Window | Qt::FramelessWindowHint);

    ui->setupUi(this);
    ui->gridLayout->indexOf(this);

    strawEngineRed = false;

    for(int i=0;i<rowCount;i++)
    {
        QVector<QLabel*> row;
        for(int j=0;j<colCount; j++)
             row.append(new QLabel(this));
        table.push_back(row);
    }
    setInitText();

    for(int i=0;i<rowCount;i++)
        for(int j=0;j<colCount; j++)
             ui->gridLayout->addWidget(table[i][j],i,j);

    for(auto* label: this->findChildren<QLabel *>())
    {
        label->installEventFilter(this);
    }
}

void MenuStatsAlarmTableWidget::setColor(int rowIndex, QString color)
{
    table[rowIndex][ICON_COLUMN]->setPixmap(Helper::ScalePixmap(Helper::SetPixmapColor(icons[rowIndex], QColor(color)), 64));
    table[rowIndex][DESC_COLUMN]->setStyleSheet(QString("QLabel{font: 20pt; color: ")+color+";}");
    table[rowIndex][TIME_COLUMN]->setStyleSheet(QString("QLabel{color: ")+color+";}");
}

void MenuStatsAlarmTableWidget::setTime(int rowIndex, double hours)
{
    if(hours<0)
        table[rowIndex][TIME_COLUMN]->setText("---");
    else
        table[rowIndex][TIME_COLUMN]->setText(QString::number(hours, 'f', 1) + QString::fromUtf8("ч"));

    if(hours>0)
        setColor(rowIndex,"red");
    else
        setColor(rowIndex,"black");

    if(rowIndex == 10) {
        strawEngineRed = hours > 0;
    }
}

void MenuStatsAlarmTableWidget::setStrawEngineType(CombineStrawEngineType engineType)
{
    if(engineType == CombineStrawEngineType::strawEngine_shaker)
    {
        table[6][ICON_COLUMN]->setVisible(true);
        table[6][DESC_COLUMN]->setVisible(true);
        table[6][TIME_COLUMN]->setVisible(true);

        table[10][DESC_COLUMN]->setText(QString::fromUtf8("Обороты соломотряса ниже допустимых"));
        icons[10] = Helper::LoadPixmap("image105.svg");
        table[10][ICON_COLUMN]->setPixmap(Helper::ScalePixmap(icons[10], 64));

    } else { //separator
        table[6][ICON_COLUMN]->setVisible(false);
        table[6][DESC_COLUMN]->setVisible(false);
        table[6][TIME_COLUMN]->setVisible(false);

        table[10][DESC_COLUMN]->setText(QString::fromUtf8("Обороты соломосепаратора ниже допустимых"));
        icons[10] = Helper::LoadPixmap("image60.png");
        table[10][ICON_COLUMN]->setPixmap(Helper::ScalePixmap(icons[10], 64));
    }

    setColor(10, strawEngineRed ? "red" : "black");
}

void MenuStatsAlarmTableWidget::setInitText()
{
    table[0][DESC_COLUMN]->setText(QString::fromUtf8("Аварийное давление масла в двигателе"));
    icons[0] = Helper::LoadPixmap("image89.svg");

    table[1][DESC_COLUMN]->setText(QString::fromUtf8("Аварийная температура охлаждающей жидкости в двигателе"));
    icons[1] = Helper::LoadPixmap("image92.svg");

    table[2][DESC_COLUMN]->setText(QString::fromUtf8("Аварийная температура масла в гидросистеме силовых цилиндров"));
    icons[2] = Helper::LoadPixmap("image85.svg");

    table[3][DESC_COLUMN]->setText(QString::fromUtf8("Аварийная температура масла в гидросистеме ходовой части"));
    icons[3] = Helper::LoadPixmap("image134.svg");

    table[4][DESC_COLUMN]->setText(QString::fromUtf8("Аварийный уровень масла в маслобаке"));
    icons[4] = Helper::LoadPixmap("image82.svg");

    table[5][DESC_COLUMN]->setText(QString::fromUtf8("Переливная секция гидроблока"));
    icons[5] = Helper::LoadPixmap("image128.svg");

    table[6][DESC_COLUMN]->setText(QString::fromUtf8("Произошло забивание соломотряса"));
    icons[6] = Helper::LoadPixmap("image103.svg");

    table[7][DESC_COLUMN]->setText(QString::fromUtf8("Обороты молотильного барабана ниже запомненных"));
    icons[7] = Helper::LoadPixmap("image121.svg");

    table[8][DESC_COLUMN]->setText(QString::fromUtf8("Обороты вентилятора очистки ниже запомненных"));
    icons[8] = Helper::LoadPixmap("image131.svg");

    table[9][DESC_COLUMN]->setText(QString::fromUtf8("Обороты барабана измельчителя ниже допустимых"));
    icons[9] = Helper::LoadPixmap("image130.svg");

    table[10][DESC_COLUMN]->setText(QString::fromUtf8("Обороты соломотряса ниже допустимых"));
    icons[10] = Helper::LoadPixmap("image105.svg");

    table[11][DESC_COLUMN]->setText(QString::fromUtf8("Обороты колосового шнека ниже допустимых"));
    icons[11] = Helper::LoadPixmap("image125.svg");

    table[12][DESC_COLUMN]->setText(QString::fromUtf8("Обороты зернового шнека ниже допустимых"));
    icons[12] = Helper::LoadPixmap("image126.svg");

    table[13][DESC_COLUMN]->setText(QString::fromUtf8("Обороты вала копнителя ниже допустимых"));
    icons[13] = Helper::LoadPixmap("image145.svg");

    table[14][DESC_COLUMN]->setText(QString::fromUtf8("Аварийный уровень охлаждающей жидкости двигателя"));
    icons[14] = Helper::LoadPixmap("image141.svg");

    table[15][DESC_COLUMN]->setText(QString::fromUtf8("Открыт вход в зерновой бункер"));
    icons[15] = Helper::LoadPixmap("image140.svg");

    table[16][DESC_COLUMN]->setText(QString::fromUtf8("Включен привод выгрузного шнека при сложенной выгрузной трубе"));
    icons[16] = Helper::LoadPixmap("image142.svg");

    table[17][DESC_COLUMN]->setText(QString::fromUtf8("Авария бортсети"));
    icons[17] = Helper::LoadPixmap("image18.svg");

    for(int i=0;i<rowCount;i++)
    {
        time[i] = 0;
        setTime(i, -1);
        table[i][ICON_COLUMN]->setPixmap(Helper::ScalePixmap(icons[i], 64));
        table[i][DESC_COLUMN]->setStyleSheet("QLabel{font: 18pt;}");
    }
}

MenuStatsAlarmTableWidget::~MenuStatsAlarmTableWidget()
{
    delete ui;
}

bool MenuStatsAlarmTableWidget::eventFilter(QObject *, QEvent *event)
{
    if(event->type()==QEvent::MouseMove)
    {
        this->event(event);
    }

    return false;
}
