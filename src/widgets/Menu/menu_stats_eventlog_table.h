#pragma once


#include <QWidget>

namespace Ui {
class MenuStatsEventlogTableWidget;
}

class MenuStatsEventlogTableWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MenuStatsEventlogTableWidget(QWidget *parent = 0);
    void ChangeItem(int row, int col, QString text);
    ~MenuStatsEventlogTableWidget();
public slots:
    void ScrollUp();
    void ScrollDown();

private:
    Ui::MenuStatsEventlogTableWidget *ui;
    static const int rowCount = 5;
    static const int queueAllocatedLength = 18;
    int firstIndex;
    int lastIndex;
//QLabel* table[rowCount][colCount];
    int recordNo[queueAllocatedLength];
    QString date[queueAllocatedLength];
    QString time[queueAllocatedLength];
    int rotations[queueAllocatedLength];
    int sensorNo[queueAllocatedLength];
    bool sensorState[queueAllocatedLength]; //false = off, true =
};
