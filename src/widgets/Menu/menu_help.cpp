#include <QAbstractItemView>
#include <QPainter>
#include <QStandardItemModel>
#include <QTableWidget>

#include "3rdparty/flickcharm/flickcharm.h"
#include "widgets/EventFilters/FocusEventFilter.h"
#include "core/helper.h"
#include "widgets/Menu/menu_help.h"
#include "widgets/WindowManager.h"
#include "ui_menu_help.h"
#include <QScrollBar>
#include <QDebug>
#include "widgets/EventFilters/KeyPressRedirectFilter.h"

MenuHelpWidget::MenuHelpWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MenuHelpWidget)
{
    setAttribute(Qt::WA_DeleteOnClose);
    setWindowFlags(Qt::Window | Qt::FramelessWindowHint);

    ui->setupUi(this);

    ApplyNightDay(SettingsManager::settings()->currentNightDayMode());
    QObject::connect(SettingsManager::settings(),SIGNAL(NightDayChanged(EnumNightDayMode)), this, SLOT(ApplyNightDay(EnumNightDayMode)));

    ui->stackedWidget->installEventFilter(this);

    ui->stackedWidget->setCurrentIndex(0);
    int index = ui->stackedWidget->currentIndex();
    QString windowIndex = QString::number(index+1);
    windowIndex +="/6";
    ui->LabelPageNumber->setText(windowIndex);

    auto* eventFilter = new FocusEventFilter(this);
    ui->Btn1Close->installEventFilter(eventFilter);
    ui->Btn2Backward->installEventFilter(eventFilter);
    ui->Btn3Forward->installEventFilter(eventFilter);

    auto *flickCharm = new FlickCharm(this);
    flickCharm->activateOn(ui->TableWidgetPage_1);
    flickCharm->activateOn(ui->TableWidgetPage_2);
    flickCharm->activateOn(ui->TableWidgetPage_3);
    flickCharm->activateOn(ui->TableWidgetPage_4);
    flickCharm->activateOn(ui->TableWidgetPage_5);
    flickCharm->activateOn(ui->TableWidgetPage_6);

    ui->TableWidgetPage_2->horizontalHeader()->setDefaultSectionSize(232); //размеры ячеек
    ui->TableWidgetPage_2->verticalHeader()->setDefaultSectionSize(45);
    ui->TableWidgetPage_2->horizontalHeader()->hide();
    ui->TableWidgetPage_2->verticalHeader()->hide();

    ui->TableWidgetPage_3->horizontalHeader()->setDefaultSectionSize(232); //размеры ячеек
    ui->TableWidgetPage_3->verticalHeader()->setDefaultSectionSize(45);
    ui->TableWidgetPage_3->horizontalHeader()->hide();
    ui->TableWidgetPage_3->verticalHeader()->hide();

    ui->TableWidgetPage_1->horizontalHeader()->setDefaultSectionSize(232); //размеры ячеек
    ui->TableWidgetPage_1->verticalHeader()->setDefaultSectionSize(45);
    ui->TableWidgetPage_1->horizontalHeader()->hide();
    ui->TableWidgetPage_1->verticalHeader()->hide();

    ui->TableWidgetPage_5->horizontalHeader()->setDefaultSectionSize(95); //размеры ячеек
    ui->TableWidgetPage_5->verticalHeader()->setDefaultSectionSize(45);
    ui->TableWidgetPage_5->horizontalHeader()->hide();
    ui->TableWidgetPage_5->verticalHeader()->hide();

    ui->TableWidgetPage_6->horizontalHeader()->setDefaultSectionSize(192); //размеры ячеек
    ui->TableWidgetPage_6->verticalHeader()->setDefaultSectionSize(45);
    ui->TableWidgetPage_6->horizontalHeader()->hide();
    ui->TableWidgetPage_6->verticalHeader()->hide();

    ui->TableWidgetPage_4->horizontalHeader()->setDefaultSectionSize(160);
    ui->TableWidgetPage_4->verticalHeader()->setDefaultSectionSize(45);
    ui->TableWidgetPage_4->verticalHeader()->hide();

    ui->TableWidgetPage_2->setSpan(0,0,4,1);
    ui->TableWidgetPage_2->setSpan(0,1,1,3);
    ui->TableWidgetPage_2->setSpan(1,1,1,3);
    ui->TableWidgetPage_2->setSpan(2,1,1,3);
    ui->TableWidgetPage_2->setSpan(5,3,4,1);
    ui->TableWidgetPage_2->setSpan(9,3,2,1);

    ui->TableWidgetPage_3->setSpan(0,0,4,1);
    ui->TableWidgetPage_3->setSpan(1,1,1,3);
    ui->TableWidgetPage_3->setSpan(2,1,2,1);
    ui->TableWidgetPage_3->setSpan(2,2,2,1);
    ui->TableWidgetPage_3->setSpan(2,3,2,1);
    ui->TableWidgetPage_3->setSpan(4,2,9,1);
    ui->TableWidgetPage_3->setSpan(4,3,9,1);

    ui->TableWidgetPage_1->setSpan(0,0,5,1);
    ui->TableWidgetPage_1->setSpan(1,1,1,3);
    ui->TableWidgetPage_1->setSpan(2,1,3,1);
    ui->TableWidgetPage_1->setSpan(2,2,3,1);
    ui->TableWidgetPage_1->setSpan(2,3,3,1);

    ui->TableWidgetPage_5->setSpan(0,0,1,10);
    ui->TableWidgetPage_5->setSpan(1,0,3,2);
    ui->TableWidgetPage_5->setSpan(1,2,3,2);
    ui->TableWidgetPage_5->setSpan(1,4,3,2);
    ui->TableWidgetPage_5->setSpan(1,6,3,2);
    ui->TableWidgetPage_5->setSpan(1,8,3,2);

    ui->TableWidgetPage_5->setSpan(4,0,2,2);
    ui->TableWidgetPage_5->setSpan(6,0,2,2);
    ui->TableWidgetPage_5->setSpan(8,0,2,2);
    ui->TableWidgetPage_5->setSpan(10,0,2,2);
    ui->TableWidgetPage_5->setSpan(12,0,3,2);
    ui->TableWidgetPage_5->setSpan(15,0,3,2);
    ui->TableWidgetPage_5->setSpan(18,0,2,2);
    ui->TableWidgetPage_5->setSpan(20,0,3,2);
    ui->TableWidgetPage_5->setSpan(23,0,2,2);

    ui->TableWidgetPage_5->setSpan(4,2,2,2);
    ui->TableWidgetPage_5->setSpan(6,2,2,2);
    ui->TableWidgetPage_5->setSpan(8,2,2,2);
    ui->TableWidgetPage_5->setSpan(10,2,2,2);
    ui->TableWidgetPage_5->setSpan(12,2,3,2);
    ui->TableWidgetPage_5->setSpan(15,2,3,2);
    ui->TableWidgetPage_5->setSpan(18,2,2,2);
    ui->TableWidgetPage_5->setSpan(20,2,3,2);
    ui->TableWidgetPage_5->setSpan(23,2,2,2);

    ui->TableWidgetPage_5->setSpan(4,4,2,2);
    ui->TableWidgetPage_5->setSpan(6,4,2,2);
    ui->TableWidgetPage_5->setSpan(8,4,2,2);
    ui->TableWidgetPage_5->setSpan(10,4,2,2);
    ui->TableWidgetPage_5->setSpan(12,4,3,2);
    ui->TableWidgetPage_5->setSpan(15,4,3,2);
    ui->TableWidgetPage_5->setSpan(18,4,2,2);
    ui->TableWidgetPage_5->setSpan(20,4,3,2);
    ui->TableWidgetPage_5->setSpan(23,4,2,2);

    ui->TableWidgetPage_5->setSpan(4,6,2,2);
    ui->TableWidgetPage_5->setSpan(6,6,2,2);
    ui->TableWidgetPage_5->setSpan(8,6,2,2);
    ui->TableWidgetPage_5->setSpan(10,6,2,2);
    ui->TableWidgetPage_5->setSpan(12,6,3,2);
    ui->TableWidgetPage_5->setSpan(15,6,3,2);
    ui->TableWidgetPage_5->setSpan(18,6,2,2);
    ui->TableWidgetPage_5->setSpan(20,6,3,2);
    ui->TableWidgetPage_5->setSpan(23,6,2,2);

    ui->TableWidgetPage_5->setSpan(4,8,2,2);
    ui->TableWidgetPage_5->setSpan(6,8,2,2);
    ui->TableWidgetPage_5->setSpan(8,8,2,2);
    ui->TableWidgetPage_5->setSpan(10,8,2,2);
    ui->TableWidgetPage_5->setSpan(12,8,3,2);
    ui->TableWidgetPage_5->setSpan(15,8,3,2);
    ui->TableWidgetPage_5->setSpan(18,8,2,2);
    ui->TableWidgetPage_5->setSpan(20,8,3,2);
    ui->TableWidgetPage_5->setSpan(23,8,2,2);

    ui->TableWidgetPage_6->setSpan(0,0,4,3);
    ui->TableWidgetPage_6->setSpan(4,0,7,1);
    ui->TableWidgetPage_6->setSpan(11,0,4,1);
    ui->TableWidgetPage_6->setSpan(15,0,1,3);
    ui->TableWidgetPage_6->setSpan(4,1,3,2);
    ui->TableWidgetPage_6->setSpan(7,1,2,2);
    ui->TableWidgetPage_6->setSpan(9,1,2,2);
    ui->TableWidgetPage_6->setSpan(11,1,2,2);
    ui->TableWidgetPage_6->setSpan(13,1,2,2);
    ui->TableWidgetPage_6->setSpan(4,3,3,1);
    ui->TableWidgetPage_6->setSpan(7,3,2,1);
    ui->TableWidgetPage_6->setSpan(9,3,2,1);
    ui->TableWidgetPage_6->setSpan(11,3,2,1);
    ui->TableWidgetPage_6->setSpan(13,3,2,1);
    ui->TableWidgetPage_6->setSpan(4,4,3,1);
    ui->TableWidgetPage_6->setSpan(7,4,2,1);
    ui->TableWidgetPage_6->setSpan(9,4,2,1);
    ui->TableWidgetPage_6->setSpan(11,4,2,1);
    ui->TableWidgetPage_6->setSpan(13,4,2,1);
    ui->TableWidgetPage_6->setSpan(0,3,1,2);
    ui->TableWidgetPage_6->setSpan(1,3,3,1);
    ui->TableWidgetPage_6->setSpan(1,4,3,1);

    ui->TableWidgetPage_4->setSpan(0,0,20,1);

    ui->TableWidgetPage_1->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    ui->TableWidgetPage_2->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    ui->TableWidgetPage_3->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    ui->TableWidgetPage_4->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    ui->TableWidgetPage_5->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    ui->TableWidgetPage_6->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);

//    for (int i=0;i< ui->TableWidgetPage_1->rowCount();i++) {
//            for (int j=0;j< ui->TableWidgetPage_1->columnCount();j++) {
//                QTableWidgetItem *item =  ui->TableWidgetPage_1->item(i,j);
//                //item->setFlags(Qt::NoItemFlags);
//                item->
//            }
//        }

    ui->TableWidgetPage_1->viewport()->setFocusPolicy(Qt::NoFocus);
    ui->TableWidgetPage_2->viewport()->setFocusPolicy(Qt::NoFocus);
    ui->TableWidgetPage_3->viewport()->setFocusPolicy(Qt::NoFocus);
    ui->TableWidgetPage_4->viewport()->setFocusPolicy(Qt::NoFocus);
    ui->TableWidgetPage_5->viewport()->setFocusPolicy(Qt::NoFocus);
    ui->TableWidgetPage_6->viewport()->setFocusPolicy(Qt::NoFocus);

    ui->Btn1Close->setFocusPolicy(Qt::StrongFocus);
    ui->Btn2Backward->setFocusPolicy(Qt::StrongFocus);
    ui->Btn3Forward->setFocusPolicy(Qt::StrongFocus);

    ui->Btn1Close->setFocus();

    setTabOrder(ui->Btn1Close, ui->Btn2Backward);
    setTabOrder(ui->Btn2Backward, ui->Btn3Forward);
    setTabOrder(ui->Btn3Forward, ui->Btn1Close);

    QVector<int> buttons;
    buttons.append(Qt::Key_Up);
    buttons.append(Qt::Key_Down);

    auto* keyRedirectFilter = new  KeyPressRedirectFilter(window(), buttons,this);
    for(auto* button: this->findChildren<QPushButton *>())
    {
        button->installEventFilter(keyRedirectFilter);
    }
}

MenuHelpWidget::~MenuHelpWidget()
{
    delete ui;
}

void MenuHelpWidget::keyPressEvent(QKeyEvent* e)
{
    switch(e->key())
    {
    case Qt::Key_Escape:
        WindowManager::getInstance()->CloseMe(this);
        break;
    case Qt::Key_Down:
        ScrollDown();
        break;
    case Qt::Key_Up:
        ScrollUp();
        break;
    }
}

bool MenuHelpWidget::eventFilter(QObject *obj, QEvent *event)
{
    QStackedWidget* cb = qobject_cast<QStackedWidget*>(obj);
    if(cb==nullptr)
        return false;

    if(event->type()!=QEvent::KeyPress)
        return false;

    QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);

    switch(keyEvent->key())
    {
        case Qt::Key_Left:
            on_Btn2Backward_clicked();
            return true;
        case Qt::Key_Right:
            on_Btn3Forward_clicked();
            return true;
        default:
            break;
    }

    return false;
}

void MenuHelpWidget::ScrollUp()
{
    qDebug("SCROLL UP");

    auto a = ui->stackedWidget->currentWidget();
    QTableWidget* page = a->findChild<QTableWidget*>();
    QScrollBar *sc = page->verticalScrollBar();
    sc->setValue(sc->value() - sc->pageStep());
}

void MenuHelpWidget::ScrollDown()
{
    qDebug("SCROLL DOWN");

    auto a = ui->stackedWidget->currentWidget();
    QTableWidget* page = a->findChild<QTableWidget*>();
    QScrollBar *sc = page->verticalScrollBar();
    sc->setValue(sc->value() + sc->pageStep());
}

void MenuHelpWidget::on_Btn1Close_clicked()
{
    WindowManager::getInstance()->CloseMe(this);
}

void MenuHelpWidget::on_Btn2Backward_clicked()
{
    int index = ui->stackedWidget->currentIndex();
    if(index>0)
        {
        index--;
        QString windowIndex = QString::number(index+1);
        windowIndex +="/6";
        ui->LabelPageNumber->setText(windowIndex);
        ui->stackedWidget->setCurrentIndex(index);
    }
}

void MenuHelpWidget::on_Btn3Forward_clicked()
{

    int index = ui->stackedWidget->currentIndex();
    if(index<5)
    {
        index++;
        QString windowIndex = QString::number(index+1);
        windowIndex +="/6";
        ui->LabelPageNumber->setText(windowIndex);
        ui->stackedWidget->setCurrentIndex(index);

//        auto* pageWidget = ui->stackedWidget->widget(index);
//        auto* tableWidget = qobject_cast<QTableWidget*>(pageWidget->children().front());
//        if(tableWidget != nullptr)
//        {
//            tableWidget->setFocus();
//        }
    }
}

void MenuHelpWidget::ApplyNightDay(EnumNightDayMode mode)
{
    QColor foreColor = (mode==EnumNightDayMode::Day)?Qt::black:Qt::white;
    QTableWidgetItem *pic107 = new QTableWidgetItem();
    pic107->setData(Qt::DecorationRole, Helper::LoadPixmap("image107.svg",Helper::defIconSize(),foreColor));
    ui->TableWidgetPage_2->setItem(0, 1, pic107);


    QTableWidgetItem *pic108 = new QTableWidgetItem();
    QTableWidgetItem *pic101 = new QTableWidgetItem();
    pic108->setData(Qt::DecorationRole, Helper::LoadPixmap("image108.svg",Helper::defIconSize(),foreColor));
    pic101->setData(Qt::DecorationRole, Helper::LoadPixmap("image101.svg",Helper::defIconSize(),foreColor));
    ui->TableWidgetPage_3->setItem(0, 1, pic108);
    ui->TableWidgetPage_3->setItem(0, 2, pic101);

    QTableWidgetItem *pic121 = new QTableWidgetItem();
    QTableWidgetItem *pic115 = new QTableWidgetItem();
    QTableWidgetItem *pic131 = new QTableWidgetItem();
    pic121->setData(Qt::DecorationRole, Helper::LoadPixmap("image121.svg",Helper::defIconSize(),foreColor));
    pic115->setData(Qt::DecorationRole, Helper::LoadPixmap("image115.svg",Helper::defIconSize(),foreColor));
    pic131->setData(Qt::DecorationRole, Helper::LoadPixmap("image131.svg",Helper::defIconSize(),foreColor));
    ui->TableWidgetPage_1->setItem(0, 1, pic121);
    ui->TableWidgetPage_1->setItem(0, 2, pic115);
    ui->TableWidgetPage_1->setItem(0, 3, pic131);
}
