#include <QDialog>
#include <QMessageBox>

#include "core/AudioHelper.h"
#include "core/PathHelper.h"

#include "peripherials/PeripherialsManager.h"
#include "peripherials/can/packets/Settings.h"

#include "widgets/EventFilters/FocusEventFilter.h"
#include "widgets/Menu/menu_settings.h"
#include "widgets/Menu/menu_settings_system.h"
#include "widgets/Menu/menu_settings_workmodes.h"
#include "widgets/Menu/menu_settings_terminal.h"

#include "widgets/Windows/PasswordEnterWindow.h"
#include "widgets/Windows/ConfirmationWindow.h"

#include "ui_menu_settings.h"
#include "widgets/WindowManager.h"

MenuSettingsWidget::MenuSettingsWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MenuSettingsWidget)
{
    setAttribute(Qt::WA_DeleteOnClose);
    setWindowFlags(Qt::Window | Qt::FramelessWindowHint);

    ui->setupUi(this);

    auto* eventFilter = new FocusEventFilter(this);
    ui->Btn1SettingsSystem->installEventFilter(eventFilter);
    ui->Btn2SettingsWorkmodes->installEventFilter(eventFilter);
    ui->Btn3SettingsTerminal->installEventFilter(eventFilter);
    ui->Btn5Close->installEventFilter(eventFilter);

#ifdef BUILD_DEVICE
    _password =-1;
#else
    _password = 1234;
#endif

    //can signals
    auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
    if(canworker!=nullptr)
    {
        connect(canworker,SIGNAL(received(CanMessage)),this,SLOT(OnCanMessage(CanMessage)));
        connect(canworker,SIGNAL(connectionLost()),this,SLOT(OnCanConnectionLost()));
        CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_password_1);
    }
}

MenuSettingsWidget::~MenuSettingsWidget()
{
    delete ui;
}

void MenuSettingsWidget::OnCanMessage(CanMessage msg)
{
    if(msg.can_id == CanPacketSettingsGetResponse::packet_id)
    {
        auto packet = CanPacketSettingsGetResponse::Parse(msg.data);
        if(packet.type== CanPacketSettingsType::settings_password_1)
        {
            _password = packet.value;
        }
    }
}

void MenuSettingsWidget::OnCanConnectionLost()
{

}

void MenuSettingsWidget::keyPressEvent(QKeyEvent* e)
{
    switch(e->key())
    {
    case Qt::Key_Escape:
        WindowManager::getInstance()->CloseMe(this);
        break;
    case Qt::Key_F1:
        ui->Btn1SettingsSystem->click();
        break;
    case Qt::Key_F2:
        ui->Btn2SettingsWorkmodes->click();
        break;
    case Qt::Key_F3:
        ui->Btn3SettingsTerminal->click();
        break;
    }
}

void MenuSettingsWidget::on_Btn1SettingsSystem_clicked()
{
    //настройки системы
    auto* passwordWindow =  new PasswordEnterWindowWidget(this);
    passwordWindow->SetTitle(QString::fromUtf8("Введите пароль 1"));
    int code = passwordWindow->exec();

#ifndef NO_CAN
    if(_password==-1)
    {
        auto* w = new ConfirmationWindow(this);
        w->SetText(QString::fromUtf8("Блок контроля недоступен."));
        w->SetTitle(QString::fromUtf8("Ошибка"));
        w->SetCancelVisibility(false);
        //WindowManager::getInstance()->Open(this,w);
        w->exec();
        CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_password_1);
        return;
    }
#endif
    if(code == QDialog::Accepted)
    {
        if(passwordWindow->password() == _password)
        {
            auto* w = new MenuSettingsSystemWidget(this);
            w->move(pos());
            w->resize(size());
            WindowManager::getInstance()->Open(this,w);
        }
        else
        {
            auto* w = new ConfirmationWindow(this);
            w->SetText(QString::fromUtf8("Неверный пароль 1."));
            w->SetTitle(QString::fromUtf8("Ошибка"));
            w->SetCancelVisibility(false);
            w->exec();
        }
    }
}

void MenuSettingsWidget::on_Btn2SettingsWorkmodes_clicked()
{
    auto *w = new MenuSettingsWorkmodesWidget(this);
    w->move(pos());
    w->resize(size());
    WindowManager::getInstance()->Open(this,w);
}

void MenuSettingsWidget::on_Btn3SettingsTerminal_clicked()
{
    auto *w = new MenuSettingsTerminalWidget(this);
    w->move(pos());
    w->resize(size());
    WindowManager::getInstance()->Open(this,w);
}

void MenuSettingsWidget::on_Btn5Close_clicked()
{
    WindowManager::getInstance()->CloseMe(this);
}
