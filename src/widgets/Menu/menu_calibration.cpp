#include "widgets/Menu/menu_calibration.h"
#include "widgets/Menu/menu_calibration_clearance.h"
#include "widgets/EventFilters/FocusEventFilter.h"

#include "ui_menu_calibration.h"
#include "widgets/WindowManager.h"

MenuCalibrationWidget::MenuCalibrationWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MenuCalibrationWidget)
{
    setAttribute(Qt::WA_DeleteOnClose);
    setWindowFlags(Qt::Window | Qt::FramelessWindowHint);

    ui->setupUi(this);

    auto* eventFilter = new FocusEventFilter(this);
    ui->Btn1Clearance->installEventFilter(eventFilter);
    ui->Btn2Lowersieves->installEventFilter(eventFilter);
    ui->Btn3Highersieves->installEventFilter(eventFilter);
    ui->Btn4Close->installEventFilter(eventFilter);
}

MenuCalibrationWidget::~MenuCalibrationWidget()
{
    delete ui;
}

void MenuCalibrationWidget::keyPressEvent(QKeyEvent* e)
{
    switch(e->key())
    {
    case Qt::Key_Escape:
        WindowManager::getInstance()->CloseMe(this);
        break;
    case Qt::Key_F1:
        ui->Btn1Clearance->click();
        break;
    case Qt::Key_F2:
        ui->Btn2Lowersieves->click();
        break;
    case Qt::Key_F3:
        ui->Btn3Highersieves->click();
        break;
    }
}

void MenuCalibrationWidget::on_Btn1Clearance_clicked()
{
    auto* w = new MenuCalibrationClearanceWidget(CanPackets::CalibrationType::clearance, this);
    w->move(pos());
    w->resize(size());
    WindowManager::getInstance()->Open(this,w);
}

void MenuCalibrationWidget::on_Btn2Lowersieves_clicked()
{
    auto* w = new MenuCalibrationClearanceWidget(CanPackets::CalibrationType::lower_sieves, this);
    w->move(pos());
    w->resize(size());
    WindowManager::getInstance()->Open(this,w);
}

void MenuCalibrationWidget::on_Btn3Highersieves_clicked()
{
    auto* w = new MenuCalibrationClearanceWidget(CanPackets::CalibrationType::higher_siever, this);
    w->move(pos());
    w->resize(size());
    WindowManager::getInstance()->Open(this,w);
}

void MenuCalibrationWidget::on_Btn4Close_clicked()
{
    WindowManager::getInstance()->CloseMe(this);
}
