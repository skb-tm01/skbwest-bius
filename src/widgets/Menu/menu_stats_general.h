#pragma once

#include <QKeyEvent>
#include <QWidget>

#include "peripherials/PeripherialsManager.h"
#include "peripherials/can/CanMessage.h"


namespace Ui {
class MenuStatsGeneralWidget;
}

class MenuStatsGeneralWidget : public QWidget
{
    Q_OBJECT

public:
    // parent = MainWidget
    explicit MenuStatsGeneralWidget(QWidget *parent = 0);
    ~MenuStatsGeneralWidget();
protected:
    void keyPressEvent(QKeyEvent* e);
private slots:
    void OnCanMessage(CanMessage msg);
    void OnCanConnectionLost();
    void OnCanConnectionRestore();

    void on_Btn1Reset_clicked();

    void on_Btn2Close_clicked();

private:
    Ui::MenuStatsGeneralWidget *ui;
    double _combinetime_temp;
    double _area_temp;
};
