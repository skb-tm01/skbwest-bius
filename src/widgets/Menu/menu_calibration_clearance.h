#pragma once

#include <QWidget>
#include <QKeyEvent>
#include <QTimer>

#include "peripherials/can/CanMessage.h"
#include "peripherials/can/packets/Calibration.h"

namespace Ui {
class MenuCalibrationClearanceWidget;
}


class MenuCalibrationClearanceWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MenuCalibrationClearanceWidget(CanPackets::CalibrationType type, QWidget *parent = 0);
    ~MenuCalibrationClearanceWidget();

protected:
    void keyPressEvent(QKeyEvent* e);

private slots:
    void TimerTimeout();

    //can
    void OnCanMessage(CanMessage msg);
    void OnCanConnectionLost();
    void OnCanConnectionRestore();

    void on_Btn1Minimum_clicked();
    void on_Btn2Maximum_clicked();
    void on_Btn3Save_clicked();
    void on_Btn4Close_clicked();

private:
    int min;
    int max;
    bool setmin;
    uint8_t request2_id;
    CanPackets::CalibrationType _type;


    QTimer timer;
    Ui::MenuCalibrationClearanceWidget *ui;
};
