#pragma once

#include <QKeyEvent>
#include <QWidget>

#include "SettingsManager.h"

namespace Ui
{
    class MenuSettingsTerminalWidget;
}

class MenuSettingsTerminalWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MenuSettingsTerminalWidget(QWidget *parent = 0);
    ~MenuSettingsTerminalWidget();

protected:
    void keyPressEvent(QKeyEvent* e);

private slots:
    void on_Btn1Close_clicked();

    void on_sliderVolume_valueChanged(int value);

    void on_sliderBrightness_valueChanged(int value);

    void on_voiceMessagesCheckBox_stateChanged(int arg1);

    void on_languageSetting_currentIndexChanged(int index);

    void on_nightAndDayMode_currentIndexChanged(int index);

    void showTime();
    void onChange();

    void OnIlliminationChange(double val);

    void ApplyNightDay(EnumNightDayMode mode);

    void on_sliderNightAndDayModeThreshold_valueChanged(int value);

private:
    Ui::MenuSettingsTerminalWidget *ui;
    void ChangeDate();
};
