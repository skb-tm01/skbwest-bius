#include <QApplication>
#include <QMessageBox>

#include "core/helper.h"

#include "peripherials/PeripherialsManager.h"
#include "peripherials/can/packets/Taho.h"
#include "peripherials/can/packets/TahoCalibration.h"
#include "peripherials/can/packets/TahoSave.h"
#include "peripherials/can/packets/ContactState.h"
#include "peripherials/can/packets/Settings.h"

#include "widgets/mainwindow.h"
#include "widgets/EventFilters/FocusEventFilter.h"
#include "widgets/EventFilters/KeyPressRedirectFilter.h"
#include "widgets/EventFilters/mousepressredirectfilter.h"
#include "widgets/Menu/menu_sensors_rotations.h"
#include "widgets/Windows/ConfirmationWindow.h"

#include "peripherials/can/packets/Settings.h"
#include "peripherials/combines/CombineKeeper.h"
#include "ui_menu_sensors_rotations.h"
#include "widgets/WindowManager.h"

MenuSensorsRotationsWidget::MenuSensorsRotationsWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MenuSensorsRotationsWidget)
{
    setAttribute(Qt::WA_DeleteOnClose);
    setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    ui->setupUi(this);

    _canInitiailized = false;
    focused_button = ui->Btn1EngineSpeed;
    auto* eventFilter = new FocusEventFilter(this);

    buttons[0] = ui->Btn1EngineSpeed;
    button_icons[0] = Helper::LoadPixmap("image94.svg");
    button_description[0] = QString::fromUtf8("Обороты двигателя");
    button_tahorequest[0] = CanPacketTahoType::taho_speed_engine;

    buttons[1] = ui->Btn2ThreshingDrum;
    button_icons[1] = Helper::LoadPixmap("image121.svg");;
    button_description[1] = QString::fromUtf8("Обороты молотильного барабана");
    button_tahorequest[1] = CanPacketTahoType::taho_speed_threshing;

    buttons[2] = ui->Btn3CleaningFan;
    button_icons[2] = Helper::LoadPixmap("image131.svg");
    button_description[2] = QString::fromUtf8("Обороты вентилятора очистки");
    button_tahorequest[2] = CanPacketTahoType::taho_speed_fan;

    buttons[3] = ui->Btn4ChopperDrum;
    button_icons[3] = Helper::LoadPixmap("image130.svg");
    button_description[3] = QString::fromUtf8("Обороты барабана измельчителя");
    button_tahorequest[3] = CanPacketTahoType::taho_drumchopper_speed;

    buttons[4] = ui->Btn5GrainAuger;
    button_icons[4] = Helper::LoadPixmap("image126.svg");
    button_description[4] = QString::fromUtf8("Обороты зернового шнека");
    button_tahorequest[4] = CanPacketTahoType::taho_grainauger_speed;

    buttons[5] = ui->Btn6CoiledAuger;
    button_icons[5] = Helper::LoadPixmap("image125.svg");
    button_description[5] = QString::fromUtf8("Обороты колосового шнека");
    button_tahorequest[5] = CanPacketTahoType::taho_spikeauger_speed;

    buttons[6] = ui->Btn7Shaker;
    button_icons[6] = Helper::LoadPixmap("image105.svg");
    button_description[6] = QString::fromUtf8("Обороты вала соломотряса");
    button_tahorequest[6] = CanPacketTahoType::taho_shakershaft_speed;

    buttons[7] = ui->Btn7Separator;
    button_icons[7] = Helper::LoadPixmap("image60.png");
    button_description[7] = QString::fromUtf8("Обороты соломосепаратора");
    button_tahorequest[7] = CanPacketTahoType::taho_strawsep_speed;

    buttons[8] = ui->Btn8Stacker;
    button_icons[8] = Helper::LoadPixmap("image145.svg");
    button_description[8] = QString::fromUtf8("Обороты вала копнителя");
    button_tahorequest[8] = CanPacketTahoType::taho_levelingauger2_speed;

    for(int i=0;i<9;i++)
    {
        buttons[i]->installEventFilter(eventFilter);
        buttons[i]->installEventFilter(this);
        buttons[i]->setAutoFillBackground(true);
    }

     ui->Btn9SaveRotations->installEventFilter(eventFilter);
     ui->Btn10Close->installEventFilter(eventFilter);

    ApplyNightDay(SettingsManager::settings()->currentNightDayMode());
    QObject::connect(SettingsManager::settings(),SIGNAL(NightDayChanged(EnumNightDayMode)), this, SLOT(ApplyNightDay(EnumNightDayMode)));

    ui->Btn9SaveRotations->setEnabled(false);

    auto* keyRedirectFilter = new KeyPressRedirectFilter(window(), this);
    for(auto* button: this->findChildren<QPushButton *>())
    {
        button->installEventFilter(keyRedirectFilter);
    }

    auto* mouseRedirectFilter = new MousePressRedirectFilter(window(), this);
    for(auto* label: this->findChildren<QLabel *>())
    {
        label->installEventFilter(mouseRedirectFilter);
    }

    SetClosable(false);
    ui->Btn1EngineSpeed->setFocus();

    ui->Btn7Shaker->setVisible(false);
    ui->label_shaker->setVisible(false);
    ui->Btn7Separator->setVisible(true);
    ui->label_separator->setVisible(true);

    Initialize_CAN();

    mousePressX = 0;
    mousePressY = 0;
}

MenuSensorsRotationsWidget::~MenuSensorsRotationsWidget()
{
    delete ui;
}

void MenuSensorsRotationsWidget::SetClosable(bool visibility)
{
    _isClosable = visibility;
    ui->Btn10Close->setVisible(visibility);
}

void MenuSensorsRotationsWidget::Initialize_CAN()
{
    auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
    if(canworker!=nullptr)
    {
        connect(canworker,SIGNAL(received(CanMessage)),this,SLOT(OnCanMessage(CanMessage)));
        connect(canworker,SIGNAL(connectionLost()),this,SLOT(OnCanConnectionLost()));
        connect(canworker,SIGNAL(connectionRestored()),this,SLOT(OnCanConnectionRestore()));
        OnCanConnectionRestore();
        _canInitiailized = true;
    }
    else
    {
        qDebug()<<"ERROR: CANWORKER IS NULL";
    }
}

void MenuSensorsRotationsWidget::FocusButton()
{
    ui->Btn1EngineSpeed->setFocus();
}

void MenuSensorsRotationsWidget::OnCanMessage(CanMessage msg)
{
    switch(msg.can_id )
    {
    case  CanPacketTahoResponse::packet_id:
        {
            auto packet = CanPacketTahoResponse::Parse(msg.data);
            switch(packet.type)
            {
            case CanPacketTahoType::taho_drumchopper_speed:
                ui->label_chopper_drum->setText(QString::number(packet.rpm));
                break;
            case CanPacketTahoType::taho_grainauger_speed:
                ui->label_grain_auger->setText(QString::number(packet.rpm));
                break;
            case CanPacketTahoType::taho_spikeauger_speed:
                ui->label_coiled_auger->setText(QString::number(packet.rpm));
                break;
            case CanPacketTahoType::taho_shakershaft_speed:
                ui->label_shaker->setText(QString::number(packet.rpm));
                break;
            case CanPacketTahoType::taho_strawsep_speed:
                ui->label_separator->setText(QString::number(packet.rpm));
                break;
            case CanPacketTahoType::taho_speed_engine:
                ui->label_engine_speed->setText(QString::number(packet.rpm));
                break;
            case CanPacketTahoType::taho_speed_fan:
                ui->label_cleaning_fan->setText(QString::number(packet.rpm));
                break;
            case CanPacketTahoType::taho_speed_threshing:
                ui->label_threshing_drum->setText(QString::number(packet.rpm));
                break;
            case CanPacketTahoType::taho_levelingauger2_speed:
                ui->label_stacker->setText(QString::number(packet.rpm));
                break;
            default:
                break;
            }
            break;
        }
    case CanPacketTahoCalibrationResponse::packet_id:
    {
         auto packet = CanPacketTahoCalibrationResponse::Parse(msg.data);
         ui->label_marks->setText(QString::number(packet.marks));
         ui->label_percentange->setText(QString::number(packet.deviation));
         break;
    }
    case CanPacketContactStateResponse::packet_id:
    {
        auto packet = CanPacketContactStateResponse::Parse(msg.data);
        if(packet.on_work_mode)
            ui->Btn9SaveRotations->setEnabled(true);
        else
            ui->Btn9SaveRotations->setEnabled(false);
        break;
    }
    case CanPackets::TahoSave::response_id:
        break;
    case CanPacketSettingsGetResponse::packet_id:
        {
            auto packet = CanPacketSettingsGetResponse::Parse(msg.data);
            if(packet.type == CanPacketSettingsType::settings_combine_model_3)
            {
                CombineKeeper::GetInstance().setCurrentCombine(packet.value);
                auto combine = CombineKeeper::GetInstance().getCurrentCombine();
                if(combine == nullptr)
                    break;
                if(combine->getEngineType()==CombineStrawEngineType::strawEngine_shaker) {
                    ui->Btn7Shaker->setVisible(true);
                    ui->label_shaker->setVisible(true);
                    ui->Btn7Separator->setVisible(false);
                    ui->label_separator->setVisible(false);
                }
                else { //separator
                    ui->Btn7Shaker->setVisible(false);
                    ui->label_shaker->setVisible(false);
                    ui->Btn7Separator->setVisible(true);
                    ui->label_separator->setVisible(true);
                }
            }
            break;
        }
    default:
        break;
    }
}

void MenuSensorsRotationsWidget::OnCanConnectionLost()
{
    ui->label_chopper_drum->setText("---");
    ui->label_cleaning_fan->setText("---");
    ui->label_coiled_auger->setText("---");
    ui->label_engine_speed->setText("---");
    ui->label_grain_auger->setText("---");
    ui->label_shaker->setText("---");
    ui->label_separator->setText("---");
    ui->label_stacker->setText("---");
    ui->label_threshing_drum->setText("---");

    ui->Btn9SaveRotations->setEnabled(false);
}

void MenuSensorsRotationsWidget::OnCanConnectionRestore(){
    CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_combine_model_3);
}

void MenuSensorsRotationsWidget::ApplyNightDay(EnumNightDayMode mode)
{
    QColor foreColor = (mode==EnumNightDayMode::Day)?Qt::black:Qt::white;
    engine_label_focused_stylesheet = (mode==EnumNightDayMode::Day)? "color:blue":"color:cyan";
    btn_focused_forecolor = (mode==EnumNightDayMode::Day)?Qt::white:Qt::black;
    btn_unfocused_forecolor = foreColor;

    for(int i=0;i<9;i++)
    {
        buttons[i]->setIcon(Helper::SetPixmapColor(button_icons[i],btn_unfocused_forecolor));
    }
}

void MenuSensorsRotationsWidget::keyPressEvent(QKeyEvent* e)
{
    qDebug()<<e->key();
    switch(e->key())
    {
    case Qt::Key_Return:
    case Qt::Key_Enter:
        QWidget::keyPressEvent(e);
        break;
    case Qt::Key_Escape:
        if(_isClosable)
            WindowManager::getInstance()->CloseMe(this);
        break;
    case Qt::Key_F1:
        ui->Btn9SaveRotations->click();
        break;
    case Qt::Key_Up:
        QApplication::instance()->postEvent(this, new QKeyEvent(QEvent::KeyPress,Qt::Key_Tab,Qt::ShiftModifier));
        break;
    case Qt::Key_Down:
        QApplication::instance()->postEvent(this, new QKeyEvent(QEvent::KeyPress,Qt::Key_Tab,Qt::NoModifier));
        break;
    case Qt::Key_Menu:
        // qDebug() << "SENSORS ROTATIONS KEY MENU";
        // потому что это окно можно открыть как часть главного экрана или как самостоятельное окно через меню
        // и в него приходит 2 key event'a вместо одного, меню открывается и сразу закрывается - мигает
        // поэтому ловим один тут
        break;
    default:
        qDebug()<<"key_default";
        if(qobject_cast<MainWindow*>(window()) != nullptr)
            QApplication::postEvent(window(),new QKeyEvent(e->type(),e->key(),e->modifiers()));
        break;
    }
}


void MenuSensorsRotationsWidget::on_Btn9SaveRotations_clicked()
{
    auto * w = new ConfirmationWindow(this);
    w->SetTitle(QString::fromUtf8("Предупреждение"));
    w->SetText(QString::fromUtf8("Вы уверены, что хотите cохранить обороты?"));
    if (w->exec() == QDialog::Accepted)
    {
       CanPackets::TahoSave::SendRequest(CanPackets::TahoSaveType::all);
       auto* w2 =  new ConfirmationWindow(this);
       w2->SetTitle(QString::fromUtf8("Информация"));
       w2->SetText(QString::fromUtf8("Обороты сохранены"));
       w2->SetCancelVisibility(false);
       w2->exec();
    }
}

void MenuSensorsRotationsWidget::on_Btn10Close_clicked()
{
    WindowManager::getInstance()->CloseMe(this);
}

bool MenuSensorsRotationsWidget::eventFilter(QObject *obj, QEvent *event)
{
    QPushButton *btn = qobject_cast<QPushButton*>(obj);

    if (event->type() == QEvent::FocusIn)
    {
        ui->label_marks->setText("");
        ui->label_percentange->setText("");

        int i=0;
        for(i=0;i<9;i++)
        {
            if (btn == buttons[i])
                break;
         }
        if (i==9)
            return QObject::eventFilter(obj, event);

        ui->engine->setText(button_description[i]);
        CanPacketAnalogCalibrationRequest::Send(button_tahorequest[i]);
        focused_button = buttons[i];
        ui->engine->setStyleSheet(engine_label_focused_stylesheet);
        buttons[i]->setIcon(Helper::SetPixmapColor(button_icons[i],btn_focused_forecolor));
    }
    else if(event->type() == QEvent::FocusOut)
    {
        int i=0;
        for(i=0;i<9;i++)
        {
            if (btn == buttons[i])
                break;
        }
        if (i==9)
            return QObject::eventFilter(obj, event);

        buttons[i]->setIcon(Helper::SetPixmapColor(button_icons[i],btn_unfocused_forecolor));
    }

    return false;
}

void MenuSensorsRotationsWidget::mousePressEvent(QMouseEvent *event)
{
    mousePressX = event->globalX();
    mousePressY = event->y();

    QWidget::mousePressEvent(event);

    //_isClosable = visibility;
}

void MenuSensorsRotationsWidget::mouseReleaseEvent(QMouseEvent *event)
{
    if(_isClosable)
        return;

    qDebug()<<"MenuSensorsRotationsWidget::mouseReleaseEvent, X_OLD:" << mousePressX <<" X_NEW:"<<event->x();

    if(event->button() == Qt::LeftButton && event->globalX() < 100 && mousePressX < 100)
    {
        emit LeftSideButtonClicked();
        qDebug()<< "clicked left.";
    }
    if(event->button() == Qt::LeftButton && event->x() > (this->width()-100 && mousePressX) > this->width()-100)
    {
        emit RightSideButtonClicked();
        qDebug()<< "clicked right.";
    }

    int halfX = (this->width())/2;
    int dY = 300;

    if(qAbs(event->y() - mousePressY)<dY)
    {
        if((event->button() == Qt::LeftButton)&&(mousePressX <= halfX)&&(event->x() > halfX))
        {
            RightSideButtonClicked();
            qDebug()<< "swipe right.";
        }

        else if((event->button() == Qt::LeftButton)&&(mousePressX >= halfX)&&(event->x() < halfX))
        {
            LeftSideButtonClicked();
            qDebug()<< "swipe left.";
        }
    }
}


















