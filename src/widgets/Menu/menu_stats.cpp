#include "widgets/EventFilters/FocusEventFilter.h"
#include "widgets/Menu/menu_stats.h"
#include "widgets/Menu/menu_stats_alarm.h"
#include "widgets/Menu/menu_stats_eventlog.h"
#include "widgets/Menu/menu_stats_general.h"
#include "widgets/Menu/menu_stats_j1939.h"
#include "widgets/Menu/menu_stats_to.h"

#include "ui_menu_stats.h"
#include "widgets/WindowManager.h"

MenuStatsWidget::MenuStatsWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MenuStatsWidget)
{
    setAttribute(Qt::WA_DeleteOnClose);
    setWindowFlags(Qt::Window | Qt::FramelessWindowHint);

    ui->setupUi(this);
    auto* eventFilter = new FocusEventFilter(this);
     ui->Btn1Stats->installEventFilter(eventFilter);
     ui->Btn2Crashstats->installEventFilter(eventFilter);
     ui->Btn3Eventlog->installEventFilter(eventFilter);
     ui->Btn4Techmaintain->installEventFilter(eventFilter);
     ui->Btn5Activedefects->installEventFilter(eventFilter);
     ui->Btn6Close->installEventFilter(eventFilter);
}

MenuStatsWidget::~MenuStatsWidget()
{
    delete ui;
}

void MenuStatsWidget::keyPressEvent(QKeyEvent* e)
{
    switch(e->key())
    {
    case Qt::Key_Escape:
        WindowManager::getInstance()->CloseMe(this);
        break;
    case Qt::Key_F1:
        ui->Btn1Stats->click();
        break;
    case Qt::Key_F2:
        ui->Btn2Crashstats->click();
        break;
    case Qt::Key_F3:
        ui->Btn3Eventlog->click();
        break;
    case Qt::Key_F4:
        ui->Btn4Techmaintain->click();
        break;
    case Qt::Key_F5:
        ui->Btn5Activedefects->click();
        break;
    }
}

void MenuStatsWidget::on_Btn1Stats_clicked()
{
   auto* w = new MenuStatsGeneralWidget(this);
   w->move(pos());
   w->resize(size());
   WindowManager::getInstance()->Open(this,w);
}

void MenuStatsWidget::on_Btn4Techmaintain_clicked()
{
    auto* w = new MenuStatsToWidget(this);
    w->move(pos());
    w->resize(size());
    WindowManager::getInstance()->Open(this,w);
}

void MenuStatsWidget::on_Btn2Crashstats_clicked()
{
    auto* w = new MenuStatsAlarmWidget(this);
    w->move(pos());
    w->resize(size());
    WindowManager::getInstance()->Open(this,w);
}

void MenuStatsWidget::on_Btn3Eventlog_clicked()
{
    auto* w = new MenuStatsEventlogWidget(this);
    w->move(pos());
    w->resize(size());
    WindowManager::getInstance()->Open(this,w);
}

void MenuStatsWidget::on_Btn5Activedefects_clicked()
{
    auto* w = new MenuStatsJ1939Widget(this);
    w->move(pos());
    w->resize(size());
    WindowManager::getInstance()->Open(this,w);
}

void MenuStatsWidget::on_Btn6Close_clicked()
{
    WindowManager::getInstance()->CloseMe(this);
}
