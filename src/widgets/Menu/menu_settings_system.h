#pragma once

#include <QWidget>
#include <QKeyEvent>

#include "peripherials/PeripherialsManager.h"
#include "peripherials/can/CanMessage.h"
#include "peripherials/can/packets/Settings.h"


namespace Ui {
class MenuSettingsSystemWidget;
}

class MenuSettingsSystemWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MenuSettingsSystemWidget(QWidget *parent = 0);
    ~MenuSettingsSystemWidget();

protected:
    void keyPressEvent(QKeyEvent* e);

private slots:
    //can
    void OnCanMessage(CanMessage msg);
    void OnCanConnectionLost();
    void OnCanConnectionRestore();

    void on_Btn1ClearTotalstats_clicked();
    void on_Btn6Close_clicked();
    void on_Btn4ChangePassword1_clicked();
    void on_Btn5ChangePassword2_clicked();
    void on_Btn2ClearAlarmstats_clicked();
    void on_inputcontrol_wheel_diameter_returnPressed();
    void on_inputcontrol_coeff_speed_returnPressed();
    void on_inputcontrol_coeff_engine_returnPressed();
    void on_inputcontrol_serialNumber_returnPressed();
    void on_inputcontrol_time_eto_returnPressed();
    void on_inputcontrol_time_to1_returnPressed();
    void on_inputcontrol_time_to2_returnPressed();
    void on_inputcontrol_temperature_rev_returnPressed();
    void on_inputcontrol_timeinterval_rev_returnPressed();

    void on_Btn3FactoryReset_clicked();
    void on_fuelSensType_currentIndexChanged(int index);
    void on_combineType_currentIndexChanged(int index);
    void on_reaperType_currentIndexChanged(int index);
    void on_engineType_currentIndexChanged(int index);
    void on_chk_temperature_rev_stateChanged(int state);
    void on_chk_timeinterval_rev_stateChanged(int state);

private:
    Ui::MenuSettingsSystemWidget *ui;

    int _password1;
    int _password2;

    bool _fuelSensType_initialized;
    bool _combineType_initialized;
    bool _reaperType_initialized;
    bool _engineType_initialized;

    static const int TIMEINTERVAL_REV_OFF = 0; // в протоколе "0" значит "выкл"
    static const int TIMEINTERVAL_REV_MIN = 1;
    static const int TIMEINTERVAL_REV_MAX = 60;

    static const int TEMPERATURE_REV_OFF = 84; // в протоколе "84" значит "выкл"
    static const int TEMPERATURE_REV_MIN = 85;
    static const int TEMPERATURE_REV_MAX = 95;

};
