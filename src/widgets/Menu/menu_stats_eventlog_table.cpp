#include "3rdparty/flickcharm/flickcharm.h"
#include "widgets/Menu/menu_stats_eventlog_table.h"

#include "ui_menu_stats_eventlog_table.h"
#include "widgets/EventFilters/KeyPressRedirectFilter.h"
#include <QScrollBar>
#include <QKeyEvent>

MenuStatsEventlogTableWidget::MenuStatsEventlogTableWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MenuStatsEventlogTableWidget)
{
    ui->setupUi(this);
    int rc=5000;
    ui->tableWidget->setRowCount(rc);

    for(int i=0;i<rc;i++)
    {
        ui->tableWidget->setItem(i, 0, new QTableWidgetItem("--.--.-- / --.--"));
        ui->tableWidget->setItem(i, 1, new QTableWidgetItem("----"));
        ui->tableWidget->setItem(i, 2, new QTableWidgetItem("(-) --"));
        ui->tableWidget->setItem(i, 3, new QTableWidgetItem("-"));
    }

    auto* fc = new FlickCharm(this, 0.1f);
    fc->activateOn(ui->tableWidget);
    ui->tableWidget->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    ui->tableWidget->viewport()->setFocusPolicy(Qt::NoFocus);
}


void MenuStatsEventlogTableWidget::ScrollUp()
{
    qDebug("SCROLL UP");

    QScrollBar *sc = ui->tableWidget->verticalScrollBar();
    sc->setValue(sc->value() - sc->pageStep());
}

void MenuStatsEventlogTableWidget::ScrollDown()
{
    qDebug("SCROLL DOWN");

    QScrollBar *sc = ui->tableWidget->verticalScrollBar();
    sc->setValue(sc->value() + sc->pageStep());
}


void MenuStatsEventlogTableWidget::ChangeItem(int row, int col, QString text)
{
      ui->tableWidget->setItem(row, col, new QTableWidgetItem(text));
}

MenuStatsEventlogTableWidget::~MenuStatsEventlogTableWidget()
{
    delete ui;
}
