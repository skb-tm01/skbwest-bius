#include <QScrollArea>

#include "3rdparty/flickcharm/flickcharm.h"

#include "peripherials/PeripherialsManager.h"
#include "peripherials/can/packets/Probeg.h"
#include "widgets/EventFilters/FocusEventFilter.h"
#include "widgets/Menu/menu_stats_alarm.h"

#include "peripherials/can/packets/Settings.h"
#include "peripherials/combines/CombineKeeper.h"

#include "ui_menu_stats_alarm.h"
#include "widgets/WindowManager.h"

#include <QScrollBar>
#include "widgets/EventFilters/KeyPressRedirectFilter.h"

MenuStatsAlarmWidget::MenuStatsAlarmWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MenuStatsAlarmWidget)
{
    setAttribute(Qt::WA_DeleteOnClose);
    setWindowFlags(Qt::Window | Qt::FramelessWindowHint);

    ui->setupUi(this);
    auto* eventFilter = new FocusEventFilter(this);
    ui->Btn1Close->installEventFilter(eventFilter);

    FlickCharm *flickCharm = new FlickCharm(this);
    flickCharm->activateOn(ui->scrollArea);
    //FlickCharm disables scrollbar so enables it again
    ui->scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);

    _table = qobject_cast<MenuStatsAlarmTableWidget*>(ui->scrollArea->widget());


    auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
    if(canworker!=nullptr)
    {
        connect(canworker,SIGNAL(received(CanMessage)),this,SLOT(OnCanMessage(CanMessage)));
        connect(canworker,SIGNAL(connectionLost()),this,SLOT(OnCanConnectionLost()));
        connect(canworker,SIGNAL(connectionRestored()),this,SLOT(OnCanConnectionRestore()));
        OnCanConnectionRestore();
    }

    ui->scrollArea->setFocusPolicy(Qt::NoFocus);

    QVector<int> buttons;
    buttons.append(Qt::Key_Up);
    buttons.append(Qt::Key_Down);

    auto* keyRedirectFilter = new  KeyPressRedirectFilter(window(), buttons,this);
    for(auto* button: this->findChildren<QPushButton *>())
    {
        button->installEventFilter(keyRedirectFilter);
    }
}

MenuStatsAlarmWidget::~MenuStatsAlarmWidget()
{
    delete ui;
}

void MenuStatsAlarmWidget::ScrollUp()
{
    qDebug("SCROLL UP");
    QScrollBar *sc = ui->scrollArea->verticalScrollBar();
    sc->setValue(sc->value() - sc->pageStep());
}

void MenuStatsAlarmWidget::ScrollDown()
{
    qDebug("SCROLL DOWN");
    QScrollBar *sc = ui->scrollArea->verticalScrollBar();
    sc->setValue(sc->value() + sc->pageStep());
}

void MenuStatsAlarmWidget::OnCanMessage(CanMessage msg)
{  
    if(msg.can_id == CanPacketProbegResponse::packet_id)
    {
        auto packet = CanPacketProbegResponse::Parse(msg.data);
        float value = packet.value / 10.0f;
        switch(packet.type)
        {
            //page 1
            case CanPacketProbegType::alarm_engine_oil_pressure:
                _table->setTime(0, value);
                break;
            case CanPacketProbegType::alarm_engine_coolant_temp:
                _table->setTime(1, value);
                break;
            case CanPacketProbegType::alarm_hydraulic_powercyl_oil_temp:
                _table->setTime(2, value);
                break;
            case CanPacketProbegType::alarm_hydraulic_movementpar_oil_temp:
                _table->setTime(3, value);
                break;
            case CanPacketProbegType::alarm_oiltank_oil_level:
                _table->setTime(4, value);
                break;

            //page 2
            case CanPacketProbegType::alarm_hydraulicblock_overlevel:
                _table->setTime(5, value);
                break;
            case CanPacketProbegType::alarm_shaker_stuck:
                _table->setTime(6, value);
                break;
            case CanPacketProbegType::alarm_drum_low_speed:
                _table->setTime(7, value);
                break;
            case CanPacketProbegType::alarm_cleaning_fan_low_speed:
                _table->setTime(8, value);
                break;
            case CanPacketProbegType::alarm_drum_chopper_low_speed:
                _table->setTime(9, value);
                break;

            //page 3
            case CanPacketProbegType::alarm_shaker_low_speed:
                _table->setTime(10, value);
                break;
            case CanPacketProbegType::alarm_coiled_auger_low_speed:
                _table->setTime(11, value);
                break;
            case CanPacketProbegType::alarm_grain_auger_low_speed:
                _table->setTime(12, value);
                break;
            case CanPacketProbegType::alarm_stacker_low_speed:
                _table->setTime(13, value);
                break;
            case CanPacketProbegType::alarm_engine_coolant_level:
                _table->setTime(14, value);
                break;

            //page 4
            case CanPacketProbegType::alarm_grain_tank_opened:
                _table->setTime(15, value);
                break;
            case CanPacketProbegType::alarm_discharge_auger_on:
                _table->setTime(16, value);
                break;
            case CanPacketProbegType::probeg_poweralarm_total:
                _table->setTime(17, value);
                break;

            default:
                break;
        }
    }
    else if(msg.can_id == CanPacketSettingsGetResponse::packet_id)
    {
        auto packet = CanPacketSettingsGetResponse::Parse(msg.data);
        if(packet.type == CanPacketSettingsType::settings_combine_model_3)
        {
            CombineKeeper::GetInstance().setCurrentCombine(packet.value);
            auto combine = CombineKeeper::GetInstance().getCurrentCombine();
            if(combine == nullptr)
                return;
            _table->setStrawEngineType(combine->getEngineType());
        }
    }
}

void MenuStatsAlarmWidget::OnCanConnectionLost()
{
    for(int i=0;i<_table->rowCount;i++)
    {
        _table->setTime(i,-1);
    }
}

void MenuStatsAlarmWidget::OnCanConnectionRestore()
{
    CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_combine_model_3);

    CanPacketProbegRequest::Send(CanPacketProbegType::alarm_cleaning_fan_low_speed);
    CanPacketProbegRequest::Send(CanPacketProbegType::alarm_coiled_auger_low_speed);
    CanPacketProbegRequest::Send(CanPacketProbegType::alarm_discharge_auger_on);
    CanPacketProbegRequest::Send(CanPacketProbegType::alarm_drum_chopper_low_speed);
    CanPacketProbegRequest::Send(CanPacketProbegType::alarm_drum_low_speed);
    CanPacketProbegRequest::Send(CanPacketProbegType::alarm_engine_coolant_level);
    CanPacketProbegRequest::Send(CanPacketProbegType::alarm_engine_coolant_temp);
    CanPacketProbegRequest::Send(CanPacketProbegType::alarm_engine_oil_pressure);
    CanPacketProbegRequest::Send(CanPacketProbegType::alarm_grain_auger_low_speed);
    CanPacketProbegRequest::Send(CanPacketProbegType::alarm_grain_tank_opened);
    CanPacketProbegRequest::Send(CanPacketProbegType::alarm_hydraulic_movementpar_oil_temp);
    CanPacketProbegRequest::Send(CanPacketProbegType::alarm_hydraulic_powercyl_oil_temp);
    CanPacketProbegRequest::Send(CanPacketProbegType::alarm_hydraulicblock_overlevel);
    CanPacketProbegRequest::Send(CanPacketProbegType::alarm_oiltank_oil_level);
    CanPacketProbegRequest::Send(CanPacketProbegType::alarm_shaker_low_speed);
    CanPacketProbegRequest::Send(CanPacketProbegType::alarm_shaker_stuck);
    CanPacketProbegRequest::Send(CanPacketProbegType::alarm_stacker_low_speed);
    CanPacketProbegRequest::Send(CanPacketProbegType::probeg_poweralarm_total);
    CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_combine_model_3);
}

void MenuStatsAlarmWidget::keyPressEvent(QKeyEvent* e)
{
    switch(e->key())
    {
    case Qt::Key_Escape:
        WindowManager::getInstance()->CloseMe(this);
        break;
    case Qt::Key_Down:
        ScrollDown();
        break;
    case Qt::Key_Up:
        ScrollUp();
        break;
    }
}

void MenuStatsAlarmWidget::on_Btn1Close_clicked()
{
    WindowManager::getInstance()->CloseMe(this);
}
