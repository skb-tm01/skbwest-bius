#pragma once

#include <QKeyEvent>
#include <QWidget>

#include "peripherials/can/CanMessage.h"

namespace Ui {
    class MenuStatsEventlogWidget;
}


class MenuStatsEventlogWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MenuStatsEventlogWidget(QWidget *parent = 0);
    ~MenuStatsEventlogWidget();

protected:
    void keyPressEvent(QKeyEvent* e);

private slots:
    void on_Btn1Close_clicked();

    void OnCanMessage(CanMessage msg);
    void OnCanConnectionLost();
    void OnCanConnectionRestore();

    void ScrollUp();
    void ScrollDown();
private:
    Ui::MenuStatsEventlogWidget *ui;
    int counter;
};
