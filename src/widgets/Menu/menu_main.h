#pragma once

#include <QWidget>

namespace Ui {
class MenuMain;
}

class MenuMain : public QWidget
{
    Q_OBJECT

public:
    explicit MenuMain(QWidget *parent = 0);
    ~MenuMain();

protected:
    void keyPressEvent(QKeyEvent* e);

private slots:
    void on_Btn1Statistics_clicked();

    void on_Btn2Help_clicked();

    void on_Btn3Settings_clicked();

    void on_Btn4Calibration_clicked();

    void on_Btn5Sensorstate_clicked();

    void on_Btn6Datacopy_clicked();

    void on_Btn7Close_clicked();

private:
    Ui::MenuMain *ui;
};
