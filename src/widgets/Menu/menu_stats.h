#ifndef STATSDATAWIDGET_H
#define STATSDATAWIDGET_H

#include <QWidget>
#include <QKeyEvent>

namespace Ui {
class MenuStatsWidget;
}


class MenuStatsWidget : public QWidget
{
    Q_OBJECT

public:
    // parent = MainWidget
    explicit MenuStatsWidget(QWidget *parent = 0);
    ~MenuStatsWidget();
protected:
    void keyPressEvent(QKeyEvent* e);
private slots:
    void on_Btn6Close_clicked();

    void on_Btn1Stats_clicked();

    void on_Btn4Techmaintain_clicked();

    void on_Btn2Crashstats_clicked();

    void on_Btn3Eventlog_clicked();

    void on_Btn5Activedefects_clicked();

private:
    Ui::MenuStatsWidget *ui;
};

#endif
