#pragma once

#include <QWidget>
#include <QKeyEvent>

#include "peripherials/PeripherialsManager.h"
#include "peripherials/can/CanMessage.h"


namespace Ui {
class MenuStatsJ1939Widget;
}


class MenuStatsJ1939Widget : public QWidget
{
    Q_OBJECT

public:
    explicit MenuStatsJ1939Widget(QWidget *parent = 0);
    ~MenuStatsJ1939Widget();

protected:
    void keyPressEvent(QKeyEvent* e);

private slots:
    void on_Btn1Close_clicked();

    void OnCanMessage(CanMessage msg);
    void OnCanConnectionLost();
    void OnCanConnectionRestore();

private:
    Ui::MenuStatsJ1939Widget *ui;
};
