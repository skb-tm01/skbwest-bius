#pragma once

#include <QMainWindow>
#include <QTimer>

#include "alerts/AlertDescription.h"
#include "alerts/AlertsWorker.h"

#include "peripherials/can/CanMessage.h"
#include "widgets/warningmessage.h"

#include "widgets/Menu/menu_main.h"
#include "widgets/Menu/menu_sensors_rotations.h"

#include "widgets/Panels/AlertPanel.h"
#include "widgets/Panels/AlertPanelItem.h"
#include "widgets/Panels/StatusPanel.h"
#include "widgets/Panels/StatusPanelItem.h"


#include "SettingsManager.h"

namespace Ui
{
    class MainWindow;
}

class GaugesWidget;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void Initialize_CAN();

    inline VehicleMode centerWidgetMode(){ return _centerWidgetMode;}
    WarningMessage* activeWarning();

    static MainWindow* Instance;

protected:
    void keyReleaseEvent(QKeyEvent* e);
    void keyPressEvent(QKeyEvent* e);

public slots:
    void MenuButtonClicked();
    void onIdle();
    void emitKeyEvent(QKeyEvent* ev);
    AlertPanelWidget* GetAlertPanel();
    StatusPanelWidget* GetStatusPanel();

signals:
    void centerWidgetModeChanged(int centerWidgetMode);
    void sig_emitKeyEvent(QKeyEvent* ev);
//    void alert_can_lost();
//    void alert_can_restored();
///
///ALERTS
///
public slots:
    void ShowAlertTopbar(AlertDescription* alert_description);
    void ShowAlertWindow(AlertDescription* alert_description);
    void RemoveAlert(QString alert_id);


///
private slots:
    void setCenterWidgetWorkingMode(int centerWidgetMode);
    void OnCanMessage(CanMessage msg);
    void OnCanConnectionLost();
    void onWarningClosed();
    void lossSave();
    void menuMainClosed();
    void menuOpened();
    void leftScreenButtonClicked();
    void rightScreenButtonClicked();
    void gaugeWidgetsLossSave();


private:
    qint64 _enterPressTime;

    WarningMessage* _activeWarning;
    AlertDescription* _activeDescription;

    VehicleMode _centerWidgetMode;
    int _isInWorkMode;

    Ui::MainWindow *ui;

    GaugesWidget* _gaugesWidget;
    MenuSensorsRotationsWidget* _engineRotationsWidget;

    QTimer _longPressTimer;
};
