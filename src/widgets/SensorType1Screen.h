#pragma once

#include <QWidget>
#include <QKeyEvent>
#include <QTimer>

#include "peripherials/can/CanMessage.h"
#include "peripherials/sensors/ISensor.h"

namespace Ui {
class SensorType1Screen;
}

class SensorType1Screen : public QWidget
{
    Q_OBJECT

public:
    explicit SensorType1Screen(ISensor* sensor, QWidget *parent = 0);
    ~SensorType1Screen();

private slots:
    void on_Btn1Close_clicked();

    //can
    void OnCanMessage(CanMessage msg);
    void OnCanConnectionLost();
    void OnCanConnectionRestore();



    void SendGeneralSettings();
    void SendTimeSettings();

    void on_akc_level_returnPressed();

    void on_akc_updn_returnPressed();

    void on_akc_status_returnPressed();

    void on_akc_t200ms_returnPressed();

    void on_akc_ifchan_returnPressed();

    void on_akc_ifdata_returnPressed();

protected:
    void keyPressEvent(QKeyEvent* e);

private:

    ISensor* _sensor;
    Ui::SensorType1Screen *ui;
    QTimer timer;
};
