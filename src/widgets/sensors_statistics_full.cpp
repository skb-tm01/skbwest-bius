#include <QDateTime>
#include <QList>
#include <QStandardItemModel>

#include "core/FlowLayout.h"
#include "core/helper.h"

#include "peripherials/PeripherialsManager.h"
#include "peripherials/sensors/SensorManager.h"

#include "widgets/SensorStateInstructions.h"
#include "widgets/sensors_statistics_full.h"
#include "widgets/EventFilters/FocusEventFilter.h"
#include "alerts/AlertsManager.h"
#include "ui_sensors_statistics_full.h"

#include "widgets/WindowManager.h"
#include <QDebug>

sensors_statistics_full* sensors_statistics_full::Instance = nullptr;

sensors_statistics_full::sensors_statistics_full(QWidget *parent, EnumSensorsStatisticsMode mode) :
    QWidget(parent),
    ui(new Ui::sensors_statistics_full)
{
    Instance = this;

    setAttribute(Qt::WA_DeleteOnClose);
    setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    ui->setupUi(this);

    _canworker = PeripherialsManager::instance().CAN_Worker_Get();
    _showServiceMenu = false;
    _timer = nullptr;
    _mode = mode;

    if(_mode==EnumSensorsStatisticsMode::Menu)
    {
        auto* eventFilter = new FocusEventFilter(this);
        ui->ButtonClose->installEventFilter(eventFilter);
        ui->LabelSensorName->setVisible(true);
        qDebug() << "sensorstatistics: menu mode; no clearing alerts";
    }
    else
    {
        ui->LabelSensorName->setVisible(false);
        ui->ButtonClose->setText(QString::fromUtf8("ESC. Пропустить"));
        _timer = new QTimer(this);
        _timer->setInterval(1000);
        _time = 0;
        connect(_timer,SIGNAL(timeout()),this,SLOT(OnTimer()));

        auto* alertworker = AlertsManager::instance().Alerts_Worker_Get();
        if(alertworker!=nullptr)
        {
            alertworker->ClearAlerts(true);
            qDebug() << "sensorstatistics: not menu mode; alertworker->ClearAlerts(true);";
        }
    }

    FlowLayout *flowLayout = new FlowLayout(0);

    for(SensorType type: RealSensorsTypes)
    {
        auto sensors = SensorManager::instance().GetSensors(type);

        for(int i=0;i<sensors.size();i++)
        {
            auto* w = new SensorWidget(this,sensors[i]);
            if(_mode==EnumSensorsStatisticsMode::OnStart)
            {
                w->setFocusPolicy(Qt::NoFocus);
                w->setEnabled(false);
            }
            QObject::connect(w,SIGNAL(selectedChanged(SensorWidget*)), this, SLOT(OnSensorSelected(SensorWidget*)));
            flowLayout->addWidget(w);
            QObject::connect(dynamic_cast<QObject*>(sensors[i]),SIGNAL(stateChanged()), ui->WidgetSensors, SLOT(update()));
        }
    }
    ui->WidgetSensors->setLayout(flowLayout);

    ApplyNightDay();
    QObject::connect(SettingsManager::settings(),SIGNAL(NightDayChanged(EnumNightDayMode)), this, SLOT(ApplyNightDay()));

    ShowSensorName(nullptr);

    if(_mode==EnumSensorsStatisticsMode::Menu)
        ui->WidgetSensors->installEventFilter(this);

    QObject::connect(&_requestTimer,SIGNAL(timeout()),this,SLOT(RequestSensorsState()));
}

sensors_statistics_full::~sensors_statistics_full()
{
    Instance = nullptr;
    delete ui;
}

bool sensors_statistics_full::ServiceMenuState()
{
    return _showServiceMenu;
}

void sensors_statistics_full::SetServiceMenuState(bool state)
{
    _showServiceMenu = state;
}

void sensors_statistics_full::showEvent(QShowEvent *)
{
    if(_timer != nullptr)
        _timer->start();

    if(_mode==EnumSensorsStatisticsMode::Menu)
    {
        this->_requestTimer.setInterval(1000*5);
        this->_requestTimer.start();
    }
}

void sensors_statistics_full::ShowSensorName(SensorWidget* src)
{
    QString str;

    if(src==nullptr)
        str = QString::fromUtf8("Датчик: не выбран");
    else
    {
        str = QString::fromUtf8("Датчик: ")+src->Sensor()->GetName("ru");
        if(!src->Sensor()->IsUnderControl())
            str+=QString::fromUtf8(" (снят с контроля)");
    }

    ui->LabelSensorName->setText(str);
}

void sensors_statistics_full::SelectNearSensor(Qt::Orientation orientation,bool dir)
{
    auto* selected = SelectedSensor();
    if(selected == nullptr)
    {
        FirstSensor()->Select();
        return;
    }

    auto* near = static_cast<FlowLayout*>(ui->WidgetSensors->layout())->nearWidget(selected,orientation,dir);

    if(near == nullptr)
    {
        ui->ButtonClose->setFocus();
    }
    else
    {
        static_cast<SensorWidget*>(near)->Select();
    }
}

void sensors_statistics_full::OnSensorSelected(SensorWidget* src)
{
    if(!src->isSelected())
        return;

    for(auto* w: ui->WidgetSensors->findChildren<SensorWidget *>())
    {
        if(w==src)
            continue;

        w->Unselect();
    }

    ShowSensorName(src);
}

void sensors_statistics_full::RequestSensorsState()
{
    for(auto* sensor : SensorManager::instance().GetSensors())
    {
        sensor->RequestState();
    }
}

void sensors_statistics_full::OpenInfoWindow()
{
    auto* sensor = SelectedSensor();
    if(sensor!=nullptr)
        sensor->ShowSensorInfo(_showServiceMenu);
}

SensorWidget* sensors_statistics_full::SelectedSensor()
{
    for(auto* w: ui->WidgetSensors->findChildren<SensorWidget *>())
    {
        if(w->isSelected())
            return w;
    }
    return nullptr;
}

SensorWidget* sensors_statistics_full::FirstSensor()
{
    auto l = ui->WidgetSensors->findChildren<SensorWidget *>();
    if(l.empty())
        return nullptr;
    return l.first();
}

SensorWidget* sensors_statistics_full::LastSensor()
{
    auto l = ui->WidgetSensors->findChildren<SensorWidget *>();
    if(l.empty())
        return nullptr;
    return l.last();
}

void sensors_statistics_full::keyPressEvent(QKeyEvent* e)
{
    switch(e->key())
    {
    case Qt::Key_Escape:
        closeInternal();
        return;
    }

    QWidget::keyPressEvent(e);
}


bool sensors_statistics_full::eventFilter(QObject *obj, QEvent *event)
{
    if(obj!=ui->WidgetSensors)
        return false;

    if (event->type() == QEvent::FocusIn)
    {
        if(SelectedSensor()==nullptr)
        {
            FirstSensor()->Select();
        }
    }
    else if(event->type() == QEvent::KeyPress)
    {
        QKeyEvent *ke = static_cast<QKeyEvent*>(event);
        if (ke->key() == Qt::Key_Enter||ke->key() == Qt::Key_Return)
        {
            OpenInfoWindow();
            return true;
        }
        else if (ke->key() == Qt::Key_Left)
        {
            SelectNearSensor(Qt::Horizontal,false);
            return true;
        }
        else if (ke->key() == Qt::Key_Right)
        {
            SelectNearSensor(Qt::Horizontal,true);
            return true;
        }
        else if (ke->key() == Qt::Key_Up)
        {
            SelectNearSensor(Qt::Vertical,false);
            return true;
        }
        else if (ke->key() == Qt::Key_Down)
        {
            SelectNearSensor(Qt::Vertical,true);
            return true;
        }
    }

    return false;
}

void sensors_statistics_full::ApplyNightDay()
{
    auto nightDayMode = SettingsManager::settings()->currentNightDayMode();
    for(auto* sensorWidget: ui->WidgetSensors->findChildren<SensorWidget *>())
    {
        sensorWidget->setDayNight(nightDayMode==EnumNightDayMode::Day);
    }
}

void sensors_statistics_full::on_ButtonClose_clicked()
{
    closeInternal();
}

const int CloseTime = 10;

void sensors_statistics_full::OnTimer()
{
    if(_canworker!=nullptr && _canworker->IsConnected() == false)
    {
        closeInternal();
        return;
    }
    _time++;
    if(_time>CloseTime)
    {
        closeInternal();
    }
}

void sensors_statistics_full::closeInternal()
{
    if(_timer != nullptr)
        _timer->stop();

    _requestTimer.stop();
    emit OnClosing();
    qApp->processEvents();

    if(_mode==EnumSensorsStatisticsMode::Menu) {
        WindowManager::getInstance()->CloseMe(this);
    }
    else{
        this->close();
    }
}
