#pragma once

#include <QWidget>

#include "SettingsManager.h"

#include "peripherials/PeripherialsManager.h"
#include "peripherials/can/CanMessage.h"
#include "peripherials/can/packets/Taho.h"
#include "peripherials/can/packets/Analog.h"
#include <QMouseEvent>
#include "core/ValueSmoothingHelper.h"
#include "peripherials/combines/Combine.h"

namespace Ui {
class GaugesWidget;
}

class GaugesWidget : public QWidget
{
    Q_OBJECT

public:
    explicit GaugesWidget(QWidget *parent = 0);
    ~GaugesWidget();

    void Initialize_CAN();
    inline VehicleMode workingMode(){ return _workingMode;}
    void setWorkingMode(const VehicleMode &workingMode);



public slots:
    void ApplyNightDay(EnumNightDayMode mode);

private:
    Ui::GaugesWidget *ui;
    VehicleMode _workingMode;

private slots:
    void OnCanMessage(CanMessage msg);
    void OnCanConnectionLost();
    void OnCanConnectionRestore();
    void mousePressEvent(QMouseEvent * event);
    void mouseReleaseEvent(QMouseEvent* event);
    void mouseDoubleClickEvent(QMouseEvent * event);
    void setMotorOilPressure(float value);
    void setMotorCoolantTemp(float value);
    void setAirFlowTemp(float value);
    void setHydraulicOilTemp(float value);
    void setHydraulicOilControlStatus();
signals:
    void LeftSideButtonClicked();
    void RightSideButtonClicked();
    void lossesGaugeWidgetsDoubleClicked();

private:
    ValueSmoothingHelper* _speedValue;
    ValueSmoothingHelper* _engineValue;
    ValueSmoothingHelper* _fanValue;
    ValueSmoothingHelper* _drumValue;
    ValueSmoothingHelper* _powerValue;

    ValueSmoothingHelper* _motorOilPressureValue;
    ValueSmoothingHelper* _motorCoolantTempValue;

    ValueSmoothingHelper* _hydraulicOilPressureValue;
    ValueSmoothingHelper* _hydraulicOilTempValue;

    ValueSmoothingHelper* _airFlowTempValue;

    ValueSmoothingHelper* _lossesAfterValue;
    ValueSmoothingHelper* _lossesBeforeValue;

    void OnCanMessage_ContactState(CanMessage msg);
    void OnCanMessage_Request3(CanMessage msg);
    void OnCanMessage_AirFlowTemp(CanMessage msg);

    void updateControlVisibility();

    QTimer _doubleClickTimer;
    int doubleClickMaxInterval;

    int engine_rpm;
    int fontSize;

    int mousePressX;
    int mousePressY;
};
