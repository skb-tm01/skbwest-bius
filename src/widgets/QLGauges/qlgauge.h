#ifndef QLGAUGE_H
#define QLGAUGE_H

#include <QWidget>
//#include <Qt>

class QLGauge : public QWidget
{
    Q_OBJECT
public:
    explicit QLGauge(QWidget *parent = nullptr);

    enum Error{InvalidValueRange};

    void setValueRange(float minValue,float maxValue);
    inline float currentValue(){ return _currentValue;}
    void setCurrentValue(float currentValue);

    inline float barSize() {return _barSize;}
    void setBarSize(float barSize);

    inline float ticksLength() {return _ticksLength;}
    void setTicksLength(float tickLength);

    inline float ticksWidth() {return _ticksWidth;}
    void setTicksWidth(float ticksWidth);

    inline float ticksStep(){return _ticksStep;}
    void setTicksStep(float ticksStep);

    inline Qt::Orientation orientation(){ return _orientation;}
    void setOrientation(const Qt::Orientation &orientation);

    inline int segmentCount() {return _segmentCount;}
    void setSegmentCount(int segmentCount);

    inline QColor color() {return _color;}
    void setColor(const QColor &color);

    float getSizeFromValue(float value);
    float getPosFromValue(float value);
    QColor getColorFromValue(float value);

    inline QColor getDefColor(){return _defColor;}
    void setDefColor(const QColor &defColor);

    void AddBand(float value,QColor color);
    void SetBands(QList<QPair<float,QColor> > bands);
    QList<QPair<float,QColor>> GetBands();
    void ClearBands();
    void SetFullRangeBand(QColor color);

    inline float valuesStep(){return _valuesStep;}
    void setValuesStep(float step);

    inline float fontSize() {return _fontSize;}
    void setFontSize(float fontSize);

    inline bool boldFont() {return _boldFont;}
    void setBoldFont(bool boldFont);

protected:

    virtual QSize minimumSizeHint() const;

    float _minValue;
    float _maxValue;
    float _currentValue;

    float _barSize;
    Qt::Orientation _orientation;
    int _segmentCount;

    float _ticksLength;
    float _ticksWidth;

    QColor _color;

    QColor _defColor;
    QList<QPair<float,QColor> > _bands;

    int _fontSize;
    bool _boldFont;
    float _valuesStep;
    float _ticksStep;

    void paintEvent(QPaintEvent *);

    void drawBar(QPainter *painter);    
    void drawBands(QPainter *painter);
    void drawTicks(QPainter *painter);
    void drawValues(QPainter *painter);

    QRectF barRect();
    QRectF ticksRect();

signals:

public slots:

private:    
    void drawLine(QPainter *painter, float val, const QRectF& rect);
};

#endif // QLGAUGE_H
