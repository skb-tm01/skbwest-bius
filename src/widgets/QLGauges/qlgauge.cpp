#include "qlgauge.h"
#include <QPainter>
#include<QFont>
#include<QDebug>

QLGauge::QLGauge(QWidget *parent) : QWidget(parent)
{
    setContentsMargins(5,15,5,10);
    _barSize = 25;
    _ticksLength = 10;
    _ticksWidth = 2;
    _ticksStep = 25;
    _orientation = Qt::Vertical;
    _segmentCount = 20;
    _minValue=0;
    _maxValue=100;
    _currentValue = 23;
    _color = Qt::black;
    _defColor = Qt::red;

    _fontSize = 12;
    _boldFont = false;
    _valuesStep = 25;    
}

void QLGauge::setValueRange(float minValue, float maxValue)
{
    if(!(minValue<=maxValue))
        throw( InvalidValueRange);

    if(_minValue==minValue&&_maxValue == maxValue)
        return;

    _minValue = minValue;
    _maxValue = maxValue;

    setCurrentValue(_currentValue);
}

void QLGauge::setCurrentValue(float currentValue)
{
    currentValue = qBound(_minValue, currentValue, _maxValue);
    _currentValue = currentValue;
    update();
}

void QLGauge::setBarSize(float barWidth)
{
    if(_barSize == barWidth)
        return;
    _barSize = barWidth;
    update();
}

void QLGauge::setTicksLength(float tickSize)
{
    if(_ticksLength == tickSize)
        return;
    _ticksLength = tickSize;
    update();
}

void QLGauge::setOrientation(const Qt::Orientation &orientation)
{
    _orientation = orientation;
}

void QLGauge::setSegmentCount(int segmentCount)
{
    _segmentCount = segmentCount;
}

void QLGauge::setColor(const QColor &color)
{
    if(_color == color)
        return;
    _color = color;
    update();
}

float QLGauge::getSizeFromValue(float value)
{
    QRectF rect = barRect();

    if(!rect.isValid())
        return 0;

    return value*rect.height()/(_maxValue - _minValue);
}

float QLGauge::getPosFromValue(float value)
{
    QRectF rect = barRect();
    return rect.bottom()-getSizeFromValue(value);
}

QColor QLGauge::getColorFromValue(float value)
{
    //    if(_bands.isEmpty())
    //        return _defColor;

    QColor res = _defColor;

    for(int i=0;i<_bands.size();i++)
    {
        if(value > _bands[i].first)
            res = _bands[i].second;
    }
    return res;
}

void QLGauge::setDefColor(const QColor &defColor)
{
    _defColor = defColor;
}

void QLGauge::AddBand(float value, QColor color)
{
    _bands.append(QPair<float,QColor>(value,color));
    update();
}

void QLGauge::SetBands(QList<QPair<float, QColor> > bands)
{
    _bands = bands;
    update();
}

QList<QPair<float, QColor> > QLGauge::GetBands()
{
    return _bands;
}

void QLGauge::ClearBands()
{
    if(_bands.isEmpty())
        return;
    _bands.clear();
    update();
}

void QLGauge::SetFullRangeBand(QColor color)
{
    _bands.clear();
    _bands.append(QPair<float,QColor>(_maxValue,color));
    update();
}

void QLGauge::setValuesStep(float step)
{
    if(_valuesStep == step)
        return;
    _valuesStep = step;
    update();
}

void QLGauge::setFontSize(float fontSize)
{
    if(_fontSize == fontSize)
        return;
    _fontSize = fontSize;
    update();
}

void QLGauge::setBoldFont(bool boldFont)
{
    if(_boldFont == boldFont)
        return;
    _boldFont = boldFont;
    update();
}

QSize QLGauge::minimumSizeHint() const
{
    auto size = geometry().size();
    size.setWidth(_barSize+_ticksLength+50+2);
    return size;
}

void QLGauge::setTicksWidth(float ticksWidth)
{
    if(_ticksWidth == ticksWidth)
        return;
    _ticksWidth = ticksWidth;
    update();
}

void QLGauge::setTicksStep(float ticksStep)
{
    if(_ticksStep == ticksStep)
        return;
    _ticksStep = ticksStep;
    update();
}

void QLGauge::drawBar(QPainter *painter)
{
    QRectF rect = barRect();
    QPen pen(_color);
    pen.setWidthF(1);
    painter->setPen(pen);

    // draw segments
    float segmentStep = (_maxValue - _minValue)/_segmentCount;
    int segment = qRound((_currentValue- _minValue)/segmentStep);

    QRectF colorrect = rect;
    float val = _minValue+segmentStep*segment;
    colorrect.setTop(getPosFromValue(val));

    painter->fillRect(colorrect,getColorFromValue(_currentValue));

    for(int i=1;i<=segment;i++)
        drawLine(painter, _minValue+segmentStep*i, rect);

    pen.setWidthF(2);
    painter->setPen(pen);
    painter->drawRect(rect);
}

void QLGauge::drawBands(QPainter *painter)
{
    QRectF rect = ticksRect();

    rect.setLeft(rect.right()-5);

    painter->fillRect(rect,_defColor);

    for(int i=0;i<_bands.size();i++)
    {
        rect.setBottom(getPosFromValue(_bands[i].first));
        painter->fillRect(rect,_bands[i].second);
    }
}

void QLGauge::drawLine(QPainter *painter, float val, const QRectF& rect)
{
    float pos = getPosFromValue(val);
    QPointF beg(rect.left()+1,pos);
    QPointF end(rect.right(),pos);
    painter->drawLine(beg,end);
}

void QLGauge::drawTicks(QPainter *painter)
{
    QRectF rect = ticksRect();

    QPen pen(_color);
    pen.setWidthF(_ticksWidth);
    painter->setPen(pen);

    for(float val = _minValue;val<=_maxValue;val+=_ticksStep)
        drawLine(painter, val, rect);

}

void QLGauge::drawValues(QPainter *painter)
{
    QRectF rect = ticksRect();

    QFont font(this->font());
    font.setPointSizeF(_fontSize);
    font.setWeight(_boldFont?QFont::Bold:QFont::Normal);
    painter->setFont(font);

    painter->setPen(_color);
    for(float val = _minValue;val<=_maxValue;val+=_valuesStep)
    {
        float pos = getPosFromValue(val);
        QPointF pt(rect.right()+5, pos);

        QString strVal = QString::number(val);
        QFontMetrics fMetrics = painter->fontMetrics();
        QSize sz = fMetrics.size( Qt::TextSingleLine, strVal );
        QRectF txtRect(QPointF(0,0), sz );
        txtRect.moveCenter(pt);
        txtRect.moveRight(rect.left()-5);
        //        if(_correctPos)
        //            Helper::SetInRect(rect,txtRect);

        painter->drawText( txtRect, Qt::TextSingleLine, strVal );
    }
}

QRectF QLGauge::barRect()
{
    QRectF rect = contentsRect();
    if(rect.width()>_barSize)
        rect.setLeft(rect.right() - _barSize);
    return rect;
}

QRectF QLGauge::ticksRect()
{
    QRectF rect = barRect();
    rect.setLeft(rect.left()-_ticksLength);
    rect.setWidth(_ticksLength);
    return rect;
}

void QLGauge::paintEvent(QPaintEvent */*paintEvt*/)
{
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);

    drawBands(&painter);
    drawTicks(&painter);
    drawBar(&painter);
    drawValues(&painter);
}
