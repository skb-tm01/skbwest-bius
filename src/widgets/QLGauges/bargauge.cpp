#include "bargauge.h"
#include "ui_bargauge.h"
#include "qlgauge.h"

#include "core/helper.h"

BarGauge::BarGauge(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::BarGauge)
{
    _timer = NULL;

    _blinking_state = false;
    _blinking_enabled = false;

    ui->setupUi(this);
    _barWidget = new QLGauge(this);
    ui->horizontalLayout->insertWidget(1,_barWidget);

    ui->_labelUnits->setContentsMargins(1,1,1,5);

    setFocusPolicy(Qt::FocusPolicy::NoFocus);
    setCurrentValue(0);
    setST(QString::fromUtf8(""));

    isEnabled = true;
    _saved_gaugeColor = Qt::lightGray;
    _saved_defColor = Qt::lightGray;
    _disabled_color = Qt::GlobalColor::lightGray;
}

BarGauge::~BarGauge()
{
    delete ui;
}

void BarGauge::setST(const QString& symbol)
{
    ui->_label_st->setText(QString(" ")+symbol);
}

void BarGauge::setValueRange(float minValue, float maxValue)
{
    _barWidget->setValueRange(minValue,maxValue);
    _barWidget->setValuesStep((maxValue - minValue)/2.0f);
    _barWidget->setTicksStep((maxValue - minValue)/4.0f);
}

float BarGauge::currentValue()
{
    return _barWidget->currentValue();
}

void BarGauge::setEnabled(bool setEnabled)
{
    if(isEnabled == setEnabled) {
        return;
    }

    isEnabled = setEnabled;
    if(isEnabled) {
        _barWidget->setColor(_saved_gaugeColor);
        _barWidget->setDefColor(_saved_defColor);
        _barWidget->SetBands(_saved_bands);
        _image_default = Helper::SetPixmapColor(_image,_defaultColor);
        ui->_labelImage->setPixmap(_image_default);
        _blinking_enabled = _saved_blinkingState;
    }
    else {
        _barWidget->setColor(_disabled_color);
        _barWidget->setDefColor(_disabled_color);
        _barWidget->SetFullRangeBand(_disabled_color);
        _image_default = Helper::SetPixmapColor(_image,_disabled_color);
        ui->_labelImage->setPixmap(_image_default);
        _blinking_enabled = false;
    }
    updateLabelStylesheets();
}

void BarGauge::setCurrentValue(float value, int fractionalDigits)
{
    _barWidget->setCurrentValue(value);
    ui->_labelValue->setText(QString::number(value,'f',fractionalDigits));
}

void BarGauge::blinkedTimerTimeout()
{
    if(!isEnabled) return;

    _blinking_state= !_blinking_state;
    if(_blinking_state)
        ui->_labelImage->setPixmap(_image_blinking);
    else
        ui->_labelImage->setPixmap(_image_default);
}

void BarGauge::initTimer()
{
    if(_timer!=NULL)
        return;

    _timer = new QTimer();
    _timer->setInterval(1000);
    QObject::connect(_timer,SIGNAL(timeout()),this,SLOT(blinkedTimerTimeout()));
}

void BarGauge::updateLabelStylesheets()
{
    QString textColor;
    if(isEnabled)
        textColor = _saved_gaugeColor.name();
    else
        textColor = _disabled_color.name();

    ui->_labelValue->setStyleSheet(QString("QLabel {font-size: %1pt ; color:" + textColor + " }").arg(_fontSize));
    ui->_labelUnits->setStyleSheet(QString("QLabel {font-size: %1pt ; color:" + textColor + " }").arg(_fontSize*0.5f));
}

bool BarGauge::valueVisible()
{
    return ui->_labelValue->isVisible();
}

void BarGauge::setValueVisible(bool value)
{
    ui->_labelValue->setVisible(value);
}

QColor BarGauge::gaugeColor()
{
    return _saved_gaugeColor;
}

void BarGauge::setGaugeColor(const QColor &gaugeColor)
{    
    _saved_gaugeColor = gaugeColor;
    if(isEnabled)
    {
        _barWidget->setColor(_saved_gaugeColor);
        updateLabelStylesheets();
    }
}

void BarGauge::setDefColor(const QColor &defColor)
{
    _saved_defColor = defColor;
    if(isEnabled)
        _barWidget->setDefColor(_saved_defColor);
}

void BarGauge::AddBand(float value, QColor color)
{
    _saved_bands.append(QPair<float,QColor>(value,color));
    if(isEnabled)
        _barWidget->AddBand(value,color);
}

void BarGauge::ClearBands()
{
    if(isEnabled)
        _barWidget->ClearBands();
    else
        _saved_bands.clear();
}

void BarGauge::setFontSize(float fontSize)
{
    _fontSize = fontSize;
    _barWidget->setFontSize(_fontSize*0.75f);
    updateLabelStylesheets();
}

QString BarGauge::units()
{
    return ui->_labelUnits->text();
}

void BarGauge::setUnits(const QString &units)
{
    ui->_labelUnits->setText(units);
}

void BarGauge::setImage(const QPixmap &image)
{    
    _image = image;
}

void BarGauge::setDefaultColor(QColor color)
{
    _defaultColor = color;
    if(isEnabled)
        _image_default = Helper::SetPixmapColor(_image,_defaultColor);
    else
        _image_default = Helper::SetPixmapColor(_image,_disabled_color);
    if(!_blinking_state)
    {
        ui->_labelImage->setPixmap(_image_default);
    }
}

void BarGauge::setBlinkedColor(QColor color)
{
    _blinkedColor = color;
    _image_blinking = Helper::SetPixmapColor(_image,_blinkedColor);
    if(_blinking_state)
    {
        ui->_labelImage->setPixmap(_image_blinking);
    }
}

void BarGauge::setBlinkedState(bool enabled)
{
    if(_blinking_enabled==enabled)
        return;

    _saved_blinkingState = enabled;
    if(isEnabled)
    {
         _blinking_enabled = enabled;
         if(enabled)
         {
             initTimer();
             _timer->start();
         }
         else
         {
             if(_timer!=NULL)
                 _timer->stop();

             _blinking_enabled = false;
             _blinking_state = false;
             ui->_labelImage->setPixmap(_image_default);
         }
    }
}
