#ifndef LOSSESBEFOREGAUGE_H
#define LOSSESBEFOREGAUGE_H

#include <QWidget>
#include <QTimer>

class QLGauge;

namespace Ui {
class BarGauge;
}

class BarGauge : public QWidget
{
    Q_OBJECT

public:
    explicit BarGauge(QWidget *parent = 0);
    ~BarGauge();

    void setValueRange(float minValue,float maxValue);
    bool valueVisible();
    void setValueVisible(bool value);

    QColor gaugeColor();

    void setGaugeColor(const QColor &gaugeColor);

    void setDefColor(const QColor &defColor);

    void AddBand(float value,QColor color);
    void ClearBands();

    void setFontSize(float fontSize);

    QString units();
    void setUnits(const QString& units);

    void setImage(const QPixmap& image);
    void setDefaultColor(QColor color);
    void setBlinkedColor(QColor color);

    void setBlinkedState(bool enabled);

    void setST(const QString& symbol);

    float currentValue();

    void setEnabled(bool enabled);
public slots:
    void setCurrentValue(float value,int fractionalDigits =0);
    void blinkedTimerTimeout();

private:
    QLGauge* _barWidget;
    Ui::BarGauge *ui;

    //blinking
    QColor _defaultColor;
    QColor _blinkedColor;

    QPixmap _image;
    QPixmap _image_default;
    QPixmap _image_blinking;

    bool _blinking_state;
    bool _blinking_enabled;

    void initTimer();
    void updateLabelStylesheets();

    QTimer* _timer;

    bool isEnabled; // turnes gray if not
    //--- save current state when disabling
    QColor _saved_gaugeColor;
    QColor _saved_defColor;
    QList<QPair<float,QColor> > _saved_bands;
    bool _saved_blinkingState;
    //---
    QColor _disabled_color;

    float _fontSize;
};

#endif // LOSSESBEFOREGAUGE_H
