#pragma once

#include <QKeyEvent>
#include <QWidget>

#include <alerts/AlertDescription.h>
#include "peripherials/sensors/ISensor.h"
#include "alerts/AlertDescription.h"

#define WARNING_TIMER_TIMEOUT_SEC 3

namespace Ui {
class WarningMessage;
}

class WarningMessage : public QWidget
{
    Q_OBJECT

signals:
    void closed();
    void menu_requested();

public:
   WarningMessage(AlertDescription* alertDescription, QWidget *parent = 0);
   AlertDescription* GetAlertDescription();
    ~WarningMessage();

protected:
    void keyPressEvent(QKeyEvent* e);
    void resizeEvent(QResizeEvent *e);

private slots:
    void onTimerTimeout();
    void on_warningTextLabel_clicked();
    void onInfoWindowRequested();

signals:
    void sig_infoWindowRequest();

private:
    Ui::WarningMessage *ui;
    AlertDescription* alert_description;
};
