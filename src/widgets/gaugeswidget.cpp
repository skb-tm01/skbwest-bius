#include "gaugeswidget.h"
#include "ui_gaugeswidget.h"
#include "core/helper.h"
#include "peripherials/can/packets/Settings.h"
#include "peripherials/can/packets/ContactState.h"
#include "core/ByteConversion.h"
#include "peripherials/can/packets/Request3.h"
#include "peripherials/can/packets/AirFlowTemp.h"
#include "peripherials/sensors/SensorManager.h"
#include "SettingsManager.h"
#include "peripherials/combines/CombineKeeper.h"

GaugesWidget::GaugesWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::GaugesWidget)
{    
    ui->setupUi(this);

    fontSize = 28;
    doubleClickMaxInterval = 800;
    engine_rpm = 0;

    mousePressX = 0;
    mousePressY = 0;

    ui->_airFlowTempGauge->setVisible(false);

    ui->_lossesBeforeGauge->setImage(Helper::LoadPixmap("strawshaker_losses.svg",Helper::defIconSize()));
    ui->_lossesBeforeGauge->setDefaultColor(Helper::gaugeIconColor());
    ui->_lossesBeforeGauge->setBlinkedColor(Helper::gaugeBlinkingColorY());
    ui->_lossesBeforeGauge->setUnits(QString::fromUtf8("%"));
    ui->_lossesBeforeGauge->setValueVisible(true);
    ui->_lossesBeforeGauge->setValueRange(0,100);
    ui->_lossesBeforeGauge->setDefColor(Qt::yellow);
    ui->_lossesBeforeGauge->AddBand(25,Qt::green);
    ui->_lossesBeforeGauge->AddBand(75,Qt::red);
    ui->_lossesBeforeGauge->setFontSize(fontSize);
    ui->_lossesBeforeGauge->setST(QString::fromUtf8("s"));

    ui->_lossesAfterGauge->setImage(Helper::LoadPixmap("image127.svg",Helper::defIconSize()));
    ui->_lossesAfterGauge->setDefaultColor(Helper::gaugeIconColor());
    ui->_lossesAfterGauge->setBlinkedColor(Helper::gaugeBlinkingColorY());
    ui->_lossesAfterGauge->setUnits(QString::fromUtf8("%"));
    ui->_lossesAfterGauge->setValueVisible(true);
    ui->_lossesAfterGauge->setValueRange(0,100);
    ui->_lossesAfterGauge->setDefColor(Qt::yellow);
    ui->_lossesAfterGauge->AddBand(25,Qt::green);
    ui->_lossesAfterGauge->AddBand(75,Qt::red);
    ui->_lossesAfterGauge->setFontSize(fontSize);
    ui->_lossesAfterGauge->setST(QString::fromUtf8("s"));

    ui->_hydraulicOilPressureGauge->setImage(Helper::LoadPixmap("image84.svg",Helper::defIconSize()));
    ui->_hydraulicOilPressureGauge->setDefaultColor(Helper::gaugeIconColor());
    ui->_hydraulicOilPressureGauge->setBlinkedColor(Helper::gaugeBlinkingColorY());
    ui->_hydraulicOilPressureGauge->setUnits(QString::fromUtf8("МПа"));
    ui->_hydraulicOilPressureGauge->setValueVisible(true);
    ui->_hydraulicOilPressureGauge->setValueRange(0,20);
    ui->_hydraulicOilPressureGauge->setCurrentValue(0);
    ui->_hydraulicOilPressureGauge->setDefColor(Qt::red);
    ui->_hydraulicOilPressureGauge->AddBand(5,Qt::yellow);
    ui->_hydraulicOilPressureGauge->AddBand(10,Qt::green);
    ui->_hydraulicOilPressureGauge->setFontSize(fontSize);

    ui->_airFlowTempGauge->setImage(Helper::LoadPixmap("airflowtemp.png",Helper::defIconSize()));
    ui->_airFlowTempGauge->setDefaultColor(Helper::gaugeIconColor());
    ui->_airFlowTempGauge->setBlinkedColor(Helper::gaugeBlinkingColorY());
    ui->_airFlowTempGauge->setUnits(QString::fromUtf8("°C"));
    ui->_airFlowTempGauge->setValueRange(0,120);
    ui->_airFlowTempGauge->setDefColor(Qt::blue);
    ui->_airFlowTempGauge->AddBand(30,Qt::green);
    ui->_airFlowTempGauge->AddBand(90,Qt::red);
    ui->_airFlowTempGauge->setFontSize(fontSize);

    ui->_hydraulicOilTempGauge->setImage(Helper::LoadPixmap("image135.svg",Helper::defIconSize()));
    ui->_hydraulicOilTempGauge->setDefaultColor(Helper::gaugeIconColor());
    ui->_hydraulicOilTempGauge->setBlinkedColor(Helper::gaugeBlinkingColorY());
    ui->_hydraulicOilTempGauge->setUnits(QString::fromUtf8("°C"));
    ui->_hydraulicOilTempGauge->setValueRange(0,120);
    ui->_hydraulicOilTempGauge->setDefColor(Qt::blue);
    ui->_hydraulicOilTempGauge->AddBand(30,Qt::green);
    ui->_hydraulicOilTempGauge->AddBand(90,Qt::red);
    ui->_hydraulicOilTempGauge->setFontSize(fontSize);

    ui->_motorCoolantTempGauge->setImage(Helper::LoadPixmap("image93.svg",Helper::defIconSize()));
    ui->_motorCoolantTempGauge->setDefaultColor(Helper::gaugeIconColor());
    ui->_motorCoolantTempGauge->setBlinkedColor(Helper::gaugeBlinkingColorY());
    ui->_motorCoolantTempGauge->setUnits(QString::fromUtf8("°C"));
    ui->_motorCoolantTempGauge->setValueRange(0,120);
    ui->_motorCoolantTempGauge->setDefColor(Qt::blue);
    ui->_motorCoolantTempGauge->AddBand(30,Qt::green);
    ui->_motorCoolantTempGauge->AddBand(90,Qt::red);
    ui->_motorCoolantTempGauge->setFontSize(fontSize);

    ui->_motorOilPressureGauge->setImage(Helper::LoadPixmap("image90.svg",Helper::defIconSize()));
    ui->_motorOilPressureGauge->setDefaultColor(Helper::gaugeIconColor());
    ui->_motorOilPressureGauge->setBlinkedColor(Helper::gaugeBlinkingColorY());
    ui->_motorOilPressureGauge->setUnits(QString::fromUtf8("МПа"));
    ui->_motorOilPressureGauge->setValueRange(0,1);
    ui->_motorOilPressureGauge->setDefColor(Qt::red);
    ui->_motorOilPressureGauge->AddBand(0.05,Qt::yellow);
    ui->_motorOilPressureGauge->AddBand(0.5,Qt::green);
    ui->_motorOilPressureGauge->setFontSize(fontSize);
    ui->_motorOilPressureGauge->setCurrentValue(0);

    ui->MotorCoolantTempIcon->setPixmap(Helper::LoadPixmap("image93.svg",Helper::defIconSize(),Helper::gaugeIconColor()));
    ui->MotorOilPressureIcon->setPixmap(Helper::LoadPixmap("image90.svg",Helper::defIconSize(),Helper::gaugeIconColor()));
    ui->AirFlowIcon->setPixmap(Helper::LoadPixmap("airflowtemp.png",Helper::defIconSize(),Helper::gaugeIconColor()));
    ui->HydraulicOilTempIcon->setPixmap(Helper::LoadPixmap("image135.svg",Helper::defIconSize(),Helper::gaugeIconColor()));

    ui->MotorCoolantTempValue->setStyleSheet(QString("QLabel {font-size: %1pt }").arg(fontSize));
    ui->MotorOilPressureValue->setStyleSheet(QString("QLabel {font-size: %1pt }").arg(fontSize));
    ui->AirFlowValue->setStyleSheet(QString("QLabel {font-size: %1pt }").arg(fontSize));
    ui->HydraulicOilTempValue->setStyleSheet(QString("QLabel {font-size: %1pt }").arg(fontSize));

    setMotorCoolantTemp(0);
    setMotorOilPressure(0);
    setAirFlowTemp(0);
    setHydraulicOilTemp(0);

    _speedValue = new ValueSmoothingHelper(ui->_speedGauge);
    connect(_speedValue,SIGNAL(ValueChanged(float)),ui->_speedGauge,SLOT(setCurrentValue(float)));

    _engineValue = new ValueSmoothingHelper(ui->_engineGauge);
    connect(_engineValue,SIGNAL(ValueChanged(float)),ui->_engineGauge,SLOT(setCurrentValue(float)));

    _fanValue = new ValueSmoothingHelper(ui->_fanGauge);
    connect(_fanValue,SIGNAL(ValueChanged(float)),ui->_fanGauge,SLOT(setCurrentValue(float)));

    _drumValue = new ValueSmoothingHelper(ui->_drumGauge);
    connect(_drumValue,SIGNAL(ValueChanged(float)),ui->_drumGauge,SLOT(setCurrentValue(float)));

    _powerValue = new ValueSmoothingHelper(ui->_powerGauge);
    connect(_powerValue,SIGNAL(ValueChanged(float)),ui->_powerGauge,SLOT(setCurrentValue(float)));

    _motorCoolantTempValue = new ValueSmoothingHelper(this);
    connect(_motorCoolantTempValue,SIGNAL(ValueChanged(float)),this,SLOT(setMotorCoolantTemp(float)));
    _motorOilPressureValue = new ValueSmoothingHelper(this);
    connect(_motorOilPressureValue,SIGNAL(ValueChanged(float)),this,SLOT(setMotorOilPressure(float)));

    _hydraulicOilPressureValue = new ValueSmoothingHelper(ui->_hydraulicOilPressureGauge);
    connect(_hydraulicOilPressureValue,SIGNAL(ValueChanged(float)),ui->_hydraulicOilPressureGauge,SLOT(setCurrentValue(float)));
    _hydraulicOilTempValue = new ValueSmoothingHelper(ui->_hydraulicOilTempGauge);
    connect(_hydraulicOilTempValue,SIGNAL(ValueChanged(float)),this, SLOT(setHydraulicOilTemp(float)));

    _airFlowTempValue = new ValueSmoothingHelper(ui->_airFlowTempGauge);
    connect(_airFlowTempValue,SIGNAL(ValueChanged(float)),ui->_airFlowTempGauge,SLOT(setCurrentValue(float)));
    connect(_airFlowTempValue,SIGNAL(ValueChanged(float)),this,SLOT(setAirFlowTemp(float)));

    _lossesBeforeValue = new ValueSmoothingHelper(ui->_lossesBeforeGauge);
    connect(_lossesBeforeValue,SIGNAL(ValueChanged(float)),ui->_lossesBeforeGauge,SLOT(setCurrentValue(float)));
    _lossesAfterValue = new ValueSmoothingHelper(ui->_lossesAfterGauge);
    connect(_lossesAfterValue,SIGNAL(ValueChanged(float)),ui->_lossesAfterGauge,SLOT(setCurrentValue(float)));

    ApplyNightDay(SettingsManager::settings()->currentNightDayMode());
    QObject::connect(SettingsManager::settings(),SIGNAL(NightDayChanged(EnumNightDayMode)), this, SLOT(ApplyNightDay(EnumNightDayMode)));

    Initialize_CAN();
}

void GaugesWidget::Initialize_CAN()
{
    //can signals
    auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
    if(canworker!=nullptr)
    {
        connect(canworker,SIGNAL(received(CanMessage)),this,SLOT(OnCanMessage(CanMessage)));
        connect(canworker,SIGNAL(connectionLost()),this,SLOT(OnCanConnectionLost()));
        connect(canworker,SIGNAL(connectionRestored()),this,SLOT(OnCanConnectionRestore()));
        OnCanConnectionRestore();
    }

    _doubleClickTimer.setInterval(doubleClickMaxInterval);
    _doubleClickTimer.setSingleShot(true);
}

GaugesWidget::~GaugesWidget()
{
    delete ui;
}

void GaugesWidget::setWorkingMode(const VehicleMode& workingMode)
{
    _workingMode = workingMode;
    if(workingMode==VehicleMode::mode_road)
    {
        ui->_bottomWidget->setCurrentIndex(0);
        ui->MotorCoolantTempIcon->setVisible(false);
        ui->MotorCoolantTempValue->setVisible(false);
        ui->MotorOilPressureIcon->setVisible(false);
        ui->MotorOilPressureValue->setVisible(false);

        ui->_hydraulicOilTempGauge->ClearBands();
        ui->_hydraulicOilTempGauge->setDefColor(Qt::green);
        ui->_hydraulicOilTempGauge->AddBand(65,Qt::red);

        ui->_airFlowTempGauge->ClearBands();
        ui->_airFlowTempGauge->setDefColor(Qt::green);
        ui->_airFlowTempGauge->AddBand(65,Qt::red);

        ui->HSpacerTop->changeSize(ui->HSpacerTop->sizeHint().width(),ui->HSpacerTop->sizeHint().height(),QSizePolicy::Expanding);
        ui->HSpacerTop->invalidate();

        updateControlVisibility();
    }
    else if(workingMode==VehicleMode::mode_field)
    {
        ui->_bottomWidget->setCurrentIndex(1);
        ui->MotorCoolantTempIcon->setVisible(true);
        ui->MotorCoolantTempValue->setVisible(true);
        ui->MotorOilPressureIcon->setVisible(true);
        ui->MotorOilPressureValue->setVisible(true);

        ui->_hydraulicOilTempGauge->ClearBands();
        ui->_hydraulicOilTempGauge->setDefColor(Qt::black);
        ui->_hydraulicOilTempGauge->AddBand(65,Qt::red);

        ui->_airFlowTempGauge->ClearBands();
        ui->_airFlowTempGauge->setDefColor(Qt::green);
        ui->_airFlowTempGauge->AddBand(65,Qt::red);

        ui->HSpacerTop->changeSize(ui->HSpacerTop->sizeHint().width(),ui->HSpacerTop->sizeHint().height(),QSizePolicy::Preferred);
        ui->HSpacerTop->invalidate();

        updateControlVisibility();
    }
}

void GaugesWidget::ApplyNightDay(EnumNightDayMode mode)
{
    QColor foreColor = (mode==EnumNightDayMode::Day)?Qt::black:Qt::white;

    ui->_lossesBeforeGauge->setGaugeColor(foreColor);
    ui->_lossesAfterGauge->setGaugeColor(foreColor);
    ui->_hydraulicOilPressureGauge->setGaugeColor(foreColor);

    ui->_hydraulicOilTempGauge->setGaugeColor(foreColor);
    ui->_airFlowTempGauge->setGaugeColor(foreColor);
    ui->_motorCoolantTempGauge->setGaugeColor(foreColor);
    ui->_motorOilPressureGauge->setGaugeColor(foreColor);

    ui->_speedGauge->setGaugeColor(foreColor);
    ui->_engineGauge->setGaugeColor(foreColor);
    ui->_fanGauge->setGaugeColor(foreColor);
    ui->_drumGauge->setGaugeColor(foreColor);
    ui->_fuelGauge->setGaugeColor(foreColor);
    ui->_powerGauge->setGaugeColor(foreColor);
}

void GaugesWidget::setMotorOilPressure(float value)
{
    auto fractionalDigits = (value==0)?0:2;

    ui->_motorOilPressureGauge->setCurrentValue(value, fractionalDigits);
    ui->MotorOilPressureValue->setText(QString::number(value,'f',fractionalDigits)+QString::fromUtf8(" МПа"));
}

void GaugesWidget::setMotorCoolantTemp(float value)
{
    ui->_motorCoolantTempGauge->setCurrentValue(value);
    ui->MotorCoolantTempValue->setText(QString::number(value,'f',0)+QString::fromUtf8(" °C"));
}

void GaugesWidget::setAirFlowTemp(float value)
{
    ui->_airFlowTempGauge->setCurrentValue(value);
    ui->AirFlowValue->setText(QString::number(value,'f',0)+QString::fromUtf8(" °C"));
}

void GaugesWidget::setHydraulicOilTemp(float value)
{
    ui->_hydraulicOilTempGauge->setCurrentValue(value);
    ui->HydraulicOilTempValue->setText(QString::number(value,'f',0)+QString::fromUtf8(" °C"));
}

void GaugesWidget::setHydraulicOilControlStatus()
{
    auto HydraulicOilPressureOnControl = SensorManager::instance().GetSensor("oilpressure_hydraulic_actcyl")->IsUnderControl();
    ui->_hydraulicOilPressureGauge->setEnabled(HydraulicOilPressureOnControl);
}

void GaugesWidget::mousePressEvent(QMouseEvent *event)
{   
    if (_doubleClickTimer.isActive())
    {
        mouseDoubleClickEvent(event);
        mousePressX = -1;
        mousePressY = 1;
    } else
    {
        mousePressX = event->x();
        mousePressY = event->y();

        _doubleClickTimer.setSingleShot(true);
        _doubleClickTimer.start();
    }
}

void GaugesWidget::mouseReleaseEvent(QMouseEvent *event)
{
    if(mousePressX < 0 || mousePressY < 1) {
        return;
    }

    if(event->button() == Qt::LeftButton && event->x() < 100 && mousePressX < 100)
    {
        emit LeftSideButtonClicked();
        qDebug()<< "clicked left.";
    }
    if(event->button() == Qt::LeftButton && event->x() > this->width()-100 && mousePressX > this->width()-100)
    {
        emit RightSideButtonClicked();
        qDebug()<< "clicked right.";
    }

     int halfX = (this->width())/2;
     int dY = 300;

     if(qAbs(event->y() - mousePressY)<dY)
     {
         if((event->button() == Qt::LeftButton)&&(mousePressX <= halfX)&&(event->x() > halfX))
         {
             RightSideButtonClicked();
             qDebug()<< "swipe right." << "X_before: "<<mousePressX<<" X_after"<<event->x();
         }

         else if((event->button() == Qt::LeftButton)&&(mousePressX >= halfX)&&(event->x() < halfX))
         {
             LeftSideButtonClicked();
             qDebug()<< "swipe left.";
         }
     }
}

void GaugesWidget::mouseDoubleClickEvent(QMouseEvent *event)
{
   if(event->button() == Qt::LeftButton)
   {
       QPoint pos = ui->_lossesAfterGauge->mapFromGlobal(mapToGlobal(event->pos()));
       QRect rect = ui->_lossesAfterGauge->rect();

       QPoint pos1 = ui->_lossesBeforeGauge->mapFromGlobal(mapToGlobal(event->pos()));
       QRect rect1 = ui->_lossesBeforeGauge->rect();

//       rect.moveTopLeft(pos);
//       qDebug()<<rect;
//       qDebug()<<event->pos();
//       qDebug()<<pos;
        if(rect.contains(pos) || rect1.contains(pos1))
        {
           qDebug()<<"contains";
           qDebug()<< "rect" << rect;
           qDebug()<< "pos" << pos;
           qDebug()<< "pos1" << pos1;
           emit lossesGaugeWidgetsDoubleClicked();
        }
    }
}

/*&&  event->x() > ui->_lossesAfterGauge->x()
&&  event->y() < ui->_lossesAfterGauge->y()
&&  event->x() < (ui->_lossesAfterGauge->x() + ui->_lossesAfterGauge->width())
&&  event->y() > (ui->_lossesAfterGauge->y() + ui->_lossesAfterGauge->height())
&&  event->x() > ui->_lossesBeforeGauge->x()
&&  event->y() < ui->_lossesBeforeGauge->y()
&&  event->x() < (ui->_lossesBeforeGauge->x() + ui->_lossesBeforeGauge->width())
&&  event->y() > (ui->_lossesBeforeGauge->y() + ui->_lossesBeforeGauge->height())*/

void GaugesWidget::OnCanMessage(CanMessage msg)
{
    switch(msg.can_id)
    {
    case  CanPacketTahoResponse::packet_id:
        {
            auto packet = CanPacketTahoResponse::Parse(msg.data);
            switch(packet.type)
            {
            case CanPacketTahoType::taho_loss_left:
                _lossesBeforeValue->setValue(packet.rpm);
                break;
            case CanPacketTahoType::taho_loss_right:
                _lossesAfterValue->setValue(packet.rpm);
                break;
            case CanPacketTahoType::taho_speed_current:
                _speedValue->setValue(packet.rpm/10.0f);
                break;
            case CanPacketTahoType::taho_speed_engine:
                engine_rpm = packet.rpm;
                _engineValue->setValue(packet.rpm);
                break;
            case CanPacketTahoType::taho_speed_fan:
            {
                _fanValue->setValue(packet.rpm);
                float required1 = engine_rpm * packet.calibrcoeff / 1000.0f;
                ui->_fanGauge->setBands(0,0.9f*required1,1.0f*required1);
                break;
            }
            case CanPacketTahoType::taho_speed_threshing:
            {
                _drumValue->setValue(packet.rpm);
                float required2 = engine_rpm * packet.calibrcoeff / 1000.f;
                ui->_drumGauge->setBands(0,0.9f*required2,1.0f*required2);
                break;
            }
            case CanPacketTahoType::taho_unknown_load:
                _powerValue->setValue(packet.rpm);
                break;
            default:
                break;
            }
            break;
        }
    case CanPacketAnalogResponse::packet_id:
        {
            auto packet = CanPacketAnalogResponse::Parse(msg.data);
            switch(packet.type)
            {
            case CanPacketAnalogType::can_analog_type_fuel_level:
                ui->_fuelGauge->setCurrentValue(packet.state);
                break;
            case CanPacketAnalogType::can_analog_type_hydraulic_oiltemp:
                _hydraulicOilTempValue->setValue(packet.state);
                break;
            case CanPacketAnalogType::can_analog_type_hydraulicpressure:
                _hydraulicOilPressureValue->setValue(packet.state/10.0f);
                break;
            case CanPacketAnalogType::can_analog_type_oilpressure:
                _motorOilPressureValue->setValue(packet.state/100.0f);
                break;
            case CanPacketAnalogType::can_analog_type_engine_temp:
                _motorCoolantTempValue->setValue(packet.state);
                break;
            default:
                break;
            }
            break;
        }
    case CanPacketSettingsGetResponse::packet_id:
        {
            auto packet = CanPacketSettingsGetResponse::Parse(msg.data);
            if(packet.type== CanPacketSettingsType::settings_test_poteri)
            {
                if(packet.value == 0)
                {
                    ui->_lossesAfterGauge->setST("s");
                    ui->_lossesBeforeGauge->setST("s");
                }
                else
                {
                    ui->_lossesAfterGauge->setST("t");
                    ui->_lossesBeforeGauge->setST("t");
                }
            }
            else if(packet.type == CanPacketSettingsType::settings_combine_model_3)
            {
                CombineKeeper::GetInstance().setCurrentCombine(packet.value);

                updateControlVisibility();
            }
            break;
        }
    case CanPacketContactStateResponse::packet_id:
        OnCanMessage_ContactState(msg);
        break;
    case CanPackets::Request3::response_id:
        OnCanMessage_Request3(msg);
        break;
    case CanPackets::AirFlowTemp::response_id:
        OnCanMessage_AirFlowTemp(msg);
        break;
    default:
        break;
    }
    setHydraulicOilControlStatus();
}

void GaugesWidget::OnCanMessage_ContactState(CanMessage msg)
{
    CanPacketContactStateData packet = CanPacketContactStateResponse::Parse(msg.data);
    ui->_fuelGauge->setBlinkingState(ByteConversion::GetBitState(packet.raw_data,10));
}

void GaugesWidget::OnCanMessage_Request3(CanMessage msg)
{
    CanPackets::Request3Data packet = CanPackets::Request3::ParseResponse(msg.data);
    ui->_engineGauge->setBlinkingState(ByteConversion::GetBitState(packet.raw_data,3));
    ui->_lossesBeforeGauge->setBlinkedState(ByteConversion::GetBitState(packet.raw_data,0));
    ui->_lossesAfterGauge->setBlinkedState(ByteConversion::GetBitState(packet.raw_data,1));
    ui->_fanGauge->setBlinkingState(ByteConversion::GetBitState(packet.raw_data,10));
    ui->_drumGauge->setBlinkingState(ByteConversion::GetBitState(packet.raw_data,11));
}

void GaugesWidget::OnCanMessage_AirFlowTemp(CanMessage msg)
{
    auto packet = CanPackets::AirFlowTemp::ParseResponse(msg.data);
    float v = packet.air_flow_temp;
    _airFlowTempValue->setValue(v);

    QString color;
    if(v<65)
    {
        color = "black";
        ui->AirFlowIcon->setPixmap(Helper::LoadPixmap("airflowtemp.png",Helper::defIconSize(),Helper::gaugeIconColor()));
    }
    else
    {
        color = "red";
        ui->AirFlowIcon->setPixmap(Helper::LoadPixmap("airflowtemp.png",Helper::defIconSize(),Qt::red));
    }

    ui->AirFlowValue->setStyleSheet(QString("QLabel {color: " + color +"; font-size: %1pt }").arg(fontSize));
}

void GaugesWidget::updateControlVisibility()
{
    if(_workingMode==VehicleMode::mode_road) {
        ui->AirFlowIcon->setVisible(false);
        ui->AirFlowValue->setVisible(false);
        ui->HydraulicOilTempIcon->setVisible(false);
        ui->HydraulicOilTempValue->setVisible(false);
    }

    auto _combine = CombineKeeper::GetInstance().getCurrentCombine();
    auto tempScale = TempScaleToDisplay::scale_hydraulicOil;

    if(_combine == nullptr)
        return;

    tempScale = _combine->getTempScale();
    if(tempScale == TempScaleToDisplay::scale_hydraulicOil)
    {
        ui->_hydraulicOilTempGauge->setVisible(true);
        ui->_airFlowTempGauge->setVisible(false);

        if(_workingMode==VehicleMode::mode_field) {
            ui->HydraulicOilTempIcon->setVisible(true);
            ui->HydraulicOilTempValue->setVisible(true);
            ui->AirFlowIcon->setVisible(false);
            ui->AirFlowValue->setVisible(false);
        }
    }
    else if (tempScale == TempScaleToDisplay::scale_airFlow)
    {
        ui->_hydraulicOilTempGauge->setVisible(false);
        ui->_airFlowTempGauge->setVisible(true);

        if(_workingMode==VehicleMode::mode_field) {
            ui->HydraulicOilTempIcon->setVisible(false);
            ui->HydraulicOilTempValue->setVisible(false);
            ui->AirFlowIcon->setVisible(true);
            ui->AirFlowValue->setVisible(true);
        }
    }
    else
    {
        //cannot happen
    }

    if(_combine->getModel() == CombineModel::combine_gs4118)
    {
        ui->_motorOilPressureGauge->ClearBands();
        // ? ui->_motorOilPressureGauge->setDefColor(Qt::red);
        ui->_motorOilPressureGauge->AddBand(0.12,Qt::yellow);
        ui->_motorOilPressureGauge->AddBand(0.5,Qt::green);
    }
    else {
        ui->_motorOilPressureGauge->ClearBands();
        // ? ui->_motorOilPressureGauge->setDefColor(Qt::red);
        ui->_motorOilPressureGauge->AddBand(0.05,Qt::yellow);
        ui->_motorOilPressureGauge->AddBand(0.5,Qt::green);
    }
}

void GaugesWidget::OnCanConnectionLost()
{
    _drumValue->setValue(0);
    _engineValue->setValue(0);
    _fanValue->setValue(0);
    ui->_fuelGauge->setCurrentValue(0);
    _hydraulicOilPressureValue->setValue(0);
    _hydraulicOilTempValue->setValue(0);
    _lossesAfterValue->setValue(0);
    _lossesBeforeValue->setValue(0);
    _motorCoolantTempValue->setValue(0);
    _motorOilPressureValue->setValue(0);
    _powerValue->setValue(0);
    _speedValue->setValue(0);
    _airFlowTempValue->setValue(0);
}

void GaugesWidget::OnCanConnectionRestore()
{
   CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_combine_model_3, false);
   CanPackets::AirFlowTemp::SendRequest(0);
   setHydraulicOilControlStatus();
}
