#include "SensorType1Screen.h"
#include "ui_SensorType1Screen.h"
#include "widgets/EventFilters/FocusEventFilter.h"
#include "peripherials/PeripherialsManager.h"
#include "peripherials/can/packets/ContactStateSettings.h"
#include "peripherials/can/packets/ContactStateTime.h"
#include "widgets/WindowManager.h"

SensorType1Screen::SensorType1Screen(ISensor* sensor, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SensorType1Screen)
{
    _sensor = sensor;
    setAttribute(Qt::WA_DeleteOnClose);
    setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    ui->setupUi(this);

    setTabOrder(ui->akc_level , ui->akc_updn);
    setTabOrder(ui->akc_updn  , ui->akc_status);
    setTabOrder(ui->akc_status, ui->akc_t200ms);
    setTabOrder(ui->akc_t200ms, ui->akc_ifchan);
    setTabOrder(ui->akc_ifchan, ui->akc_ifdata);
    setTabOrder(ui->akc_ifdata, ui->Btn1Close);

    ui->akc_updn->init(8,2);
    ui->akc_status->init(8,2);

    auto* eventFilter = new FocusEventFilter(this);
    ui->Btn1Close->installEventFilter(eventFilter);

    ui->title->setStyleSheet("QLabel {font: 34pt;}");
    ui->title->setText(sensor->GetName("ru"));

    for(auto* item : this->findChildren<ArrowKeyControl*>())
    {
        item->setFontSize(24);
    }

    ui->Btn1Close->setFocus();

    auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
    if(canworker!=nullptr)
    {
        connect(canworker,SIGNAL(received(CanMessage)),this,SLOT(OnCanMessage(CanMessage)));
        connect(canworker,SIGNAL(connectionLost()),this,SLOT(OnCanConnectionLost()));
        connect(canworker,SIGNAL(connectionRestored()),this,SLOT(OnCanConnectionRestore()));
        OnCanConnectionRestore();

        timer.setInterval(500);
        QObject::connect(&timer,SIGNAL(timeout()),this,SLOT(OnCanConnectionRestore()));
        timer.start();
    }
}

SensorType1Screen::~SensorType1Screen()
{
    delete ui;
}

void SensorType1Screen::keyPressEvent(QKeyEvent* e)
{
    switch(e->key())
    {
    case Qt::Key_Escape:
        WindowManager::getInstance()->CloseMe(this);
        break;
    }
}

void SensorType1Screen::on_Btn1Close_clicked()
{
    WindowManager::getInstance()->CloseMe(this);
}

void SensorType1Screen::OnCanMessage(CanMessage msg)
{
    if(msg.can_id == CanPackets::ContactStateSettingsGet::response_id)
    {
        auto packet = CanPackets::ContactStateSettingsGet::ParseResponse(msg.data);
        if(packet.contactStateId != (_sensor->GetChannelId() | _sensor->GetType()))
            return;

        ui->ADC->setText(QString::number(packet.adc_value));
        ui->ON_off->setText(QString::number(packet.current_state,2).rightJustified(8,'0'));

        if(!ui->akc_level->isBeingEdited())
            ui->akc_level->setNumber(packet.adc_threshold);

        if(!ui->akc_updn->isBeingEdited())
            ui->akc_updn->setNumber(packet.type);

        if(!ui->akc_status->isBeingEdited())
            ui->akc_status->setNumber(packet.on_control);
    }
    else if(msg.can_id == CanPackets::ContactStateTimeGet::response_id)
    {
        auto packet = CanPackets::ContactStateTimeGet::ParseResponse(msg.data);
        if(packet.contactStateId != (_sensor->GetChannelId() | _sensor->GetType()))
            return;

        ui->TIME_ON->setText(QString::number(packet.timeCounter));

        if(!ui->akc_t200ms->isBeingEdited())
            ui->akc_t200ms->setNumber(packet.timeToEvent);

        if(!ui->akc_ifchan->isBeingEdited())
            ui->akc_ifchan->setNumber(packet.dependency);

        if(!ui->akc_ifdata->isBeingEdited())
            ui->akc_ifdata->setNumber(packet.dependency_excess);
    }
}

void SensorType1Screen::OnCanConnectionLost()
{

}

void SensorType1Screen::OnCanConnectionRestore()
{
    CanPackets::ContactStateSettingsGet::SendRequest( _sensor->GetChannelId() | _sensor->GetType());
    CanPackets::ContactStateTimeGet::SendRequest(_sensor->GetChannelId() | _sensor->GetType());
}


void SensorType1Screen::SendGeneralSettings()
{
    CanPackets::ContactStateSettingsData data{};
    data.contactStateId =  _sensor->GetChannelId() | _sensor->GetType();
    data.adc_value = static_cast<uint16_t>(ui->ADC->text().toInt());
    data.adc_threshold = static_cast<uint16_t>(ui->akc_level->number());
    data.current_state = static_cast<uint8_t>(ui->ON_off->text().toInt(nullptr, 2));
    data.type = static_cast<uint8_t>(ui->akc_updn->number());
    data.on_control = static_cast<uint8_t>(ui->akc_status->number());
    CanPackets::ContactStateSettingsSet::SendRequest(data);
}

void SensorType1Screen::SendTimeSettings()
{
    CanPackets::ContactStateTimeData data{};
    data.contactStateId = _sensor->GetChannelId() | _sensor->GetType();
    data.timeToEvent = static_cast<uint16_t>(ui->akc_t200ms->number());
    data.timeCounter = static_cast<uint16_t>(ui->TIME_ON->text().toInt());
    data.dependency = static_cast<uint8_t>(ui->akc_ifchan->number());
    data.dependency_excess = static_cast<uint16_t>(ui->akc_ifdata->number());
    CanPackets::ContactStateTimeSet::SendRequest(data);
}

void SensorType1Screen::on_akc_level_returnPressed()
{
    SendGeneralSettings();
}

void SensorType1Screen::on_akc_updn_returnPressed()
{
    SendGeneralSettings();
}

void SensorType1Screen::on_akc_status_returnPressed()
{
    SendGeneralSettings();
}

void SensorType1Screen::on_akc_t200ms_returnPressed()
{
    SendTimeSettings();
}

void SensorType1Screen::on_akc_ifchan_returnPressed()
{
    SendTimeSettings();
}

void SensorType1Screen::on_akc_ifdata_returnPressed()
{
    SendTimeSettings();
}
