#ifndef FLOWWINDOW_H
#define FLOWWINDOW_H

#include <QWidget>

namespace Ui {
class FlowWindow;
}

class FlowWindow : public QWidget
{
    Q_OBJECT

public:
    explicit FlowWindow(QWidget *parent = 0);
    ~FlowWindow();
private slots:
    void on_b_close_clicked();
private:
    Ui::FlowWindow *ui;
};

#endif // FLOWWINDOW_H
