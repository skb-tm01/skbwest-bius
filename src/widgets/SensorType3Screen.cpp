#include "SensorType3Screen.h"
#include "ui_SensorType3Screen.h"
#include "widgets/EventFilters/FocusEventFilter.h"
#include "peripherials/PeripherialsManager.h"
#include "peripherials/can/packets/ContactStateSettings.h"
#include "peripherials/can/packets/ContactStateTime.h"
#include "peripherials/can/packets/RpmSettings1.h"
#include "peripherials/can/packets/RpmSettings2.h"
#include "peripherials/can/packets/RpmSettings3.h"
#include "widgets/WindowManager.h"

#define DIG 0
const uint8_t taho_to_contact_chanel[] = {
 35|DIG, 32|DIG, 33|DIG, 34|DIG, 37|DIG, 39|DIG, 38|DIG, 36|DIG,
 40|DIG, 41|DIG, 42|DIG, 43|DIG, 44|DIG, 45|DIG, 46|DIG, 47|DIG
};

SensorType3Screen::SensorType3Screen(ISensor* sensor, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SensorType3Screen)
{
    _sensor = sensor;
    setAttribute(Qt::WA_DeleteOnClose);
    setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    ui->setupUi(this);

    setTabOrder(ui->akc_points   , ui->akc_ca_rpm);
    setTabOrder(ui->akc_ca_rpm   , ui->akc_percent);
    setTabOrder(ui->akc_percent  , ui->akc_status);
    setTabOrder(ui->akc_status   , ui->akc_coeff);
    setTabOrder(ui->akc_coeff    , ui->akc_lorpm);
    setTabOrder(ui->akc_lorpm    , ui->akc_tms100);
    setTabOrder(ui->akc_tms100   , ui->akc_level);
    setTabOrder(ui->akc_level    , ui->akc_updn);
    setTabOrder(ui->akc_level    , ui->akc_updn);
    setTabOrder(ui->akc_updn     , ui->akc_status_2);
    setTabOrder(ui->akc_status_2 , ui->akc_t200ms);
    setTabOrder(ui->akc_t200ms   , ui->akc_ifchan);
    setTabOrder(ui->akc_ifchan   , ui->akc_ifdata);
    setTabOrder(ui->akc_ifdata   , ui->Btn1Close);

    ui->akc_updn->init(8,2);
    ui->akc_status_2->init(8,2);
    ui->akc_status->init(8,2);

    auto* eventFilter = new FocusEventFilter(this);
    ui->Btn1Close->installEventFilter(eventFilter);

    for(auto* item : this->findChildren<ArrowKeyControl*>())
    {
        item->setFontSize(24);
    }

    ui->title->setStyleSheet("QLabel {font: 34pt;}");

    ui->title->setText(sensor->GetName("ru"));


    ui->Btn1Close->setFocus();

    auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
    if(canworker!=nullptr)
    {
        connect(canworker,SIGNAL(received(CanMessage)),this,SLOT(OnCanMessage(CanMessage)));
        connect(canworker,SIGNAL(connectionLost()),this,SLOT(OnCanConnectionLost()));
        connect(canworker,SIGNAL(connectionRestored()),this,SLOT(OnCanConnectionRestore()));
        OnCanConnectionRestore();

        timer.setInterval(500);
        QObject::connect(&timer,SIGNAL(timeout()),this,SLOT(OnCanConnectionRestore()));
        timer.start();
    }
}

SensorType3Screen::~SensorType3Screen()
{
    delete ui;
}

void SensorType3Screen::keyPressEvent(QKeyEvent* e)
{
    switch(e->key())
    {
        case Qt::Key_Escape:
            WindowManager::getInstance()->CloseMe(this);
            break;
    }
}

void SensorType3Screen::on_Btn1Close_clicked()
{
    WindowManager::getInstance()->CloseMe(this);
}

void SensorType3Screen::OnCanMessage(CanMessage msg)
{
    if(msg.can_id == CanPackets::ContactStateSettingsGet::response_id)
    {
        auto packet = CanPackets::ContactStateSettingsGet::ParseResponse(msg.data);
        if(packet.contactStateId != taho_to_contact_chanel[_sensor->GetChannelId() & 0x0F])
            return;

        ui->ADC->setText(QString::number(packet.adc_value));
        ui->label_onoff->setText(QString::number(packet.current_state,2).rightJustified(8,'0'));

        if(!ui->akc_level->isBeingEdited())
            ui->akc_level->setNumber(packet.adc_threshold);


        if(!ui->akc_updn->isBeingEdited())
            ui->akc_updn->setNumber(packet.type);

        if(!ui->akc_status_2->isBeingEdited())
            ui->akc_status_2->setNumber(packet.on_control);
    }
    else if(msg.can_id == CanPackets::ContactStateTimeGet::response_id)
    {
        auto packet = CanPackets::ContactStateTimeGet::ParseResponse(msg.data);
        if(packet.contactStateId != taho_to_contact_chanel[_sensor->GetChannelId() & 0x0F])
            return;

        ui->TIME_ON->setText(QString::number(packet.timeCounter));

        if(!ui->akc_t200ms->isBeingEdited())
            ui->akc_t200ms->setNumber(packet.timeToEvent);

        if(!ui->akc_ifchan->isBeingEdited())
            ui->akc_ifchan->setNumber(packet.dependency);

        if(!ui->akc_ifdata->isBeingEdited())
            ui->akc_ifdata->setNumber(packet.dependency_excess);
    }
    else if(msg.can_id == CanPackets::RpmSettings1Get::response_id)
    {
        auto packet = CanPackets::RpmSettings1Get::ParseResponse(msg.data);
        if(packet.contactStateId != (_sensor->GetChannelId() | _sensor->GetType()))
            return;

        if(!ui->akc_points->isBeingEdited())
            ui->akc_points->setNumber(packet.points);

        ui->label_dpulse->setText(QString::number(packet.d_pulse));
        ui->label_dhz->setText(QString::number(packet.d_hz));
        ui->label_drpm->setText(QString::number(packet.d_rpm));
    }
    else if(msg.can_id == CanPackets::RpmSettings2Get::response_id)
    {
        auto packet = CanPackets::RpmSettings2Get::ParseResponse(msg.data);
        if(packet.contactStateId != (_sensor->GetChannelId() | _sensor->GetType()))
            return;

        ui->label_1s_hz->setText(QString::number(packet.s1_hz));
        ui->label_ca_k->setText(QString::number(packet.ca_k));

        if(!ui->akc_ca_rpm->isBeingEdited())
            ui->akc_ca_rpm->setNumber(packet.ca_rpm);

        if(!ui->akc_percent->isBeingEdited())
            ui->akc_percent->setNumber(packet.percent);
    }
    else if(msg.can_id == CanPackets::RpmSettings3Get::response_id)
    {
        auto packet = CanPackets::RpmSettings3Get::ParseResponse(msg.data);
        if(packet.contactStateId != (_sensor->GetChannelId() | _sensor->GetType()))
            return;

        ui->label_proskal->setText(QString::number(packet.proskal));

        if(!ui->akc_status->isBeingEdited())
            ui->akc_status->setNumber(packet.status);

        if(!ui->akc_coeff->isBeingEdited())
            ui->akc_coeff->setNumber(packet.coeff);

        if(!ui->akc_tms100->isBeingEdited())
            ui->akc_tms100->setNumber(packet.tms100);

        if(!ui->akc_lorpm->isBeingEdited())
            ui->akc_lorpm->setNumber(packet.lo_rpm);
    }
}

void SensorType3Screen::OnCanConnectionLost()
{

}

void SensorType3Screen::OnCanConnectionRestore()
{
    CanPackets::ContactStateSettingsGet::SendRequest( taho_to_contact_chanel[_sensor->GetChannelId() & 0x0F]);
    CanPackets::ContactStateTimeGet::SendRequest(taho_to_contact_chanel[_sensor->GetChannelId() & 0x0F]);
    CanPackets::RpmSettings1Get::SendRequest(_sensor->GetChannelId() | _sensor->GetType());
    CanPackets::RpmSettings2Get::SendRequest(_sensor->GetChannelId() | _sensor->GetType());
    CanPackets::RpmSettings3Get::SendRequest(_sensor->GetChannelId() | _sensor->GetType());
}

void SensorType3Screen::SendGeneralSettings()
{
    CanPackets::ContactStateSettingsData data{};
    data.contactStateId = taho_to_contact_chanel[_sensor->GetChannelId() & 0x0F];
    data.adc_value = static_cast<uint16_t>(ui->ADC->text().toInt());
    data.adc_threshold = static_cast<uint16_t>(ui->akc_level->number());
    data.current_state = static_cast<uint8_t>(ui->label_onoff->text().toInt(nullptr, 2));
    data.type = static_cast<uint8_t>(ui->akc_updn->number());
    data.on_control = static_cast<uint8_t>(ui->akc_status_2->number());
    CanPackets::ContactStateSettingsSet::SendRequest(data);
}

void SensorType3Screen::SendTimeSettings()
{
    CanPackets::ContactStateTimeData data{};
    data.contactStateId = taho_to_contact_chanel[_sensor->GetChannelId() & 0x0F];
    data.timeToEvent = static_cast<uint16_t>(ui->akc_t200ms->number());
    data.timeCounter = static_cast<uint16_t>(ui->TIME_ON->text().toInt());
    data.dependency = static_cast<uint8_t>(ui->akc_ifchan->number());
    data.dependency_excess = static_cast<uint8_t>(ui->akc_ifdata->number());
    CanPackets::ContactStateTimeSet::SendRequest(data);
}

void SensorType3Screen::SendRpmSettings1()
{
    CanPackets::RpmSettings1Data data{};
    data.contactStateId= _sensor->GetChannelId() | _sensor->GetType();
    data.points = static_cast<uint8_t>(ui->akc_points->number());
    data.d_pulse = static_cast<uint16_t>(ui->label_dpulse->text().toInt());
    data.d_hz = static_cast<uint16_t>(ui->label_dhz->text().toInt());
    data.d_rpm = static_cast<uint16_t>(ui->label_drpm->text().toInt());
    CanPackets::RpmSettings1Set::SendRequest(data);
}

void SensorType3Screen::SendRpmSettings2()
{
    CanPackets::RpmSettings2Data data{};
    data.contactStateId= _sensor->GetChannelId() | _sensor->GetType();
    data.s1_hz = static_cast<uint16_t>(ui->label_1s_hz->text().toInt());
    data.ca_rpm = static_cast<uint16_t>(ui->akc_ca_rpm->number());
    data.ca_k = static_cast<uint16_t>(ui->label_ca_k->text().toInt());
    data.percent = static_cast<uint8_t>(ui->akc_percent->number());
    CanPackets::RpmSettings2Set::SendRequest(data);
}

void SensorType3Screen::SendRpmSettings3()
{
    CanPackets::RpmSettings3Data data{};
    data.contactStateId= _sensor->GetChannelId() | _sensor->GetType();
    data.proskal = static_cast<uint8_t>(ui->label_proskal->text().toInt());
    data.status = static_cast<uint8_t>(ui->akc_status->number());
    data.coeff = static_cast<uint16_t>(ui->akc_coeff->number());
    data.tms100 = static_cast<uint8_t>(ui->akc_tms100->number());
    data.lo_rpm = static_cast<uint16_t>(ui->akc_lorpm->number());
    CanPackets::RpmSettings3Set::SendRequest(data);
}

void SensorType3Screen::on_akc_level_returnPressed()
{
    SendGeneralSettings();
}

void SensorType3Screen::on_akc_updn_returnPressed()
{
    SendGeneralSettings();
}

void SensorType3Screen::on_akc_t200ms_returnPressed()
{
    SendTimeSettings();
}

void SensorType3Screen::on_akc_ifchan_returnPressed()
{
    SendTimeSettings();
}

void SensorType3Screen::on_akc_ifdata_returnPressed()
{
    SendTimeSettings();
}

void SensorType3Screen::on_akc_points_returnPressed()
{
    SendRpmSettings1();
}

void SensorType3Screen::on_akc_ca_rpm_returnPressed()
{
    SendRpmSettings2();
}

void SensorType3Screen::on_akc_percent_returnPressed()
{
    SendRpmSettings2();
}

void SensorType3Screen::on_akc_status_returnPressed()
{
    SendRpmSettings3();
}

void SensorType3Screen::on_akc_status_2_returnPressed()
{
    SendGeneralSettings();
}

void SensorType3Screen::on_akc_coeff_returnPressed()
{
    SendRpmSettings3();
}

void SensorType3Screen::on_akc_lorpm_returnPressed()
{
    SendRpmSettings3();
}

void SensorType3Screen::on_akc_tms100_returnPressed()
{
    SendRpmSettings3();
}
