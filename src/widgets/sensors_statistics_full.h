#pragma once

#include <QKeyEvent>
#include <QWidget>
#include <QTimer>

#include "peripherials/can/CanWorker.h"
#include "widgets/SensorWidget.h"
#include "SettingsManager.h"
#include "widgets/SensorsStatisticsEnum.h"

namespace Ui {
class sensors_statistics_full;
}

class sensors_statistics_full : public QWidget
{
    Q_OBJECT

public:
    explicit sensors_statistics_full(QWidget *parent = 0, EnumSensorsStatisticsMode mode = EnumSensorsStatisticsMode::Menu);
    ~sensors_statistics_full();
    static sensors_statistics_full* Instance;

    bool ServiceMenuState();
    void SetServiceMenuState(bool state);

private slots:
    void on_ButtonClose_clicked();
    void ApplyNightDay();
    void OnSensorSelected(SensorWidget* src);
    void RequestSensorsState();
    void OpenInfoWindow();
    void OnTimer();

protected:
    bool eventFilter(QObject *obj, QEvent *event);
    void keyPressEvent(QKeyEvent* e);

    void showEvent(QShowEvent *e);

signals:
    void OnClosing();

private:

    SensorWidget* SelectedSensor();
    void SelectNearSensor(Qt::Orientation orientation,bool dir);

    SensorWidget *FirstSensor();
    SensorWidget *LastSensor();
    SensorWidget *LastSelectedSensor();
    void ShowSensorName(SensorWidget* src);
    void closeInternal();

    Ui::sensors_statistics_full *ui;
    bool _showServiceMenu;
    qint64 _menuPressTime;


    CanWorker* _canworker;

    EnumSensorsStatisticsMode _mode;

    QTimer _requestTimer;
    QTimer* _timer;
    int _time;

};

