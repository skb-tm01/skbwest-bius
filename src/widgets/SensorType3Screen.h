#pragma once

#include <QKeyEvent>
#include <QWidget>
#include <QTimer>

#include "peripherials/can/CanMessage.h"
#include "peripherials/sensors/ISensor.h"

namespace Ui {
    class SensorType3Screen;
}

class SensorType3Screen : public QWidget
{
    Q_OBJECT

public:
    explicit SensorType3Screen(ISensor* sensor, QWidget *parent = nullptr);
    ~SensorType3Screen();
protected:
    void keyPressEvent(QKeyEvent* e);
private slots:
    void on_Btn1Close_clicked();

    //can
    void OnCanMessage(CanMessage msg);
    void OnCanConnectionLost();
    void OnCanConnectionRestore();

    void SendGeneralSettings();
    void SendTimeSettings();
    void SendRpmSettings1();
    void SendRpmSettings2();
    void SendRpmSettings3();

    void on_akc_updn_returnPressed();

    void on_akc_status_returnPressed();

    void on_akc_level_returnPressed();

    void on_akc_t200ms_returnPressed();

    void on_akc_ifchan_returnPressed();

    void on_akc_ifdata_returnPressed();

    void on_akc_points_returnPressed();

    void on_akc_ca_rpm_returnPressed();

    void on_akc_percent_returnPressed();

    void on_akc_status_2_returnPressed();

    void on_akc_coeff_returnPressed();

    void on_akc_lorpm_returnPressed();

    void on_akc_tms100_returnPressed();

private:

    ISensor* _sensor;
    Ui::SensorType3Screen *ui;
    QTimer timer;
};
