#include <QTimer>

#include "core/AudioHelper.h"
#include "peripherials/sensors/ISensor.h"
#include "widgets/warningmessage.h"
#include "ui_warningmessage.h"
#include "widgets/mainwindow.h"
#include "Menu/menu_main.h"
#include "ToInstructions.h"
#include "peripherials/sensors/SensorManager.h"
#include "core/helper.h"
#include <QDebug>
#include "widgets/WindowManager.h"

WarningMessage::WarningMessage(AlertDescription* description, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WarningMessage)
{
    alert_description = description;
    setWindowFlags(Qt::FramelessWindowHint| Qt::Window);
    ui->setupUi(this);
    this->setStyleSheet("QWidget { background-color : "+description->GetColor() +"; color : black; }");
    ui->warningTextLabel->setWordWrap(true);
    ui->warningTextLabel->setText(description->GetText("ru"));

    auto imageName = description->GetImage();
    if(imageName!="")
        ui->icon->setPixmap(Helper::LoadPixmap(description->GetImage(), 150));

    AudioHelper::PlayWaveFile(description->GetSoundFile("ru"),description->GetColor()=="red" ? RingingType::Error : RingingType::Warning);

    auto* mainmenu = qobject_cast<MainWindow*>(parent);
    if(mainmenu != nullptr)
    {
        QObject::connect(this, SIGNAL(menu_requested()),mainmenu,SLOT(MenuButtonClicked()));
    }

    auto* timer = new QTimer(this);
    timer->setInterval(1000*5);
    QObject::connect(timer,SIGNAL(timeout()),this,SLOT(onTimerTimeout()));
    timer->start();

    QObject::connect(this,SIGNAL(sig_infoWindowRequest()),this,SLOT(onInfoWindowRequested()));
}

AlertDescription *WarningMessage::GetAlertDescription()
{
    return alert_description;
}

WarningMessage::~WarningMessage()
{
    delete ui;
}

void WarningMessage::on_warningTextLabel_clicked()
{
    onInfoWindowRequested();
}

void WarningMessage::onInfoWindowRequested()
{
    this->hide();
    this->setStyleSheet("");
    auto *w = new ToInstructions(this);
    w->SetData(SensorManager::instance().GetSensor(alert_description->GetId()));
    w->SetFontSize(26);
    w->move(0,80);
    w->resize(1024,608);
    WindowManager::getInstance()->Open(this,w);
}

void WarningMessage::resizeEvent(QResizeEvent *e)
{
    this->move((1024-this->rect().width())/2, 768-96-this->rect().height());
    QWidget::resizeEvent(e);
}

void WarningMessage::onTimerTimeout()
{
    emit closed();
    this->close();
}

void WarningMessage::keyPressEvent(QKeyEvent* e)
{
    switch(e->key())
    {
    case Qt::Key_Escape:
        this->close();
        return;
    case Qt::Key_Menu:
        emit menu_requested();
        this->close();
        return;
    case Qt::Key_Return:
    case Qt::Key_Enter:
        emit sig_infoWindowRequest();
        break;
    default:
        if(parent() != nullptr)
            QApplication::postEvent(parent(),new QKeyEvent(e->type(),e->key(),e->modifiers()));
        this->close();
    }
    QWidget::keyPressEvent(e);
}
