#include <QDateTime>

#include "3rdparty/flickcharm/flickcharm.h"

#include "alerts/AlertDescription.h"

#include "widgets/EventFilters/FocusEventFilter.h"
#include "widgets/Windows/ConfirmationWindow.h"
#include "widgets/SensorWidget.h"
#include "widgets/SensorType1Screen.h"
#include "widgets/SensorType2Screen.h"
#include "widgets/SensorType3Screen.h"
#include "widgets/SensorType4Screen.h"
#include "widgets/sensors_statistics_full.h"
#include "widgets/SensorStateInstructions.h"

#include "ui_SensorStateInstructions.h"
#include "widgets/WindowManager.h"

SensorStateInstructions::SensorStateInstructions(SensorWidget *parent) :
    QWidget(parent),
    ui(new Ui::SensorStateInstructions)
{
    setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    ui->setupUi(this);
    auto* eventFilter = new FocusEventFilter(this);
    ui->Btn3Close->installEventFilter(eventFilter);
    ui->Btn2Control->installEventFilter(eventFilter);
    ui->Btn1Setup->installEventFilter(eventFilter);
    instructions = new QLabel(this);
    instructions->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
    instructions->setWordWrap(true);
    SetFontSize(22);

    instructions->setAlignment(Qt::AlignTop | Qt::AlignLeft);
    ui->scrollArea->setWidget(instructions);

    sensor = parent;

    FlickCharm *flickCharm = new FlickCharm(this);
    flickCharm->activateOn(ui->scrollArea);
    //FlickCharm disables scrollbar so enables it again
    ui->scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    ui->Btn1Setup->setText(QString::fromUtf8("Настройка"));
    SetData(sensor->Sensor());

#ifdef BUILD_DEVICE
    ui->Btn1Setup->setVisible(false);
#else
    ui->Btn1Setup->setVisible(true);
#endif

    ui->Btn2Control->setVisible(sensor->Sensor()->IsSetControlEnabled());

    _longPressTimer.setInterval(2000);
    _longPressTimer.setSingleShot(true);
    QObject::connect(&_longPressTimer, SIGNAL(timeout()), this, SLOT(onLongPress()));
    timerRan = false;
    QObject::connect(dynamic_cast<QObject*>(this->sensor->Sensor()),SIGNAL(stateChanged()),this,SLOT(onSensorStateChange()));
    onSensorStateChange();
}

void SensorStateInstructions::SetData(ISensor *sensor)
{
    SetData(sensor->GetName("ru"),sensor->GetManualDescription("ru"));
}

void SensorStateInstructions::SetData(QString title, QString text)
{
    ui->label_2->setText(title);
    instructions->setText(text);
}

void SensorStateInstructions::SetFontSize(int size)
{
    instructions->setStyleSheet("QLabel{font: "+QString::number(size)+"pt;}");
}

void SensorStateInstructions::SetServiceMenuButtonVisiblity(bool visible)
{
    ui->Btn1Setup->setVisible(visible);
}

void SensorStateInstructions::keyPressEvent(QKeyEvent* e)
{
    switch(e->key())
    {
    case Qt::Key_Escape:
        WindowManager::getInstance()->CloseMe(this);
        break;
    case Qt::Key_F7:
        _longPressTimer.start();
        break;
    }
    QWidget::keyPressEvent(e);
}

void SensorStateInstructions::keyReleaseEvent(QKeyEvent *e)
{
    switch(e->key())
    {
        case Qt::Key_F7:
            _longPressTimer.stop();
            break;
    }
    QWidget::keyReleaseEvent(e);
}

SensorStateInstructions::~SensorStateInstructions()
{
    delete ui;
}

void SensorStateInstructions::on_Btn3Close_clicked()
{

}

void SensorStateInstructions::on_Btn2Control_clicked()
{
    auto* w = new ConfirmationWindow(this);
    w->SetTitle(QString::fromUtf8("Предупреждение"));
    w->SetText(sensor->Sensor()->IsUnderControl()
               ?QString::fromUtf8("Вы действительно хотите снять датчик с контроля?")
               :QString::fromUtf8("Вы действительно хотите поставить датчик на контроль?"));

    if (w->exec() == QDialog::Accepted)
    {
        //Will be enabled back in onSensorStateChange() after processing of received CAN packet
        ui->Btn2Control->setEnabled(false);

        sensor->Sensor()->SetUnderControl(!sensor->Sensor()->IsUnderControl());
    }
}

void SensorStateInstructions::on_Btn1Setup_clicked()
{
    QWidget* widget = nullptr;
    switch(sensor->Sensor()->GetType())
    {
        case DIG:
            widget = new SensorType1Screen(sensor->Sensor(),this);
            break;
        case ADS:
            widget = new SensorType2Screen(sensor->Sensor(),this);
            break;
        case RPM:
            widget = new SensorType3Screen(sensor->Sensor(),this);
            break;
        case OUT:
            widget = new SensorType4Screen(sensor->Sensor(),this);
            break;
        default:
            break;
    }

    if(widget!=nullptr)
    {
        widget->move(pos());
        widget->resize(size());
        WindowManager::getInstance()->Open(this,widget);
    }
}

void SensorStateInstructions::onLongPress()
{
    SetServiceMenuButtonVisiblity(true);
    sensors_statistics_full::Instance->SetServiceMenuState(true);
    timerRan = true;
}

void SensorStateInstructions::onSensorStateChange()
{
    if (sensor->Sensor()->IsUnderControl())
    {
        ui->Btn2Control->setText(QString::fromUtf8("Снять с контроля"));
    }
    else
    {
        ui->Btn2Control->setText(QString::fromUtf8("Установить на контроль"));
    }
    ui->Btn2Control->setEnabled(true);
}


void SensorStateInstructions::on_Btn3Close_pressed()
{
    _longPressTimer.start();
    timerRan = false;
}

void SensorStateInstructions::on_Btn3Close_released()
{
    _longPressTimer.stop();
     if(!timerRan)
     {
         timerRan = false;
         WindowManager::getInstance()->CloseMe(this);
     }
}
