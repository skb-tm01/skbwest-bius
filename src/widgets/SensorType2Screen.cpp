#include "SensorType2Screen.h"
#include "ui_SensorType2Screen.h"
#include "widgets/EventFilters/FocusEventFilter.h"
#include "peripherials/PeripherialsManager.h"

#include "peripherials/can/packets/AdcSettings1.h"
#include "peripherials/can/packets/AdcSettings2.h"
#include "peripherials/can/packets/AdcSettings3.h"
#include "peripherials/can/packets/AdcSettings4.h"

#include "widgets/WindowManager.h"

#include "widgets/WindowManager.h"

SensorType2Screen::SensorType2Screen(ISensor* sensor, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SensorType2Screen)
{
    _sensor = sensor;
    setAttribute(Qt::WA_DeleteOnClose);
    setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    ui->setupUi(this);

    setTabOrder(ui->akc_err_up   , ui->akc_err_dn);
    setTabOrder(ui->akc_err_dn   , ui->akc_on_off);
    setTabOrder(ui->akc_on_off   , ui->akc_status);
    setTabOrder(ui->akc_status   , ui->akc_sum200);
    setTabOrder(ui->akc_sum200   , ui->akc_adc_p1);

    setTabOrder(ui->akc_adc_p1, ui->akc_data_1);
    setTabOrder(ui->akc_data_1, ui->akc_adc_p2);
    setTabOrder(ui->akc_adc_p2, ui->akc_data_2);
    setTabOrder(ui->akc_data_2, ui->akc_adc_p3);
    setTabOrder(ui->akc_adc_p3, ui->akc_data_3);
    setTabOrder(ui->akc_data_3, ui->akc_adc_p4);
    setTabOrder(ui->akc_adc_p4, ui->akc_data_4);
    setTabOrder(ui->akc_data_4, ui->akc_adc_p5);
    setTabOrder(ui->akc_adc_p5, ui->akc_data_5);
    setTabOrder(ui->akc_data_5, ui->akc_adc_p6);
    setTabOrder(ui->akc_adc_p6, ui->akc_data_6);
    setTabOrder(ui->akc_data_6, ui->akc_adc_p7);
    setTabOrder(ui->akc_adc_p7, ui->akc_data_7);
    setTabOrder(ui->akc_data_7, ui->akc_adc_p8);
    setTabOrder(ui->akc_adc_p8, ui->akc_data_8);
    setTabOrder(ui->akc_data_8, ui->Btn1Close);

    auto* eventFilter = new FocusEventFilter(this);
    ui->Btn1Close->installEventFilter(eventFilter);

    for(auto* item : this->findChildren<ArrowKeyControl*>())
    {
        item->setFontSize(24);
        QObject::connect(item,SIGNAL(longArrowUp()),this,SLOT(onLongPress_KeyUp()));
        QObject::connect(item,SIGNAL(longArrowDown()),this,SLOT(onLongPress_KeyDown()));
    }

    ui->title->setStyleSheet("QLabel {font: 34pt;}");

    ui->title->setText(sensor->GetName("ru"));


    ui->Btn1Close->setFocus();

    auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
    if(canworker!=nullptr)
    {
        connect(canworker,SIGNAL(received(CanMessage)),this,SLOT(OnCanMessage(CanMessage)));
        connect(canworker,SIGNAL(connectionLost()),this,SLOT(OnCanConnectionLost()));
        connect(canworker,SIGNAL(connectionRestored()),this,SLOT(OnCanConnectionRestore()));
        OnCanConnectionRestore();

        timer.setInterval(500);
        QObject::connect(&timer,SIGNAL(timeout()),this,SLOT(OnCanConnectionRestore()));
        timer.start();
    }

    _longPressTimer_KeyUp.setInterval(2000);
    _longPressTimer_KeyDown.setInterval(2000);
    _longPressTimer_KeyUp.setSingleShot(true);
    _longPressTimer_KeyDown.setSingleShot(true);

    ADCValue = 0;

    QObject::connect(&_longPressTimer_KeyUp, SIGNAL(timeout()), this, SLOT(onLongPress_KeyUp()));
    QObject::connect(&_longPressTimer_KeyDown, SIGNAL(timeout()), this, SLOT(onLongPress_KeyDown()));
}

SensorType2Screen::~SensorType2Screen()
{
    delete ui;
}

void SensorType2Screen::keyPressEvent(QKeyEvent* e)
{
    switch(e->key())
    {
    case Qt::Key_Escape:
        WindowManager::getInstance()->CloseMe(this);
        break;
    case Qt::Key_Up:
        _longPressTimer_KeyUp.start();
        break;
    case Qt::Key_Down:
        _longPressTimer_KeyDown.start();
        break;
    }
}

void SensorType2Screen::keyReleaseEvent(QKeyEvent *e)
{
    switch(e->key())
    {
    case Qt::Key_Up:
        _longPressTimer_KeyUp.stop();
        break;
    case Qt::Key_Down:
        _longPressTimer_KeyDown.stop();
        break;
    }
   // QWidget::keyReleaseEvent(e);
}

void SensorType2Screen::onLongPress_KeyUp()
{
    ui->akc_err_up->setNumber(ADCValue);
    SendAdcSettings1();
    OnCanConnectionRestore();
}

void SensorType2Screen::onLongPress_KeyDown()
{
    ui->akc_err_dn->setNumber(ADCValue);
    SendAdcSettings2();
    OnCanConnectionRestore();
}

void SensorType2Screen::on_Btn1Close_clicked()
{
    WindowManager::getInstance()->CloseMe(this);
}

void SensorType2Screen::OnCanMessage(CanMessage msg)
{
    if(msg.can_id == CanPackets::AdcSettings1Get::response_id)
    {
        auto packet = CanPackets::AdcSettings1Get::ParseResponse(msg.data);
        if(packet.contactStateId != (_sensor->GetChannelId() | _sensor->GetType()))
            return;

        ui->ADC->setText(QString::number(packet.adc));
        ADCValue = packet.adc;
        if(!ui->akc_err_up->isBeingEdited())
            ui->akc_err_up->setNumber(packet.err_up);
    }
    else if(msg.can_id == CanPackets::AdcSettings2Get::response_id)
    {
        auto packet = CanPackets::AdcSettings2Get::ParseResponse(msg.data);
        if(packet.contactStateId != (_sensor->GetChannelId() | _sensor->GetType()))
            return;

        if(!ui->akc_err_dn->isBeingEdited())
            ui->akc_err_dn->setNumber(packet.err_dn);

        ui->label_now->setText(QString::number(packet.now));

        if(!ui->akc_on_off->isBeingEdited())
            ui->akc_on_off->setNumber(packet.on_off);
        if(!ui->akc_status->isBeingEdited())
            ui->akc_status->setNumber(packet.status);
    }
    else if(msg.can_id == CanPackets::AdcSettings3Get::response_id)
    {
        auto packet = CanPackets::AdcSettings3Get::ParseResponse(msg.data);
        if(packet.contactStateId != (_sensor->GetChannelId() | _sensor->GetType()))
            return;

        if(!ui->akc_sum200->isBeingEdited())
            ui->akc_sum200->setNumber(packet.val);
    }
    else if(msg.can_id == CanPackets::AdcSettings4Get::response_id)
    {
        auto packet = CanPackets::AdcSettings4Get::ParseResponse(msg.data);
        if(packet.contactStateId != (_sensor->GetChannelId() | _sensor->GetType()))
            return;

        switch(packet.point_id)
        {
            case 0:
                if(!ui->akc_adc_p1->isBeingEdited())
                    ui->akc_adc_p1->setNumber(packet.adc_point);
                if(!ui->akc_data_1->isBeingEdited())
                    ui->akc_data_1->setNumber(packet.adc_point_data);
                break;
            case 1:
                if(!ui->akc_adc_p2->isBeingEdited())
                    ui->akc_adc_p2->setNumber(packet.adc_point);
                if(!ui->akc_data_2->isBeingEdited())
                    ui->akc_data_2->setNumber(packet.adc_point_data);
                break;
            case 2:
                if(!ui->akc_adc_p3->isBeingEdited())
                    ui->akc_adc_p3->setNumber(packet.adc_point);
                if(!ui->akc_data_3->isBeingEdited())
                    ui->akc_data_3->setNumber(packet.adc_point_data);
                break;
            case 3:
                if(!ui->akc_adc_p4->isBeingEdited())
                    ui->akc_adc_p4->setNumber(packet.adc_point);
                if(!ui->akc_data_4->isBeingEdited())
                    ui->akc_data_4->setNumber(packet.adc_point_data);
                break;
            case 4:
                if(!ui->akc_adc_p5->isBeingEdited())
                    ui->akc_adc_p5->setNumber(packet.adc_point);
                if(!ui->akc_data_5->isBeingEdited())
                    ui->akc_data_5->setNumber(packet.adc_point_data);
                break;
            case 5:
                if(!ui->akc_adc_p6->isBeingEdited())
                    ui->akc_adc_p6->setNumber(packet.adc_point);
                if(!ui->akc_data_6->isBeingEdited())
                    ui->akc_data_6->setNumber(packet.adc_point_data);
                break;
            case 6:
                if(!ui->akc_adc_p7->isBeingEdited())
                    ui->akc_adc_p7->setNumber(packet.adc_point);
                if(!ui->akc_data_7->isBeingEdited())
                    ui->akc_data_7->setNumber(packet.adc_point_data);
                break;
            case 7:
                if(!ui->akc_adc_p8->isBeingEdited())
                    ui->akc_adc_p8->setNumber(packet.adc_point);
                if(!ui->akc_data_8->isBeingEdited())
                    ui->akc_data_8->setNumber(packet.adc_point_data);
                break;
        }
 }
}

void SensorType2Screen::OnCanConnectionLost()
{

}

void SensorType2Screen::OnCanConnectionRestore()
{
    CanPackets::AdcSettings1Get::SendRequest(_sensor->GetChannelId() | _sensor->GetType());

    CanPackets::AdcSettings2Get::SendRequest(_sensor->GetChannelId() | _sensor->GetType());

    CanPackets::AdcSettings3Get::SendRequest(_sensor->GetChannelId() | _sensor->GetType());

    for(uint8_t i=0 ; i<8;i++)
    {
        CanPackets::AdcSettings4Get::SendRequest(_sensor->GetChannelId() | _sensor->GetType(), i);
    }
}


void SensorType2Screen::SendAdcSettings2()
{
    CanPackets::AdcSettings2Data packet{};
    packet.contactStateId = _sensor->GetChannelId() | _sensor->GetType();
    packet.err_dn = ui->akc_err_dn->number();
    packet.now = ui->label_now->text().toInt();
    packet.on_off = ui->akc_on_off->number();
    packet.status = ui->akc_status->number();
    CanPackets::AdcSettings2Set::SendRequest(packet);
}

void SensorType2Screen::SendAdcSettings3()
{
    CanPackets::AdcSettings3Data packet{};
    packet.contactStateId = _sensor->GetChannelId() | _sensor->GetType();
    packet.val = ui->akc_sum200->number();
    CanPackets::AdcSettings3Set::SendRequest(packet);
}

void SensorType2Screen::SendAdcSettings4(uint8_t pointId, uint16_t adc, uint16_t data)
{
    CanPackets::AdcSettings4Data packet{};
    packet.point_id = pointId;
    packet.contactStateId = _sensor->GetChannelId() | _sensor->GetType();
    packet.adc_point = adc;
    packet.adc_point_data = data;
    CanPackets::AdcSettings4Set::SendRequest(packet);
}

void SensorType2Screen::SendAdcSettings1()
{
    CanPackets::AdcSettings1Data packet{};
    packet.contactStateId = _sensor->GetChannelId() | _sensor->GetType();
    packet.err_up = ui->akc_err_up->number();
    CanPackets::AdcSettings1Set::SendRequest(packet);
}

void SensorType2Screen::on_akc_err_up_returnPressed()
{
    SendAdcSettings1();
}

void SensorType2Screen::on_akc_err_dn_returnPressed()
{
    SendAdcSettings2();
}

void SensorType2Screen::on_akc_on_off_returnPressed()
{
    SendAdcSettings2();
}

void SensorType2Screen::on_akc_status_returnPressed()
{
    SendAdcSettings2();
}

void SensorType2Screen::on_akc_sum200_returnPressed()
{
    SendAdcSettings3();
}

void SensorType2Screen::on_akc_adc_p1_returnPressed()
{
    SendAdcSettings4(
                0,
                ui->akc_adc_p1->number(),
                ui->akc_data_1->number());
}

void SensorType2Screen::on_akc_data_1_returnPressed()
{
    SendAdcSettings4(
                0,
                ui->akc_adc_p1->number(),
                ui->akc_data_1->number());
}

void SensorType2Screen::on_akc_adc_p2_returnPressed()
{
    SendAdcSettings4(
                1,
                ui->akc_adc_p2->number(),
                ui->akc_data_2->number());
}

void SensorType2Screen::on_akc_data_2_returnPressed()
{
    SendAdcSettings4(
                1,
                ui->akc_adc_p2->number(),
                ui->akc_data_2->number());
}

void SensorType2Screen::on_akc_adc_p3_returnPressed()
{
    SendAdcSettings4(
                2,
                ui->akc_adc_p3->number(),
                ui->akc_data_3->number());

}

void SensorType2Screen::on_akc_data_3_returnPressed()
{
    SendAdcSettings4(
                2,
                ui->akc_adc_p3->number(),
                ui->akc_data_3->number());
}

void SensorType2Screen::on_akc_adc_p4_returnPressed()
{
    SendAdcSettings4(
                3,
                ui->akc_adc_p4->number(),
                ui->akc_data_4->number());

}

void SensorType2Screen::on_akc_data_4_returnPressed()
{
    SendAdcSettings4(
                3,
                ui->akc_adc_p4->number(),
                ui->akc_data_4->number());

}

void SensorType2Screen::on_akc_adc_p5_returnPressed()
{
    SendAdcSettings4(
                4,
                ui->akc_adc_p5->number(),
                ui->akc_data_5->number());

}

void SensorType2Screen::on_akc_data_5_returnPressed()
{
    SendAdcSettings4(
                4,
                ui->akc_adc_p5->number(),
                ui->akc_data_5->number());

}

void SensorType2Screen::on_akc_adc_p6_returnPressed()
{
    SendAdcSettings4(
                5,
                ui->akc_adc_p6->number(),
                ui->akc_data_6->number());
}

void SensorType2Screen::on_akc_data_6_returnPressed()
{
    SendAdcSettings4(
                5,
                ui->akc_adc_p6->number(),
                ui->akc_data_6->number());
}

void SensorType2Screen::on_akc_adc_p7_returnPressed()
{
    SendAdcSettings4(
                6,
                ui->akc_adc_p7->number(),
                ui->akc_data_7->number());
}

void SensorType2Screen::on_akc_data_7_returnPressed()
{
    SendAdcSettings4(
                6,
                ui->akc_adc_p7->number(),
                ui->akc_data_7->number());
}

void SensorType2Screen::on_akc_adc_p8_returnPressed()
{
    SendAdcSettings4(
                7,
                ui->akc_adc_p8->number(),
                ui->akc_data_8->number());
}

void SensorType2Screen::on_akc_data_8_returnPressed()
{
    SendAdcSettings4(
                7,
                ui->akc_adc_p8->number(),
                ui->akc_data_8->number());
}


