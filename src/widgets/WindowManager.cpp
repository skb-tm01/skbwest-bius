#include "WindowManager.h"
#include <QApplication>
#include <QWidget>
#include <QStack>
#include <QDebug>

WindowManager* WindowManager::inst = nullptr;

WindowManager::WindowManager(QObject *parent): QObject(parent)
{
    debugMode = true;
}

/*  1) сделать стек окошек
 *  2) при открытии окошка помещать его наверх стека, переместить на него фокус
 *  3) при закрытии окошка закрывать и удалять верхнее,
 *  сделать активным окошком предыдущее в стеке, переместить на него фокус
 */

void WindowManager::Open(QWidget *sender, QWidget *window)
{
    if(window == nullptr) {
        return;
    }

    if (SenderIsTop_or_StackIsEmpty(sender))
    {
        if(IsEmpty()) emit stackNotEmpty();
        widgetstack.push(window);

        window->show();
        window->setFocus();
        window->activateWindow();

        if(debugMode)
            qDebug() << "new " <<  window << " opened";
        return;
    }
    qDebug() << "WindowManager:attempt to open new window when not allowed ";
    qDebug() << "WindowManager:sender " << sender << " window " << window;
    qDebug() << "WindowManager:stack: " << widgetstack.count();
}

void WindowManager::CloseTopWidget()
{
    if(IsEmpty())
        return;

    int stackbefore = widgetstack.count();
    auto* w = widgetstack.pop();
    if(w == nullptr) {
        return;
    }

    w->close();

    if(debugMode)
        qDebug() << "WindowManager:top widget " << w << " closed. stackbefore: " << stackbefore << " stackafter: " << widgetstack.count();

    setFocusOnTop();
    CheckEmpty_EmitSignal();
}

void WindowManager::CloseMe(QWidget *sender)
{
    if(sender == nullptr) {
        return;
    }

    if(SenderIsTop_or_StackIsEmpty(sender))
        CloseTopWidget();

    // TODO if sender is not top, close all at higher level

    CheckEmpty_EmitSignal();
}

void WindowManager::setFocusOnTop()
{
    if(IsEmpty())
        return;

    auto* w = widgetstack.top();
    if(w==nullptr) {
        return;
    }

    if(debugMode)
        qDebug() << "Focus set at top "<< w << " stackcount : " << widgetstack.count();

    w->setFocus();
    w->activateWindow();
}

void WindowManager::CloseAll()
{
    while(!IsEmpty())
    {
        CloseTopWidget();
    }
}

bool WindowManager::CheckEmpty_EmitSignal()
{
    if (IsEmpty())
    {
        emit stackCleared();
        return true;
    }
    setFocusOnTop();
    return false;
}

bool WindowManager::SenderIsTop_or_StackIsEmpty(QWidget *sender)
{
    if(IsEmpty())
        return true;
    return (widgetstack.at(widgetstack.count()-1) == sender);
}

bool WindowManager::IsEmpty()
{
    return widgetstack.isEmpty();
}

WindowManager* WindowManager::getInstance()
{
    if(inst == nullptr)
    {
        inst = new WindowManager(QApplication::instance());
    }
    return inst;
}
