#pragma once

#include <QLabel>
#include <QWidget>
#include <QKeyEvent>
#include <QTimer>

#include "alerts/AlertDescription.h"
#include "widgets/SensorWidget.h"

namespace Ui {
class SensorStateInstructions;
}

class SensorStateInstructions : public QWidget
{
    Q_OBJECT
public:
    SensorStateInstructions(SensorWidget *parent);
    void SetFontSize(int size);
    void SetServiceMenuButtonVisiblity(bool visible);

    ~SensorStateInstructions();
protected:
    void keyPressEvent(QKeyEvent* e);
    void keyReleaseEvent(QKeyEvent *e);

private slots:
    void on_Btn3Close_clicked();
    void on_Btn2Control_clicked();

    void on_Btn1Setup_clicked();
    void onLongPress();
    void onSensorStateChange();

    void on_Btn3Close_pressed();

    void on_Btn3Close_released();

private:
    Ui::SensorStateInstructions *ui;
    QLabel *instructions;
    void setControlButtonText();
    SensorWidget *sensor;
    QTimer _longPressTimer;
    bool timerRan;
    void SetData(ISensor *sensor);
    void SetData(QString title, QString text);
};


