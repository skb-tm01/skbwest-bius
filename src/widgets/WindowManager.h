#pragma once

#include <QObject>
#include <QWidget>

#include <QStack>

class WindowManager : public QObject
{
    Q_OBJECT
signals:
    void stackCleared();
    void stackNotEmpty();
public:
    explicit WindowManager(QObject *parent = nullptr);
    static WindowManager *getInstance();
    void Open(QWidget *sender, QWidget *window);
///*    int Exec(QWidget *sender, QDialog *w*/indow);
    void CloseTopWidget();
    void CloseMe(QWidget *sender);
    bool IsEmpty();
    void setFocusOnTop();
    void CloseAll();

private:
    static WindowManager* inst;
    QStack<QWidget*> widgetstack;
    bool CheckEmpty_EmitSignal();
    bool SenderIsTop_or_StackIsEmpty(QWidget *sender);
    bool WindowHasRightToClose(QWidget *sender);

    bool debugMode;
};
