#ifndef SENSORS_STATISTICS_SHORT_H
#define SENSORS_STATISTICS_SHORT_H

#include <QWidget>
#include <QKeyEvent>

#include "peripherials/can/CanWorker.h"
#include "widgets/SensorsStatisticsEnum.h"

namespace Ui {
class sensors_statistics_short;
}

//enum class EnumSensorsStatisticsMode
//{
//    OnStart,
//    Menu
//};

class sensors_statistics_short : public QWidget
{
    Q_OBJECT

public:
    explicit sensors_statistics_short(QWidget *parent = nullptr, EnumSensorsStatisticsMode mode = EnumSensorsStatisticsMode::Menu);
    ~sensors_statistics_short();

signals:
    void OnClosing();

private slots:
    void on_BtnClose_clicked();
    void OnTimer();
    void OnConnectionLost();
    void OnConnectionRestore();
    void OnCanMessage(CanMessage msg);

protected:
    void keyPressEvent(QKeyEvent* e);    


private:
    Ui::sensors_statistics_short *ui;
    void UpdateStats();
    void closeInternal();
    void UpdateBIOVersion();
    EnumSensorsStatisticsMode _mode;
    QTimer* _timer;
    int _time;

    CanWorker* _canworker;

    int model;
    int checksum;
    QString date1;
    QString date2;

};

#endif // SENSORS_STATISTICS_SHORT_H
