#include <QLabel>

#include "peripherials/PeripherialsManager.h"
#include "peripherials/can/packets/OutSensorSettings.h"
#include "widgets/EventFilters/FocusEventFilter.h"
#include "widgets/SensorType4Screen.h"
#include "widgets/WindowManager.h"
#include "ui_SensorType4Screen.h"

SensorType4Screen::SensorType4Screen(ISensor* sensor,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SensorType4Screen)
{
    _sensor = sensor;
    setAttribute(Qt::WA_DeleteOnClose);
    setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    ui->setupUi(this);

    setTabOrder(ui->akc_onoff , ui->akc_status);
    setTabOrder(ui->akc_status, ui->akc_times);
    setTabOrder(ui->akc_times , ui->akc_t3ms);
    setTabOrder(ui->akc_t3ms  , ui->akc_ton3ms);
    setTabOrder(ui->akc_ton3ms, ui->Btn1Close);

    ui->akc_onoff->init(8,2);
    ui->akc_status->init(8,2);

    auto* eventFilter = new FocusEventFilter(this);
    ui->Btn1Close->installEventFilter(eventFilter);

    for(auto* item : this->findChildren<ArrowKeyControl*>())
    {
        item->setFontSize(24);
    }

    ui->title->setStyleSheet("QLabel {font: 34pt;}");

    ui->title->setText(sensor->GetName("ru"));


    ui->Btn1Close->setFocus();

    auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
    if(canworker!=nullptr)
    {
        connect(canworker,SIGNAL(received(CanMessage)),this,SLOT(OnCanMessage(CanMessage)));
        connect(canworker,SIGNAL(connectionLost()),this,SLOT(OnCanConnectionLost()));
        connect(canworker,SIGNAL(connectionRestored()),this,SLOT(OnCanConnectionRestore()));
        OnCanConnectionRestore();

        timer.setInterval(500);
        QObject::connect(&timer,SIGNAL(timeout()),this,SLOT(OnCanConnectionRestore()));
        timer.start();
    }
}

SensorType4Screen::~SensorType4Screen()
{
    delete ui;
}

void SensorType4Screen::keyPressEvent(QKeyEvent* e)
{
    switch(e->key())
    {
        case Qt::Key_Escape:
            WindowManager::getInstance()->CloseMe(this);
            break;
    }
}

void SensorType4Screen::on_Btn1Close_clicked()
{
    WindowManager::getInstance()->CloseMe(this);
}

void SensorType4Screen::OnCanMessage(CanMessage msg)
{
    if(msg.can_id == CanPackets::OutSensorSettingsGet::response_id)
    {
        auto packet = CanPackets::OutSensorSettingsGet::ParseResponse(msg.data);
        if(packet.contactStateId != (_sensor->GetChannelId() | _sensor->GetType()))
            return;

        if(!ui->akc_onoff->isBeingEdited())
            ui->akc_onoff->setNumber(packet.on_off);

        if(!ui->akc_status->isBeingEdited())
            ui->akc_status->setNumber(packet.status);

        if(!ui->akc_times->isBeingEdited())
            ui->akc_times->setNumber(packet.time_s);

        if(!ui->akc_t3ms->isBeingEdited())
            ui->akc_t3ms->setNumber(packet.t_3ms);

        if(!ui->akc_ton3ms->isBeingEdited())
            ui->akc_ton3ms->setNumber(packet.ton_3ms);
    }
}

void SensorType4Screen::OnCanConnectionLost()
{

}

void SensorType4Screen::OnCanConnectionRestore()
{
    CanPackets::OutSensorSettingsGet::SendRequest(_sensor->GetChannelId() | _sensor->GetType());
}

void SensorType4Screen::SendSettings()
{
    CanPackets::OutSensorSettingsData msg{};
    msg.contactStateId = _sensor->GetChannelId() | _sensor->GetType();
    msg.on_off = static_cast<uint8_t>(ui->akc_onoff->number());
    msg.status = static_cast<uint8_t>(ui->akc_status->number());
    msg.time_s = static_cast<uint8_t>(ui->akc_times->number());
    msg.t_3ms = static_cast<uint16_t>(ui->akc_t3ms->number());
    msg.ton_3ms = static_cast<uint16_t>(ui->akc_ton3ms->number());
    CanPackets::OutSensorSettingsSet::SendRequest(msg);
}

void SensorType4Screen::on_akc_onoff_returnPressed()
{
    SendSettings();
}

void SensorType4Screen::on_akc_status_returnPressed()
{
    SendSettings();
}

void SensorType4Screen::on_akc_times_returnPressed()
{
    SendSettings();
}

void SensorType4Screen::on_akc_t3ms_returnPressed()
{
    SendSettings();
}

void SensorType4Screen::on_akc_ton3ms_returnPressed()
{
    SendSettings();
}
