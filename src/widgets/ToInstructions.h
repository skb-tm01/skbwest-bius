#pragma once

#include <QLabel>
#include <QWidget>
#include <QKeyEvent>

#include "alerts/AlertDescription.h"
#include "peripherials/sensors/ISensor.h"

namespace Ui {
class ToInstructions;
}

class ToInstructions : public QWidget
{
    Q_OBJECT

public:
    ToInstructions(QWidget *parent);

    void SetData(ISensor *sensor, QString title = "");
    void SetData(QString title, QString text);

    void SetFontSize(int size);
    void Close();
    ~ToInstructions();
protected:
    void keyPressEvent(QKeyEvent* e);

private slots:
    void on_Btn1Close_clicked();

    void on_Btn2To_clicked();

private:
    Ui::ToInstructions *ui;
    QLabel *instructions;
    QString id;
};
