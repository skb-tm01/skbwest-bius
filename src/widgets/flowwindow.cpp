#include <QPixmap>
#include <QPushButton>
#include <QWidget>

#include "core/FlowLayout.h"
#include "core/helper.h"

#include "widgets/SensorWidget.h"
#include "widgets/flowwindow.h"

#include "ui_flowwindow.h"
#include "widgets/WindowManager.h"

FlowWindow::FlowWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FlowWindow)
{
    setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    FlowLayout *flowLayout = new FlowLayout;

    QPushButton *b_close = new QPushButton(tr("close"));
    flowLayout->addWidget(b_close);

    setLayout(flowLayout);
    connect(b_close,SIGNAL(clicked(bool)),this,SLOT(on_b_close_clicked()));
    ui->setupUi(this);
}

FlowWindow::~FlowWindow()
{
    delete ui;
}

void FlowWindow::on_b_close_clicked()
{
    WindowManager::getInstance()->CloseMe(this);
}
