#pragma once

#include <QWidget>
#include <QKeyEvent>
#include <QTimer>


#include "peripherials/can/CanMessage.h"
#include "peripherials/sensors/ISensor.h"

namespace Ui {
class SensorType2Screen;
}

class SensorType2Screen : public QWidget
{
    Q_OBJECT

public:
    explicit SensorType2Screen(ISensor* sensor, QWidget *parent = 0);
    ~SensorType2Screen();

private slots:
    void on_Btn1Close_clicked();

    //can
    void OnCanMessage(CanMessage msg);
    void OnCanConnectionLost();
    void OnCanConnectionRestore();


    void on_akc_err_up_returnPressed();

    void on_akc_err_dn_returnPressed();

    void on_akc_on_off_returnPressed();

    void on_akc_status_returnPressed();

    void on_akc_sum200_returnPressed();

    void on_akc_adc_p1_returnPressed();

    void on_akc_data_1_returnPressed();

    void on_akc_adc_p2_returnPressed();

    void on_akc_data_2_returnPressed();

    void on_akc_adc_p3_returnPressed();

    void on_akc_data_3_returnPressed();

    void on_akc_adc_p4_returnPressed();

    void on_akc_data_4_returnPressed();

    void on_akc_adc_p5_returnPressed();

    void on_akc_data_5_returnPressed();

    void on_akc_adc_p6_returnPressed();

    void on_akc_data_6_returnPressed();

    void on_akc_adc_p7_returnPressed();

    void on_akc_data_7_returnPressed();

    void on_akc_adc_p8_returnPressed();

    void on_akc_data_8_returnPressed();

    void onLongPress_KeyUp();
    void onLongPress_KeyDown();
protected:
    void keyPressEvent(QKeyEvent* e);
    void keyReleaseEvent(QKeyEvent* e);
private:
    void SendAdcSettings1();
    void SendAdcSettings2();
    void SendAdcSettings3();
    void SendAdcSettings4(uint8_t pointId, uint16_t adc, uint16_t data);

    ISensor* _sensor;
    Ui::SensorType2Screen *ui;
    QTimer timer;
    QTimer _longPressTimer_KeyUp;
    QTimer _longPressTimer_KeyDown;

    double ADCValue;
};

