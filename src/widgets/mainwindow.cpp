#include <QDateTime>
#include <QDebug>
#include <QMouseEvent>
#include <QTimer>
#include <QLayout>
#include <QLabel>
#include <QListWidget>

#include "alerts/AlertsManager.h"
#include "alerts/AlertsWorker.h"

#include "core/AudioHelper.h"
#include "core/PathHelper.h"
#include "core/helper.h"

#include "peripherials/can/packets/Analog.h"
#include "peripherials/can/packets/AirFlowTemp.h"
#include "peripherials/can/packets/ContactState.h"
#include "peripherials/can/packets/LossSave.h"
#include "peripherials/can/packets/IPSState.h"
#include "peripherials/can/packets/Settings.h"
#include "peripherials/sensors/SensorManager.h"
#include "peripherials/can/packets/Request3.h"

#include "widgets/gaugeswidget.h"
#include "widgets/Controls/Clock.h"
#include "widgets/Menu/menu_main.h"
#include "widgets/Panels/AlertPanel.h"

#include "SettingsManager.h"

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "WindowManager.h"

MainWindow* MainWindow::Instance = nullptr;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    Instance = this;

    //setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    _isInWorkMode = 0;
    _activeWarning = nullptr;
    _activeDescription = nullptr;

    ui->setupUi(this);

    _gaugesWidget = new GaugesWidget(this);
    ui->_centralWidgets->addWidget(_gaugesWidget);

    _engineRotationsWidget = new MenuSensorsRotationsWidget(this);
    _engineRotationsWidget->SetClosable(false);
    ui->_centralWidgets->addWidget(_engineRotationsWidget);

    QObject::connect(_gaugesWidget,SIGNAL(LeftSideButtonClicked()), this, SLOT(leftScreenButtonClicked()));
    QObject::connect(_gaugesWidget,SIGNAL(RightSideButtonClicked()), this, SLOT(rightScreenButtonClicked()));
    QObject::connect(_gaugesWidget,SIGNAL(lossesGaugeWidgetsDoubleClicked()), this, SLOT(gaugeWidgetsLossSave()));

    QObject::connect(_engineRotationsWidget,SIGNAL(LeftSideButtonClicked()), this, SLOT(leftScreenButtonClicked()));
    QObject::connect(_engineRotationsWidget,SIGNAL(RightSideButtonClicked()), this, SLOT(rightScreenButtonClicked()));

    QObject::connect(this,SIGNAL(centerWidgetModeChanged(int)), this, SLOT(setCenterWidgetWorkingMode(int)));

    emit centerWidgetModeChanged(static_cast<int>(VehicleMode::mode_road));

    AlertPanelItemWidget::InitAlertDescription(this);

    _longPressTimer.setInterval(2000);
    _longPressTimer.setSingleShot(true);
    QObject::connect(&_longPressTimer, SIGNAL(timeout()), this, SLOT(lossSave()));

    QObject::connect(WindowManager::getInstance(),SIGNAL(stackCleared()), this, SLOT(menuMainClosed()));
    QObject::connect(WindowManager::getInstance(),SIGNAL(stackNotEmpty()), this, SLOT(menuOpened()));

    ui->frame_bottom->setMenuButtonAppearance(WindowManager::getInstance()->IsEmpty());
}

MainWindow::~MainWindow()
{
    Instance = nullptr;
    delete ui;
}

WarningMessage *MainWindow::activeWarning()
{
    return _activeWarning;
}

AlertPanelWidget *MainWindow::GetAlertPanel()
{
    return ui->frame_top;
}

StatusPanelWidget *MainWindow::GetStatusPanel()
{
    return ui->frame_bottom;
}

void MainWindow::MenuButtonClicked()
{
    if(!this->isVisible())
        return;

    auto aWindowIsOpen = !WindowManager::getInstance()->IsEmpty();
    if(aWindowIsOpen)
    {
        WindowManager::getInstance()->CloseTopWidget();
    } else {
        auto menuMain = new MenuMain(this);
        menuMain->move(ui->centralwidget->mapToGlobal(ui->_centralWidgets->pos()));
        menuMain->resize( ui->_centralWidgets->size());
        QObject::connect(menuMain, SIGNAL(destroyed()), this, SLOT(menuMainClosed()));
        WindowManager::getInstance()->Open(this, menuMain);
        ui->frame_bottom->setMenuButtonAppearance(false);
    }
}

//////////////////
//// Key events //
//////////////////

void MainWindow::keyReleaseEvent(QKeyEvent *e)
{
    switch(e->key())
    {
        case Qt::Key_Enter:
        case Qt::Key_Return:
            _longPressTimer.stop();
//             qDebug()<<"lossSaveStop";

            break;
    }
    QMainWindow::keyReleaseEvent(e);
}


void MainWindow::keyPressEvent(QKeyEvent* e)
{
    switch(e->key())
    {
        case Qt::Key_Left:
        {
            int new_state = static_cast<int>(_centerWidgetMode)-1;
            if(new_state<0)
                new_state = VEHICLE_MODE_COUNT -1;
            emit centerWidgetModeChanged(new_state);
            break;
        }
        case Qt::Key_Right:
        {
            int new_state = static_cast<int>(_centerWidgetMode)+1;
            if(new_state>=VEHICLE_MODE_COUNT)
                new_state = 0;
            emit centerWidgetModeChanged(new_state);
            break;
        }
        case Qt::Key_Enter:
        case Qt::Key_Return:
            if(_centerWidgetMode == VehicleMode::mode_field)
            {
                _longPressTimer.start();
            }
            break;
    }
    QMainWindow::keyPressEvent(e);
}

void MainWindow::setCenterWidgetWorkingMode(int workingMode)
{
    _centerWidgetMode = static_cast<VehicleMode>(workingMode);

    if(_centerWidgetMode==VehicleMode::mode_road)
    {
        _gaugesWidget->setWorkingMode(_centerWidgetMode);
        ui->_centralWidgets->setCurrentIndex(0);
    }
    else if(_centerWidgetMode==VehicleMode::mode_field)
    {
        _gaugesWidget->setWorkingMode(_centerWidgetMode);
        ui->_centralWidgets->setCurrentIndex(0);
    }
    else
    {
        ui->_centralWidgets->setCurrentIndex(1);

        auto* obj = qobject_cast<MenuSensorsRotationsWidget*>(ui->_centralWidgets->widget(1));
        if(obj!=nullptr)
        {
            obj->FocusButton();
        }
    }
}

/////////////////////////
///// Warnings       ////
/// /////////////////////


void MainWindow::ShowAlertWindow(AlertDescription* alert_description)
{
    if(_activeWarning != nullptr)
        return;

    auto* sensor = SensorManager::instance().GetSensor(alert_description->GetId());
    if(sensor==nullptr)
        return;

    if((sensor->GetType()!=SensorType::UNK) && (!sensor->IsUnderControl() || !(sensor->IsBroken() || sensor->IsErrorOccured())))
    {
        return;
    }

    _activeDescription = alert_description;
    _activeWarning = new WarningMessage(alert_description, this);
    QObject::connect(_activeWarning,SIGNAL(closed()),this,SLOT(onWarningClosed()));
    _activeWarning->show();
}

void MainWindow::ShowAlertTopbar(AlertDescription *alert_description)
{
    ui->frame_top->AddAlert(alert_description);
}

void MainWindow::RemoveAlert(QString alert_id)
{
    ui->frame_top->RemoveAlert(alert_id);
}

/////////////////////////
///// CAN connection ////
/////////////////////////

void MainWindow::Initialize_CAN()
{
    auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
    if(canworker==nullptr)
        return;

    QObject::connect(canworker,SIGNAL(received(CanMessage)),this,SLOT(OnCanMessage(CanMessage)));
    QObject::connect(canworker,SIGNAL(connectionLost()),this,SLOT(OnCanConnectionLost()));

    CanPacketAnalogRequest::Send(CanPacketAnalogType::can_analog_type_fuel_level,true);
    CanPacketAnalogRequest::Send(CanPacketAnalogType::can_analog_type_hydraulic_oiltemp,true);
    CanPacketAnalogRequest::Send(CanPacketAnalogType::can_analog_type_hydraulicpressure,true);
    CanPacketAnalogRequest::Send(CanPacketAnalogType::can_analog_type_oilpressure,true);
    CanPacketAnalogRequest::Send(CanPacketAnalogType::can_analog_type_engine_temp,true);
    CanPacketAnalogRequest::Send(CanPacketAnalogType::can_analog_type_power,true);

    CanPacketAnalogRequest::Send(CanPacketAnalogType::can_analog_type_concaveclearance,true);
    CanPacketAnalogRequest::Send(CanPacketAnalogType::can_analog_type_sieves_higher,true);
    CanPacketAnalogRequest::Send(CanPacketAnalogType::can_analog_type_sieves_lower,true);

    CanPacketContactStateRequest::Send(true);

    CanPackets::IPSState::SendRequest(true);

    CanPackets::Request3::SendRequest(true);

    CanPacketTahoRequest::Send(CanPacketTahoType::taho_speed_engine, true);
    CanPacketTahoRequest::Send(CanPacketTahoType::taho_speed_fan, true);
    CanPacketTahoRequest::Send(CanPacketTahoType::taho_speed_threshing, true);
    CanPacketTahoRequest::Send(CanPacketTahoType::taho_drumchopper_speed, true);
    CanPacketTahoRequest::Send(CanPacketTahoType::taho_grainauger_speed, true);
    CanPacketTahoRequest::Send(CanPacketTahoType::taho_spikeauger_speed, true);
    CanPacketTahoRequest::Send(CanPacketTahoType::taho_shakershaft_speed, true);
    CanPacketTahoRequest::Send(CanPacketTahoType::taho_strawsep_speed, true);
    CanPacketTahoRequest::Send(CanPacketTahoType::taho_levelingauger2_speed, true);

    CanPacketTahoRequest::Send(CanPacketTahoType::taho_speed_current, true);
    CanPacketTahoRequest::Send(CanPacketTahoType::taho_unknown_load, true);
    CanPacketTahoRequest::Send(CanPacketTahoType::taho_loss_left, true);
    CanPacketTahoRequest::Send(CanPacketTahoType::taho_loss_right, true);

    CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_test_poteri, true);
    CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_vid_zerno, true);
    CanPacketSettingsGetRequest::Send(CanPacketSettingsType::settings_combine_model_3, false);

    CanPackets::AirFlowTemp::SendRequest(0, true);

    if(_engineRotationsWidget!=nullptr)
        _engineRotationsWidget->Initialize_CAN();

    if(_gaugesWidget!=nullptr)
        _gaugesWidget->Initialize_CAN();

    if(ui->frame_top!=nullptr)
       ui->frame_top->Initialize_CAN();

    if(ui->frame_bottom!=nullptr)
        ui->frame_bottom->Initialize_CAN();
}

void MainWindow::OnCanMessage(CanMessage msg)
{
    //qDebug()<<"MAINWNDOW CAN RESTORED ";
    //emit alert_can_restored();
   // AlertsManager::instance().Alerts_Worker_Get()->OnCanConnection_Restore();
    if(msg.can_id != CanPacketContactStateResponse::packet_id)
        return;

    auto packet = CanPacketContactStateResponse::Parse(msg.data);
    _isInWorkMode = packet.on_work_mode != 0;
}

void MainWindow::OnCanConnectionLost()
{
   // qDebug()<<"MAINWNDOW CAN LOST ";
   // emit alert_can_lost();

    //TODO: dirty hack
  //  AlertsManager::instance().Alerts_Worker_Get()->OnCanConnection_Lost();
}

void MainWindow::onWarningClosed()
{
    if(_activeDescription!=nullptr) {
     //   AlertsManager::instance().Alerts_Worker_Get()->RemoveAlertFromList(_activeDescription->GetId());
    }
    _activeWarning=nullptr;
    _activeDescription=nullptr;
}

void MainWindow::lossSave()
{
    CanPackets::LossSave::SendRequest();
    qDebug()<<"LOSS SAVED";
}

void MainWindow::menuMainClosed()
{
    ui->frame_bottom->setMenuButtonAppearance(true);
}

void MainWindow::menuOpened()
{
    ui->frame_bottom->setMenuButtonAppearance(false);
}

void MainWindow::leftScreenButtonClicked()
{
//    qDebug()<<"left";
    int new_state = static_cast<int>(_centerWidgetMode)-1;
    if(new_state<0)
      new_state = VEHICLE_MODE_COUNT -1;
    emit centerWidgetModeChanged(new_state);
}

void MainWindow::rightScreenButtonClicked()
{
//    qDebug()<<"right";
    int new_state = static_cast<int>(_centerWidgetMode)+1;
    if(new_state>=VEHICLE_MODE_COUNT)
        new_state = 0;
    emit centerWidgetModeChanged(new_state);
}

void MainWindow::gaugeWidgetsLossSave()
{
    if(_centerWidgetMode == 1)
    {
       qDebug()<<"double clicked";
        CanPackets::LossSave::SendRequest();
        qDebug()<<"LOSS SAVED";
    }
        qDebug()<<_centerWidgetMode;
}

void MainWindow::onIdle()
{
    activateWindow();
    WindowManager::getInstance()->CloseAll();

    AlertPanelItemWidget::KillAlertDescription();

    emit centerWidgetModeChanged(_isInWorkMode);
}

void MainWindow::emitKeyEvent(QKeyEvent* ev)
{
    if(ev == nullptr)
        return;

    QWidget* w = QApplication::focusWidget();
    if (w==nullptr)
        return;

    QApplication::sendEvent(w, ev);
}
