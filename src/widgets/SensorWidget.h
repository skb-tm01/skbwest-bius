#pragma once

#include <QFrame>
#include <QLabel>
#include "alerts/AlertDescription.h"
#include "core/ClickableLabel.h"
#include"peripherials/sensors/ISensor.h"

namespace Ui {
class SensorWidget;
}

class SensorWidget : public QFrame
{
    Q_OBJECT

public:
    SensorWidget(QWidget *parent,ISensor* s);
    ~SensorWidget();
    void setImage(const QPixmap &value, bool permament = true);
    void setEmpty(bool isEmpty);
    void setDayNight(bool day_now);
    void updateImage();
    bool getOnControl();
    inline ISensor* Sensor() {return _sensor;}

    //Q_PROPERTY(bool selected READ isSelected WRITE setSelected)

    bool isSelected(){return _selected;}
    void setSelected(bool value);

    void ShowSensorInfo(bool showServiceMenu);

signals:
    void selectedChanged(SensorWidget* src);

public slots:
    void Select(){setSelected(true);}
    void Unselect(){setSelected(false);}

protected:
    void mousePressEvent(QMouseEvent* event);
    void paintEvent(QPaintEvent * event);

private:
    Ui::SensorWidget *ui;

    bool _selected;
    ISensor* _sensor;
    QPixmap _pixmap;
    QPixmap _alertPixmap;

    bool empty;
    bool visibl;  // DO NOT RENAME
    bool is_day;

    QWidget *_parent;
};
