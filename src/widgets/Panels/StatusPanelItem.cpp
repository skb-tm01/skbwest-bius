#include <QPixmap>

#include "core/helper.h"
#include "widgets/Panels/StatusPanelItem.h"

#include "ui_StatusPanelItem.h"

StatusPanelItemWidget::StatusPanelItemWidget(QWidget *parent, QString text, const QString &fileName, QString dim) :
    QWidget(parent),
    ui(new Ui::StatusPanelItemWidget)
{
    ui->setupUi(this);

    night = false;
    enabled = false;
    visibl = true;
    units = dim;

    ui->labelText->setStyleSheet("QLabel{font: 18pt;}");
    ui->labelImage->setAlignment(Qt::AlignVCenter);
    setPermamentImage(Helper::LoadPixmap(fileName));
    setText(text);
}

StatusPanelItemWidget::~StatusPanelItemWidget()
{
    delete ui;
}

bool StatusPanelItemWidget::getEnabled() const
{
    return enabled;
}

void StatusPanelItemWidget::setEnabled(bool value)
{
    if(this->enabled!=value)
    {
      enabled = value;
      updateImage();
    }
}

void StatusPanelItemWidget::SetNightMode(bool value)
{
    if(this->night!=value)
    {
        night = value;
        updateImage();
    }
}

QString StatusPanelItemWidget::getText() const
{
    return ui->labelText->text();
}

void StatusPanelItemWidget::setText(const QString &value)
{
    ui->labelText->setVisible((!value.isEmpty())&&visibl);
    ui->labelText->setText(value + " " + units);
}

void StatusPanelItemWidget::setPermamentImage(const QPixmap &value)
{
    pixmap_default = value;

    setDefaultColors(Helper::dayForeColor(), Helper::nightForeColor());
    setEnabledColors(Helper::dayForeColor(), Helper::nightForeColor());

    updateImage();
}

void StatusPanelItemWidget::setDefaultColors(const QColor &default_day, const QColor &default_night)
{
    this->pixmap_default_day=Helper::ScalePixmap(Helper::SetPixmapColor(pixmap_default, default_day),64);
    this->pixmap_default_night=Helper::ScalePixmap(Helper::SetPixmapColor(pixmap_default, default_night),64);
}

void StatusPanelItemWidget::setEnabledColors(const QColor &enabled_day, const QColor &enabled_night)
{
    this->pixmap_enabled_day=Helper::ScalePixmap(Helper::SetPixmapColor(pixmap_default, enabled_day),64);
    this->pixmap_enabled_night=Helper::ScalePixmap(Helper::SetPixmapColor(pixmap_default, enabled_night),64);
}

void StatusPanelItemWidget::updateImage()
{
    if(enabled)
    {
        if(!night)
        {
            ui->labelImage->setPixmap(this->pixmap_enabled_day);
        }
        else
        {
            ui->labelImage->setPixmap(this->pixmap_enabled_night);
        }
    }
    else
    {
        if(!night)
        {
            ui->labelImage->setPixmap(this->pixmap_default_day);
        }
        else
        {
            ui->labelImage->setPixmap(this->pixmap_default_night);
        }
    }
}

void StatusPanelItemWidget::setFontSize(int value)
{
    ui->labelText->setStyleSheet(QString("QLabel {font-size: %1pt }").arg(value));
}

bool StatusPanelItemWidget::getVisibl() const
{
    return visibl;
}

void StatusPanelItemWidget::setVisibl(bool value)
{
    visibl = value;
    ui->labelText->setVisible((!ui->labelText->text().isEmpty())&&visibl);
    ui->labelImage->setVisible(visibl);
}

