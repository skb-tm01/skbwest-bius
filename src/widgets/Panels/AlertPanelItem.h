#pragma once

#include <QLabel>
#include <QWidget>

#include "alerts/AlertDescription.h"
#include "core/ClickableLabel.h"
#include "peripherials/can/CanMessage.h"
#include "widgets/ToInstructions.h"

namespace Ui
{
    class AlertPanelItemWidget;
}

class AlertPanelItemWidget : public QWidget
{
    Q_OBJECT

public:
    explicit AlertPanelItemWidget(QWidget *parent = nullptr);
    ~AlertPanelItemWidget();

    bool getVisibl() const;
    void setVisibl(bool value);

    AlertDescription* getAlertDescription();

    bool isEmpty() const;

    void Initialize(AlertDescription* alert_description);
    void Deinitialize();
    static void InitAlertDescription(QWidget *parent);
    static void KillAlertDescription();

private slots:
    void labelClicked();
private:
    void setImage(const QPixmap &value, bool permament = true);
    void setForeColor(QColor color);
    void setEmpty(bool isEmpty);

    Ui::AlertPanelItemWidget *ui;
    QPixmap pixmap;
    QColor foreColor;
    bool visibl;  // DO NOT RENAME
    bool empty;
    ClickableLabel *labelImage;
    AlertDescription* alert_description;
    QPoint p;
    QSize s;

    static ToInstructions* _alertDescriptionWidget;
};
