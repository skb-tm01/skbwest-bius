#include <QDebug>
#include <QPixmap>

#include "core/helper.h"
#include "peripherials/sensors/SensorManager.h"
#include "widgets/Panels/AlertPanelItem.h"

#include "ui_AlertPanelItem.h"
#include "widgets/WindowManager.h"
ToInstructions* AlertPanelItemWidget::_alertDescriptionWidget = nullptr;

AlertPanelItemWidget::AlertPanelItemWidget(QWidget *parent) : QWidget(parent),
    ui(new Ui::AlertPanelItemWidget)
{
    ui->setupUi(this);
    visibl = true;
    foreColor = Qt::black;
    alert_description = nullptr;
    labelImage = new ClickableLabel(this);
    labelImage->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    ui->horizontalLayout->addWidget(labelImage);

    connect(labelImage, SIGNAL(clicked()), this, SLOT(labelClicked()));
    Deinitialize();
}

AlertPanelItemWidget::~AlertPanelItemWidget()
{
    delete ui;
}

void AlertPanelItemWidget::InitAlertDescription(QWidget *parent)
{
    _alertDescriptionWidget = new ToInstructions(parent);
}

void AlertPanelItemWidget::KillAlertDescription()
{
    _alertDescriptionWidget->Close();
}

void AlertPanelItemWidget::labelClicked()
{
    if(getAlertDescription()==nullptr)
        return;

    _alertDescriptionWidget->SetData(SensorManager::instance().GetSensor(alert_description->GetId()));
    _alertDescriptionWidget->SetFontSize(26);
    _alertDescriptionWidget->move(0,80);
    _alertDescriptionWidget->resize(1024,608);
    WindowManager::getInstance()->Open(this,_alertDescriptionWidget);
}

void AlertPanelItemWidget::setForeColor(QColor color)
{
    foreColor = color;
    setImage(Helper::SetPixmapColor(pixmap, color), false);
}

void AlertPanelItemWidget::setEmpty(bool isEmpty)
{
    empty = isEmpty;
}

void AlertPanelItemWidget::Deinitialize()
{
    alert_description = nullptr;
    setImage(Helper::LoadPixmap("empty.png"));
    setEmpty(true);
}

void AlertPanelItemWidget::setImage(const QPixmap &value, bool permament)
{
    if(permament)
        pixmap = value;
    labelImage->setVisible(visibl);
    labelImage->setPixmap(Helper::ScalePixmap(value, 64));
}

bool AlertPanelItemWidget::getVisibl() const
{
    return visibl;
}

void AlertPanelItemWidget::setVisibl(bool value)
{
    visibl = value;
    labelImage->setVisible(visibl);
}

AlertDescription *AlertPanelItemWidget::getAlertDescription()
{
    return alert_description;
}

bool AlertPanelItemWidget::isEmpty() const
{
    return empty;
}

void AlertPanelItemWidget::Initialize(AlertDescription *alert_description)
{
    setImage(Helper::LoadPixmap(alert_description->GetImage()));
    setForeColor(alert_description->GetColor());
    setEmpty(false);
    this->alert_description = alert_description;
}
