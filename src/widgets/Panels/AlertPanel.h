#pragma once

#include <QFrame>
#include <QTimer>

#include "Config.h"
#include "alerts/AlertDescription.h"
#include "widgets/Panels/AlertPanelItem.h"
#include "alerts/AlertsWorker.h"
#include <QMutex>

namespace Ui {
    class AlertPanelWidget;
}

class AlertPanelWidget : public QFrame
{
    Q_OBJECT

public:
    explicit AlertPanelWidget(QWidget *parent = 0);
    ~AlertPanelWidget();
    bool GetCanState();

public slots:
    void Initialize_CAN();
    void AddAlert(AlertDescription* alert_description);
    void RemoveAlert(QString alert_id);
    void ClearAlerts();

    void MarkAlertsAsDisplayedOnStartup(AlertsWorker* worker, bool leaveSpecialAlertsShown);

private slots:
    void OnRingingTimer();
    void OnCanConnectionLost();
    void OnCanConnectionRestored();

private:
    QMutex mutex;
    bool _isCanConnectionOK;
    Ui::AlertPanelWidget *ui;
    QTimer _ringing_timer;
    AlertPanelItemWidget* topPanelAlertArray[NUM_OF_TOP_PANEL_ALERTS];
};
