#pragma once

#include <QWidget>

#include "peripherials/can/CanMessage.h"

namespace Ui {
class StatusPanelItemWidget;
}

class StatusPanelItemWidget : public QWidget
{
    Q_OBJECT

public:
    StatusPanelItemWidget(QWidget *parent, QString text, const QString &fileName, QString dim);

    ~StatusPanelItemWidget();

    QString getUnits() const;
    void setUnits(const QString &value);

    QString getText() const;
    void setText(const QString &value);

    void setPermamentImage(const QPixmap &value);
    void setDefaultColors(const QColor& default_day, const QColor& default_night);
    void setEnabledColors(const QColor& enabled_day, const QColor& enabled_night);

    void updateImage();

    void setFontSize(int value);

    bool getVisibl() const;
    void setVisibl(bool value);

    bool getEnabled() const;
    void setEnabled(bool value);

    void SetNightMode(bool value);

private:
    Ui::StatusPanelItemWidget *ui;
    QString units;
    QString text;

    QPixmap pixmap_default;
    QPixmap pixmap_default_day;
    QPixmap pixmap_default_night;
    QPixmap pixmap_enabled_day;
    QPixmap pixmap_enabled_night;

    bool night;
    bool visibl;  // DO NOT RENAME
    bool enabled; // for blinking ((P)) and light
};
