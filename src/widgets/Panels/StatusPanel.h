#pragma once

#include <QFrame>
#include <QTimer>

#include "SettingsManager.h"
#include "peripherials/can/CanMessage.h"
#include "widgets/Panels/StatusPanelItem.h"

namespace Ui {
    class StatusPanelWidget;
}

class StatusPanelWidget : public QFrame
{
    Q_OBJECT

public:
    explicit StatusPanelWidget(QWidget *parent = 0);
    ~StatusPanelWidget();

public slots:
    void Initialize_CAN();
    void OnCanMessage(CanMessage msg);
    void OnCanConnectionLost();

    void ApplyNightDay(EnumNightDayMode mode);

    void highlightHighbeamIcon(bool isOn);
    void highlightParkingBreaksIcon(bool isOn);
    void highlightBattery(bool isOn);
    void showTiltCamera(bool status, bool noControl);

    void setMenuButtonAppearance(bool menuMode);
protected:
    void OnCanMessage_SettingsSet(uint8_t* data);
    void OnCanMessage_Analog(uint8_t* data);
    void OnCanMessage_Request3(CanMessage msg);
private slots:
    void flashBottomIcon();

    void on_btnMenu_clicked();

private:
    QTimer _flashTimer;
    bool _flash;

    StatusPanelItemWidget *cultureTypeInfoWidget;
    StatusPanelItemWidget* highBeamInfoWidget;
    StatusPanelItemWidget* parkingBrakesInfoWidget;
    StatusPanelItemWidget* tiltCameraWidget;
    StatusPanelItemWidget* currentFuelInfoWidget;
    StatusPanelItemWidget* totalFuelInfoWidget;
    StatusPanelItemWidget* roadFieldModeInfoWidget;
    StatusPanelItemWidget* clearanceInfoWidget;
    StatusPanelItemWidget* higherSievesInfoWidget;
    StatusPanelItemWidget* lowerSievesInfoWidget;
    StatusPanelItemWidget* voltageInfoWidget;

    Ui::StatusPanelWidget *ui;
};
