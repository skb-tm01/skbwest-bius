#include "core/AudioHelper.h"
#include "core/PathHelper.h"

#include "peripherials/PeripherialsManager.h"

#include "widgets/Controls/Clock.h"
#include "widgets/Panels/AlertPanel.h"

#include "ui_AlertPanel.h"
#include <QDebug>
#include <QDateTime>

AlertPanelWidget::AlertPanelWidget(QWidget *parent) :
    QFrame(parent),
    ui(new Ui::AlertPanelWidget)
{
    _isCanConnectionOK = true; // "= true" is important
    ui->setupUi(this);
    for(int i=0;i<NUM_OF_TOP_PANEL_ALERTS;i++)
    {
        topPanelAlertArray[i] = new AlertPanelItemWidget(this);
        ui->horizontalLayout->addWidget(topPanelAlertArray[i]);
    }
    ui->horizontalLayout->addStretch();
    ui->horizontalLayout->addWidget(new Clock(this));

    //Timer
    QObject::connect(&_ringing_timer,SIGNAL(timeout()),this,SLOT(OnRingingTimer()));
    _ringing_timer.setInterval(90000);
    _ringing_timer.start();
}

AlertPanelWidget::~AlertPanelWidget()
{
    delete ui;
}

bool AlertPanelWidget::GetCanState()
{
    return _isCanConnectionOK;
}

void AlertPanelWidget::Initialize_CAN()
{
    auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
    if(canworker==nullptr)
        return;

    QObject::connect(canworker,SIGNAL(connectionLost()),this,SLOT(OnCanConnectionLost()));
    QObject::connect(canworker,SIGNAL(connectionRestored()),this,SLOT(OnCanConnectionRestored()));
}

void AlertPanelWidget::AddAlert(AlertDescription* alert_description)
{
//    if(!_isCanConnectionOK && alert_description->GetId()!="can_connection")
//        return;

//    if(_isCanConnectionOK && alert_description->GetId()=="can_connection")
//        return;

    //top alert
    //TODO: rewrite this crap
    for(int i=0;i<NUM_OF_TOP_PANEL_ALERTS;i++)
    {
        auto* alertdesc = topPanelAlertArray[i]->getAlertDescription();
        if(alertdesc!=nullptr && alertdesc->GetId() == alert_description->GetId())
           return;
    }

    if(alert_description->GetColor()=="red")
    {
        for(int i=0;i<NUM_OF_TOP_PANEL_ALERTS;i++)
        {
           if (topPanelAlertArray[i]->isEmpty() == true)
           {
               topPanelAlertArray[i]->Initialize(alert_description);
               return;
           }
           else if(topPanelAlertArray[i]->getAlertDescription() != nullptr && topPanelAlertArray[i]->getAlertDescription()->GetColor() == "yellow")
           {
               topPanelAlertArray[i]->Deinitialize();
               topPanelAlertArray[i]->Initialize(alert_description);
               return;
           }
        }
    }
    else
    {
        for(int i=NUM_OF_TOP_PANEL_ALERTS-1;i>=0;i--)
        {
           if (topPanelAlertArray[i]->isEmpty() == true)
           {
               topPanelAlertArray[i]->Initialize(alert_description);
               break;
           }
        }
    }
}

void AlertPanelWidget::RemoveAlert(QString alert_id)
{
    for(int i=0;i<NUM_OF_TOP_PANEL_ALERTS;i++)
    {
       auto* alertdesc = topPanelAlertArray[i]->getAlertDescription();
       if(alertdesc!=nullptr && alertdesc->GetId() == alert_id)
       {
           topPanelAlertArray[i]->Deinitialize();
           break;
       }
    }
}

void AlertPanelWidget::ClearAlerts()
{
    for(int i=0;i<NUM_OF_TOP_PANEL_ALERTS;i++)
    {
       auto* alertdesc = topPanelAlertArray[i]->getAlertDescription();
       if(alertdesc!=nullptr)
       {
           qDebug() << "alert panel: deinitialized (!leaveSpecialAlertsShown) " << topPanelAlertArray[i]->getAlertDescription()->GetText("ru");
           topPanelAlertArray[i]->Deinitialize();
           continue;
       }
    }
}

void AlertPanelWidget::MarkAlertsAsDisplayedOnStartup(AlertsWorker *worker, bool leaveSpecialAlertsShown)
{
    for(int i=0;i<NUM_OF_TOP_PANEL_ALERTS;i++)
    {
       auto* alertdesc = topPanelAlertArray[i]->getAlertDescription();
       if(alertdesc!=nullptr)
       {
           if(!leaveSpecialAlertsShown)
           {
               qDebug() << "alert panel: deinitialized (!leaveSpecialAlertsShown) " << topPanelAlertArray[i]->getAlertDescription()->GetText("ru");
               topPanelAlertArray[i]->Deinitialize();
               continue;
           }

           else if(!alertdesc->GetShowOnStartup())
           {
               if(worker != nullptr)
                   worker->MarkAsDisplayed(alertdesc);
               qDebug() << "alert panel: deinitialized (!alertdesc->GetShowOnStartup()) " << topPanelAlertArray[i]->getAlertDescription()->GetText("ru");
               topPanelAlertArray[i]->Deinitialize();
               continue;
           }
       }
    }
}

void AlertPanelWidget::OnRingingTimer()
{
    bool there_is_red = false;
    bool there_is_yellow = false;
    bool should_ring = false;

    for(int i=0;i<NUM_OF_TOP_PANEL_ALERTS;i++)
    {
       auto* alertdesc = topPanelAlertArray[i]->getAlertDescription();
       if(alertdesc!=nullptr)
       {
           if(alertdesc->ShouldRing()) {
               should_ring = true;
           }

           QString color = alertdesc->GetColor();
           if(color=="red")
               there_is_red = true;
           else if(color=="yellow")
               there_is_yellow = true;
       }
    }

    if(should_ring) {
        if(there_is_red)
        {
            AudioHelper::PlayWaveFileWhenQueryIsEmpty(PathHelper::pathAppend(PathHelper::GetDataDirectory(),"sounds/ringing.wav"));
        }
        else if(there_is_yellow)
        {
            AudioHelper::PlayWaveFileWhenQueryIsEmpty(PathHelper::pathAppend(PathHelper::GetDataDirectory(),"sounds/notify.wav"));
        }
    }
}

void AlertPanelWidget::OnCanConnectionLost()
{
    qDebug() << "ALERT PANEL CAN LOST";
    ClearAlerts();
    mutex.lock();
    _isCanConnectionOK = false;
    mutex.unlock();
}

void AlertPanelWidget::OnCanConnectionRestored()
{
    mutex.lock();
    _isCanConnectionOK = true;
    mutex.unlock();
    RemoveAlert("can_connection");
    qDebug() << "ALERT PANEL CAN RESTORED";
}

