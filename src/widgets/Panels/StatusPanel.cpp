﻿#include "core/AudioHelper.h"
#include "core/helper.h"
#include "core/PathHelper.h"

#include "peripherials/PeripherialsManager.h"
#include "peripherials/can/packets/Analog.h"
#include "peripherials/can/packets/Settings.h"
#include "peripherials/can/packets/Request3.h"
#include "core/ByteConversion.h"
#include "widgets/Panels/StatusPanel.h"
#include "widgets/mainwindow.h"
#include "ui_StatusPanel.h"

StatusPanelWidget::StatusPanelWidget(QWidget *parent) :
    QFrame(parent),
    ui(new Ui::StatusPanelWidget)
{
    ui->setupUi(this);

    _flash = false;

    // culture type
    cultureTypeInfoWidget = new StatusPanelItemWidget(this, "", "corn.svg", "");
    ui->horizontalLayout->insertWidget(0, cultureTypeInfoWidget);
    cultureTypeInfoWidget->setFontSize(14);

    //high beam
    highBeamInfoWidget = new StatusPanelItemWidget(this, "", "image77.svg", "");

    highBeamInfoWidget->setDefaultColors(
                Helper::dayDisabledColor(),
                Helper::nightDisabledColor());
    highBeamInfoWidget->setEnabledColors(
                Qt::blue,
                Qt::blue);

    ui->horizontalLayout->insertWidget(1,highBeamInfoWidget);

    //parking brakes
    parkingBrakesInfoWidget = new StatusPanelItemWidget(this, "", "image78.svg", "");

    parkingBrakesInfoWidget->setDefaultColors(
                Helper::dayDisabledColor(),
                Helper::nightDisabledColor());
    parkingBrakesInfoWidget->setEnabledColors(
                Qt::red,
                Qt::red);

    ui->horizontalLayout->insertWidget(2,parkingBrakesInfoWidget);

    // tilt camera
    tiltCameraWidget = new StatusPanelItemWidget(this, "", "tilt_camera_cropped.png", "");
    ui->horizontalLayout->insertWidget(3,tiltCameraWidget);
    tiltCameraWidget->setVisibl(true);

    //current fuel consumption
    currentFuelInfoWidget = new StatusPanelItemWidget(this, "0.0", "/fuel1.png", QString::fromUtf8("л/ч"));
    ui->horizontalLayout->insertWidget(4,currentFuelInfoWidget);
    currentFuelInfoWidget->setVisibl(false);

    // total fuel consumption
    totalFuelInfoWidget = new StatusPanelItemWidget(this, "0.0", "/fuelSum.png", QString::fromUtf8("л/ч"));
    ui->horizontalLayout->insertWidget(5,totalFuelInfoWidget);
    totalFuelInfoWidget->setVisibl(false);

    // mode (road/field)
    roadFieldModeInfoWidget = new StatusPanelItemWidget(this, "", ":/media/empty.png", "");
     ui->horizontalLayout->insertWidget(6,roadFieldModeInfoWidget);

    // clearance
    clearanceInfoWidget = new StatusPanelItemWidget(this, "0.0", "image115.svg", QString::fromUtf8("мм"));
    ui->horizontalLayout->insertWidget(7,clearanceInfoWidget);

    // higher sieves clearance
    higherSievesInfoWidget = new StatusPanelItemWidget(this, "0.0", "image107.svg", QString::fromUtf8("мм"));
    ui->horizontalLayout->insertWidget(8, higherSievesInfoWidget);

    // lower sieves clearance
    lowerSievesInfoWidget = new StatusPanelItemWidget(this, "0.0", "image108.svg", QString::fromUtf8("мм"));
    ui->horizontalLayout->insertWidget(9,lowerSievesInfoWidget);

    // voltage
    voltageInfoWidget = new StatusPanelItemWidget(this, "--.-", "image18.svg", QString::fromUtf8("В"));

    voltageInfoWidget->setDefaultColors(
                Helper::dayForeColor(),
                Helper::nightForeColor());

    voltageInfoWidget->setEnabledColors(
                Qt::red,
                Qt::red);

    ui->horizontalLayout->insertWidget(10,voltageInfoWidget);

    ApplyNightDay(SettingsManager::settings()->currentNightDayMode());
    QObject::connect(SettingsManager::settings(),SIGNAL(NightDayChanged(EnumNightDayMode)), this, SLOT(ApplyNightDay(EnumNightDayMode)));

    highlightHighbeamIcon(false);
    highlightParkingBreaksIcon(false);

    _flashTimer.setInterval(500);
    QObject::connect(&_flashTimer, SIGNAL(timeout()), this, SLOT(flashBottomIcon()));
    _flashTimer.start();
    _flash = false;

    for(auto* item : this->findChildren<StatusPanelItemWidget*>())
    {
        item->updateImage();
    }

    ui->btnMenu->setMinimumSize(QSize(100, 73));
    //auto size: назад: QSize(78, 73) меню: QSize(82, 73)
}

StatusPanelWidget::~StatusPanelWidget()
{
    delete ui;
}

void StatusPanelWidget::Initialize_CAN()
{
    auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
    if(canworker==nullptr)
        return;

    QObject::connect(canworker,SIGNAL(received(CanMessage)),this,SLOT(OnCanMessage(CanMessage)));
    QObject::connect(canworker,SIGNAL(connectionLost()),this,SLOT(OnCanConnectionLost()));
}

void StatusPanelWidget::OnCanMessage(CanMessage msg)
{
    if(msg.can_id==CanPacketSettingsGetResponse::packet_id)
        OnCanMessage_SettingsSet(msg.data);
    else if (msg.can_id == CanPacketAnalogResponse::packet_id)
        OnCanMessage_Analog(msg.data);
    else if (msg.can_id == CanPackets::Request3::response_id)
        OnCanMessage_Request3(msg);
}

void StatusPanelWidget::OnCanMessage_Request3(CanMessage msg)
{
    CanPackets::Request3Data packet = CanPackets::Request3::ParseResponse(msg.data);
    bool tilt_camera_no_control = ByteConversion::GetBitState(packet.raw_data,26);
    bool tilt_camera_status = ByteConversion::GetBitState(packet.raw_data,27);
    showTiltCamera(tilt_camera_status, tilt_camera_no_control);
}

void StatusPanelWidget::OnCanMessage_SettingsSet(uint8_t *data)
{
    auto packet = CanPacketSettingsGetResponse::Parse(data);
    if(packet.type != CanPacketSettingsType::settings_vid_zerno)
        return;

    switch(static_cast<CanPacketSettignsGrainType>(packet.value))
    {
       case CanPacketSettignsGrainType::grain_alfalfa:
           this->cultureTypeInfoWidget->setPermamentImage(Helper::LoadPixmap("alfalfa.svg"));
           break;
       case CanPacketSettignsGrainType::grain_barley:
          this->cultureTypeInfoWidget->setPermamentImage(Helper::LoadPixmap("barley.svg"));
           break;
       case CanPacketSettignsGrainType::grain_buckwheat:
          this->cultureTypeInfoWidget->setPermamentImage(Helper::LoadPixmap("buckwheat.svg"));
           break;
       case CanPacketSettignsGrainType::grain_chickpea:
          this->cultureTypeInfoWidget->setPermamentImage(Helper::LoadPixmap("chickpea.svg"));
           break;
        case CanPacketSettignsGrainType::grain_clover:
            this->cultureTypeInfoWidget->setPermamentImage(Helper::LoadPixmap("clover.svg"));
            break;
        case CanPacketSettignsGrainType::grain_colza:
            this->cultureTypeInfoWidget->setPermamentImage(Helper::LoadPixmap("colza.svg"));
            break;
       case CanPacketSettignsGrainType::grain_corn:
          this->cultureTypeInfoWidget->setPermamentImage(Helper::LoadPixmap("corn.svg"));
           break;
       case CanPacketSettignsGrainType::grain_oats:
           this->cultureTypeInfoWidget->setPermamentImage(Helper::LoadPixmap("oats.svg"));
           break;
       case CanPacketSettignsGrainType::grain_rye:
          this->cultureTypeInfoWidget->setPermamentImage(Helper::LoadPixmap("rye.svg"));
           break;
       case CanPacketSettignsGrainType::grain_soy:
          this->cultureTypeInfoWidget->setPermamentImage(Helper::LoadPixmap("soy.svg"));
           break;
       case CanPacketSettignsGrainType::grain_sunflower:
          this->cultureTypeInfoWidget->setPermamentImage(Helper::LoadPixmap("sunflower.svg"));
           break;
       case CanPacketSettignsGrainType::grain_wheat:
          this->cultureTypeInfoWidget->setPermamentImage(Helper::LoadPixmap("wheat.svg"));
           break;
       default:
           break;
    }
    this->cultureTypeInfoWidget->setDefaultColors(
                Helper::dayForeColor(),
                Helper::nightForeColor()
    );
}

void StatusPanelWidget::OnCanMessage_Analog(uint8_t *data)
{
    auto packet = CanPacketAnalogResponse::Parse(data);
    switch(packet.type)
    {
        case CanPacketAnalogType::can_analog_type_concaveclearance:
            clearanceInfoWidget->setText(QString::number(packet.state));
            break;
        case CanPacketAnalogType::can_analog_type_sieves_higher:
            higherSievesInfoWidget->setText(QString::number(packet.state/10.,'f',1));
            break;
        case CanPacketAnalogType::can_analog_type_sieves_lower:
            lowerSievesInfoWidget->setText(QString::number(packet.state/10.,'f',1));
            break;
        case CanPacketAnalogType::can_analog_type_power:
            voltageInfoWidget->setText(QString::number(packet.state/10.,'f',1));
            break;
        default:
            break;
    }
}

void StatusPanelWidget::OnCanConnectionLost()
{
    clearanceInfoWidget->setText("---");
    higherSievesInfoWidget->setText("---");
    lowerSievesInfoWidget->setText("---");
    voltageInfoWidget->setText("---");
}

void StatusPanelWidget::ApplyNightDay(EnumNightDayMode mode)
{
    QColor foreColor = SettingsManager::settings()->currentForeColor();

    for(auto* item: this->findChildren<StatusPanelItemWidget*>())
    {
        item->SetNightMode(mode==EnumNightDayMode::Night);
    }
}

void StatusPanelWidget::highlightHighbeamIcon(bool isOn)
{
    highBeamInfoWidget->setEnabled(isOn);
}

void StatusPanelWidget::highlightParkingBreaksIcon(bool isOn)
{
    if(_flash == isOn)
        return;
    _flash = isOn;

    if(!_flash)
    {
        parkingBrakesInfoWidget->setEnabled(false);
    }


    if (isOn == true)
    {
        AudioHelper::PlayWaveFileWhenQueryIsEmpty(PathHelper::pathAppend(PathHelper::GetDataDirectory(),"sounds/notify_ru.wav"));
    }
}

void StatusPanelWidget::highlightBattery(bool isOn)
{
    voltageInfoWidget->setEnabled(isOn);
}

void StatusPanelWidget::showTiltCamera(bool status, bool noControl)
{
    if( noControl == 1) {
        //пиктограммы нет. знакоместо пусто
        tiltCameraWidget->setVisibl(false);
    } else {
        if( status == 1) {
            //пиктограмма не активна
            tiltCameraWidget->setVisibl(true);
            tiltCameraWidget->setEnabled(false);
        } else {
            //пиктограмма активна
            tiltCameraWidget->setVisibl(true);
            tiltCameraWidget->setEnabled(true);
        }
    }
}

void StatusPanelWidget::setMenuButtonAppearance(bool menuMode)
{
    // menu mode
    if(menuMode)
    {
    ui->btnMenu->setStyleSheet("QPushButton{ font-size: 24px; color: black; background-color: blue}");
    ui->btnMenu->setText(QString::fromUtf8("МЕНЮ"));
    }
    // esc mode
    else {
    ui->btnMenu->setStyleSheet("QPushButton{ font-size: 24px; color: black; background-color: red}");
    ui->btnMenu->setText(QString::fromUtf8("НАЗАД"));
    }
}

void StatusPanelWidget::flashBottomIcon()
{
    if(!_flash)
        return;

    parkingBrakesInfoWidget->setEnabled(!parkingBrakesInfoWidget->getEnabled());
}

void StatusPanelWidget::on_btnMenu_clicked()
{
    //It should be controlled by menu itself?
    MainWindow::Instance->MenuButtonClicked();
}
