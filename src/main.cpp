#include <QtGlobal>

#include <QApplication>
#include <QKeyEvent>
#include <QFile>
#include <QDir>
#include <QMetaType>
#include <QSettings>
#include <QString>

#ifdef Q_WS_QWS
#include <QWSServer>
#endif

#include "alerts/AlertsManager.h"
#include "alerts/AlertDescription.h"

#include "core/PathHelper.h"

#include "peripherials/PeripherialsManager.h"
#include "peripherials/can/CanMessage.h"
#include "peripherials/can/CanBackend_SocketCan.h"
#include "peripherials/sensors/SensorManager.h"

#include "widgets/mainwindow.h"
#include "widgets/EventFilters/IdleEventFilter.h"
#include "widgets/EventFilters/MenuButtonEventFilter.h"
#include "widgets/EventFilters/ClickSoundEventFilter.h"
#include "widgets/EventFilters/ScreenShotEventFilter.h"
#include "widgets/sensors_statistics_short.h"
#include "widgets/sensors_statistics_full.h"

#include "SettingsManager.h"

#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
#include "widgets/VirtualKeyboard/BEVirtualKeyboardContext.h"
#endif

#include <stdio.h>
#include <execinfo.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <execinfo.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <execinfo.h>  // for backtrace
#include <dlfcn.h>     // for dladdr
#include <cxxabi.h> // for __cxa_demangle
#include <string>
#include <sstream>

void handler(int sig) {
    void *array[10];
      size_t size;

      // get void*'s for all entries on the stack
      size = backtrace(array, 10);

      // print out all the frames to stderr
      fprintf(stderr, "Error: signal %d:\n", sig);
      backtrace_symbols_fd(array, size, STDERR_FILENO);
      exit(1);
}

void init_metatypes()
{
    qRegisterMetaType<AlertDescription>();
    qRegisterMetaType<AlertDescription*>();
    qRegisterMetaType<CanMessage>();
    qRegisterMetaType<CanMessage*>();
}

void init_platform()
{
    #ifdef Q_WS_QWS
    QWSServer::setCursorVisible( false );
    #endif
}

void init_settings()
{
    SettingsManager::settings();
}

void init_alerts(MainWindow* mw)
{
    AlertsManager::instance().Alerts_Worker_Initialize(mw);

    auto* alertworker = AlertsManager::instance().Alerts_Worker_Get();
    Q_ASSERT(alertworker != nullptr);

    auto* canworker = PeripherialsManager::instance().CAN_Worker_Get();
    Q_ASSERT(canworker != nullptr);

    QObject::connect(canworker, SIGNAL(connectionLost()), alertworker, SLOT(OnCanConnection_Lost()));
    QObject::connect(canworker, SIGNAL(connectionRestored()), alertworker, SLOT(OnCanConnection_Restored()));
}

void init_peripherials_kb_virtual()
{
    //There is no QInputContext in Qt5
    //TODO: rewrite with Qt Virtual keyboard for Qt5
    #if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
        BEVirtualKeyboardContext *inputContext = new BEVirtualKeyboardContext();
        qApp->setInputContext(inputContext);
    #endif
}

void init_peripherials_kb_gpio(MainWindow* mw)
{
    PeripherialsManager::instance().GPIO_Keyboard_Initialize();
    QObject::connect(PeripherialsManager::instance().GPIO_Keyboard_Get(), SIGNAL(sig_emitKeyEvent(QKeyEvent*)), mw, SLOT(emitKeyEvent(QKeyEvent*)));
}

void init_peripherials_audio()
{
    PeripherialsManager::instance().AudioWorker_Initialize();
}

void init_peripherials_can()
{
    //TODO: implement canhacker backend
    auto* backend = new CanBackend_SocketCan("can0", 250000);
    backend->Open();
    PeripherialsManager::instance().CAN_Worker_Receiver_Initialize(backend);
}

void init_peripherials_brightness()
{
    PeripherialsManager::instance().illumMonitor_Init();
    QObject::connect(PeripherialsManager::instance().illumMonitor_Get(), SIGNAL (BrightnessChanged(double)), SettingsManager::settings(), SLOT (ApplyIllumination(double)));
    PeripherialsManager::instance().illumMonitor_Get()->start();
}

void init_sensors(MainWindow* mw)
{
    SensorManager::instance().InitSensors(mw);
    SensorManager::instance().RequestSensorsData();
}

void init_filter_idle(MainWindow* mw)
{
    //TODO: move idle time to config.h
    auto* filter = new IdleEventFilter(qApp);
    filter->setIdleTime(60000);
    QObject::connect(filter, SIGNAL(IdleOccurred()), mw, SLOT(onIdle()));
    qApp->installEventFilter(filter);
}

void init_filter_menubutton(MainWindow* mw)
{
    auto* MenuButtonFilter = new MenuButtonEventFilter(qApp);
    QObject::connect(MenuButtonFilter, SIGNAL(MenuButtonPressed()), mw, SLOT(MenuButtonClicked()));
    qApp->installEventFilter(MenuButtonFilter);
}

void init_filter_clicksound()
{
    qApp->installEventFilter(new ClickSoundEventFilter(qApp));
}

void init_filter_screenshots()
{
    if(!QFile::exists(PathHelper::pathAppend(PathHelper::GetAppDirectory(),"create_screenshots")))
        return;

    auto* ScreenShotFilter = new ScreenShotEventFilter(qApp);
    qApp->installEventFilter(ScreenShotFilter);
}

int perform_application_activity(MainWindow* mw)
{
    mw->Initialize_CAN();

    auto* sensorsWindow = new sensors_statistics_short(nullptr,EnumSensorsStatisticsMode::OnStart);
    auto* sensorsWindow2 = new sensors_statistics_full(nullptr,EnumSensorsStatisticsMode::OnStart);

#ifdef BUILD_DEVICE
    QObject::connect(sensorsWindow, SIGNAL(OnClosing()), sensorsWindow2, SLOT(showFullScreen()));
    QObject::connect(sensorsWindow2, SIGNAL(OnClosing()), mw, SLOT(showFullScreen()));
#else
    QObject::connect(sensorsWindow, SIGNAL(OnClosing()), sensorsWindow2, SLOT(show()));
    QObject::connect(sensorsWindow2, SIGNAL(OnClosing()), mw, SLOT(show()));
#endif
    QObject::connect(sensorsWindow2, SIGNAL(OnClosing()), AlertsManager::instance().Alerts_Worker_Get(), SLOT(MainWindowInitialized()));

    sensorsWindow->showFullScreen();

    return qApp->exec();
}

void deinit_application()
{
    SettingsManager::settings()->Deinitialize();
}

int main(int argc, char *argv[])
{
       signal(SIGSEGV, handler);   // install our handler
    QApplication a(argc, argv);

    MainWindow mainWindow;

    init_metatypes();
    init_platform();
    init_settings();

    init_peripherials_brightness();
    init_peripherials_can();
    init_peripherials_audio();
    init_peripherials_kb_virtual();
    init_peripherials_kb_gpio(&mainWindow);

    init_alerts(&mainWindow);
    init_sensors(&mainWindow);

    init_filter_idle(&mainWindow);
    init_filter_menubutton(&mainWindow);
    init_filter_clicksound();
    init_filter_screenshots();

    int ret = perform_application_activity(&mainWindow);
    deinit_application();
    return ret;
}
