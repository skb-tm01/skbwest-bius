SET(CMAKE_SYSTEM_NAME Linux)

SET(CMAKE_C_COMPILER /opt/OSELAS.Toolchain-2011.11.1/arm-cortexa9-linux-gnueabi/gcc-4.6.2-glibc-2.14.1-binutils-2.21.1a-kernel-2.6.39-sanitized/bin/arm-cortexa9-linux-gnueabi-gcc)
SET(CMAKE_CXX_COMPILER /opt/OSELAS.Toolchain-2011.11.1/arm-cortexa9-linux-gnueabi/gcc-4.6.2-glibc-2.14.1-binutils-2.21.1a-kernel-2.6.39-sanitized/bin/arm-cortexa9-linux-gnueabi-g++)
SET(CMAKE_FIND_ROOT_PATH /opt/OSELAS.Toolchain-2011.11.1/arm-cortexa9-linux-gnueabi/gcc-4.6.2-glibc-2.14.1-binutils-2.21.1a-kernel-2.6.39-sanitized/sysroot-arm-cortexa9-linux-gnueabi/)
LIST(APPEND CMAKE_FIND_ROOT_PATH /opt/PHYTEC_BSPs/BSP-phyBOARD-SUBRA-i.MX6-PD14.0.0/platform-phyBOARD-SUBRA-i.MX6/sysroot-target/usr)


set(QT_QMAKE_EXECUTABLE /opt/PHYTEC_BSPs/BSP-phyBOARD-SUBRA-i.MX6-PD14.0.0/platform-phyBOARD-SUBRA-i.MX6/sysroot-cross/bin/qmake-cross)

SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

